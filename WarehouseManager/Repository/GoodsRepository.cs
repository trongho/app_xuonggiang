﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WarehouseManager.Helpers;
using WarehouseManager.Models;

namespace WarehouseManager.Repository
{
    class GoodsRepository
    {
        public GoodsRepository()
        {
        }

        public async Task<List<Goods>> getAllGoodsAsync()
        {
            string URI = Constant.BaseURL+"goods";
            List<Goods> list = new List<Goods>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            list = JsonConvert.DeserializeObject<Goods[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }          
            return list;
        }
    }
}
