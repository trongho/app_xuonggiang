﻿using DevExpress.XtraEditors.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarehouseManager.Models;
using WarehouseManager.Repository;

namespace WarehouseManager
{
    public partial class RFIDReaderManagerControl : UserControl
    {
        RFIDReaderRepository readerRepository;
        public RFIDReaderManagerControl()
        {
            InitializeComponent();
            readerRepository = new RFIDReaderRepository();
            sbLoadDataForGridWRDataGeneralAsync();
        }

        private async Task sbLoadDataForGridWRDataGeneralAsync()
        {
            List<RFIDReader> rFIDReaders= await readerRepository.GetAll();         
            gridControl1.DataSource = rFIDReaders;
        }

    }
}
