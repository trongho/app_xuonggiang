﻿using DevExpress.XtraBars.Docking2010;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarehouseManager.Repository;

namespace WarehouseManager
{
    public partial class GoodManagerControl : DevExpress.XtraEditors.XtraUserControl
    {
        GoodsRepository goodsRepository;
        public GoodManagerControl()
        {
            InitializeComponent();
            this.Load += GoodsManagerControl_Load;

        }

        private void GoodsManagerControl_Load(Object sender, EventArgs e)
        {
            windowsUIButtonPanel1.ButtonClick += button_Click;
            goodsRepository = new GoodsRepository();
            sbLoadDataForGridGoodsAsync();
        }

        private async Task sbLoadDataForGridGoodsAsync()
        {
            gridControlGoods.DataSource = await goodsRepository.getAllGoodsAsync();
        }

        //barbutton click
        public void button_Click(object sender, EventArgs e)
        {

            ButtonEventArgs eArg = (DevExpress.XtraBars.Docking2010.ButtonEventArgs)e;
            String tag = eArg.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "new":
                    break;
                case "save":
                    break;
                case "delete":                  
                    break;
                case "search":               
                    break;
                case "refesh":                 
                    break;
                case "import":

                    break;
                case "export":
                    break;
                case "print":
                    break;
                case "close":
                    break;
            }

        }
    }
}
