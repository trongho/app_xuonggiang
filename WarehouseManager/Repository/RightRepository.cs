﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WarehouseManager.Helpers;
using WarehouseManager.Models;

namespace WarehouseManager.Repository
{
    class RightRepository
    {
        public async Task<List<Right>> getAllRightAsync()
        {
            string URI = Constant.BaseURL+"right";
            List<Right> rights = new List<Right>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            rights = JsonConvert.DeserializeObject<Right[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            foreach (Right right in rights)
            {
                right.GrantRight = 0;
            }
            return rights;
        }
    }
}
