﻿using DevExpress.XtraEditors;
using Symbol.RFID3;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WarehouseManager
{
    
    public partial class ReaderConfigForm : DevExpress.XtraEditors.XtraForm
    {
        internal MainForm m_MainForm = null;
        private bool m_IsLoaded;
        
        public ReaderConfigForm(MainForm mainForm)
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
            m_MainForm = mainForm;
            m_IsLoaded = false;
        }

        private void comboBoxEditHopTableIndex_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (m_MainForm.m_ReaderAPI.IsConnected)
                {
                    if (m_MainForm.m_ReaderAPI.ReaderCapabilities.IsHoppingEnabled)
                    {
                        FrequencyHopInfo hopInfo = m_MainForm.m_ReaderAPI.ReaderCapabilities.FrequencyHopInfo;
                        int index = int.Parse(comboBoxEditHopTableIndex.SelectedItem.ToString());
                        int[] freqs = hopInfo[index - 1].FrequencyHopValues;

                        string hopTableFreqListMultiline = "";
                        hopFrequencies_TB.Text = "";
                        foreach (int freq in freqs)
                        {
                            if (hopTableFreqListMultiline != "")
                            {
                                hopTableFreqListMultiline = hopTableFreqListMultiline + ", ";
                            }
                            hopTableFreqListMultiline = hopTableFreqListMultiline + freq.ToString();
                        }
                        hopFrequencies_TB.Text = hopTableFreqListMultiline;
                    }
                }
            }
            catch (Exception ex)
            {
                m_MainForm.StatusBottom.Caption = ex.Message;
            }
        }

        private void ReaderConfigForm_Load(object sender, EventArgs e)
        {
            try
            {
                if (m_MainForm.m_ReaderAPI.IsConnected && !m_IsLoaded)
                {
                    ushort[] antID = m_MainForm.m_ReaderAPI.Config.Antennas.AvailableAntennas;
                    int[] rxValues = m_MainForm.m_ReaderAPI.ReaderCapabilities.ReceiveSensitivityValues;
                    int[] txValues = m_MainForm.m_ReaderAPI.ReaderCapabilities.TransmitPowerLevelValues;

                    if (antID.Length > 0)
                    {
                        foreach (ushort id in antID)
                            comboBoxEditAntennaID.Properties.Items.Add(id);
                        comboBoxEditAntennaID.SelectedIndex = 0;

                        Antennas.Config antConfig =
                            m_MainForm.m_ReaderAPI.Config.Antennas[antID[comboBoxEditAntennaID.SelectedIndex]].GetConfig();

                        foreach (int rx in rxValues)
                            comboBoxEditReceiveSensitivity.Properties.Items.Add(rx);
                        comboBoxEditReceiveSensitivity.SelectedIndex = antConfig.ReceiveSensitivityIndex;

                        foreach (int tx in txValues)
                            comboBoxEditTransmitPower.Properties.Items.Add(tx);
                        comboBoxEditTransmitPower.SelectedIndex = antConfig.TransmitPowerIndex;

                        if (m_MainForm.m_ReaderAPI.ReaderCapabilities.IsHoppingEnabled)
                        {
                            //this.Controls.Add(this.hopFrequencies_GB);
                            FrequencyHopInfo hopInfo = m_MainForm.m_ReaderAPI.ReaderCapabilities.FrequencyHopInfo;
                            for (int i = 0; i < hopInfo.Length; i++)
                            {
                                comboBoxEditHopTableIndex.Properties.Items.Add(hopInfo[i].HopTableID);
                                if (hopInfo[i].HopTableID == antConfig.TransmitFrequencyIndex)
                                {
                                    comboBoxEditHopTableIndex.SelectedIndex = i;
                                }
                            }
                        }
                        //else
                        //{
                        //    this.Controls.Add(this.transmitFreq_GB);
                        //    int[] freq = m_MainForm.m_ReaderAPI.ReaderCapabilities.FixedFreqValues;
                        //    for (int i = 0; i < freq.Length; i++)
                        //        transmitFreq_CB.Items.Add(freq[i].ToString());
                        //    if (freq.Length > 0)
                        //        transmitFreq_CB.SelectedIndex = 0;
                        //}
                    }
                }

                m_IsLoaded = true;
            }
            catch (Exception ex)
            {
                m_MainForm.StatusBottom.Caption = ex.Message;
            }
        }

        private void comboBoxEditAntennaID_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (m_MainForm.m_IsConnected && m_IsLoaded)
                {
                    ushort[] antID = m_MainForm.m_ReaderAPI.Config.Antennas.AvailableAntennas;
                    Antennas.Config antConfig =
                    m_MainForm.m_ReaderAPI.Config.Antennas[antID[comboBoxEditAntennaID.SelectedIndex]].GetConfig();
                    comboBoxEditTransmitPower.SelectedIndex = antConfig.TransmitPowerIndex;
                }
            }
            catch (Exception ex)
            {
                m_MainForm.StatusBottom.Caption= ex.Message;
            }
        }

        private void simpleButtonApply_Click(object sender, EventArgs e)
        {
            try
            {
                if (m_MainForm.m_IsConnected)
                {
                    ushort[] antID = m_MainForm.m_ReaderAPI.Config.Antennas.AvailableAntennas;
                    Antennas.Config antConfig =
                        m_MainForm.m_ReaderAPI.Config.Antennas[antID[comboBoxEditAntennaID.SelectedIndex]].GetConfig();

                    antConfig.ReceiveSensitivityIndex = (ushort)comboBoxEditReceiveSensitivity.SelectedIndex;

                    antConfig.TransmitPowerIndex = (ushort)comboBoxEditTransmitPower.SelectedIndex;

                    if (!m_MainForm.m_ReaderAPI.ReaderCapabilities.IsHoppingEnabled)
                    {
                        //antConfig.TransmitFrequencyIndex = (ushort)(transmitFreq_CB.SelectedIndex + 1);
                    }
                    else
                    {
                        antConfig.TransmitFrequencyIndex = (ushort)(comboBoxEditHopTableIndex.SelectedIndex + 1);
                    }
                    m_MainForm.m_ReaderAPI.Config.Antennas[antID[comboBoxEditAntennaID.SelectedIndex]].SetConfig(antConfig);

                    m_MainForm.StatusBottom.Caption = "Set Antenna Configuration Successfully";
                }
                else
                {
                    m_MainForm.StatusBottom.Caption  = "Please connect to a reader";
                }
            }
            catch (InvalidUsageException iue)
            {
                m_MainForm.StatusBottom.Caption  = iue.VendorMessage;
            }
            catch (OperationFailureException ofe)
            {
                m_MainForm.StatusBottom.Caption  = ofe.StatusDescription;
            }
            catch (Exception ex)
            {
                m_MainForm.StatusBottom.Caption  = ex.Message;
            }
            this.Close();
        }
    }
}