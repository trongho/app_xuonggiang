﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WarehouseManager.Helpers;
using WarehouseManager.Models;
namespace WarehouseManager.Repository
{
    class ScreenRepository
    {
        public async Task<List<Screen>> getAllScreenAsync()
        {
            string URI = Constant.BaseURL+"screen";
            List<Screen> screens = new List<Screen>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            screens = JsonConvert.DeserializeObject<Screen[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            foreach(Screen screen in screens)
            {
                screen.SaveNewRight = 0;
                screen.SaveChangeRight = 0;
                screen.DeleteRight = 0;
                screen.PrintRight = 0;
                screen.ImportRight = 0;
                screen.ExportRight = 0;
            }
            return screens;
        }
    }
}
