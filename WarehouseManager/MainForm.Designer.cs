﻿
namespace WarehouseManager
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.ribbon = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barHeaderUserInfo = new DevExpress.XtraBars.BarHeaderItem();
            this.barButtonItemVietnam = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemEnglish = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem6 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemLogin = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemLogout = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemChangePass = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemAddUser = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemGood = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemGoodsType = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemGoodsCategory = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemGoodsLine = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemGoods = new DevExpress.XtraBars.BarButtonItem();
            this.popupMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barButtonItemWarehouseReceipt = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemIssueOrder = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemTallySheet = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemProcessTally = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemProcessInventory = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem7 = new DevExpress.XtraBars.BarButtonItem();
            this.popupMenu2 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barButtonItemReceiptRequisition = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemReceiptData = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemIssueRequisition = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemIssueData = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemReport = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemColor = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemUnit = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemBackupRestore = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemConnectReader = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemReadForm = new DevExpress.XtraBars.BarButtonItem();
            this.ConnectStatusHeader = new DevExpress.XtraBars.BarStaticItem();
            this.StatusBottom = new DevExpress.XtraBars.BarStaticItem();
            this.barButtonItemRFIDReaderManager = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemReaderConfig = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPageSystem = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroupUser = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupUserManager = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageCategory = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroupGoodsManager = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageWarehouse = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroupWarehouseManager = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupReport = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGro = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageHelp = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup4 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.connectBackgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.accessBackgroundWorker = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbon
            // 
            this.ribbon.CaptionBarItemLinks.Add(this.barHeaderUserInfo);
            this.ribbon.ExpandCollapseItem.Id = 0;
            this.ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barHeaderUserInfo,
            this.ribbon.ExpandCollapseItem,
            this.ribbon.SearchEditItem,
            this.barButtonItemVietnam,
            this.barButtonItemEnglish,
            this.barButtonItem3,
            this.barButtonItem4,
            this.barButtonItem5,
            this.barButtonItem6,
            this.barButtonItemLogin,
            this.barButtonItemLogout,
            this.barButtonItemChangePass,
            this.barButtonItemAddUser,
            this.barButtonItemGood,
            this.barButtonItemGoodsType,
            this.barButtonItemGoodsCategory,
            this.barButtonItemGoodsLine,
            this.barButtonItemGoods,
            this.barButtonItemWarehouseReceipt,
            this.barButtonItemIssueOrder,
            this.barButtonItemTallySheet,
            this.barButtonItemProcessTally,
            this.barButtonItemProcessInventory,
            this.barButtonItem7,
            this.barButtonItemReceiptRequisition,
            this.barButtonItemIssueRequisition,
            this.barButtonItemReceiptData,
            this.barButtonItemIssueData,
            this.barButtonItemReport,
            this.barButtonItemColor,
            this.barButtonItemUnit,
            this.barButtonItemBackupRestore,
            this.barButtonItemConnectReader,
            this.barButtonItemReadForm,
            this.ConnectStatusHeader,
            this.StatusBottom,
            this.barButtonItemRFIDReaderManager,
            this.barButtonItemReaderConfig});
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.MaxItemId = 41;
            this.ribbon.Name = "ribbon";
            this.ribbon.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPageSystem,
            this.ribbonPageCategory,
            this.ribbonPageWarehouse,
            this.ribbonPageHelp});
            this.ribbon.QuickToolbarItemLinks.Add(this.barButtonItemVietnam);
            this.ribbon.QuickToolbarItemLinks.Add(this.barButtonItemEnglish);
            this.ribbon.QuickToolbarItemLinks.Add(this.ConnectStatusHeader);
            this.ribbon.QuickToolbarItemLinks.Add(this.barButtonItemConnectReader);
            this.ribbon.Size = new System.Drawing.Size(1089, 147);
            this.ribbon.StatusBar = this.ribbonStatusBar;
            // 
            // barHeaderUserInfo
            // 
            this.barHeaderUserInfo.Caption = "Xin chào";
            this.barHeaderUserInfo.Id = 17;
            this.barHeaderUserInfo.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barHeaderUserInfo.ImageOptions.SvgImage")));
            this.barHeaderUserInfo.Name = "barHeaderUserInfo";
            // 
            // barButtonItemVietnam
            // 
            this.barButtonItemVietnam.Caption = "barButtonItem1";
            this.barButtonItemVietnam.Id = 1;
            this.barButtonItemVietnam.ImageOptions.Image = global::WarehouseManager.Properties.Resources.vietnamese;
            this.barButtonItemVietnam.Name = "barButtonItemVietnam";
            this.barButtonItemVietnam.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemVietnam_ItemClick);
            // 
            // barButtonItemEnglish
            // 
            this.barButtonItemEnglish.Caption = "barButtonItem2";
            this.barButtonItemEnglish.Id = 2;
            this.barButtonItemEnglish.ImageOptions.Image = global::WarehouseManager.Properties.Resources.usa;
            this.barButtonItemEnglish.Name = "barButtonItemEnglish";
            this.barButtonItemEnglish.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemEnglish_ItemClick);
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "barButtonItem3";
            this.barButtonItem3.Id = 3;
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "Đăng nhập";
            this.barButtonItem4.Id = 4;
            this.barButtonItem4.Name = "barButtonItem4";
            // 
            // barButtonItem5
            // 
            this.barButtonItem5.Caption = "Đăng nhập";
            this.barButtonItem5.Id = 5;
            this.barButtonItem5.Name = "barButtonItem5";
            this.barButtonItem5.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) 
            | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            // 
            // barButtonItem6
            // 
            this.barButtonItem6.Caption = "Đăng xuất";
            this.barButtonItem6.Id = 6;
            this.barButtonItem6.Name = "barButtonItem6";
            this.barButtonItem6.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) 
            | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            // 
            // barButtonItemLogin
            // 
            this.barButtonItemLogin.Caption = "Đăng nhập";
            this.barButtonItemLogin.Id = 7;
            this.barButtonItemLogin.ImageOptions.Image = global::WarehouseManager.Properties.Resources.login;
            this.barButtonItemLogin.Name = "barButtonItemLogin";
            this.barButtonItemLogin.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) 
            | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            this.barButtonItemLogin.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemLogin_ItemClick);
            // 
            // barButtonItemLogout
            // 
            this.barButtonItemLogout.Caption = "Đăng xuất";
            this.barButtonItemLogout.Id = 9;
            this.barButtonItemLogout.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItemLogout.ImageOptions.Image")));
            this.barButtonItemLogout.Name = "barButtonItemLogout";
            this.barButtonItemLogout.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) 
            | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            // 
            // barButtonItemChangePass
            // 
            this.barButtonItemChangePass.Caption = "Đổi mật khẩu";
            this.barButtonItemChangePass.Id = 10;
            this.barButtonItemChangePass.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItemChangePass.ImageOptions.Image")));
            this.barButtonItemChangePass.Name = "barButtonItemChangePass";
            this.barButtonItemChangePass.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) 
            | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            // 
            // barButtonItemAddUser
            // 
            this.barButtonItemAddUser.AllowHtmlText = DevExpress.Utils.DefaultBoolean.False;
            this.barButtonItemAddUser.Caption = "Tạo tài khoản và phân quyền";
            this.barButtonItemAddUser.Id = 11;
            this.barButtonItemAddUser.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItemAddUser.ImageOptions.Image")));
            this.barButtonItemAddUser.Name = "barButtonItemAddUser";
            this.barButtonItemAddUser.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) 
            | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            this.barButtonItemAddUser.Tag = "barButtonItemAddUser";
            // 
            // barButtonItemGood
            // 
            this.barButtonItemGood.Caption = "Hàng hóa";
            this.barButtonItemGood.Id = 12;
            this.barButtonItemGood.ImageOptions.Image = global::WarehouseManager.Properties.Resources.good;
            this.barButtonItemGood.Name = "barButtonItemGood";
            this.barButtonItemGood.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) 
            | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            // 
            // barButtonItemGoodsType
            // 
            this.barButtonItemGoodsType.Caption = "Loại hàng";
            this.barButtonItemGoodsType.Id = 13;
            this.barButtonItemGoodsType.ImageOptions.Image = global::WarehouseManager.Properties.Resources.goodtype;
            this.barButtonItemGoodsType.Name = "barButtonItemGoodsType";
            // 
            // barButtonItemGoodsCategory
            // 
            this.barButtonItemGoodsCategory.Caption = "Nhóm hàng";
            this.barButtonItemGoodsCategory.Id = 14;
            this.barButtonItemGoodsCategory.ImageOptions.Image = global::WarehouseManager.Properties.Resources.goodscategory;
            this.barButtonItemGoodsCategory.Name = "barButtonItemGoodsCategory";
            // 
            // barButtonItemGoodsLine
            // 
            this.barButtonItemGoodsLine.Caption = "Ngành hàng";
            this.barButtonItemGoodsLine.Id = 15;
            this.barButtonItemGoodsLine.ImageOptions.Image = global::WarehouseManager.Properties.Resources.goodsline;
            this.barButtonItemGoodsLine.Name = "barButtonItemGoodsLine";
            // 
            // barButtonItemGoods
            // 
            this.barButtonItemGoods.ActAsDropDown = true;
            this.barButtonItemGoods.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.barButtonItemGoods.Caption = "Hàng hóa";
            this.barButtonItemGoods.DropDownControl = this.popupMenu1;
            this.barButtonItemGoods.Id = 16;
            this.barButtonItemGoods.ImageOptions.Image = global::WarehouseManager.Properties.Resources.good;
            this.barButtonItemGoods.Name = "barButtonItemGoods";
            this.barButtonItemGoods.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) 
            | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            // 
            // popupMenu1
            // 
            this.popupMenu1.Name = "popupMenu1";
            this.popupMenu1.Ribbon = this.ribbon;
            // 
            // barButtonItemWarehouseReceipt
            // 
            this.barButtonItemWarehouseReceipt.Caption = "Phiếu nhập kho";
            this.barButtonItemWarehouseReceipt.Id = 18;
            this.barButtonItemWarehouseReceipt.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItemWarehouseReceipt.ImageOptions.SvgImage")));
            this.barButtonItemWarehouseReceipt.Name = "barButtonItemWarehouseReceipt";
            this.barButtonItemWarehouseReceipt.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) 
            | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            // 
            // barButtonItemIssueOrder
            // 
            this.barButtonItemIssueOrder.Caption = "Phiếu xuất kho";
            this.barButtonItemIssueOrder.Id = 19;
            this.barButtonItemIssueOrder.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItemIssueOrder.ImageOptions.SvgImage")));
            this.barButtonItemIssueOrder.Name = "barButtonItemIssueOrder";
            this.barButtonItemIssueOrder.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) 
            | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            // 
            // barButtonItemTallySheet
            // 
            this.barButtonItemTallySheet.Caption = "Phiếu kiểm kê";
            this.barButtonItemTallySheet.Id = 20;
            this.barButtonItemTallySheet.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItemTallySheet.ImageOptions.SvgImage")));
            this.barButtonItemTallySheet.Name = "barButtonItemTallySheet";
            // 
            // barButtonItemProcessTally
            // 
            this.barButtonItemProcessTally.Caption = "Xử lý kiểm kê";
            this.barButtonItemProcessTally.Id = 21;
            this.barButtonItemProcessTally.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItemProcessTally.ImageOptions.SvgImage")));
            this.barButtonItemProcessTally.Name = "barButtonItemProcessTally";
            // 
            // barButtonItemProcessInventory
            // 
            this.barButtonItemProcessInventory.Caption = "Xử lý tồn kho";
            this.barButtonItemProcessInventory.Id = 22;
            this.barButtonItemProcessInventory.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItemProcessInventory.ImageOptions.SvgImage")));
            this.barButtonItemProcessInventory.Name = "barButtonItemProcessInventory";
            // 
            // barButtonItem7
            // 
            this.barButtonItem7.ActAsDropDown = true;
            this.barButtonItem7.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.barButtonItem7.Caption = "Công cụ";
            this.barButtonItem7.DropDownControl = this.popupMenu2;
            this.barButtonItem7.Id = 23;
            this.barButtonItem7.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItem7.ImageOptions.SvgImage")));
            this.barButtonItem7.Name = "barButtonItem7";
            // 
            // popupMenu2
            // 
            this.popupMenu2.ItemLinks.Add(this.barButtonItemReceiptRequisition);
            this.popupMenu2.ItemLinks.Add(this.barButtonItemReceiptData);
            this.popupMenu2.ItemLinks.Add(this.barButtonItemIssueRequisition);
            this.popupMenu2.ItemLinks.Add(this.barButtonItemIssueData);
            this.popupMenu2.Name = "popupMenu2";
            this.popupMenu2.Ribbon = this.ribbon;
            // 
            // barButtonItemReceiptRequisition
            // 
            this.barButtonItemReceiptRequisition.Caption = "Yêu cầu nhập";
            this.barButtonItemReceiptRequisition.Id = 24;
            this.barButtonItemReceiptRequisition.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItemReceiptRequisition.ImageOptions.Image")));
            this.barButtonItemReceiptRequisition.Name = "barButtonItemReceiptRequisition";
            // 
            // barButtonItemReceiptData
            // 
            this.barButtonItemReceiptData.Caption = "Dữ liệu nhập";
            this.barButtonItemReceiptData.Id = 26;
            this.barButtonItemReceiptData.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItemReceiptData.ImageOptions.Image")));
            this.barButtonItemReceiptData.Name = "barButtonItemReceiptData";
            // 
            // barButtonItemIssueRequisition
            // 
            this.barButtonItemIssueRequisition.Caption = "Yêu cầu xuất";
            this.barButtonItemIssueRequisition.Id = 25;
            this.barButtonItemIssueRequisition.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItemIssueRequisition.ImageOptions.Image")));
            this.barButtonItemIssueRequisition.Name = "barButtonItemIssueRequisition";
            // 
            // barButtonItemIssueData
            // 
            this.barButtonItemIssueData.Caption = "Dữ liệu xuất";
            this.barButtonItemIssueData.Id = 27;
            this.barButtonItemIssueData.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItemIssueData.ImageOptions.Image")));
            this.barButtonItemIssueData.Name = "barButtonItemIssueData";
            // 
            // barButtonItemReport
            // 
            this.barButtonItemReport.Caption = "Báo cáo";
            this.barButtonItemReport.Id = 28;
            this.barButtonItemReport.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItemReport.ImageOptions.SvgImage")));
            this.barButtonItemReport.Name = "barButtonItemReport";
            // 
            // barButtonItemColor
            // 
            this.barButtonItemColor.Caption = "Màu sắc";
            this.barButtonItemColor.Id = 29;
            this.barButtonItemColor.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItemColor.ImageOptions.SvgImage")));
            this.barButtonItemColor.Name = "barButtonItemColor";
            // 
            // barButtonItemUnit
            // 
            this.barButtonItemUnit.Caption = "Đơn vị";
            this.barButtonItemUnit.Id = 30;
            this.barButtonItemUnit.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItemUnit.ImageOptions.SvgImage")));
            this.barButtonItemUnit.Name = "barButtonItemUnit";
            // 
            // barButtonItemBackupRestore
            // 
            this.barButtonItemBackupRestore.Caption = "Lưu trữ và phục hồi CSDL";
            this.barButtonItemBackupRestore.Id = 31;
            this.barButtonItemBackupRestore.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItemBackupRestore.ImageOptions.SvgImage")));
            this.barButtonItemBackupRestore.Name = "barButtonItemBackupRestore";
            // 
            // barButtonItemConnectReader
            // 
            this.barButtonItemConnectReader.Caption = "Connect RFID Reader";
            this.barButtonItemConnectReader.Id = 33;
            this.barButtonItemConnectReader.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItemConnectReader.ImageOptions.Image")));
            this.barButtonItemConnectReader.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItemConnectReader.ImageOptions.LargeImage")));
            this.barButtonItemConnectReader.Name = "barButtonItemConnectReader";
            this.barButtonItemConnectReader.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) 
            | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            this.barButtonItemConnectReader.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemConnectReader_ItemClick);
            // 
            // barButtonItemReadForm
            // 
            this.barButtonItemReadForm.Caption = "Read RFID";
            this.barButtonItemReadForm.Id = 35;
            this.barButtonItemReadForm.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItemReadForm.ImageOptions.Image")));
            this.barButtonItemReadForm.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItemReadForm.ImageOptions.LargeImage")));
            this.barButtonItemReadForm.Name = "barButtonItemReadForm";
            // 
            // ConnectStatusHeader
            // 
            this.ConnectStatusHeader.Caption = "Not Connect";
            this.ConnectStatusHeader.Id = 37;
            this.ConnectStatusHeader.Name = "ConnectStatusHeader";
            // 
            // StatusBottom
            // 
            this.StatusBottom.Caption = "Status";
            this.StatusBottom.Id = 38;
            this.StatusBottom.Name = "StatusBottom";
            // 
            // barButtonItemRFIDReaderManager
            // 
            this.barButtonItemRFIDReaderManager.Caption = "Reader Manager";
            this.barButtonItemRFIDReaderManager.Id = 39;
            this.barButtonItemRFIDReaderManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItemRFIDReaderManager.ImageOptions.Image")));
            this.barButtonItemRFIDReaderManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItemRFIDReaderManager.ImageOptions.LargeImage")));
            this.barButtonItemRFIDReaderManager.Name = "barButtonItemRFIDReaderManager";
            // 
            // barButtonItemReaderConfig
            // 
            this.barButtonItemReaderConfig.Caption = "Reader Config";
            this.barButtonItemReaderConfig.Enabled = false;
            this.barButtonItemReaderConfig.Id = 40;
            this.barButtonItemReaderConfig.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItemReaderConfig.ImageOptions.Image")));
            this.barButtonItemReaderConfig.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItemReaderConfig.ImageOptions.LargeImage")));
            this.barButtonItemReaderConfig.Name = "barButtonItemReaderConfig";
            // 
            // ribbonPageSystem
            // 
            this.ribbonPageSystem.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupUser,
            this.ribbonPageGroupUserManager,
            this.ribbonPageGroup2});
            this.ribbonPageSystem.Name = "ribbonPageSystem";
            this.ribbonPageSystem.Text = "Hệ thống";
            // 
            // ribbonPageGroupUser
            // 
            this.ribbonPageGroupUser.ItemLinks.Add(this.barButtonItemLogin);
            this.ribbonPageGroupUser.ItemLinks.Add(this.barButtonItemLogout);
            this.ribbonPageGroupUser.ItemLinks.Add(this.barButtonItemChangePass);
            this.ribbonPageGroupUser.Name = "ribbonPageGroupUser";
            this.ribbonPageGroupUser.Text = "Người dùng";
            // 
            // ribbonPageGroupUserManager
            // 
            this.ribbonPageGroupUserManager.ItemLinks.Add(this.barButtonItemAddUser);
            this.ribbonPageGroupUserManager.Name = "ribbonPageGroupUserManager";
            this.ribbonPageGroupUserManager.Text = "Quản lý tài khoản";
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.barButtonItemBackupRestore);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.Text = "Quản lý hệ thống";
            // 
            // ribbonPageCategory
            // 
            this.ribbonPageCategory.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupGoodsManager,
            this.ribbonPageGroup1});
            this.ribbonPageCategory.Name = "ribbonPageCategory";
            this.ribbonPageCategory.Text = "Danh mục";
            // 
            // ribbonPageGroupGoodsManager
            // 
            this.ribbonPageGroupGoodsManager.ItemLinks.Add(this.barButtonItemGoodsCategory);
            this.ribbonPageGroupGoodsManager.ItemLinks.Add(this.barButtonItemGoodsLine);
            this.ribbonPageGroupGoodsManager.ItemLinks.Add(this.barButtonItemGoodsType);
            this.ribbonPageGroupGoodsManager.ItemLinks.Add(this.barButtonItemGoods);
            this.ribbonPageGroupGoodsManager.Name = "ribbonPageGroupGoodsManager";
            this.ribbonPageGroupGoodsManager.Text = "Hàng hóa";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItemColor);
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItemUnit);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.Text = "ribbonPageGroup1";
            // 
            // ribbonPageWarehouse
            // 
            this.ribbonPageWarehouse.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupWarehouseManager,
            this.ribbonPageGroupReport,
            this.ribbonPageGro});
            this.ribbonPageWarehouse.Name = "ribbonPageWarehouse";
            this.ribbonPageWarehouse.Text = "Kho hàng";
            // 
            // ribbonPageGroupWarehouseManager
            // 
            this.ribbonPageGroupWarehouseManager.ItemLinks.Add(this.barButtonItemWarehouseReceipt);
            this.ribbonPageGroupWarehouseManager.ItemLinks.Add(this.barButtonItemIssueOrder);
            this.ribbonPageGroupWarehouseManager.ItemLinks.Add(this.barButtonItemTallySheet);
            this.ribbonPageGroupWarehouseManager.ItemLinks.Add(this.barButtonItemProcessTally);
            this.ribbonPageGroupWarehouseManager.ItemLinks.Add(this.barButtonItemProcessInventory);
            this.ribbonPageGroupWarehouseManager.ItemLinks.Add(this.barButtonItem7);
            this.ribbonPageGroupWarehouseManager.Name = "ribbonPageGroupWarehouseManager";
            this.ribbonPageGroupWarehouseManager.Text = "Quản lý xuất, nhập, tồn";
            // 
            // ribbonPageGroupReport
            // 
            this.ribbonPageGroupReport.ItemLinks.Add(this.barButtonItemReport);
            this.ribbonPageGroupReport.Name = "ribbonPageGroupReport";
            this.ribbonPageGroupReport.Text = "Báo cáo";
            // 
            // ribbonPageGro
            // 
            this.ribbonPageGro.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.True;
            this.ribbonPageGro.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("ribbonPageGro.ImageOptions.Image")));
            this.ribbonPageGro.ItemLinks.Add(this.barButtonItemReadForm);
            this.ribbonPageGro.ItemLinks.Add(this.barButtonItemRFIDReaderManager);
            this.ribbonPageGro.ItemLinks.Add(this.barButtonItemReaderConfig);
            this.ribbonPageGro.Name = "ribbonPageGro";
            this.ribbonPageGro.Text = "RFID";
            // 
            // ribbonPageHelp
            // 
            this.ribbonPageHelp.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup4});
            this.ribbonPageHelp.Name = "ribbonPageHelp";
            this.ribbonPageHelp.Text = "Trợ giúp";
            // 
            // ribbonPageGroup4
            // 
            this.ribbonPageGroup4.Name = "ribbonPageGroup4";
            this.ribbonPageGroup4.Text = "ribbonPageGroup4";
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.ItemLinks.Add(this.StatusBottom);
            this.ribbonStatusBar.Location = new System.Drawing.Point(0, 613);
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.ribbon;
            this.ribbonStatusBar.Size = new System.Drawing.Size(1089, 23);
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.ClosePageButtonShowMode = DevExpress.XtraTab.ClosePageButtonShowMode.InActiveTabPageHeader;
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 147);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.Size = new System.Drawing.Size(1089, 466);
            this.xtraTabControl1.TabIndex = 3;
            // 
            // connectBackgroundWorker
            // 
            this.connectBackgroundWorker.WorkerReportsProgress = true;
            this.connectBackgroundWorker.WorkerSupportsCancellation = true;
            // 
            // accessBackgroundWorker
            // 
            this.accessBackgroundWorker.WorkerReportsProgress = true;
            this.accessBackgroundWorker.WorkerSupportsCancellation = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1089, 636);
            this.Controls.Add(this.xtraTabControl1);
            this.Controls.Add(this.ribbonStatusBar);
            this.Controls.Add(this.ribbon);
            this.IsMdiContainer = true;
            this.KeyPreview = true;
            this.Name = "MainForm";
            this.Ribbon = this.ribbon;
            this.StatusBar = this.ribbonStatusBar;
            this.Text = "MainForm";
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbon;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPageSystem;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupUser;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private DevExpress.XtraBars.BarButtonItem barButtonItemVietnam;
        private DevExpress.XtraBars.BarButtonItem barButtonItemEnglish;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPageCategory;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupGoodsManager;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPageWarehouse;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupWarehouseManager;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPageHelp;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup4;
        private DevExpress.XtraBars.BarButtonItem barButtonItem5;
        private DevExpress.XtraBars.BarButtonItem barButtonItem6;
        private DevExpress.XtraBars.BarButtonItem barButtonItemLogin;
        private DevExpress.XtraBars.BarButtonItem barButtonItemLogout;
        private DevExpress.XtraBars.BarButtonItem barButtonItemChangePass;
        private DevExpress.XtraBars.BarButtonItem barButtonItemAddUser;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupUserManager;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraBars.BarButtonItem barButtonItemGood;
        private DevExpress.XtraBars.BarButtonItem barButtonItemGoodsType;
        private DevExpress.XtraBars.BarButtonItem barButtonItemGoodsCategory;
        private DevExpress.XtraBars.BarButtonItem barButtonItemGoodsLine;
        private DevExpress.XtraBars.BarButtonItem barButtonItemGoods;
        private DevExpress.XtraBars.BarHeaderItem barHeaderUserInfo;
        private DevExpress.XtraBars.PopupMenu popupMenu1;
        private DevExpress.XtraBars.BarButtonItem barButtonItemWarehouseReceipt;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupReport;
        private DevExpress.XtraBars.BarButtonItem barButtonItemIssueOrder;
        private DevExpress.XtraBars.BarButtonItem barButtonItemTallySheet;
        private DevExpress.XtraBars.BarButtonItem barButtonItemProcessTally;
        private DevExpress.XtraBars.BarButtonItem barButtonItemProcessInventory;
        private DevExpress.XtraBars.BarButtonItem barButtonItem7;
        private DevExpress.XtraBars.PopupMenu popupMenu2;
        private DevExpress.XtraBars.BarButtonItem barButtonItemReceiptRequisition;
        private DevExpress.XtraBars.BarButtonItem barButtonItemReceiptData;
        private DevExpress.XtraBars.BarButtonItem barButtonItemIssueRequisition;
        private DevExpress.XtraBars.BarButtonItem barButtonItemIssueData;
        private DevExpress.XtraBars.BarButtonItem barButtonItemReport;
        private DevExpress.XtraBars.BarButtonItem barButtonItemColor;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.BarButtonItem barButtonItemUnit;
        private DevExpress.XtraBars.BarButtonItem barButtonItemBackupRestore;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.BarButtonItem barButtonItemConnectReader;
        private DevExpress.XtraBars.BarButtonItem barButtonItemReadForm;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGro;
        public DevExpress.XtraBars.BarStaticItem ConnectStatusHeader;
        public DevExpress.XtraBars.BarStaticItem StatusBottom;
        public System.ComponentModel.BackgroundWorker connectBackgroundWorker;
        public System.ComponentModel.BackgroundWorker accessBackgroundWorker;
        private DevExpress.XtraBars.BarButtonItem barButtonItemRFIDReaderManager;
        private DevExpress.XtraBars.BarButtonItem barButtonItemReaderConfig;
    }
}