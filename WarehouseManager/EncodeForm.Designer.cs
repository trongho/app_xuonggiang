﻿
namespace WarehouseManager
{
    partial class EncodeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraLayout.ColumnDefinition columnDefinition1 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition2 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition3 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition1 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition2 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition3 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EncodeForm));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.textEditBarcode = new DevExpress.XtraEditors.TextEdit();
            this.simpleButtonEnCode = new DevExpress.XtraEditors.SimpleButton();
            this.textEditQuantity = new DevExpress.XtraEditors.TextEdit();
            this.textEditPassword = new DevExpress.XtraEditors.TextEdit();
            this.comboBoxEditMemoryBank = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEditOffset = new DevExpress.XtraEditors.TextEdit();
            this.textEditNoOfBytesLenght = new DevExpress.XtraEditors.TextEdit();
            this.textEditEncoded = new DevExpress.XtraEditors.TextEdit();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.gridControlEncode = new DevExpress.XtraGrid.GridControl();
            this.gridViewEncode = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTagID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTagEvent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditBarcode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditQuantity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPassword.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditMemoryBank.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditOffset.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditNoOfBytesLenght.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditEncoded.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlEncode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEncode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.textEditBarcode);
            this.layoutControl1.Controls.Add(this.simpleButtonEnCode);
            this.layoutControl1.Controls.Add(this.textEditQuantity);
            this.layoutControl1.Controls.Add(this.textEditPassword);
            this.layoutControl1.Controls.Add(this.comboBoxEditMemoryBank);
            this.layoutControl1.Controls.Add(this.textEditOffset);
            this.layoutControl1.Controls.Add(this.textEditNoOfBytesLenght);
            this.layoutControl1.Controls.Add(this.textEditEncoded);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(968, 0, 650, 400);
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(918, 100);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // textEditBarcode
            // 
            this.textEditBarcode.Location = new System.Drawing.Point(113, 12);
            this.textEditBarcode.Name = "textEditBarcode";
            this.textEditBarcode.Size = new System.Drawing.Size(254, 20);
            this.textEditBarcode.StyleController = this.layoutControl1;
            this.textEditBarcode.TabIndex = 4;
            // 
            // simpleButtonEnCode
            // 
            this.simpleButtonEnCode.Enabled = false;
            this.simpleButtonEnCode.Location = new System.Drawing.Point(730, 12);
            this.simpleButtonEnCode.Name = "simpleButtonEnCode";
            this.simpleButtonEnCode.Size = new System.Drawing.Size(176, 22);
            this.simpleButtonEnCode.StyleController = this.layoutControl1;
            this.simpleButtonEnCode.TabIndex = 5;
            this.simpleButtonEnCode.Text = "EnCode";
            this.simpleButtonEnCode.Visible = false;
            // 
            // textEditQuantity
            // 
            this.textEditQuantity.EditValue = "";
            this.textEditQuantity.Location = new System.Drawing.Point(113, 38);
            this.textEditQuantity.Name = "textEditQuantity";
            this.textEditQuantity.Size = new System.Drawing.Size(254, 20);
            this.textEditQuantity.StyleController = this.layoutControl1;
            this.textEditQuantity.TabIndex = 6;
            // 
            // textEditPassword
            // 
            this.textEditPassword.Location = new System.Drawing.Point(472, 12);
            this.textEditPassword.Name = "textEditPassword";
            this.textEditPassword.Size = new System.Drawing.Size(254, 20);
            this.textEditPassword.StyleController = this.layoutControl1;
            this.textEditPassword.TabIndex = 5;
            // 
            // comboBoxEditMemoryBank
            // 
            this.comboBoxEditMemoryBank.Location = new System.Drawing.Point(472, 38);
            this.comboBoxEditMemoryBank.Name = "comboBoxEditMemoryBank";
            this.comboBoxEditMemoryBank.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditMemoryBank.Properties.Items.AddRange(new object[] {
            "RESERVED",
            "EPC",
            "TID",
            "USER"});
            this.comboBoxEditMemoryBank.Size = new System.Drawing.Size(254, 20);
            this.comboBoxEditMemoryBank.StyleController = this.layoutControl1;
            this.comboBoxEditMemoryBank.TabIndex = 6;
            // 
            // textEditOffset
            // 
            this.textEditOffset.Location = new System.Drawing.Point(472, 65);
            this.textEditOffset.Name = "textEditOffset";
            this.textEditOffset.Size = new System.Drawing.Size(254, 20);
            this.textEditOffset.StyleController = this.layoutControl1;
            this.textEditOffset.TabIndex = 7;
            // 
            // textEditNoOfBytesLenght
            // 
            this.textEditNoOfBytesLenght.Location = new System.Drawing.Point(831, 65);
            this.textEditNoOfBytesLenght.Name = "textEditNoOfBytesLenght";
            this.textEditNoOfBytesLenght.Size = new System.Drawing.Size(75, 20);
            this.textEditNoOfBytesLenght.StyleController = this.layoutControl1;
            this.textEditNoOfBytesLenght.TabIndex = 8;
            // 
            // textEditEncoded
            // 
            this.textEditEncoded.Location = new System.Drawing.Point(113, 65);
            this.textEditEncoded.Name = "textEditEncoded";
            this.textEditEncoded.Size = new System.Drawing.Size(254, 20);
            this.textEditEncoded.StyleController = this.layoutControl1;
            this.textEditEncoded.TabIndex = 9;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem2,
            this.layoutControlItem9});
            this.Root.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.Root.Name = "Root";
            columnDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition1.Width = 40D;
            columnDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition2.Width = 40D;
            columnDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition3.Width = 20D;
            this.Root.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition1,
            columnDefinition2,
            columnDefinition3});
            rowDefinition1.Height = 33.333333333333336D;
            rowDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition2.Height = 33.333333333333336D;
            rowDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition3.Height = 33.333333333333336D;
            rowDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            this.Root.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition1,
            rowDefinition2,
            rowDefinition3});
            this.Root.Size = new System.Drawing.Size(918, 100);
            this.Root.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.simpleButtonEnCode;
            this.layoutControlItem3.Location = new System.Drawing.Point(718, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItem3.Size = new System.Drawing.Size(180, 26);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.textEditQuantity;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItem4.Size = new System.Drawing.Size(359, 27);
            this.layoutControlItem4.Text = "Qty";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.textEditPassword;
            this.layoutControlItem5.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem5.CustomizationFormText = "Password( Hex)";
            this.layoutControlItem5.Location = new System.Drawing.Point(359, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItem5.Size = new System.Drawing.Size(359, 26);
            this.layoutControlItem5.Text = "Password( Hex)";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.comboBoxEditMemoryBank;
            this.layoutControlItem6.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem6.CustomizationFormText = "Memory Bank";
            this.layoutControlItem6.Location = new System.Drawing.Point(359, 26);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItem6.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItem6.Size = new System.Drawing.Size(359, 27);
            this.layoutControlItem6.Text = "Memory Bank";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.textEditOffset;
            this.layoutControlItem7.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem7.CustomizationFormText = "Offset( Byte)";
            this.layoutControlItem7.Location = new System.Drawing.Point(359, 53);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItem7.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItem7.Size = new System.Drawing.Size(359, 27);
            this.layoutControlItem7.Text = "Offset( Byte)";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.textEditNoOfBytesLenght;
            this.layoutControlItem8.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem8.CustomizationFormText = "No. Of Bytes Lenght";
            this.layoutControlItem8.Location = new System.Drawing.Point(718, 53);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItem8.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItem8.Size = new System.Drawing.Size(180, 27);
            this.layoutControlItem8.Text = "No. Of Bytes Lenght";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.textEditBarcode;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(359, 26);
            this.layoutControlItem2.Text = "Barcode";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.textEditEncoded;
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 53);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItem9.Size = new System.Drawing.Size(359, 27);
            this.layoutControlItem9.Text = "Qty đã encode";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.gridControlEncode);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(0, 100);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.Root = this.layoutControlGroup1;
            this.layoutControl2.Size = new System.Drawing.Size(918, 370);
            this.layoutControl2.TabIndex = 1;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // gridControlEncode
            // 
            this.gridControlEncode.Location = new System.Drawing.Point(12, 12);
            this.gridControlEncode.MainView = this.gridViewEncode;
            this.gridControlEncode.Name = "gridControlEncode";
            this.gridControlEncode.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEdit1});
            this.gridControlEncode.Size = new System.Drawing.Size(894, 346);
            this.gridControlEncode.TabIndex = 4;
            this.gridControlEncode.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewEncode});
            // 
            // gridViewEncode
            // 
            this.gridViewEncode.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTagID,
            this.colTagEvent,
            this.gridColumn1});
            this.gridViewEncode.GridControl = this.gridControlEncode;
            this.gridViewEncode.Name = "gridViewEncode";
            // 
            // colTagID
            // 
            this.colTagID.Caption = "Tag ID";
            this.colTagID.FieldName = "TagID";
            this.colTagID.MinWidth = 60;
            this.colTagID.Name = "colTagID";
            this.colTagID.Visible = true;
            this.colTagID.VisibleIndex = 0;
            this.colTagID.Width = 80;
            // 
            // colTagEvent
            // 
            this.colTagEvent.Caption = "TagEvent";
            this.colTagEvent.FieldName = "TagEvent";
            this.colTagEvent.MinWidth = 30;
            this.colTagEvent.Name = "colTagEvent";
            this.colTagEvent.Visible = true;
            this.colTagEvent.VisibleIndex = 1;
            this.colTagEvent.Width = 421;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Encode";
            this.gridColumn1.ColumnEdit = this.repositoryItemButtonEdit1;
            this.gridColumn1.MinWidth = 40;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 2;
            this.gridColumn1.Width = 40;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            editorButtonImageOptions1.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("editorButtonImageOptions1.SvgImage")));
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", "encode", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.Tag = "encode";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(918, 370);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridControlEncode;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(898, 350);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // EncodeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(918, 470);
            this.Controls.Add(this.layoutControl2);
            this.Controls.Add(this.layoutControl1);
            this.Name = "EncodeForm";
            this.Text = "EncodeForm";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEditBarcode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditQuantity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPassword.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditMemoryBank.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditOffset.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditNoOfBytesLenght.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditEncoded.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlEncode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEncode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        public DevExpress.XtraGrid.GridControl gridControlEncode;
        public DevExpress.XtraGrid.Views.Grid.GridView gridViewEncode;
        private DevExpress.XtraGrid.Columns.GridColumn colTagID;
        private DevExpress.XtraGrid.Columns.GridColumn colTagEvent;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.TextEdit textEditBarcode;
        public DevExpress.XtraEditors.SimpleButton simpleButtonEnCode;
        public DevExpress.XtraEditors.TextEdit textEditQuantity;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        public DevExpress.XtraEditors.TextEdit textEditPassword;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        public DevExpress.XtraEditors.ComboBoxEdit comboBoxEditMemoryBank;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        public DevExpress.XtraEditors.TextEdit textEditOffset;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        public DevExpress.XtraEditors.TextEdit textEditNoOfBytesLenght;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraEditors.TextEdit textEditEncoded;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
    }
}