﻿
namespace WarehouseManager
{
    partial class ReceiptRequisitionControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions10 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReceiptRequisitionControl));
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions11 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions12 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions13 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions14 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions15 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions16 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions17 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions18 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition7 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition8 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition9 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition10 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition11 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition12 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition6 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition7 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition8 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition9 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition10 = new DevExpress.XtraLayout.RowDefinition();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.windowsUIButtonPanel1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.label1 = new System.Windows.Forms.Label();
            this.textEditWRRNumber = new DevExpress.XtraEditors.TextEdit();
            this.dateEditWRRDate = new DevExpress.XtraEditors.DateEdit();
            this.textEditTotalQuantityByItem = new DevExpress.XtraEditors.TextEdit();
            this.textEditTotalQuantityByPack = new DevExpress.XtraEditors.TextEdit();
            this.comboBoxEditHandlingStatus = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEditBranch = new DevExpress.XtraEditors.ComboBoxEdit();
            this.simpleButtonRead = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonWriteTag = new DevExpress.XtraEditors.SimpleButton();
            this.textEditReaded = new DevExpress.XtraEditors.TextEdit();
            this.textEditEncoded = new DevExpress.XtraEditors.TextEdit();
            this.textEditTagIDs = new DevExpress.XtraEditors.MemoEdit();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonStart = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonStop = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemWRRNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemTotalQuantityByItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemTotalQuantityByPack = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemBranch = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemWRRDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemHandlingStatus = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.wRRDetailBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.wARHOUSE_HPDataSet1 = new WarehouseManager.WARHOUSE_HPDataSet1();
            this.wRRDetailTableAdapter = new WarehouseManager.WARHOUSE_HPDataSet1TableAdapters.WRRDetailTableAdapter();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.gridControlWRRDetail = new DevExpress.XtraGrid.GridControl();
            this.gridViewWRRDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWRRNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrdinal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRFIDTagID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBarcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStyleColorSize = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStyle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colColor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSku = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSize = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGender = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductGroup = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescriptionVN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLifeStylePerformance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDiv = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOutsole = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Productline = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEXWorkUSD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEXWorkVND = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAmountVND = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRetailPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSeason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCoo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaterial = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInvoiceNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateOfInvoice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaleContract = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShipment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFactoryName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFactoryAdress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFactoryNameVN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFactoryAdressVN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNewPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colChangePrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditWRRNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditWRRDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditWRRDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalQuantityByItem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalQuantityByPack.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditHandlingStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditBranch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditReaded.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditEncoded.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTagIDs.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWRRNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalQuantityByItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalQuantityByPack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemBranch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWRRDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemHandlingStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wRRDetailBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wARHOUSE_HPDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlWRRDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewWRRDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.windowsUIButtonPanel1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(400, 144, 812, 500);
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(1240, 37);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // windowsUIButtonPanel1
            // 
            this.windowsUIButtonPanel1.AllowGlyphSkinning = false;
            this.windowsUIButtonPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            windowsUIButtonImageOptions10.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions10.Image")));
            windowsUIButtonImageOptions10.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions11.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions11.Image")));
            windowsUIButtonImageOptions11.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions12.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions12.Image")));
            windowsUIButtonImageOptions12.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions13.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions13.Image")));
            windowsUIButtonImageOptions13.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions14.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions14.Image")));
            windowsUIButtonImageOptions14.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions15.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions15.Image")));
            windowsUIButtonImageOptions15.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions16.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions16.Image")));
            windowsUIButtonImageOptions16.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions17.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions17.Image")));
            windowsUIButtonImageOptions17.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions18.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions18.Image")));
            windowsUIButtonImageOptions18.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            this.windowsUIButtonPanel1.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F2) Thêm mới", true, windowsUIButtonImageOptions10, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "new", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F3) Lưu", true, windowsUIButtonImageOptions11, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "save", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F4) Xóa", true, windowsUIButtonImageOptions12, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "delete", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F5) Tìm kiếm", true, windowsUIButtonImageOptions13, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "search", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F6) Làm mới", true, windowsUIButtonImageOptions14, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "refesh", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F7) Nhập dữ liệu", true, windowsUIButtonImageOptions15, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "import", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F8) Xuất dữ liệu", true, windowsUIButtonImageOptions16, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "export", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F9) In ấn", true, windowsUIButtonImageOptions17, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "print", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F10) Thoát", true, windowsUIButtonImageOptions18, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "close", -1, false)});
            this.windowsUIButtonPanel1.ContentAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.windowsUIButtonPanel1.Location = new System.Drawing.Point(2, 2);
            this.windowsUIButtonPanel1.Name = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.Padding = new System.Windows.Forms.Padding(5);
            this.windowsUIButtonPanel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.windowsUIButtonPanel1.Size = new System.Drawing.Size(1236, 32);
            this.windowsUIButtonPanel1.TabIndex = 4;
            this.windowsUIButtonPanel1.Text = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.UseButtonBackgroundImages = false;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.Root.Name = "Root";
            this.Root.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.Root.Size = new System.Drawing.Size(1240, 37);
            this.Root.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.windowsUIButtonPanel1;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1240, 37);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.label1);
            this.layoutControl2.Controls.Add(this.textEditWRRNumber);
            this.layoutControl2.Controls.Add(this.dateEditWRRDate);
            this.layoutControl2.Controls.Add(this.textEditTotalQuantityByItem);
            this.layoutControl2.Controls.Add(this.textEditTotalQuantityByPack);
            this.layoutControl2.Controls.Add(this.comboBoxEditHandlingStatus);
            this.layoutControl2.Controls.Add(this.comboBoxEditBranch);
            this.layoutControl2.Controls.Add(this.simpleButtonRead);
            this.layoutControl2.Controls.Add(this.simpleButtonWriteTag);
            this.layoutControl2.Controls.Add(this.textEditReaded);
            this.layoutControl2.Controls.Add(this.textEditEncoded);
            this.layoutControl2.Controls.Add(this.textEditTagIDs);
            this.layoutControl2.Controls.Add(this.simpleButton1);
            this.layoutControl2.Controls.Add(this.simpleButtonStart);
            this.layoutControl2.Controls.Add(this.simpleButtonStop);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl2.Location = new System.Drawing.Point(0, 37);
            this.layoutControl2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(661, 293, 812, 500);
            this.layoutControl2.Root = this.layoutControlGroup1;
            this.layoutControl2.Size = new System.Drawing.Size(1240, 156);
            this.layoutControl2.TabIndex = 1;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // label1
            // 
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(248, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 23);
            this.label1.TabIndex = 6;
            this.label1.Text = "(*)";
            // 
            // textEditWRRNumber
            // 
            this.textEditWRRNumber.Location = new System.Drawing.Point(99, 12);
            this.textEditWRRNumber.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditWRRNumber.Name = "textEditWRRNumber";
            this.textEditWRRNumber.Size = new System.Drawing.Size(145, 20);
            this.textEditWRRNumber.StyleController = this.layoutControl2;
            this.textEditWRRNumber.TabIndex = 4;
            // 
            // dateEditWRRDate
            // 
            this.dateEditWRRDate.EditValue = null;
            this.dateEditWRRDate.Location = new System.Drawing.Point(368, 12);
            this.dateEditWRRDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dateEditWRRDate.Name = "dateEditWRRDate";
            this.dateEditWRRDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditWRRDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditWRRDate.Properties.CalendarTimeProperties.DisplayFormat.FormatString = "d";
            this.dateEditWRRDate.Properties.CalendarTimeProperties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditWRRDate.Properties.CalendarTimeProperties.EditFormat.FormatString = "d";
            this.dateEditWRRDate.Properties.CalendarTimeProperties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditWRRDate.Size = new System.Drawing.Size(149, 20);
            this.dateEditWRRDate.StyleController = this.layoutControl2;
            this.dateEditWRRDate.TabIndex = 7;
            // 
            // textEditTotalQuantityByItem
            // 
            this.textEditTotalQuantityByItem.Location = new System.Drawing.Point(1079, 12);
            this.textEditTotalQuantityByItem.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditTotalQuantityByItem.Name = "textEditTotalQuantityByItem";
            this.textEditTotalQuantityByItem.Size = new System.Drawing.Size(149, 20);
            this.textEditTotalQuantityByItem.StyleController = this.layoutControl2;
            this.textEditTotalQuantityByItem.TabIndex = 10;
            // 
            // textEditTotalQuantityByPack
            // 
            this.textEditTotalQuantityByPack.Location = new System.Drawing.Point(1079, 39);
            this.textEditTotalQuantityByPack.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditTotalQuantityByPack.Name = "textEditTotalQuantityByPack";
            this.textEditTotalQuantityByPack.Size = new System.Drawing.Size(149, 20);
            this.textEditTotalQuantityByPack.StyleController = this.layoutControl2;
            this.textEditTotalQuantityByPack.TabIndex = 11;
            // 
            // comboBoxEditHandlingStatus
            // 
            this.comboBoxEditHandlingStatus.Location = new System.Drawing.Point(368, 39);
            this.comboBoxEditHandlingStatus.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBoxEditHandlingStatus.Name = "comboBoxEditHandlingStatus";
            this.comboBoxEditHandlingStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditHandlingStatus.Size = new System.Drawing.Size(149, 20);
            this.comboBoxEditHandlingStatus.StyleController = this.layoutControl2;
            this.comboBoxEditHandlingStatus.TabIndex = 13;
            // 
            // comboBoxEditBranch
            // 
            this.comboBoxEditBranch.Location = new System.Drawing.Point(96, 39);
            this.comboBoxEditBranch.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBoxEditBranch.Name = "comboBoxEditBranch";
            this.comboBoxEditBranch.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditBranch.Size = new System.Drawing.Size(148, 20);
            this.comboBoxEditBranch.StyleController = this.layoutControl2;
            this.comboBoxEditBranch.TabIndex = 14;
            // 
            // simpleButtonRead
            // 
            this.simpleButtonRead.Location = new System.Drawing.Point(758, 12);
            this.simpleButtonRead.Name = "simpleButtonRead";
            this.simpleButtonRead.Size = new System.Drawing.Size(233, 22);
            this.simpleButtonRead.StyleController = this.layoutControl2;
            this.simpleButtonRead.TabIndex = 15;
            this.simpleButtonRead.Text = "Read Tag";
            this.simpleButtonRead.Click += new System.EventHandler(this.simpleButtonRead_Click);
            // 
            // simpleButtonWriteTag
            // 
            this.simpleButtonWriteTag.Location = new System.Drawing.Point(758, 39);
            this.simpleButtonWriteTag.Name = "simpleButtonWriteTag";
            this.simpleButtonWriteTag.Size = new System.Drawing.Size(233, 22);
            this.simpleButtonWriteTag.StyleController = this.layoutControl2;
            this.simpleButtonWriteTag.TabIndex = 16;
            this.simpleButtonWriteTag.Text = "Write Tag";
            this.simpleButtonWriteTag.Click += new System.EventHandler(this.simpleButtonWriteTag_Click);
            // 
            // textEditReaded
            // 
            this.textEditReaded.Location = new System.Drawing.Point(605, 12);
            this.textEditReaded.Name = "textEditReaded";
            this.textEditReaded.Size = new System.Drawing.Size(149, 20);
            this.textEditReaded.StyleController = this.layoutControl2;
            this.textEditReaded.TabIndex = 17;
            // 
            // textEditEncoded
            // 
            this.textEditEncoded.Location = new System.Drawing.Point(605, 39);
            this.textEditEncoded.Name = "textEditEncoded";
            this.textEditEncoded.Size = new System.Drawing.Size(149, 20);
            this.textEditEncoded.StyleController = this.layoutControl2;
            this.textEditEncoded.TabIndex = 18;
            // 
            // textEditTagIDs
            // 
            this.textEditTagIDs.Location = new System.Drawing.Point(96, 66);
            this.textEditTagIDs.Name = "textEditTagIDs";
            this.textEditTagIDs.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.textEditTagIDs.Size = new System.Drawing.Size(658, 50);
            this.textEditTagIDs.StyleController = this.layoutControl2;
            this.textEditTagIDs.TabIndex = 19;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(758, 66);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(233, 22);
            this.simpleButton1.StyleController = this.layoutControl2;
            this.simpleButton1.TabIndex = 20;
            this.simpleButton1.Text = "Encode All";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // simpleButtonStart
            // 
            this.simpleButtonStart.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Success;
            this.simpleButtonStart.Appearance.Options.UseBackColor = true;
            this.simpleButtonStart.Location = new System.Drawing.Point(758, 93);
            this.simpleButtonStart.Name = "simpleButtonStart";
            this.simpleButtonStart.Size = new System.Drawing.Size(233, 22);
            this.simpleButtonStart.StyleController = this.layoutControl2;
            this.simpleButtonStart.TabIndex = 21;
            this.simpleButtonStart.Text = "ON";
            // 
            // simpleButtonStop
            // 
            this.simpleButtonStop.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Danger;
            this.simpleButtonStop.Appearance.Options.UseBackColor = true;
            this.simpleButtonStop.Enabled = false;
            this.simpleButtonStop.Location = new System.Drawing.Point(758, 120);
            this.simpleButtonStop.Name = "simpleButtonStop";
            this.simpleButtonStop.Size = new System.Drawing.Size(233, 22);
            this.simpleButtonStop.StyleController = this.layoutControl2;
            this.simpleButtonStop.TabIndex = 22;
            this.simpleButtonStop.Text = "OFF";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemWRRNumber,
            this.layoutControlItem3,
            this.layoutControlItemTotalQuantityByItem,
            this.layoutControlItemTotalQuantityByPack,
            this.layoutControlItemBranch,
            this.layoutControlItemWRRDate,
            this.layoutControlItemHandlingStatus,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem11});
            this.layoutControlGroup1.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.layoutControlGroup1.Name = "Root";
            columnDefinition7.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition7.Width = 19.405592722871681D;
            columnDefinition8.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition8.Width = 2.9720363856415997D;
            columnDefinition9.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition9.Width = 19.405592722871681D;
            columnDefinition10.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition10.Width = 19.405592722871681D;
            columnDefinition11.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition11.Width = 19.405592722871681D;
            columnDefinition12.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition12.Width = 19.405592722871681D;
            this.layoutControlGroup1.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition7,
            columnDefinition8,
            columnDefinition9,
            columnDefinition10,
            columnDefinition11,
            columnDefinition12});
            rowDefinition6.Height = 20D;
            rowDefinition6.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition7.Height = 20D;
            rowDefinition7.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition8.Height = 20D;
            rowDefinition8.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition9.Height = 20D;
            rowDefinition9.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition10.Height = 20D;
            rowDefinition10.SizeType = System.Windows.Forms.SizeType.Percent;
            this.layoutControlGroup1.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition6,
            rowDefinition7,
            rowDefinition8,
            rowDefinition9,
            rowDefinition10});
            this.layoutControlGroup1.Size = new System.Drawing.Size(1240, 156);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItemWRRNumber
            // 
            this.layoutControlItemWRRNumber.Control = this.textEditWRRNumber;
            this.layoutControlItemWRRNumber.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemWRRNumber.Name = "layoutControlItemWRRNumber";
            this.layoutControlItemWRRNumber.Size = new System.Drawing.Size(236, 27);
            this.layoutControlItemWRRNumber.Text = "Số phiếu yêu cầu";
            this.layoutControlItemWRRNumber.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItemWRRNumber.TextSize = new System.Drawing.Size(82, 13);
            this.layoutControlItemWRRNumber.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.label1;
            this.layoutControlItem3.Location = new System.Drawing.Point(236, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItem3.Size = new System.Drawing.Size(36, 27);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItemTotalQuantityByItem
            // 
            this.layoutControlItemTotalQuantityByItem.Control = this.textEditTotalQuantityByItem;
            this.layoutControlItemTotalQuantityByItem.Location = new System.Drawing.Point(983, 0);
            this.layoutControlItemTotalQuantityByItem.Name = "layoutControlItemTotalQuantityByItem";
            this.layoutControlItemTotalQuantityByItem.OptionsTableLayoutItem.ColumnIndex = 5;
            this.layoutControlItemTotalQuantityByItem.Size = new System.Drawing.Size(237, 27);
            this.layoutControlItemTotalQuantityByItem.Text = "∑ SL yêu cầu";
            this.layoutControlItemTotalQuantityByItem.TextSize = new System.Drawing.Size(81, 13);
            // 
            // layoutControlItemTotalQuantityByPack
            // 
            this.layoutControlItemTotalQuantityByPack.Control = this.textEditTotalQuantityByPack;
            this.layoutControlItemTotalQuantityByPack.CustomizationFormText = "Σ Mã yêu cầu";
            this.layoutControlItemTotalQuantityByPack.Location = new System.Drawing.Point(983, 27);
            this.layoutControlItemTotalQuantityByPack.Name = "layoutControlItemTotalQuantityByPack";
            this.layoutControlItemTotalQuantityByPack.OptionsTableLayoutItem.ColumnIndex = 5;
            this.layoutControlItemTotalQuantityByPack.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemTotalQuantityByPack.Size = new System.Drawing.Size(237, 27);
            this.layoutControlItemTotalQuantityByPack.Text = "Σ Mã yêu cầu";
            this.layoutControlItemTotalQuantityByPack.TextSize = new System.Drawing.Size(81, 13);
            // 
            // layoutControlItemBranch
            // 
            this.layoutControlItemBranch.Control = this.comboBoxEditBranch;
            this.layoutControlItemBranch.Location = new System.Drawing.Point(0, 27);
            this.layoutControlItemBranch.Name = "layoutControlItemBranch";
            this.layoutControlItemBranch.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemBranch.Size = new System.Drawing.Size(236, 27);
            this.layoutControlItemBranch.Text = "Chi nhánh";
            this.layoutControlItemBranch.TextSize = new System.Drawing.Size(81, 13);
            // 
            // layoutControlItemWRRDate
            // 
            this.layoutControlItemWRRDate.Control = this.dateEditWRRDate;
            this.layoutControlItemWRRDate.Location = new System.Drawing.Point(272, 0);
            this.layoutControlItemWRRDate.Name = "layoutControlItemWRRDate";
            this.layoutControlItemWRRDate.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItemWRRDate.Size = new System.Drawing.Size(237, 27);
            this.layoutControlItemWRRDate.Text = "Ngày yêu cầu";
            this.layoutControlItemWRRDate.TextSize = new System.Drawing.Size(81, 13);
            // 
            // layoutControlItemHandlingStatus
            // 
            this.layoutControlItemHandlingStatus.Control = this.comboBoxEditHandlingStatus;
            this.layoutControlItemHandlingStatus.Location = new System.Drawing.Point(272, 27);
            this.layoutControlItemHandlingStatus.Name = "layoutControlItemHandlingStatus";
            this.layoutControlItemHandlingStatus.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItemHandlingStatus.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemHandlingStatus.Size = new System.Drawing.Size(237, 27);
            this.layoutControlItemHandlingStatus.Text = "Trạng thái xử lý";
            this.layoutControlItemHandlingStatus.TextSize = new System.Drawing.Size(81, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.simpleButtonRead;
            this.layoutControlItem4.Location = new System.Drawing.Point(746, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItem4.Size = new System.Drawing.Size(237, 27);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.simpleButtonWriteTag;
            this.layoutControlItem5.Location = new System.Drawing.Point(746, 27);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItem5.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItem5.Size = new System.Drawing.Size(237, 27);
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.textEditReaded;
            this.layoutControlItem6.Location = new System.Drawing.Point(509, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.OptionsTableLayoutItem.ColumnIndex = 3;
            this.layoutControlItem6.Size = new System.Drawing.Size(237, 27);
            this.layoutControlItem6.Text = "∑ Tag nhận đc";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(81, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.textEditEncoded;
            this.layoutControlItem7.Location = new System.Drawing.Point(509, 27);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.OptionsTableLayoutItem.ColumnIndex = 3;
            this.layoutControlItem7.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItem7.Size = new System.Drawing.Size(237, 27);
            this.layoutControlItem7.Text = "∑ Tag đã encode";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(81, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.textEditTagIDs;
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 54);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.OptionsTableLayoutItem.ColumnSpan = 4;
            this.layoutControlItem8.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItem8.OptionsTableLayoutItem.RowSpan = 2;
            this.layoutControlItem8.Size = new System.Drawing.Size(746, 54);
            this.layoutControlItem8.Text = "List Tag ID";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(81, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.simpleButton1;
            this.layoutControlItem9.Location = new System.Drawing.Point(746, 54);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItem9.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItem9.Size = new System.Drawing.Size(237, 27);
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.simpleButtonStart;
            this.layoutControlItem10.Location = new System.Drawing.Point(746, 81);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItem10.OptionsTableLayoutItem.RowIndex = 3;
            this.layoutControlItem10.Size = new System.Drawing.Size(237, 27);
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.simpleButtonStop;
            this.layoutControlItem11.Location = new System.Drawing.Point(746, 108);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItem11.OptionsTableLayoutItem.RowIndex = 4;
            this.layoutControlItem11.Size = new System.Drawing.Size(237, 28);
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextVisible = false;
            // 
            // wRRDetailBindingSource
            // 
            this.wRRDetailBindingSource.DataMember = "WRRDetail";
            this.wRRDetailBindingSource.DataSource = this.wARHOUSE_HPDataSet1;
            // 
            // wARHOUSE_HPDataSet1
            // 
            this.wARHOUSE_HPDataSet1.DataSetName = "WARHOUSE_HPDataSet1";
            this.wARHOUSE_HPDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // wRRDetailTableAdapter
            // 
            this.wRRDetailTableAdapter.ClearBeforeFill = true;
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.gridControlWRRDetail);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl3.Location = new System.Drawing.Point(0, 193);
            this.layoutControl3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(571, 354, 650, 400);
            this.layoutControl3.Root = this.layoutControlGroup2;
            this.layoutControl3.Size = new System.Drawing.Size(1240, 453);
            this.layoutControl3.TabIndex = 2;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // gridControlWRRDetail
            // 
            this.gridControlWRRDetail.DataSource = this.wRRDetailBindingSource;
            this.gridControlWRRDetail.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gridControlWRRDetail.Location = new System.Drawing.Point(12, 12);
            this.gridControlWRRDetail.MainView = this.gridViewWRRDetail;
            this.gridControlWRRDetail.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gridControlWRRDetail.Name = "gridControlWRRDetail";
            this.gridControlWRRDetail.Size = new System.Drawing.Size(1216, 429);
            this.gridControlWRRDetail.TabIndex = 5;
            this.gridControlWRRDetail.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewWRRDetail});
            // 
            // gridViewWRRDetail
            // 
            this.gridViewWRRDetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colNo,
            this.colWRRNumber,
            this.colOrdinal,
            this.colRFIDTagID,
            this.colBarcode,
            this.colStyleColorSize,
            this.colStyle,
            this.colColor,
            this.colSku,
            this.colSize,
            this.colQuantity,
            this.colGender,
            this.colProductGroup,
            this.colDescriptionVN,
            this.colLifeStylePerformance,
            this.colDiv,
            this.colOutsole,
            this.colCategory,
            this.Productline,
            this.colDescription,
            this.colEXWorkUSD,
            this.colEXWorkVND,
            this.colAmountVND,
            this.colRetailPrice,
            this.colSeason,
            this.colCoo,
            this.colMaterial,
            this.colInvoiceNo,
            this.colDateOfInvoice,
            this.colSaleContract,
            this.colShipment,
            this.colFactoryName,
            this.colFactoryAdress,
            this.colFactoryNameVN,
            this.colFactoryAdressVN,
            this.colNewPrice,
            this.colChangePrice,
            this.colStatus});
            this.gridViewWRRDetail.DetailHeight = 284;
            this.gridViewWRRDetail.GridControl = this.gridControlWRRDetail;
            this.gridViewWRRDetail.Name = "gridViewWRRDetail";
            this.gridViewWRRDetail.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.True;
            this.gridViewWRRDetail.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.Click;
            this.gridViewWRRDetail.OptionsCustomization.AllowSort = false;
            this.gridViewWRRDetail.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.gridViewWRRDetail.OptionsView.RowAutoHeight = true;
            this.gridViewWRRDetail.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridViewWRRDetail_CustomDrawCell);
            // 
            // colNo
            // 
            this.colNo.Caption = "No.";
            this.colNo.FieldName = "No";
            this.colNo.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colNo.Name = "colNo";
            this.colNo.Visible = true;
            this.colNo.VisibleIndex = 0;
            this.colNo.Width = 43;
            // 
            // colWRRNumber
            // 
            this.colWRRNumber.AppearanceHeader.Options.UseTextOptions = true;
            this.colWRRNumber.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.colWRRNumber.FieldName = "WRRNumber";
            this.colWRRNumber.MinWidth = 21;
            this.colWRRNumber.Name = "colWRRNumber";
            this.colWRRNumber.Width = 81;
            // 
            // colOrdinal
            // 
            this.colOrdinal.FieldName = "Ordinal";
            this.colOrdinal.MinWidth = 21;
            this.colOrdinal.Name = "colOrdinal";
            this.colOrdinal.Width = 81;
            // 
            // colRFIDTagID
            // 
            this.colRFIDTagID.Caption = "RFID TagID";
            this.colRFIDTagID.FieldName = "RFIDTagID";
            this.colRFIDTagID.MinWidth = 171;
            this.colRFIDTagID.Name = "colRFIDTagID";
            this.colRFIDTagID.Width = 171;
            // 
            // colBarcode
            // 
            this.colBarcode.Caption = "Barcode";
            this.colBarcode.FieldName = "Barcode";
            this.colBarcode.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colBarcode.MinWidth = 80;
            this.colBarcode.Name = "colBarcode";
            this.colBarcode.Visible = true;
            this.colBarcode.VisibleIndex = 1;
            this.colBarcode.Width = 80;
            // 
            // colStyleColorSize
            // 
            this.colStyleColorSize.Caption = "Style Color Size";
            this.colStyleColorSize.FieldName = "StyleColorSize";
            this.colStyleColorSize.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colStyleColorSize.MinWidth = 100;
            this.colStyleColorSize.Name = "colStyleColorSize";
            this.colStyleColorSize.Visible = true;
            this.colStyleColorSize.VisibleIndex = 3;
            this.colStyleColorSize.Width = 100;
            // 
            // colStyle
            // 
            this.colStyle.Caption = "Style";
            this.colStyle.FieldName = "Style";
            this.colStyle.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colStyle.MinWidth = 80;
            this.colStyle.Name = "colStyle";
            this.colStyle.Visible = true;
            this.colStyle.VisibleIndex = 2;
            this.colStyle.Width = 80;
            // 
            // colColor
            // 
            this.colColor.Caption = "Color";
            this.colColor.FieldName = "Color";
            this.colColor.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colColor.MinWidth = 80;
            this.colColor.Name = "colColor";
            this.colColor.Visible = true;
            this.colColor.VisibleIndex = 4;
            this.colColor.Width = 80;
            // 
            // colSku
            // 
            this.colSku.Caption = "Sku";
            this.colSku.FieldName = "Sku";
            this.colSku.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colSku.MinWidth = 80;
            this.colSku.Name = "colSku";
            this.colSku.Visible = true;
            this.colSku.VisibleIndex = 5;
            this.colSku.Width = 80;
            // 
            // colSize
            // 
            this.colSize.Caption = "Size";
            this.colSize.FieldName = "Size";
            this.colSize.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colSize.MinWidth = 50;
            this.colSize.Name = "colSize";
            this.colSize.Visible = true;
            this.colSize.VisibleIndex = 6;
            this.colSize.Width = 64;
            // 
            // colQuantity
            // 
            this.colQuantity.Caption = "Quantity";
            this.colQuantity.FieldName = "Quantity";
            this.colQuantity.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colQuantity.MinWidth = 50;
            this.colQuantity.Name = "colQuantity";
            this.colQuantity.Visible = true;
            this.colQuantity.VisibleIndex = 7;
            this.colQuantity.Width = 64;
            // 
            // colGender
            // 
            this.colGender.Caption = "Gender";
            this.colGender.FieldName = "Gender";
            this.colGender.MinWidth = 80;
            this.colGender.Name = "colGender";
            this.colGender.Visible = true;
            this.colGender.VisibleIndex = 8;
            this.colGender.Width = 80;
            // 
            // colProductGroup
            // 
            this.colProductGroup.Caption = "ProductGroup";
            this.colProductGroup.FieldName = "ProductGroup";
            this.colProductGroup.MinWidth = 80;
            this.colProductGroup.Name = "colProductGroup";
            this.colProductGroup.Visible = true;
            this.colProductGroup.VisibleIndex = 9;
            this.colProductGroup.Width = 80;
            // 
            // colDescriptionVN
            // 
            this.colDescriptionVN.Caption = "Mô tả hành hóa tiếng Việt";
            this.colDescriptionVN.FieldName = "DescriptionVN";
            this.colDescriptionVN.MinWidth = 100;
            this.colDescriptionVN.Name = "colDescriptionVN";
            this.colDescriptionVN.Visible = true;
            this.colDescriptionVN.VisibleIndex = 10;
            this.colDescriptionVN.Width = 150;
            // 
            // colLifeStylePerformance
            // 
            this.colLifeStylePerformance.Caption = "LifeStyle/Performance";
            this.colLifeStylePerformance.FieldName = "LifeStylePerformance";
            this.colLifeStylePerformance.MinWidth = 100;
            this.colLifeStylePerformance.Name = "colLifeStylePerformance";
            this.colLifeStylePerformance.Visible = true;
            this.colLifeStylePerformance.VisibleIndex = 11;
            this.colLifeStylePerformance.Width = 100;
            // 
            // colDiv
            // 
            this.colDiv.Caption = "Div";
            this.colDiv.FieldName = "Div";
            this.colDiv.MinWidth = 100;
            this.colDiv.Name = "colDiv";
            this.colDiv.Visible = true;
            this.colDiv.VisibleIndex = 12;
            this.colDiv.Width = 100;
            // 
            // colOutsole
            // 
            this.colOutsole.Caption = "Outsole";
            this.colOutsole.FieldName = "Outsole";
            this.colOutsole.MinWidth = 100;
            this.colOutsole.Name = "colOutsole";
            this.colOutsole.Visible = true;
            this.colOutsole.VisibleIndex = 13;
            this.colOutsole.Width = 100;
            // 
            // colCategory
            // 
            this.colCategory.Caption = "Category";
            this.colCategory.FieldName = "Category";
            this.colCategory.MinWidth = 100;
            this.colCategory.Name = "colCategory";
            this.colCategory.Visible = true;
            this.colCategory.VisibleIndex = 14;
            this.colCategory.Width = 100;
            // 
            // Productline
            // 
            this.Productline.Caption = "Product Line";
            this.Productline.FieldName = "Productline";
            this.Productline.MinWidth = 100;
            this.Productline.Name = "Productline";
            this.Productline.Visible = true;
            this.Productline.VisibleIndex = 15;
            this.Productline.Width = 100;
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Description";
            this.colDescription.FieldName = "Description";
            this.colDescription.MinWidth = 100;
            this.colDescription.Name = "colDescription";
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 16;
            this.colDescription.Width = 100;
            // 
            // colEXWorkUSD
            // 
            this.colEXWorkUSD.Caption = "EX-Work(USD)";
            this.colEXWorkUSD.FieldName = "EXWorkUSD";
            this.colEXWorkUSD.MinWidth = 100;
            this.colEXWorkUSD.Name = "colEXWorkUSD";
            this.colEXWorkUSD.Visible = true;
            this.colEXWorkUSD.VisibleIndex = 17;
            this.colEXWorkUSD.Width = 100;
            // 
            // colEXWorkVND
            // 
            this.colEXWorkVND.Caption = "EX-Work(VND)";
            this.colEXWorkVND.FieldName = "EXWorkVND";
            this.colEXWorkVND.MinWidth = 100;
            this.colEXWorkVND.Name = "colEXWorkVND";
            this.colEXWorkVND.Visible = true;
            this.colEXWorkVND.VisibleIndex = 18;
            this.colEXWorkVND.Width = 100;
            // 
            // colAmountVND
            // 
            this.colAmountVND.Caption = "Amount(VND)";
            this.colAmountVND.FieldName = "AmountVND";
            this.colAmountVND.MinWidth = 100;
            this.colAmountVND.Name = "colAmountVND";
            this.colAmountVND.Visible = true;
            this.colAmountVND.VisibleIndex = 19;
            this.colAmountVND.Width = 100;
            // 
            // colRetailPrice
            // 
            this.colRetailPrice.Caption = "Retail Price";
            this.colRetailPrice.FieldName = "RetailPrice";
            this.colRetailPrice.MinWidth = 100;
            this.colRetailPrice.Name = "colRetailPrice";
            this.colRetailPrice.Visible = true;
            this.colRetailPrice.VisibleIndex = 20;
            this.colRetailPrice.Width = 100;
            // 
            // colSeason
            // 
            this.colSeason.Caption = "Season";
            this.colSeason.FieldName = "Season";
            this.colSeason.MinWidth = 100;
            this.colSeason.Name = "colSeason";
            this.colSeason.Visible = true;
            this.colSeason.VisibleIndex = 21;
            this.colSeason.Width = 100;
            // 
            // colCoo
            // 
            this.colCoo.Caption = "Coo";
            this.colCoo.FieldName = "Coo";
            this.colCoo.MinWidth = 100;
            this.colCoo.Name = "colCoo";
            this.colCoo.Visible = true;
            this.colCoo.VisibleIndex = 22;
            this.colCoo.Width = 100;
            // 
            // colMaterial
            // 
            this.colMaterial.Caption = "Material";
            this.colMaterial.FieldName = "Material";
            this.colMaterial.MinWidth = 200;
            this.colMaterial.Name = "colMaterial";
            this.colMaterial.Visible = true;
            this.colMaterial.VisibleIndex = 23;
            this.colMaterial.Width = 200;
            // 
            // colInvoiceNo
            // 
            this.colInvoiceNo.Caption = "Invoice No";
            this.colInvoiceNo.FieldName = "InvoiceNo";
            this.colInvoiceNo.MinWidth = 100;
            this.colInvoiceNo.Name = "colInvoiceNo";
            this.colInvoiceNo.Visible = true;
            this.colInvoiceNo.VisibleIndex = 24;
            this.colInvoiceNo.Width = 100;
            // 
            // colDateOfInvoice
            // 
            this.colDateOfInvoice.Caption = "Date Of Invoice";
            this.colDateOfInvoice.FieldName = "DateOfInvoice";
            this.colDateOfInvoice.MinWidth = 100;
            this.colDateOfInvoice.Name = "colDateOfInvoice";
            this.colDateOfInvoice.Visible = true;
            this.colDateOfInvoice.VisibleIndex = 25;
            this.colDateOfInvoice.Width = 100;
            // 
            // colSaleContract
            // 
            this.colSaleContract.Caption = "Sale Contract";
            this.colSaleContract.FieldName = "SaleContract";
            this.colSaleContract.MinWidth = 100;
            this.colSaleContract.Name = "colSaleContract";
            this.colSaleContract.Visible = true;
            this.colSaleContract.VisibleIndex = 26;
            this.colSaleContract.Width = 100;
            // 
            // colShipment
            // 
            this.colShipment.Caption = "Shipment";
            this.colShipment.FieldName = "Shipment";
            this.colShipment.MinWidth = 100;
            this.colShipment.Name = "colShipment";
            this.colShipment.Visible = true;
            this.colShipment.VisibleIndex = 27;
            this.colShipment.Width = 100;
            // 
            // colFactoryName
            // 
            this.colFactoryName.Caption = "Factory Name";
            this.colFactoryName.FieldName = "FactoryName";
            this.colFactoryName.MinWidth = 100;
            this.colFactoryName.Name = "colFactoryName";
            this.colFactoryName.Visible = true;
            this.colFactoryName.VisibleIndex = 28;
            this.colFactoryName.Width = 100;
            // 
            // colFactoryAdress
            // 
            this.colFactoryAdress.Caption = "Factory Adress";
            this.colFactoryAdress.FieldName = "FactoryAdress";
            this.colFactoryAdress.MinWidth = 100;
            this.colFactoryAdress.Name = "colFactoryAdress";
            this.colFactoryAdress.Visible = true;
            this.colFactoryAdress.VisibleIndex = 29;
            this.colFactoryAdress.Width = 100;
            // 
            // colFactoryNameVN
            // 
            this.colFactoryNameVN.Caption = "Tên công ty tiếng Việt";
            this.colFactoryNameVN.FieldName = "FactoryNameVN";
            this.colFactoryNameVN.MinWidth = 100;
            this.colFactoryNameVN.Name = "colFactoryNameVN";
            this.colFactoryNameVN.Visible = true;
            this.colFactoryNameVN.VisibleIndex = 30;
            this.colFactoryNameVN.Width = 100;
            // 
            // colFactoryAdressVN
            // 
            this.colFactoryAdressVN.Caption = "Địa chỉ";
            this.colFactoryAdressVN.FieldName = "FactoryAdressVN";
            this.colFactoryAdressVN.MinWidth = 100;
            this.colFactoryAdressVN.Name = "colFactoryAdressVN";
            this.colFactoryAdressVN.Visible = true;
            this.colFactoryAdressVN.VisibleIndex = 31;
            this.colFactoryAdressVN.Width = 100;
            // 
            // colNewPrice
            // 
            this.colNewPrice.Caption = "New Price";
            this.colNewPrice.FieldName = "NewPrice";
            this.colNewPrice.MinWidth = 100;
            this.colNewPrice.Name = "colNewPrice";
            this.colNewPrice.Visible = true;
            this.colNewPrice.VisibleIndex = 32;
            this.colNewPrice.Width = 100;
            // 
            // colChangePrice
            // 
            this.colChangePrice.Caption = "Change Price";
            this.colChangePrice.FieldName = "ChangePrice";
            this.colChangePrice.MinWidth = 100;
            this.colChangePrice.Name = "colChangePrice";
            this.colChangePrice.Visible = true;
            this.colChangePrice.VisibleIndex = 33;
            this.colChangePrice.Width = 100;
            // 
            // colStatus
            // 
            this.colStatus.Caption = "Encoded";
            this.colStatus.FieldName = "Status";
            this.colStatus.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colStatus.MinWidth = 100;
            this.colStatus.Name = "colStatus";
            this.colStatus.Visible = true;
            this.colStatus.VisibleIndex = 34;
            this.colStatus.Width = 100;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1240, 453);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.gridControlWRRDetail;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(1220, 433);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // ReceiptRequisitionControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl3);
            this.Controls.Add(this.layoutControl2);
            this.Controls.Add(this.layoutControl1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "ReceiptRequisitionControl";
            this.Size = new System.Drawing.Size(1240, 646);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEditWRRNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditWRRDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditWRRDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalQuantityByItem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalQuantityByPack.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditHandlingStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditBranch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditReaded.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditEncoded.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTagIDs.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWRRNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalQuantityByItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalQuantityByPack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemBranch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWRRDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemHandlingStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wRRDetailBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wARHOUSE_HPDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlWRRDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewWRRDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel windowsUIButtonPanel1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraEditors.TextEdit textEditWRRNumber;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemWRRNumber;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.DateEdit dateEditWRRDate;
        private DevExpress.XtraEditors.TextEdit textEditTotalQuantityByItem;
        private DevExpress.XtraEditors.TextEdit textEditTotalQuantityByPack;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTotalQuantityByItem;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTotalQuantityByPack;
        private System.Windows.Forms.BindingSource wRRDetailBindingSource;
        private WARHOUSE_HPDataSet1 wARHOUSE_HPDataSet1;
        private WARHOUSE_HPDataSet1TableAdapters.WRRDetailTableAdapter wRRDetailTableAdapter;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditHandlingStatus;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraGrid.GridControl gridControlWRRDetail;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewWRRDetail;
        private DevExpress.XtraGrid.Columns.GridColumn colWRRNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colOrdinal;
        private DevExpress.XtraGrid.Columns.GridColumn colNo;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditBranch;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemBranch;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemWRRDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemHandlingStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colRFIDTagID;
        private DevExpress.XtraGrid.Columns.GridColumn colBarcode;
        private DevExpress.XtraGrid.Columns.GridColumn colStyleColorSize;
        private DevExpress.XtraGrid.Columns.GridColumn colStyle;
        private DevExpress.XtraGrid.Columns.GridColumn colColor;
        private DevExpress.XtraGrid.Columns.GridColumn colSku;
        private DevExpress.XtraGrid.Columns.GridColumn colSize;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colGender;
        private DevExpress.XtraGrid.Columns.GridColumn colProductGroup;
        private DevExpress.XtraGrid.Columns.GridColumn colDescriptionVN;
        private DevExpress.XtraGrid.Columns.GridColumn colLifeStylePerformance;
        private DevExpress.XtraGrid.Columns.GridColumn colDiv;
        private DevExpress.XtraGrid.Columns.GridColumn colOutsole;
        private DevExpress.XtraGrid.Columns.GridColumn colCategory;
        private DevExpress.XtraGrid.Columns.GridColumn Productline;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colEXWorkUSD;
        private DevExpress.XtraGrid.Columns.GridColumn colEXWorkVND;
        private DevExpress.XtraGrid.Columns.GridColumn colAmountVND;
        private DevExpress.XtraGrid.Columns.GridColumn colRetailPrice;
        private DevExpress.XtraGrid.Columns.GridColumn colSeason;
        private DevExpress.XtraGrid.Columns.GridColumn colCoo;
        private DevExpress.XtraGrid.Columns.GridColumn colMaterial;
        private DevExpress.XtraGrid.Columns.GridColumn colInvoiceNo;
        private DevExpress.XtraGrid.Columns.GridColumn colDateOfInvoice;
        private DevExpress.XtraGrid.Columns.GridColumn colSaleContract;
        private DevExpress.XtraGrid.Columns.GridColumn colShipment;
        private DevExpress.XtraGrid.Columns.GridColumn colFactoryName;
        private DevExpress.XtraGrid.Columns.GridColumn colFactoryAdress;
        private DevExpress.XtraGrid.Columns.GridColumn colFactoryNameVN;
        private DevExpress.XtraGrid.Columns.GridColumn colFactoryAdressVN;
        private DevExpress.XtraGrid.Columns.GridColumn colNewPrice;
        private DevExpress.XtraGrid.Columns.GridColumn colChangePrice;
        private DevExpress.XtraEditors.SimpleButton simpleButtonRead;
        private DevExpress.XtraEditors.SimpleButton simpleButtonWriteTag;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.TextEdit textEditReaded;
        private DevExpress.XtraEditors.TextEdit textEditEncoded;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraEditors.MemoEdit textEditTagIDs;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraEditors.SimpleButton simpleButtonStart;
        private DevExpress.XtraEditors.SimpleButton simpleButtonStop;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
    }
}
