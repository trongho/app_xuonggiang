﻿
namespace WarehouseManager
{
    partial class ReadForm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition1 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition2 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition3 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition4 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition5 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition1 = new DevExpress.XtraLayout.RowDefinition();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.simpleButtonRead = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonClearTag = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonSaveDatabase = new DevExpress.XtraEditors.SimpleButton();
            this.textEditTotalTag = new DevExpress.XtraEditors.TextEdit();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleLabelItem1 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.inventoryList = new System.Windows.Forms.ListView();
            this.EPCID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Antenna = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.TagCount = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.RSSI = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.PCBits = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.MemoryBankData = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.MB = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Offset = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.GoodsName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItemTagData = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemReadTag = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemWriteTag = new System.Windows.Forms.ToolStripMenuItem();
            this.TagEvent = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalTag.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.simpleButtonRead);
            this.layoutControl1.Controls.Add(this.simpleButtonClearTag);
            this.layoutControl1.Controls.Add(this.simpleButtonSaveDatabase);
            this.layoutControl1.Controls.Add(this.textEditTotalTag);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(508, 182, 650, 400);
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(1310, 52);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // simpleButtonRead
            // 
            this.simpleButtonRead.Enabled = false;
            this.simpleButtonRead.Location = new System.Drawing.Point(12, 12);
            this.simpleButtonRead.Name = "simpleButtonRead";
            this.simpleButtonRead.Size = new System.Drawing.Size(383, 22);
            this.simpleButtonRead.StyleController = this.layoutControl1;
            this.simpleButtonRead.TabIndex = 4;
            this.simpleButtonRead.Text = "Start Reading";
            // 
            // simpleButtonClearTag
            // 
            this.simpleButtonClearTag.Location = new System.Drawing.Point(915, 12);
            this.simpleButtonClearTag.Name = "simpleButtonClearTag";
            this.simpleButtonClearTag.Size = new System.Drawing.Size(189, 22);
            this.simpleButtonClearTag.StyleController = this.layoutControl1;
            this.simpleButtonClearTag.TabIndex = 6;
            this.simpleButtonClearTag.Text = "Clear Tags";
            // 
            // simpleButtonSaveDatabase
            // 
            this.simpleButtonSaveDatabase.Location = new System.Drawing.Point(1108, 12);
            this.simpleButtonSaveDatabase.Name = "simpleButtonSaveDatabase";
            this.simpleButtonSaveDatabase.Size = new System.Drawing.Size(190, 22);
            this.simpleButtonSaveDatabase.StyleController = this.layoutControl1;
            this.simpleButtonSaveDatabase.TabIndex = 7;
            this.simpleButtonSaveDatabase.Text = "Save To Database";
            // 
            // textEditTotalTag
            // 
            this.textEditTotalTag.Location = new System.Drawing.Point(528, 12);
            this.textEditTotalTag.Name = "textEditTotalTag";
            this.textEditTotalTag.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.textEditTotalTag.Properties.Appearance.Options.UseFont = true;
            this.textEditTotalTag.Size = new System.Drawing.Size(383, 26);
            this.textEditTotalTag.StyleController = this.layoutControl1;
            this.textEditTotalTag.TabIndex = 8;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem3,
            this.layoutControlItem5,
            this.simpleLabelItem1,
            this.layoutControlItem2});
            this.Root.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.Root.Name = "Root";
            columnDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition1.Width = 30D;
            columnDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition2.Width = 10D;
            columnDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition3.Width = 30D;
            columnDefinition4.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition4.Width = 15D;
            columnDefinition5.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition5.Width = 15D;
            this.Root.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition1,
            columnDefinition2,
            columnDefinition3,
            columnDefinition4,
            columnDefinition5});
            rowDefinition1.Height = 100D;
            rowDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            this.Root.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition1});
            this.Root.Size = new System.Drawing.Size(1310, 52);
            this.Root.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.simpleButtonRead;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(387, 32);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.simpleButtonClearTag;
            this.layoutControlItem3.Location = new System.Drawing.Point(903, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.OptionsTableLayoutItem.ColumnIndex = 3;
            this.layoutControlItem3.Size = new System.Drawing.Size(193, 32);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.simpleButtonSaveDatabase;
            this.layoutControlItem5.Location = new System.Drawing.Point(1096, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItem5.Size = new System.Drawing.Size(194, 32);
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // simpleLabelItem1
            // 
            this.simpleLabelItem1.AllowHotTrack = false;
            this.simpleLabelItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.simpleLabelItem1.Location = new System.Drawing.Point(387, 0);
            this.simpleLabelItem1.Name = "simpleLabelItem1";
            this.simpleLabelItem1.OptionsTableLayoutItem.ColumnIndex = 1;
            this.simpleLabelItem1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.simpleLabelItem1.Size = new System.Drawing.Size(129, 32);
            this.simpleLabelItem1.Text = "Total Tag: ";
            this.simpleLabelItem1.TextSize = new System.Drawing.Size(52, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.textEditTotalTag;
            this.layoutControlItem2.Location = new System.Drawing.Point(516, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItem2.Size = new System.Drawing.Size(387, 32);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.inventoryList);
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 52);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(519, 146, 650, 400);
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(1310, 567);
            this.dataLayoutControl1.TabIndex = 1;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // inventoryList
            // 
            this.inventoryList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.EPCID,
            this.Antenna,
            this.TagCount,
            this.RSSI,
            this.PCBits,
            this.MemoryBankData,
            this.MB,
            this.Offset,
            this.GoodsName,
            this.TagEvent});
            this.inventoryList.FullRowSelect = true;
            this.inventoryList.GridLines = true;
            this.inventoryList.HideSelection = false;
            this.inventoryList.HoverSelection = true;
            this.inventoryList.Location = new System.Drawing.Point(12, 12);
            this.inventoryList.MultiSelect = false;
            this.inventoryList.Name = "inventoryList";
            this.inventoryList.Size = new System.Drawing.Size(1286, 543);
            this.inventoryList.TabIndex = 4;
            this.inventoryList.UseCompatibleStateImageBehavior = false;
            this.inventoryList.View = System.Windows.Forms.View.Details;
            this.inventoryList.MouseClick += new System.Windows.Forms.MouseEventHandler(this.inventoryList_MouseClick);
            // 
            // EPCID
            // 
            this.EPCID.Text = "EPC ID";
            this.EPCID.Width = 160;
            // 
            // Antenna
            // 
            this.Antenna.Text = "Antenna";
            this.Antenna.Width = 120;
            // 
            // TagCount
            // 
            this.TagCount.Text = "Tag Count";
            this.TagCount.Width = 120;
            // 
            // RSSI
            // 
            this.RSSI.Text = "RSSI";
            this.RSSI.Width = 120;
            // 
            // PCBits
            // 
            this.PCBits.Text = "PC Bits";
            this.PCBits.Width = 120;
            // 
            // MemoryBankData
            // 
            this.MemoryBankData.Text = "Memory Bank Data";
            this.MemoryBankData.Width = 120;
            // 
            // MB
            // 
            this.MB.Text = "MB";
            this.MB.Width = 120;
            // 
            // Offset
            // 
            this.Offset.Text = "Offset";
            this.Offset.Width = 120;
            // 
            // GoodsName
            // 
            this.GoodsName.Text = "Goods Name";
            this.GoodsName.Width = 100;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1310, 567);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.inventoryList;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(1290, 547);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemTagData,
            this.toolStripMenuItemReadTag,
            this.toolStripMenuItemWriteTag});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(124, 70);
            // 
            // toolStripMenuItemTagData
            // 
            this.toolStripMenuItemTagData.Name = "toolStripMenuItemTagData";
            this.toolStripMenuItemTagData.Size = new System.Drawing.Size(123, 22);
            this.toolStripMenuItemTagData.Text = "Tag Data";
            this.toolStripMenuItemTagData.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // toolStripMenuItemReadTag
            // 
            this.toolStripMenuItemReadTag.Name = "toolStripMenuItemReadTag";
            this.toolStripMenuItemReadTag.Size = new System.Drawing.Size(123, 22);
            this.toolStripMenuItemReadTag.Text = "Read Tag";
            this.toolStripMenuItemReadTag.Click += new System.EventHandler(this.toolStripMenuItemReadTag_Click);
            // 
            // toolStripMenuItemWriteTag
            // 
            this.toolStripMenuItemWriteTag.Name = "toolStripMenuItemWriteTag";
            this.toolStripMenuItemWriteTag.Size = new System.Drawing.Size(123, 22);
            this.toolStripMenuItemWriteTag.Text = "Write Tag";
            this.toolStripMenuItemWriteTag.Click += new System.EventHandler(this.toolStripMenuItemWriteTag_Click);
            // 
            // TagEvent
            // 
            this.TagEvent.Text = "Tag Event";
            // 
            // ReadForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1310, 619);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.layoutControl1);
            this.Name = "ReadForm";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalTag.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        public DevExpress.XtraEditors.SimpleButton simpleButtonRead;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        public System.Windows.Forms.ListView inventoryList;
        private System.Windows.Forms.ColumnHeader EPCID;
        private System.Windows.Forms.ColumnHeader Antenna;
        private System.Windows.Forms.ColumnHeader TagCount;
        private System.Windows.Forms.ColumnHeader RSSI;
        private System.Windows.Forms.ColumnHeader PCBits;
        private System.Windows.Forms.ColumnHeader MemoryBankData;
        private System.Windows.Forms.ColumnHeader MB;
        private System.Windows.Forms.ColumnHeader Offset;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private DevExpress.XtraEditors.SimpleButton simpleButtonClearTag;
        private DevExpress.XtraEditors.SimpleButton simpleButtonSaveDatabase;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        public DevExpress.XtraEditors.TextEdit textEditTotalTag;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private System.Windows.Forms.ColumnHeader GoodsName;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemTagData;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemReadTag;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemWriteTag;
        private System.Windows.Forms.ColumnHeader TagEvent;
    }
}
