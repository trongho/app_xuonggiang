﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webservice.Entities
{
    public class WRRDetail
    {
        public string WRRNumber { get; set; }
        public int Ordinal { get; set; }
        public string RFIDTagID { get; set; }
        public string Barcode { get; set; }
        public string StyleColorSize { get; set; }
        public string Style { get; set; }
        public string Color { get; set; }
        public string Sku { get; set; }
        public int? Size { get; set; }
        public int? Quantity { get; set; }
        public string Gender { get; set; }
        public string ProductGroup { get; set; }
        public string DescriptionVN { get; set; }
        public string LifeStylePerformance { get; set; }
        public string Div { get; set; }
        public string Outsole { get; set; }
        public string Category { get; set; }
        public string Productline { get; set; }
        public string Description { get; set; }
        public decimal? EXWorkUSD { get; set; }
        public decimal? EXWorkVND { get; set; }
        public decimal? AmountVND { get; set; }
        public decimal? RetailPrice { get; set; }
        public string Season { get; set; }
        public string Coo { get; set; }
        public string Material { get; set; }
        public string InvoiceNo { get; set; }
        public DateTime? DateOfInvoice { get; set; }
        public string SaleContract { get; set; }
        public string Shipment { get; set; }
        public string FactoryAdress { get; set; }
        public string FactoryName { get; set; }
        public string FactoryNameVN { get; set; }
        public string FactoryAdressVN { get; set; }
        public decimal? NewPrice { get; set; }
        public decimal? ChangePrice { get; set; }
        public string Status { get; set; }
    }
}
