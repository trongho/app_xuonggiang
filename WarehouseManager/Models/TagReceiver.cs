﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarehouseManager.Models
{
    class TagReceiver
    {
        public string TagID { get; set; }
        public string TagEvent { get; set; }
    }
}
