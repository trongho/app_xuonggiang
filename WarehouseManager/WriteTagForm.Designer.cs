﻿
namespace WarehouseManager
{
    partial class WriteTagForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraLayout.ColumnDefinition columnDefinition1 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition2 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition1 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition2 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition3 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition4 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition5 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition6 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition7 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition8 = new DevExpress.XtraLayout.RowDefinition();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.textEditTagID = new DevExpress.XtraEditors.TextEdit();
            this.textEditPassword = new DevExpress.XtraEditors.TextEdit();
            this.comboBoxEditMemoryBank = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEditOffset = new DevExpress.XtraEditors.TextEdit();
            this.textEditNoOfBytesLenght = new DevExpress.XtraEditors.TextEdit();
            this.textEditDataRead = new DevExpress.XtraEditors.MemoEdit();
            this.simpleButtonWriteTag = new DevExpress.XtraEditors.SimpleButton();
            this.textEditBarcode = new DevExpress.XtraEditors.TextEdit();
            this.textEditTagEvent = new DevExpress.XtraEditors.TextEdit();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.Root1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTagID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPassword.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditMemoryBank.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditOffset.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditNoOfBytesLenght.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDataRead.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditBarcode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTagEvent.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.textEditTagID);
            this.layoutControl1.Controls.Add(this.textEditPassword);
            this.layoutControl1.Controls.Add(this.comboBoxEditMemoryBank);
            this.layoutControl1.Controls.Add(this.textEditOffset);
            this.layoutControl1.Controls.Add(this.textEditNoOfBytesLenght);
            this.layoutControl1.Controls.Add(this.textEditDataRead);
            this.layoutControl1.Controls.Add(this.simpleButtonWriteTag);
            this.layoutControl1.Controls.Add(this.textEditBarcode);
            this.layoutControl1.Controls.Add(this.textEditTagEvent);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(555, 148, 650, 400);
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(501, 390);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // textEditTagID
            // 
            this.textEditTagID.Location = new System.Drawing.Point(123, 22);
            this.textEditTagID.Name = "textEditTagID";
            this.textEditTagID.Size = new System.Drawing.Size(356, 20);
            this.textEditTagID.StyleController = this.layoutControl1;
            this.textEditTagID.TabIndex = 4;
            // 
            // textEditPassword
            // 
            this.textEditPassword.Location = new System.Drawing.Point(123, 55);
            this.textEditPassword.Name = "textEditPassword";
            this.textEditPassword.Size = new System.Drawing.Size(356, 20);
            this.textEditPassword.StyleController = this.layoutControl1;
            this.textEditPassword.TabIndex = 5;
            // 
            // comboBoxEditMemoryBank
            // 
            this.comboBoxEditMemoryBank.Location = new System.Drawing.Point(123, 89);
            this.comboBoxEditMemoryBank.Name = "comboBoxEditMemoryBank";
            this.comboBoxEditMemoryBank.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditMemoryBank.Properties.Items.AddRange(new object[] {
            "RESERVED",
            "EPC",
            "TID",
            "USER"});
            this.comboBoxEditMemoryBank.Size = new System.Drawing.Size(356, 20);
            this.comboBoxEditMemoryBank.StyleController = this.layoutControl1;
            this.comboBoxEditMemoryBank.TabIndex = 6;
            // 
            // textEditOffset
            // 
            this.textEditOffset.Location = new System.Drawing.Point(123, 123);
            this.textEditOffset.Name = "textEditOffset";
            this.textEditOffset.Size = new System.Drawing.Size(356, 20);
            this.textEditOffset.StyleController = this.layoutControl1;
            this.textEditOffset.TabIndex = 7;
            // 
            // textEditNoOfBytesLenght
            // 
            this.textEditNoOfBytesLenght.Location = new System.Drawing.Point(123, 157);
            this.textEditNoOfBytesLenght.Name = "textEditNoOfBytesLenght";
            this.textEditNoOfBytesLenght.Size = new System.Drawing.Size(356, 20);
            this.textEditNoOfBytesLenght.StyleController = this.layoutControl1;
            this.textEditNoOfBytesLenght.TabIndex = 8;
            // 
            // textEditDataRead
            // 
            this.textEditDataRead.Location = new System.Drawing.Point(123, 226);
            this.textEditDataRead.Name = "textEditDataRead";
            this.textEditDataRead.Properties.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.textEditDataRead.Size = new System.Drawing.Size(356, 94);
            this.textEditDataRead.StyleController = this.layoutControl1;
            this.textEditDataRead.TabIndex = 9;
            // 
            // simpleButtonWriteTag
            // 
            this.simpleButtonWriteTag.Location = new System.Drawing.Point(252, 324);
            this.simpleButtonWriteTag.Name = "simpleButtonWriteTag";
            this.simpleButtonWriteTag.Size = new System.Drawing.Size(227, 22);
            this.simpleButtonWriteTag.StyleController = this.layoutControl1;
            this.simpleButtonWriteTag.TabIndex = 10;
            this.simpleButtonWriteTag.Text = "Write Tag";
            // 
            // textEditBarcode
            // 
            this.textEditBarcode.Location = new System.Drawing.Point(123, 191);
            this.textEditBarcode.Name = "textEditBarcode";
            this.textEditBarcode.Size = new System.Drawing.Size(125, 20);
            this.textEditBarcode.StyleController = this.layoutControl1;
            this.textEditBarcode.TabIndex = 11;
            // 
            // textEditTagEvent
            // 
            this.textEditTagEvent.Location = new System.Drawing.Point(353, 191);
            this.textEditTagEvent.Name = "textEditTagEvent";
            this.textEditTagEvent.Size = new System.Drawing.Size(126, 20);
            this.textEditTagEvent.StyleController = this.layoutControl1;
            this.textEditTagEvent.TabIndex = 12;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.Root1});
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(501, 390);
            this.Root.TextVisible = false;
            // 
            // Root1
            // 
            this.Root1.CustomizationFormText = "Root";
            this.Root1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root1.GroupBordersVisible = false;
            this.Root1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9});
            this.Root1.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.Root1.Location = new System.Drawing.Point(0, 0);
            this.Root1.Name = "Root1";
            columnDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition1.Width = 50D;
            columnDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition2.Width = 50D;
            this.Root1.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition1,
            columnDefinition2});
            rowDefinition1.Height = 9.589041095890412D;
            rowDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition2.Height = 9.589041095890412D;
            rowDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition3.Height = 9.589041095890412D;
            rowDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition4.Height = 9.589041095890412D;
            rowDefinition4.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition5.Height = 9.589041095890412D;
            rowDefinition5.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition6.Height = 10D;
            rowDefinition6.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition7.Height = 28D;
            rowDefinition7.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition8.Height = 13.698630136986301D;
            rowDefinition8.SizeType = System.Windows.Forms.SizeType.Percent;
            this.Root1.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition1,
            rowDefinition2,
            rowDefinition3,
            rowDefinition4,
            rowDefinition5,
            rowDefinition6,
            rowDefinition7,
            rowDefinition8});
            this.Root1.Size = new System.Drawing.Size(481, 370);
            this.Root1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.Root1.Text = "Root";
            this.Root1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.textEditTagID;
            this.layoutControlItem1.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem1.CustomizationFormText = "Tag ID( Hex)";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.OptionsTableLayoutItem.ColumnSpan = 2;
            this.layoutControlItem1.Size = new System.Drawing.Size(461, 33);
            this.layoutControlItem1.Text = "Tag ID( Hex)";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.textEditPassword;
            this.layoutControlItem2.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem2.CustomizationFormText = "Password( Hex)";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 33);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.OptionsTableLayoutItem.ColumnSpan = 2;
            this.layoutControlItem2.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItem2.Size = new System.Drawing.Size(461, 34);
            this.layoutControlItem2.Text = "Password( Hex)";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.comboBoxEditMemoryBank;
            this.layoutControlItem3.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem3.CustomizationFormText = "Memory Bank";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 67);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.OptionsTableLayoutItem.ColumnSpan = 2;
            this.layoutControlItem3.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItem3.Size = new System.Drawing.Size(461, 34);
            this.layoutControlItem3.Text = "Memory Bank";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.textEditOffset;
            this.layoutControlItem4.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem4.CustomizationFormText = "Offset( Byte)";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 101);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.OptionsTableLayoutItem.ColumnSpan = 2;
            this.layoutControlItem4.OptionsTableLayoutItem.RowIndex = 3;
            this.layoutControlItem4.Size = new System.Drawing.Size(461, 34);
            this.layoutControlItem4.Text = "Offset( Byte)";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.textEditNoOfBytesLenght;
            this.layoutControlItem5.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem5.CustomizationFormText = "No. Of Bytes Lenght";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 135);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.OptionsTableLayoutItem.ColumnSpan = 2;
            this.layoutControlItem5.OptionsTableLayoutItem.RowIndex = 4;
            this.layoutControlItem5.Size = new System.Drawing.Size(461, 34);
            this.layoutControlItem5.Text = "No. Of Bytes Lenght";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.textEditDataRead;
            this.layoutControlItem6.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem6.CustomizationFormText = "Data Read( Hex)";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 204);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.OptionsTableLayoutItem.ColumnSpan = 2;
            this.layoutControlItem6.OptionsTableLayoutItem.RowIndex = 6;
            this.layoutControlItem6.Size = new System.Drawing.Size(461, 98);
            this.layoutControlItem6.Text = "Data Read( Hex)";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.simpleButtonWriteTag;
            this.layoutControlItem7.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(230, 302);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItem7.OptionsTableLayoutItem.RowIndex = 7;
            this.layoutControlItem7.Size = new System.Drawing.Size(231, 48);
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.textEditBarcode;
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 169);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.OptionsTableLayoutItem.RowIndex = 5;
            this.layoutControlItem8.Size = new System.Drawing.Size(230, 35);
            this.layoutControlItem8.Text = "Barcode";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.textEditTagEvent;
            this.layoutControlItem9.Location = new System.Drawing.Point(230, 169);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItem9.OptionsTableLayoutItem.RowIndex = 5;
            this.layoutControlItem9.Size = new System.Drawing.Size(231, 35);
            this.layoutControlItem9.Text = "Tag Event";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(98, 13);
            // 
            // WriteTagForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(501, 390);
            this.Controls.Add(this.layoutControl1);
            this.Name = "WriteTagForm";
            this.Text = "WriteTagForm";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEditTagID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPassword.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditMemoryBank.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditOffset.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditNoOfBytesLenght.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDataRead.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditBarcode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTagEvent.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        public DevExpress.XtraEditors.TextEdit textEditTagID;
        public DevExpress.XtraEditors.TextEdit textEditPassword;
        public DevExpress.XtraEditors.ComboBoxEdit comboBoxEditMemoryBank;
        public DevExpress.XtraEditors.TextEdit textEditOffset;
        public DevExpress.XtraEditors.TextEdit textEditNoOfBytesLenght;
        public DevExpress.XtraEditors.MemoEdit textEditDataRead;
        public DevExpress.XtraEditors.SimpleButton simpleButtonWriteTag;
        private DevExpress.XtraLayout.LayoutControlGroup Root1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraEditors.TextEdit textEditBarcode;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        public DevExpress.XtraEditors.TextEdit textEditTagEvent;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
    }
}