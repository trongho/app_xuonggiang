﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WarehouseManager.Models
{
    public class Warehouse
    {
      public String WarehouseID{get;set;}
      public String? WarehouseName{get;set;}
      public String? Address{get;set;}
      public String? ProvinceID{get;set;}
      public String? DistrictID{get;set;}
      public String? Representative{get;set;}
      public String? Position{get;set;}
      public String? TelNumber{get;set;}
      public String? FaxNumber{get;set;}
      public String? Email{get;set;}
      public String? BranchID{get;set;}
      public String? Description{get;set;}
      public String? Status{get;set;}
      public String? CreatedUserID{get;set;}
      public DateTime? CreatedDate{get;set;}
      public String? UpdatedUserID{get;set;}
      public DateTime? UpdatedDate{get;set;}
    }
}
