﻿
namespace WarehouseManager
{
    partial class TagDataForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tagDataView = new System.Windows.Forms.ListView();
            this.EPCID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.AntennaID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // tagDataView
            // 
            this.tagDataView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.EPCID,
            this.AntennaID});
            this.tagDataView.HideSelection = false;
            this.tagDataView.Location = new System.Drawing.Point(-2, -1);
            this.tagDataView.Name = "tagDataView";
            this.tagDataView.Size = new System.Drawing.Size(504, 504);
            this.tagDataView.TabIndex = 0;
            this.tagDataView.UseCompatibleStateImageBehavior = false;
            this.tagDataView.View = System.Windows.Forms.View.Details;
            // 
            // EPCID
            // 
            this.EPCID.Text = "EPC ID";
            // 
            // AntennaID
            // 
            this.AntennaID.Text = "AntennaID";
            // 
            // TagDataForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(500, 501);
            this.Controls.Add(this.tagDataView);
            this.Name = "TagDataForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TagDataForm";
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.ListView tagDataView;
        private System.Windows.Forms.ColumnHeader EPCID;
        private System.Windows.Forms.ColumnHeader AntennaID;
    }
}