﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WarehouseManager.Helpers
{
    public class ExcelHelper
    {
        public static string SelectedTable = string.Empty;
        public static String selectFileExcel(UserControl userControl)
        {
            String fileName = "";
            OpenFileDialog fdlg = new OpenFileDialog();
            fdlg.Title = "Select file";
            fdlg.InitialDirectory = @"c:\";
            fdlg.FileName = fileName;         
            fdlg.Filter = "Excel Sheet(*.xls)|*.xlsx|All Files(*.*)|*.*";
            fdlg.FilterIndex = 1;
            fdlg.RestoreDirectory = true;
            if (fdlg.ShowDialog() == DialogResult.OK)
            {
                fileName = fdlg.FileName;           
                selectSheet(userControl,fileName);
                Application.DoEvents();
            }

            return fileName;
        }

        private static void selectSheet(UserControl userControl ,String fileName)
        {

            if (fileName.Trim() != string.Empty)
            {
                FileInfo file = new FileInfo(Path.Combine(fileName));
                ExcelPackage.LicenseContext = LicenseContext.Commercial;
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                using (ExcelPackage package = new ExcelPackage(file))
                {
                    string[] strTables = package.Workbook.Worksheets.Select(x => x.Name).ToArray();
                    frmSelectTables objSelectTable = new frmSelectTables(strTables);
                    objSelectTable.ShowDialog(userControl);
                    objSelectTable.Dispose();
                }
            }
        }
    }
}
