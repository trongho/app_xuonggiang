﻿
namespace WarehouseManager
{
    partial class ReceiptDataControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReceiptDataControl));
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions2 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions3 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions4 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions5 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions6 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions7 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions8 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions9 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition1 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition2 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition3 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition4 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition5 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition6 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition7 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition1 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition2 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition3 = new DevExpress.XtraLayout.RowDefinition();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.windowsUIButtonPanel1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.gridControlWRDataDetail = new DevExpress.XtraGrid.GridControl();
            this.wRDataDetailBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.wARHOUSE_HPDataSet1 = new WarehouseManager.WARHOUSE_HPDataSet1();
            this.gridViewWRDataDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBarcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWRDNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrdinal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantityOrg = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStyleColorSize = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStyle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colColor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSku = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSize = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGender = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductGroup = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescriptionVN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLifeStylePerformance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDiv = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOutsole = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductlinen = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEXWorkUSD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEXWorkVND = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAmountVND = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRetailPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSeason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCoo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaterial = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInvoiceNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateOfInvoice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaleContract = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShipment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFactoryAdress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFactoryName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFactoryNameVN1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFactoryAdressVN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNewPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colChangePrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInputDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.textEditTotalQuantity = new DevExpress.XtraEditors.TextEdit();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textEditWRDNumber = new DevExpress.XtraEditors.TextEdit();
            this.dateEditWRDDate = new DevExpress.XtraEditors.DateEdit();
            this.textEdit4 = new DevExpress.XtraEditors.TextEdit();
            this.textEditTotalQuantityOrg = new DevExpress.XtraEditors.TextEdit();
            this.comboBoxEditHandlingStatus = new DevExpress.XtraEditors.ComboBoxEdit();
            this.simpleButtonOn = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonOff = new DevExpress.XtraEditors.SimpleButton();
            this.textEditTotalTag = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemWRDNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemWRDDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemHandlingStatus = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemTotalQuantityOrg = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemTotalQuantity = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.wRDataDetailTableAdapter = new WarehouseManager.WARHOUSE_HPDataSet1TableAdapters.WRDataDetailTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlWRDataDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wRDataDetailBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wARHOUSE_HPDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewWRDataDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalQuantity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditWRDNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditWRDDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditWRDDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalQuantityOrg.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditHandlingStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalTag.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWRDNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWRDDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemHandlingStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalQuantityOrg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.windowsUIButtonPanel1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(684, 208, 812, 495);
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(1298, 37);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // windowsUIButtonPanel1
            // 
            this.windowsUIButtonPanel1.AllowGlyphSkinning = false;
            this.windowsUIButtonPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            windowsUIButtonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions1.Image")));
            windowsUIButtonImageOptions1.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions2.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions2.Image")));
            windowsUIButtonImageOptions2.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions3.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions3.Image")));
            windowsUIButtonImageOptions3.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions4.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions4.Image")));
            windowsUIButtonImageOptions4.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions5.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions5.Image")));
            windowsUIButtonImageOptions5.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions6.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions6.Image")));
            windowsUIButtonImageOptions6.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions7.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions7.Image")));
            windowsUIButtonImageOptions7.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions8.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions8.Image")));
            windowsUIButtonImageOptions8.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions9.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions9.Image")));
            windowsUIButtonImageOptions9.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            this.windowsUIButtonPanel1.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F2) Thêm mới", true, windowsUIButtonImageOptions1, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "new", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F3) Lưu", true, windowsUIButtonImageOptions2, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "save", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F4) Xóa", true, windowsUIButtonImageOptions3, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "delete", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F5) Tìm kiếm", true, windowsUIButtonImageOptions4, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "search", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F6) Làm mới", true, windowsUIButtonImageOptions5, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "refesh", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F7) Nhập dữ liệu", true, windowsUIButtonImageOptions6, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "import", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F8) Xuất dữ liệu", true, windowsUIButtonImageOptions7, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "export", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F9) In ấn", true, windowsUIButtonImageOptions8, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "print", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F10) Thoát", true, windowsUIButtonImageOptions9, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "close", -1, false)});
            this.windowsUIButtonPanel1.ContentAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.windowsUIButtonPanel1.Location = new System.Drawing.Point(2, 2);
            this.windowsUIButtonPanel1.Name = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.Padding = new System.Windows.Forms.Padding(5);
            this.windowsUIButtonPanel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.windowsUIButtonPanel1.Size = new System.Drawing.Size(1294, 32);
            this.windowsUIButtonPanel1.TabIndex = 5;
            this.windowsUIButtonPanel1.Text = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.UseButtonBackgroundImages = false;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.Root.Name = "Root";
            this.Root.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.Root.Size = new System.Drawing.Size(1298, 37);
            this.Root.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.windowsUIButtonPanel1;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1298, 37);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.gridControlWRDataDetail);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl3.Location = new System.Drawing.Point(0, 139);
            this.layoutControl3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.Root = this.layoutControlGroup2;
            this.layoutControl3.Size = new System.Drawing.Size(1298, 487);
            this.layoutControl3.TabIndex = 2;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // gridControlWRDataDetail
            // 
            this.gridControlWRDataDetail.DataSource = this.wRDataDetailBindingSource;
            this.gridControlWRDataDetail.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gridControlWRDataDetail.Location = new System.Drawing.Point(12, 12);
            this.gridControlWRDataDetail.MainView = this.gridViewWRDataDetail;
            this.gridControlWRDataDetail.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gridControlWRDataDetail.Name = "gridControlWRDataDetail";
            this.gridControlWRDataDetail.Size = new System.Drawing.Size(1274, 463);
            this.gridControlWRDataDetail.TabIndex = 4;
            this.gridControlWRDataDetail.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewWRDataDetail});
            this.gridControlWRDataDetail.Click += new System.EventHandler(this.gridControlWRDataDetail_Click);
            // 
            // wRDataDetailBindingSource
            // 
            this.wRDataDetailBindingSource.DataMember = "WRDataDetail";
            this.wRDataDetailBindingSource.DataSource = this.wARHOUSE_HPDataSet1;
            // 
            // wARHOUSE_HPDataSet1
            // 
            this.wARHOUSE_HPDataSet1.DataSetName = "WARHOUSE_HPDataSet1";
            this.wARHOUSE_HPDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridViewWRDataDetail
            // 
            this.gridViewWRDataDetail.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridViewWRDataDetail.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridViewWRDataDetail.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.gridViewWRDataDetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colNo,
            this.colBarcode,
            this.colWRDNumber,
            this.colOrdinal,
            this.colQuantity,
            this.colQuantityOrg,
            this.colStatus,
            this.colStyleColorSize,
            this.colStyle,
            this.colColor,
            this.colSku,
            this.colSize,
            this.colGender,
            this.colProductGroup,
            this.colDescriptionVN,
            this.colLifeStylePerformance,
            this.colDiv,
            this.colOutsole,
            this.colCategory,
            this.colProductlinen,
            this.colDescription,
            this.colEXWorkUSD,
            this.colEXWorkVND,
            this.colAmountVND,
            this.colRetailPrice,
            this.colSeason,
            this.colCoo,
            this.colMaterial,
            this.colInvoiceNo,
            this.colDateOfInvoice,
            this.colSaleContract,
            this.colShipment,
            this.colFactoryAdress,
            this.colFactoryName,
            this.colFactoryNameVN1,
            this.colFactoryAdressVN,
            this.colNewPrice,
            this.colChangePrice,
            this.colInputDate});
            this.gridViewWRDataDetail.DetailHeight = 284;
            this.gridViewWRDataDetail.GridControl = this.gridControlWRDataDetail;
            this.gridViewWRDataDetail.Name = "gridViewWRDataDetail";
            this.gridViewWRDataDetail.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            // 
            // colNo
            // 
            this.colNo.Caption = "LINE NO";
            this.colNo.FieldName = "No";
            this.colNo.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colNo.MinWidth = 50;
            this.colNo.Name = "colNo";
            this.colNo.Visible = true;
            this.colNo.VisibleIndex = 0;
            this.colNo.Width = 64;
            // 
            // colBarcode
            // 
            this.colBarcode.Caption = "Barcode";
            this.colBarcode.FieldName = "Barcode";
            this.colBarcode.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colBarcode.MinWidth = 80;
            this.colBarcode.Name = "colBarcode";
            this.colBarcode.Visible = true;
            this.colBarcode.VisibleIndex = 1;
            this.colBarcode.Width = 80;
            // 
            // colWRDNumber
            // 
            this.colWRDNumber.Caption = "CONVEYANCE NUMBER";
            this.colWRDNumber.FieldName = "WRDNumber";
            this.colWRDNumber.MinWidth = 21;
            this.colWRDNumber.Name = "colWRDNumber";
            this.colWRDNumber.Width = 81;
            // 
            // colOrdinal
            // 
            this.colOrdinal.Caption = "Số thứ tự";
            this.colOrdinal.FieldName = "Ordinal";
            this.colOrdinal.MinWidth = 21;
            this.colOrdinal.Name = "colOrdinal";
            this.colOrdinal.Width = 81;
            // 
            // colQuantity
            // 
            this.colQuantity.Caption = "SL thực nhận";
            this.colQuantity.DisplayFormat.FormatString = "G29";
            this.colQuantity.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colQuantity.FieldName = "Quantity";
            this.colQuantity.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colQuantity.MinWidth = 40;
            this.colQuantity.Name = "colQuantity";
            this.colQuantity.Visible = true;
            this.colQuantity.VisibleIndex = 8;
            this.colQuantity.Width = 201;
            // 
            // colQuantityOrg
            // 
            this.colQuantityOrg.Caption = "ASN QTY";
            this.colQuantityOrg.DisplayFormat.FormatString = "G29";
            this.colQuantityOrg.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colQuantityOrg.FieldName = "QuantityOrg";
            this.colQuantityOrg.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colQuantityOrg.MinWidth = 40;
            this.colQuantityOrg.Name = "colQuantityOrg";
            this.colQuantityOrg.Visible = true;
            this.colQuantityOrg.VisibleIndex = 7;
            this.colQuantityOrg.Width = 96;
            // 
            // colStatus
            // 
            this.colStatus.Caption = "Trạng thái";
            this.colStatus.FieldName = "Status";
            this.colStatus.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colStatus.MinWidth = 80;
            this.colStatus.Name = "colStatus";
            this.colStatus.Width = 81;
            // 
            // colStyleColorSize
            // 
            this.colStyleColorSize.Caption = "Style Color Size";
            this.colStyleColorSize.FieldName = "StyleColorSize";
            this.colStyleColorSize.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colStyleColorSize.MinWidth = 80;
            this.colStyleColorSize.Name = "colStyleColorSize";
            this.colStyleColorSize.Visible = true;
            this.colStyleColorSize.VisibleIndex = 2;
            this.colStyleColorSize.Width = 80;
            // 
            // colStyle
            // 
            this.colStyle.Caption = "Style";
            this.colStyle.FieldName = "Style";
            this.colStyle.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colStyle.MinWidth = 80;
            this.colStyle.Name = "colStyle";
            this.colStyle.Visible = true;
            this.colStyle.VisibleIndex = 3;
            this.colStyle.Width = 80;
            // 
            // colColor
            // 
            this.colColor.Caption = "Color";
            this.colColor.FieldName = "Color";
            this.colColor.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colColor.MinWidth = 80;
            this.colColor.Name = "colColor";
            this.colColor.Visible = true;
            this.colColor.VisibleIndex = 4;
            this.colColor.Width = 80;
            // 
            // colSku
            // 
            this.colSku.Caption = "Sku";
            this.colSku.FieldName = "Sku";
            this.colSku.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colSku.MinWidth = 80;
            this.colSku.Name = "colSku";
            this.colSku.Visible = true;
            this.colSku.VisibleIndex = 5;
            this.colSku.Width = 80;
            // 
            // colSize
            // 
            this.colSize.Caption = "Size";
            this.colSize.FieldName = "Size";
            this.colSize.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colSize.MinWidth = 50;
            this.colSize.Name = "colSize";
            this.colSize.Visible = true;
            this.colSize.VisibleIndex = 6;
            // 
            // colGender
            // 
            this.colGender.Caption = "Gender";
            this.colGender.FieldName = "Gender";
            this.colGender.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colGender.MinWidth = 50;
            this.colGender.Name = "colGender";
            this.colGender.Visible = true;
            this.colGender.VisibleIndex = 9;
            // 
            // colProductGroup
            // 
            this.colProductGroup.Caption = "Product Group";
            this.colProductGroup.FieldName = "ProductGroup";
            this.colProductGroup.MinWidth = 100;
            this.colProductGroup.Name = "colProductGroup";
            this.colProductGroup.Visible = true;
            this.colProductGroup.VisibleIndex = 10;
            this.colProductGroup.Width = 100;
            // 
            // colDescriptionVN
            // 
            this.colDescriptionVN.Caption = "Mô tả hàng hóa tiếng việt";
            this.colDescriptionVN.FieldName = "DescriptionVN";
            this.colDescriptionVN.MinWidth = 180;
            this.colDescriptionVN.Name = "colDescriptionVN";
            this.colDescriptionVN.Visible = true;
            this.colDescriptionVN.VisibleIndex = 11;
            this.colDescriptionVN.Width = 180;
            // 
            // colLifeStylePerformance
            // 
            this.colLifeStylePerformance.Caption = "LifeStyle/ Performance";
            this.colLifeStylePerformance.FieldName = "LifeStylePerformance";
            this.colLifeStylePerformance.MinWidth = 100;
            this.colLifeStylePerformance.Name = "colLifeStylePerformance";
            this.colLifeStylePerformance.Visible = true;
            this.colLifeStylePerformance.VisibleIndex = 12;
            this.colLifeStylePerformance.Width = 100;
            // 
            // colDiv
            // 
            this.colDiv.Caption = "Div";
            this.colDiv.FieldName = "Div";
            this.colDiv.MinWidth = 100;
            this.colDiv.Name = "colDiv";
            this.colDiv.Visible = true;
            this.colDiv.VisibleIndex = 13;
            this.colDiv.Width = 100;
            // 
            // colOutsole
            // 
            this.colOutsole.Caption = "Outsole";
            this.colOutsole.FieldName = "Outsole";
            this.colOutsole.MinWidth = 100;
            this.colOutsole.Name = "colOutsole";
            this.colOutsole.Visible = true;
            this.colOutsole.VisibleIndex = 14;
            this.colOutsole.Width = 100;
            // 
            // colCategory
            // 
            this.colCategory.Caption = "Category";
            this.colCategory.FieldName = "Category";
            this.colCategory.MinWidth = 100;
            this.colCategory.Name = "colCategory";
            this.colCategory.Visible = true;
            this.colCategory.VisibleIndex = 15;
            this.colCategory.Width = 100;
            // 
            // colProductlinen
            // 
            this.colProductlinen.Caption = "Productline";
            this.colProductlinen.FieldName = "Productline";
            this.colProductlinen.MinWidth = 100;
            this.colProductlinen.Name = "colProductlinen";
            this.colProductlinen.Visible = true;
            this.colProductlinen.VisibleIndex = 16;
            this.colProductlinen.Width = 100;
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Description";
            this.colDescription.FieldName = "Description";
            this.colDescription.MinWidth = 100;
            this.colDescription.Name = "colDescription";
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 17;
            this.colDescription.Width = 100;
            // 
            // colEXWorkUSD
            // 
            this.colEXWorkUSD.Caption = "EXWorkUSD";
            this.colEXWorkUSD.FieldName = "EXWorkUSD";
            this.colEXWorkUSD.MinWidth = 100;
            this.colEXWorkUSD.Name = "colEXWorkUSD";
            this.colEXWorkUSD.Visible = true;
            this.colEXWorkUSD.VisibleIndex = 18;
            this.colEXWorkUSD.Width = 100;
            // 
            // colEXWorkVND
            // 
            this.colEXWorkVND.Caption = "EXWorkVND";
            this.colEXWorkVND.FieldName = "EXWorkVND";
            this.colEXWorkVND.MinWidth = 100;
            this.colEXWorkVND.Name = "colEXWorkVND";
            this.colEXWorkVND.Visible = true;
            this.colEXWorkVND.VisibleIndex = 19;
            this.colEXWorkVND.Width = 100;
            // 
            // colAmountVND
            // 
            this.colAmountVND.Caption = "AmountVND";
            this.colAmountVND.FieldName = "AmountVND";
            this.colAmountVND.MinWidth = 100;
            this.colAmountVND.Name = "colAmountVND";
            this.colAmountVND.Visible = true;
            this.colAmountVND.VisibleIndex = 20;
            this.colAmountVND.Width = 100;
            // 
            // colRetailPrice
            // 
            this.colRetailPrice.Caption = "RetailPrice";
            this.colRetailPrice.FieldName = "RetailPrice";
            this.colRetailPrice.MinWidth = 100;
            this.colRetailPrice.Name = "colRetailPrice";
            this.colRetailPrice.Visible = true;
            this.colRetailPrice.VisibleIndex = 21;
            this.colRetailPrice.Width = 100;
            // 
            // colSeason
            // 
            this.colSeason.Caption = "Season";
            this.colSeason.FieldName = "Season";
            this.colSeason.MinWidth = 100;
            this.colSeason.Name = "colSeason";
            this.colSeason.Visible = true;
            this.colSeason.VisibleIndex = 22;
            this.colSeason.Width = 100;
            // 
            // colCoo
            // 
            this.colCoo.Caption = "Coo";
            this.colCoo.FieldName = "Coo";
            this.colCoo.MinWidth = 100;
            this.colCoo.Name = "colCoo";
            this.colCoo.Visible = true;
            this.colCoo.VisibleIndex = 23;
            this.colCoo.Width = 100;
            // 
            // colMaterial
            // 
            this.colMaterial.Caption = "Material";
            this.colMaterial.FieldName = "Material";
            this.colMaterial.MinWidth = 100;
            this.colMaterial.Name = "colMaterial";
            this.colMaterial.Visible = true;
            this.colMaterial.VisibleIndex = 24;
            this.colMaterial.Width = 100;
            // 
            // colInvoiceNo
            // 
            this.colInvoiceNo.Caption = "InvoiceNo";
            this.colInvoiceNo.FieldName = "InvoiceNo";
            this.colInvoiceNo.MinWidth = 100;
            this.colInvoiceNo.Name = "colInvoiceNo";
            this.colInvoiceNo.Visible = true;
            this.colInvoiceNo.VisibleIndex = 25;
            this.colInvoiceNo.Width = 100;
            // 
            // colDateOfInvoice
            // 
            this.colDateOfInvoice.Caption = "DateOfInvoice";
            this.colDateOfInvoice.FieldName = "DateOfInvoice";
            this.colDateOfInvoice.MinWidth = 100;
            this.colDateOfInvoice.Name = "colDateOfInvoice";
            this.colDateOfInvoice.Visible = true;
            this.colDateOfInvoice.VisibleIndex = 26;
            this.colDateOfInvoice.Width = 100;
            // 
            // colSaleContract
            // 
            this.colSaleContract.Caption = "SaleContract";
            this.colSaleContract.FieldName = "SaleContract";
            this.colSaleContract.MinWidth = 100;
            this.colSaleContract.Name = "colSaleContract";
            this.colSaleContract.Visible = true;
            this.colSaleContract.VisibleIndex = 27;
            this.colSaleContract.Width = 100;
            // 
            // colShipment
            // 
            this.colShipment.Caption = "Shipment";
            this.colShipment.FieldName = "Shipment";
            this.colShipment.MinWidth = 100;
            this.colShipment.Name = "colShipment";
            this.colShipment.Visible = true;
            this.colShipment.VisibleIndex = 28;
            this.colShipment.Width = 100;
            // 
            // colFactoryAdress
            // 
            this.colFactoryAdress.Caption = "FactoryAdress";
            this.colFactoryAdress.FieldName = "FactoryAdress";
            this.colFactoryAdress.MinWidth = 100;
            this.colFactoryAdress.Name = "colFactoryAdress";
            this.colFactoryAdress.Visible = true;
            this.colFactoryAdress.VisibleIndex = 29;
            this.colFactoryAdress.Width = 100;
            // 
            // colFactoryName
            // 
            this.colFactoryName.Caption = "FactoryName";
            this.colFactoryName.FieldName = "FactoryName";
            this.colFactoryName.MinWidth = 100;
            this.colFactoryName.Name = "colFactoryName";
            this.colFactoryName.Visible = true;
            this.colFactoryName.VisibleIndex = 30;
            this.colFactoryName.Width = 100;
            // 
            // colFactoryNameVN1
            // 
            this.colFactoryNameVN1.Caption = "FactoryNameVN";
            this.colFactoryNameVN1.FieldName = "FactoryNameVN";
            this.colFactoryNameVN1.MinWidth = 100;
            this.colFactoryNameVN1.Name = "colFactoryNameVN1";
            this.colFactoryNameVN1.Visible = true;
            this.colFactoryNameVN1.VisibleIndex = 31;
            this.colFactoryNameVN1.Width = 100;
            // 
            // colFactoryAdressVN
            // 
            this.colFactoryAdressVN.Caption = "FactoryAdressVN";
            this.colFactoryAdressVN.FieldName = "FactoryAdressVN";
            this.colFactoryAdressVN.MinWidth = 100;
            this.colFactoryAdressVN.Name = "colFactoryAdressVN";
            this.colFactoryAdressVN.Visible = true;
            this.colFactoryAdressVN.VisibleIndex = 32;
            this.colFactoryAdressVN.Width = 100;
            // 
            // colNewPrice
            // 
            this.colNewPrice.Caption = "NewPrice";
            this.colNewPrice.FieldName = "NewPrice";
            this.colNewPrice.MinWidth = 100;
            this.colNewPrice.Name = "colNewPrice";
            this.colNewPrice.Visible = true;
            this.colNewPrice.VisibleIndex = 33;
            this.colNewPrice.Width = 100;
            // 
            // colChangePrice
            // 
            this.colChangePrice.Caption = "ChangePrice";
            this.colChangePrice.FieldName = "ChangePrice";
            this.colChangePrice.MinWidth = 100;
            this.colChangePrice.Name = "colChangePrice";
            this.colChangePrice.Visible = true;
            this.colChangePrice.VisibleIndex = 34;
            this.colChangePrice.Width = 100;
            // 
            // colInputDate
            // 
            this.colInputDate.Caption = "InputDate";
            this.colInputDate.FieldName = "InputDate";
            this.colInputDate.MinWidth = 100;
            this.colInputDate.Name = "colInputDate";
            this.colInputDate.Visible = true;
            this.colInputDate.VisibleIndex = 35;
            this.colInputDate.Width = 100;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem12});
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1298, 487);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.gridControlWRDataDetail;
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(1278, 467);
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextVisible = false;
            // 
            // textEditTotalQuantity
            // 
            this.textEditTotalQuantity.Location = new System.Drawing.Point(1132, 39);
            this.textEditTotalQuantity.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditTotalQuantity.Name = "textEditTotalQuantity";
            this.textEditTotalQuantity.Size = new System.Drawing.Size(154, 20);
            this.textEditTotalQuantity.StyleController = this.layoutControl2;
            this.textEditTotalQuantity.TabIndex = 16;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.label2);
            this.layoutControl2.Controls.Add(this.pictureBox1);
            this.layoutControl2.Controls.Add(this.label1);
            this.layoutControl2.Controls.Add(this.textEditWRDNumber);
            this.layoutControl2.Controls.Add(this.dateEditWRDDate);
            this.layoutControl2.Controls.Add(this.textEdit4);
            this.layoutControl2.Controls.Add(this.textEditTotalQuantityOrg);
            this.layoutControl2.Controls.Add(this.textEditTotalQuantity);
            this.layoutControl2.Controls.Add(this.comboBoxEditHandlingStatus);
            this.layoutControl2.Controls.Add(this.simpleButtonOn);
            this.layoutControl2.Controls.Add(this.simpleButtonOff);
            this.layoutControl2.Controls.Add(this.textEditTotalTag);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl2.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3});
            this.layoutControl2.Location = new System.Drawing.Point(0, 37);
            this.layoutControl2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(343, 182, 812, 500);
            this.layoutControl2.Root = this.layoutControlGroup1;
            this.layoutControl2.Size = new System.Drawing.Size(1298, 102);
            this.layoutControl2.TabIndex = 1;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // label2
            // 
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(317, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 23);
            this.label2.TabIndex = 23;
            this.label2.Text = "(*)";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureBox1.Location = new System.Drawing.Point(382, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(126, 78);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 22;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(528, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(224, 25);
            this.label1.TabIndex = 7;
            this.label1.Text = "(*)";
            // 
            // textEditWRDNumber
            // 
            this.textEditWRDNumber.Location = new System.Drawing.Point(114, 12);
            this.textEditWRDNumber.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditWRDNumber.Name = "textEditWRDNumber";
            this.textEditWRDNumber.Size = new System.Drawing.Size(199, 20);
            this.textEditWRDNumber.StyleController = this.layoutControl2;
            this.textEditWRDNumber.TabIndex = 4;
            // 
            // dateEditWRDDate
            // 
            this.dateEditWRDDate.EditValue = null;
            this.dateEditWRDDate.Location = new System.Drawing.Point(114, 39);
            this.dateEditWRDDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dateEditWRDDate.Name = "dateEditWRDDate";
            this.dateEditWRDDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditWRDDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditWRDDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditWRDDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditWRDDate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditWRDDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditWRDDate.Size = new System.Drawing.Size(199, 20);
            this.dateEditWRDDate.StyleController = this.layoutControl2;
            this.dateEditWRDDate.TabIndex = 8;
            // 
            // textEdit4
            // 
            this.textEdit4.Location = new System.Drawing.Point(2540, 10);
            this.textEdit4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEdit4.Name = "textEdit4";
            this.textEdit4.Size = new System.Drawing.Size(53, 20);
            this.textEdit4.StyleController = this.layoutControl2;
            this.textEdit4.TabIndex = 11;
            // 
            // textEditTotalQuantityOrg
            // 
            this.textEditTotalQuantityOrg.Location = new System.Drawing.Point(1132, 12);
            this.textEditTotalQuantityOrg.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditTotalQuantityOrg.Name = "textEditTotalQuantityOrg";
            this.textEditTotalQuantityOrg.Size = new System.Drawing.Size(154, 20);
            this.textEditTotalQuantityOrg.StyleController = this.layoutControl2;
            this.textEditTotalQuantityOrg.TabIndex = 12;
            // 
            // comboBoxEditHandlingStatus
            // 
            this.comboBoxEditHandlingStatus.Location = new System.Drawing.Point(114, 66);
            this.comboBoxEditHandlingStatus.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBoxEditHandlingStatus.Name = "comboBoxEditHandlingStatus";
            this.comboBoxEditHandlingStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditHandlingStatus.Size = new System.Drawing.Size(199, 20);
            this.comboBoxEditHandlingStatus.StyleController = this.layoutControl2;
            this.comboBoxEditHandlingStatus.TabIndex = 17;
            // 
            // simpleButtonOn
            // 
            this.simpleButtonOn.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.simpleButtonOn.Location = new System.Drawing.Point(1030, 66);
            this.simpleButtonOn.Margin = new System.Windows.Forms.Padding(0);
            this.simpleButtonOn.Name = "simpleButtonOn";
            this.simpleButtonOn.Size = new System.Drawing.Size(126, 22);
            this.simpleButtonOn.StyleController = this.layoutControl2;
            this.simpleButtonOn.TabIndex = 18;
            this.simpleButtonOn.Text = "Mở";
            // 
            // simpleButtonOff
            // 
            this.simpleButtonOff.Enabled = false;
            this.simpleButtonOff.Location = new System.Drawing.Point(1160, 66);
            this.simpleButtonOff.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.simpleButtonOff.Name = "simpleButtonOff";
            this.simpleButtonOff.Size = new System.Drawing.Size(126, 22);
            this.simpleButtonOff.StyleController = this.layoutControl2;
            this.simpleButtonOff.TabIndex = 19;
            this.simpleButtonOff.Text = "Tắt";
            // 
            // textEditTotalTag
            // 
            this.textEditTotalTag.Location = new System.Drawing.Point(512, 12);
            this.textEditTotalTag.Name = "textEditTotalTag";
            this.textEditTotalTag.Size = new System.Drawing.Size(255, 20);
            this.textEditTotalTag.StyleController = this.layoutControl2;
            this.textEditTotalTag.TabIndex = 24;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.label1;
            this.layoutControlItem3.Location = new System.Drawing.Point(604, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.OptionsTableLayoutItem.ColumnIndex = 3;
            this.layoutControlItem3.Size = new System.Drawing.Size(265, 35);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem7,
            this.layoutControlItemWRDNumber,
            this.layoutControlItemWRDDate,
            this.layoutControlItemHandlingStatus,
            this.layoutControlItemTotalQuantityOrg,
            this.layoutControlItemTotalQuantity,
            this.layoutControlItem5,
            this.layoutControlItem2,
            this.layoutControlItem4,
            this.layoutControlItem6});
            this.layoutControlGroup1.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.layoutControlGroup1.Name = "Root";
            columnDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition1.Width = 23.965334303791163D;
            columnDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition2.Width = 5.0689777130805886D;
            columnDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition3.Width = 10.137955426161177D;
            columnDefinition4.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition4.Width = 20.275910852322355D;
            columnDefinition5.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition5.Width = 20.275910852322355D;
            columnDefinition6.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition6.Width = 10.137955426161177D;
            columnDefinition7.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition7.Width = 10.137955426161177D;
            this.layoutControlGroup1.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition1,
            columnDefinition2,
            columnDefinition3,
            columnDefinition4,
            columnDefinition5,
            columnDefinition6,
            columnDefinition7});
            rowDefinition1.Height = 33.333333333333336D;
            rowDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition2.Height = 33.333333333333336D;
            rowDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition3.Height = 33.333333333333336D;
            rowDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            this.layoutControlGroup1.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition1,
            rowDefinition2,
            rowDefinition3});
            this.layoutControlGroup1.Size = new System.Drawing.Size(1298, 102);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.pictureBox1;
            this.layoutControlItem7.Location = new System.Drawing.Point(370, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItem7.OptionsTableLayoutItem.RowSpan = 3;
            this.layoutControlItem7.Size = new System.Drawing.Size(130, 82);
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItemWRDNumber
            // 
            this.layoutControlItemWRDNumber.Control = this.textEditWRDNumber;
            this.layoutControlItemWRDNumber.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemWRDNumber.Name = "layoutControlItemWRDNumber";
            this.layoutControlItemWRDNumber.Size = new System.Drawing.Size(305, 27);
            this.layoutControlItemWRDNumber.Text = "Conveyance number";
            this.layoutControlItemWRDNumber.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItemWRDDate
            // 
            this.layoutControlItemWRDDate.Control = this.dateEditWRDDate;
            this.layoutControlItemWRDDate.Location = new System.Drawing.Point(0, 27);
            this.layoutControlItemWRDDate.Name = "layoutControlItemWRDDate";
            this.layoutControlItemWRDDate.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemWRDDate.Size = new System.Drawing.Size(305, 27);
            this.layoutControlItemWRDDate.Text = "Ngày dữ liệu";
            this.layoutControlItemWRDDate.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItemHandlingStatus
            // 
            this.layoutControlItemHandlingStatus.Control = this.comboBoxEditHandlingStatus;
            this.layoutControlItemHandlingStatus.Location = new System.Drawing.Point(0, 54);
            this.layoutControlItemHandlingStatus.Name = "layoutControlItemHandlingStatus";
            this.layoutControlItemHandlingStatus.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItemHandlingStatus.Size = new System.Drawing.Size(305, 28);
            this.layoutControlItemHandlingStatus.Text = "HandlingStatus";
            this.layoutControlItemHandlingStatus.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItemTotalQuantityOrg
            // 
            this.layoutControlItemTotalQuantityOrg.Control = this.textEditTotalQuantityOrg;
            this.layoutControlItemTotalQuantityOrg.Location = new System.Drawing.Point(1018, 0);
            this.layoutControlItemTotalQuantityOrg.Name = "layoutControlItemTotalQuantityOrg";
            this.layoutControlItemTotalQuantityOrg.OptionsTableLayoutItem.ColumnIndex = 5;
            this.layoutControlItemTotalQuantityOrg.OptionsTableLayoutItem.ColumnSpan = 2;
            this.layoutControlItemTotalQuantityOrg.Size = new System.Drawing.Size(260, 27);
            this.layoutControlItemTotalQuantityOrg.Text = "∑ SL yêu cầu";
            this.layoutControlItemTotalQuantityOrg.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItemTotalQuantity
            // 
            this.layoutControlItemTotalQuantity.Control = this.textEditTotalQuantity;
            this.layoutControlItemTotalQuantity.Location = new System.Drawing.Point(1018, 27);
            this.layoutControlItemTotalQuantity.Name = "layoutControlItemTotalQuantity";
            this.layoutControlItemTotalQuantity.OptionsTableLayoutItem.ColumnIndex = 5;
            this.layoutControlItemTotalQuantity.OptionsTableLayoutItem.ColumnSpan = 2;
            this.layoutControlItemTotalQuantity.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemTotalQuantity.Size = new System.Drawing.Size(260, 27);
            this.layoutControlItemTotalQuantity.Text = "∑ SL  Nhận";
            this.layoutControlItemTotalQuantity.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.label2;
            this.layoutControlItem5.Location = new System.Drawing.Point(305, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItem5.Size = new System.Drawing.Size(65, 27);
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.simpleButtonOn;
            this.layoutControlItem2.Location = new System.Drawing.Point(1018, 54);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.OptionsTableLayoutItem.ColumnIndex = 5;
            this.layoutControlItem2.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItem2.Size = new System.Drawing.Size(130, 28);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.simpleButtonOff;
            this.layoutControlItem4.Location = new System.Drawing.Point(1148, 54);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.OptionsTableLayoutItem.ColumnIndex = 6;
            this.layoutControlItem4.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItem4.Size = new System.Drawing.Size(130, 28);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.textEditTotalTag;
            this.layoutControlItem6.Location = new System.Drawing.Point(500, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.OptionsTableLayoutItem.ColumnIndex = 3;
            this.layoutControlItem6.Size = new System.Drawing.Size(259, 27);
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // wRDataDetailTableAdapter
            // 
            this.wRDataDetailTableAdapter.ClearBeforeFill = true;
            // 
            // ReceiptDataControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl3);
            this.Controls.Add(this.layoutControl2);
            this.Controls.Add(this.layoutControl1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "ReceiptDataControl";
            this.Size = new System.Drawing.Size(1298, 626);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlWRDataDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wRDataDetailBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wARHOUSE_HPDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewWRDataDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalQuantity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditWRDNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditWRDDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditWRDDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalQuantityOrg.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditHandlingStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalTag.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWRDNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWRDDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemHandlingStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalQuantityOrg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraGrid.GridControl gridControlWRDataDetail;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewWRDataDetail;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraEditors.TextEdit textEditTotalQuantity;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit textEditWRDNumber;
        private DevExpress.XtraEditors.DateEdit dateEditWRDDate;
        private DevExpress.XtraEditors.TextEdit textEdit4;
        private DevExpress.XtraEditors.TextEdit textEditTotalQuantityOrg;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditHandlingStatus;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private System.Windows.Forms.BindingSource wRDataDetailBindingSource;
        private WARHOUSE_HPDataSet1 wARHOUSE_HPDataSet1;
        private DevExpress.XtraGrid.Columns.GridColumn colWRDNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colOrdinal;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantityOrg;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private WARHOUSE_HPDataSet1TableAdapters.WRDataDetailTableAdapter wRDataDetailTableAdapter;
        private DevExpress.XtraEditors.SimpleButton simpleButtonOn;
        private DevExpress.XtraEditors.SimpleButton simpleButtonOff;
        private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel windowsUIButtonPanel1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemWRDNumber;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemWRDDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemHandlingStatus;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTotalQuantityOrg;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTotalQuantity;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraGrid.Columns.GridColumn colNo;
        private DevExpress.XtraEditors.TextEdit textEditTotalTag;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraGrid.Columns.GridColumn colBarcode;
        private DevExpress.XtraGrid.Columns.GridColumn colStyleColorSize;
        private DevExpress.XtraGrid.Columns.GridColumn colStyle;
        private DevExpress.XtraGrid.Columns.GridColumn colColor;
        private DevExpress.XtraGrid.Columns.GridColumn colSku;
        private DevExpress.XtraGrid.Columns.GridColumn colSize;
        private DevExpress.XtraGrid.Columns.GridColumn colGender;
        private DevExpress.XtraGrid.Columns.GridColumn colProductGroup;
        private DevExpress.XtraGrid.Columns.GridColumn colDescriptionVN;
        private DevExpress.XtraGrid.Columns.GridColumn colLifeStylePerformance;
        private DevExpress.XtraGrid.Columns.GridColumn colDiv;
        private DevExpress.XtraGrid.Columns.GridColumn colOutsole;
        private DevExpress.XtraGrid.Columns.GridColumn colCategory;
        private DevExpress.XtraGrid.Columns.GridColumn colProductlinen;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colEXWorkUSD;
        private DevExpress.XtraGrid.Columns.GridColumn colEXWorkVND;
        private DevExpress.XtraGrid.Columns.GridColumn colAmountVND;
        private DevExpress.XtraGrid.Columns.GridColumn colRetailPrice;
        private DevExpress.XtraGrid.Columns.GridColumn colSeason;
        private DevExpress.XtraGrid.Columns.GridColumn colCoo;
        private DevExpress.XtraGrid.Columns.GridColumn colMaterial;
        private DevExpress.XtraGrid.Columns.GridColumn colInvoiceNo;
        private DevExpress.XtraGrid.Columns.GridColumn colDateOfInvoice;
        private DevExpress.XtraGrid.Columns.GridColumn colSaleContract;
        private DevExpress.XtraGrid.Columns.GridColumn colShipment;
        private DevExpress.XtraGrid.Columns.GridColumn colFactoryAdress;
        private DevExpress.XtraGrid.Columns.GridColumn colFactoryName;
        private DevExpress.XtraGrid.Columns.GridColumn colFactoryNameVN1;
        private DevExpress.XtraGrid.Columns.GridColumn colFactoryAdressVN;
        private DevExpress.XtraGrid.Columns.GridColumn colNewPrice;
        private DevExpress.XtraGrid.Columns.GridColumn colChangePrice;
        private DevExpress.XtraGrid.Columns.GridColumn colInputDate;
    }
}
