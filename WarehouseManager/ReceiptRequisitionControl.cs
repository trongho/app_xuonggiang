﻿using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraLayout.Filtering.Templates;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarehouseManager.Models;
using WarehouseManager.Repository;
using webservice.Entities;
using LicenseContext = OfficeOpenXml.LicenseContext;
using DevExpress.XtraEditors.Repository;
using System.Globalization;
using Symbol.RFID3;
using DevExpress.Utils.Menu;
using System.Threading;
using DevExpress.XtraEditors.Controls;

namespace WarehouseManager
{
    public partial class ReceiptRequisitionControl : DevExpress.XtraEditors.XtraUserControl
    {
        WRRDetailRepository WRRDetailRepository;
        WRRHeaderRepository wRRHeaderRepository;
        HandlingStatusRepository handlingStatusRepository;
        BranchRepository branchRepository;
        WRDataHeaderRepository wRDataHeaderRepository;
        WRDataDetailRepository wRDataDetailRepository;
        WRDataGeneralRepository wRDataGeneralRepository;
        SGDataHeaderRepository sGDataHeaderRepository;
        SGDataGeneralRepository sGDataGeneralRepository;
        GoodsRepository goodsRepository;
        public static string SelectedTable = string.Empty;
        public static String selectedGoodsID;
        public static String selectedGoodsName;
        public static String selectedWRRNumber;
        public static DateTime selectedWRRDate;
        public static String selectedBranchID;
        public static String selectedHandlingID;
        String focusGoodsID = "";
        String lastWRRNumber = "";
        Boolean isWRRNumberExist = false;

        internal MainForm m_MainForm = null;
        internal ReadTagForm m_ReadTagForm;
        internal WriteTagForm m_WriteTagForm;
        internal EncodeForm m_EncodeForm;

        public List<string> listTag;
        public List<string> listTagEvent;

        public ReceiptRequisitionControl(MainForm mainForm)
        {
            InitializeComponent();

            m_MainForm = mainForm;
            m_ReadTagForm = new ReadTagForm(this);
            m_WriteTagForm = new WriteTagForm(this);
            m_EncodeForm = new EncodeForm(this);
            this.Load += ReceiptRequisitionControl_Load;
            WRRDetailRepository = new WRRDetailRepository();
            wRRHeaderRepository = new WRRHeaderRepository();
            handlingStatusRepository = new HandlingStatusRepository();
            branchRepository = new BranchRepository();
            wRDataHeaderRepository = new WRDataHeaderRepository();
            wRDataDetailRepository = new WRDataDetailRepository();
            wRDataGeneralRepository = new WRDataGeneralRepository();
            sGDataHeaderRepository = new SGDataHeaderRepository();
            sGDataGeneralRepository = new SGDataGeneralRepository();
            goodsRepository = new GoodsRepository();

            gridViewWRRDetail.PopupMenuShowing += GridView1_PopupMenuShowing;

    
        }

        public string DataRead
        {
            get
            {
                return m_ReadTagForm.textEditDataRead.Text;
            }
            set
            {
                m_ReadTagForm.textEditDataRead.Text = value;
            }
        }

        public string TotalTag
        {
            get
            {
                return textEditReaded.Text;
            }
            set
            {
                textEditReaded.Text = value;
            }
        }

        public string ListTagIDs
        {
            get
            {
                return textEditTagIDs.Text;
            }
            set
            {
                textEditTagIDs.Text = value;
            }
        }

        public string TagID
        {
            get
            {
                return m_WriteTagForm.textEditTagID.Text;
            }
            set
            {
                m_WriteTagForm.textEditTagID.Text = value;
            }
        }

        public string TagEvent
        {
            get
            {
                return m_WriteTagForm.textEditTagEvent.Text;
            }
            set
            {
                m_WriteTagForm.textEditTagEvent.Text = value;
            }
        }

        public Object gridEncode
        {
            get
            {
                return m_EncodeForm.gridControlEncode.DataSource;
            }
            set
            {
                m_EncodeForm.gridControlEncode.DataSource = value;
            }
        }


        private void ReceiptRequisitionControl_Load(Object sender, EventArgs e)
        {
            popupMenuSelectHandlingStatus();
            popupMenuSelectBranch();
            windowsUIButtonPanel1.ButtonClick += button_Click;
            textEditWRRNumber.KeyDown += new KeyEventHandler(WRRNumber_KeyDown);
            gridViewWRRDetail.CustomColumnDisplayText += wRRHeaderGridView_CustomColumnDisplayText;
            gridViewWRRDetail.DoubleClick += dataGridViewWRR_CellDoubleClick;
            gridViewWRRDetail.CellValueChanged += YourDGV_CellValueChanged;
            //gridViewWRRDetail.ValidateRow += GridView1_ValidateRow;
            gridViewWRRDetail.ValidatingEditor += gridView1_ValidatingEditor;
            textEditWRRNumber.DoubleClick += textEditWRRNumer_CellDoubleClick;

            gridViewWRRDetail.ClearSorting();
            sbLoadDataForGridWRRDetailAsync(textEditWRRNumber.Text);


        }

        //readform
        public void ReadTagForm_Load(object sender, EventArgs e)
        {

            if (listTag.Count>0)
            {
                m_ReadTagForm.textEditTagID.Text = listTag[0];
                //string str = "";
                
                //foreach (string s in m_MainForm.tagIDs)
                //{
                //    str += s + "\n";
                //}
                //m_ReadTagForm.textEditDataRead.Text =str;


            }
            else
            {
                m_ReadTagForm.textEditTagID.Text = "";
            }
            m_ReadTagForm.textEditDataRead.Text = "";
            m_ReadTagForm.comboBoxEditMemoryBank.SelectedIndex = (int)m_MainForm.m_ReadParams.MemoryBank;
            m_ReadTagForm.textEditPassword.Text = m_MainForm.m_ReadParams.AccessPassword.ToString("X");
            m_ReadTagForm.textEditOffset.Text = m_MainForm.m_ReadParams.ByteOffset.ToString();
            m_ReadTagForm.textEditNoOfBytesLenght.Text = m_MainForm.m_ReadParams.ByteCount.ToString();

            
        }

        public void comboBoxEditMemoryBank_SelectedIndexChanged(object sender, EventArgs e)
        {
            m_MainForm.m_ReadParams.MemoryBank = (MEMORY_BANK)m_ReadTagForm.comboBoxEditMemoryBank.SelectedIndex;

            if (m_MainForm.m_ReadParams.MemoryBank == MEMORY_BANK.MEMORY_BANK_EPC)
            {
                m_ReadTagForm.textEditOffset.Text = "4";
                m_ReadTagForm.textEditNoOfBytesLenght.Text = "14";
            }
            else
            {
                m_ReadTagForm.textEditOffset.Text = "0";
                m_ReadTagForm.textEditNoOfBytesLenght.Text = "0";
            }
        }

        public void offset_TB_TextChanged(object sender, EventArgs e)
        {
            if (m_ReadTagForm.textEditOffset.Text.Length > 0)
            {
                m_MainForm.m_ReadParams.ByteOffset = ushort.Parse(m_ReadTagForm.textEditOffset.Text);
            }
        }


        public void length_TB_TextChanged(object sender, EventArgs e)
        {
            if (m_ReadTagForm.textEditNoOfBytesLenght.Text.Length > 0)
                m_MainForm.m_ReadParams.ByteCount = ushort.Parse(m_ReadTagForm.textEditNoOfBytesLenght.Text);
        }


        public void readButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (m_ReadTagForm.textEditTagID.Text.Length == 0)
                {
                    m_MainForm.StatusBottom.Caption = "No TagID is defined";
                    return;
                }
                string accessPassword = m_ReadTagForm.textEditPassword.Text;
                m_MainForm.m_ReadParams.AccessPassword = 0;
                if (accessPassword.Length > 0)
                {
                    if (accessPassword.StartsWith("0x"))
                    {
                        accessPassword = accessPassword.Substring(2, accessPassword.Length - 2);
                    }
                    m_MainForm.m_ReadParams.AccessPassword = uint.Parse(
                        accessPassword, System.Globalization.NumberStyles.HexNumber);
                }

                m_MainForm.m_ReadParams.MemoryBank = (MEMORY_BANK)m_ReadTagForm.comboBoxEditMemoryBank.SelectedIndex;
                m_MainForm.m_ReadParams.ByteOffset = ushort.Parse(m_ReadTagForm.textEditOffset.Text);
                m_MainForm.m_ReadParams.ByteCount = ushort.Parse(m_ReadTagForm.textEditNoOfBytesLenght.Text);

                
                m_MainForm.m_SelectedTagID = m_ReadTagForm.textEditTagID.Text;
                m_MainForm.accessBackgroundWorker.RunWorkerAsync(ACCESS_OPERATION_CODE.ACCESS_OPERATION_READ);
                //m_ReadTagForm.simpleButtonReadTag.Enabled = false;
               
            }
            catch (Exception ex)
            {
                m_MainForm.StatusBottom.Caption = ex.Message;
                m_ReadTagForm.simpleButtonReadTag.Enabled = true;
            }

        }

        //writeform
        public void Write_Load(object sender, EventArgs e)
        {
            StartRead();
            if (TagID!=null&&TagEvent!=null)
            {
                m_WriteTagForm.textEditTagID.Text = TagID;
                m_WriteTagForm.textEditTagEvent.Text = TagEvent;
            }
            else
            {
                m_WriteTagForm.textEditTagID.Text = "";
            }
            m_WriteTagForm.comboBoxEditMemoryBank.SelectedIndex = (int)m_MainForm.m_ReadParams.MemoryBank;
            m_WriteTagForm.textEditPassword.Text = m_MainForm.m_ReadParams.AccessPassword.ToString("X");
            m_WriteTagForm.textEditOffset.Text = m_MainForm.m_ReadParams.ByteOffset.ToString();
            m_WriteTagForm.textEditNoOfBytesLenght.Text = m_MainForm.m_ReadParams.ByteCount.ToString();
        }
        public void Write_FormClosing(object sender, FormClosingEventArgs e)
        {
            m_WriteTagForm.Dispose();
            StopRead();
        }
        public void memBank_CB_SelectedIndexChanged(object sender, EventArgs e)
        {
            m_MainForm.m_WriteParams.MemoryBank = (MEMORY_BANK)m_WriteTagForm.comboBoxEditMemoryBank.SelectedIndex;

            if (m_MainForm.m_WriteParams.MemoryBank == MEMORY_BANK.MEMORY_BANK_EPC)
            {
                m_WriteTagForm.textEditOffset.Text = "4";
                m_WriteTagForm.textEditNoOfBytesLenght.Text = "14";
            }
            else
            {
                m_WriteTagForm.textEditOffset.Text = "0";
                m_WriteTagForm.textEditNoOfBytesLenght.Text = "0";
            }
        }
        public void writeButton_Click(object sender, EventArgs e)
        {
                try
                {
                    ushort length = ushort.Parse(m_WriteTagForm.textEditNoOfBytesLenght.Text);

                    if (m_WriteTagForm.textEditTagID.Text.Length == 0)
                    {
                        m_MainForm.StatusBottom.Caption = "No TagID is defined";
                        return;
                    }
                    else if (length * 2 > (Helpers.Constant.CompanyCode+m_WriteTagForm.Barcode).Length)
                    {
                        m_MainForm.StatusBottom.Caption = "Data length has to be a word size (2X)";
                        return;
                    }
                    else if (length % 2 != 0)
                    {
                        m_MainForm.StatusBottom.Caption = "Data length has to be a even number";
                        return;
                    }
                    else
                    {
                        m_MainForm.StatusBottom.Caption = "";
                    }

                    string accessPassword = m_WriteTagForm.textEditPassword.Text;
                    m_MainForm.m_WriteParams.AccessPassword = 0;
                    if (accessPassword.Length > 0)
                    {
                        if (accessPassword.StartsWith("0x"))
                        {
                            accessPassword = accessPassword.Substring(2, accessPassword.Length - 2);
                        }
                        m_MainForm.m_WriteParams.AccessPassword = uint.Parse(
                            accessPassword, System.Globalization.NumberStyles.HexNumber);
                    }

                    m_MainForm.m_WriteParams.MemoryBank = (MEMORY_BANK)m_WriteTagForm.comboBoxEditMemoryBank.SelectedIndex;
                    m_MainForm.m_WriteParams.ByteOffset = ushort.Parse(m_WriteTagForm.textEditOffset.Text);
                    m_MainForm.m_WriteParams.WriteDataLength = length;

                    byte[] writeData = new byte[m_MainForm.m_WriteParams.WriteDataLength];
                    for (int index = 0; index < m_MainForm.m_WriteParams.WriteDataLength; index += 2)
                    {
                        writeData[index] = byte.Parse((Helpers.Constant.CompanyCode+ m_WriteTagForm.Barcode).Substring(index * 2, 2),
                            System.Globalization.NumberStyles.HexNumber);
                        writeData[index + 1] = byte.Parse((Helpers.Constant.CompanyCode + m_WriteTagForm.Barcode).Substring((index + 1) * 2, 2),
                            System.Globalization.NumberStyles.HexNumber);
                    }
                    m_MainForm.m_WriteParams.WriteData = writeData;
                    m_MainForm.m_SelectedTagID =m_WriteTagForm.textEditTagID.Text;


                    m_MainForm.accessBackgroundWorker.RunWorkerAsync(ACCESS_OPERATION_CODE.ACCESS_OPERATION_WRITE);

                }
                catch (Exception ex)
                {
                    m_MainForm.StatusBottom.Caption = ex.Message;
                    m_WriteTagForm.simpleButtonWriteTag.Enabled = true;
                }
        }

        //encodeform
        public void EncodeFrm_Load(object sender, EventArgs e)
        {
            m_EncodeForm.comboBoxEditMemoryBank.SelectedIndex = (int)m_MainForm.m_ReadParams.MemoryBank;
            m_EncodeForm.textEditPassword.Text = m_MainForm.m_ReadParams.AccessPassword.ToString("X");
            m_EncodeForm.textEditOffset.Text = m_MainForm.m_ReadParams.ByteOffset.ToString();
            m_EncodeForm.textEditNoOfBytesLenght.Text = m_MainForm.m_ReadParams.ByteCount.ToString();
        }
        //public void EncodeFrm_FormClosing(object sender, FormClosingEventArgs e)
        //{
        //    m_EncodeForm.Dispose();
        //}
        public void EncodeFrm_memBank_CB_SelectedIndexChanged(object sender, EventArgs e)
        {
            m_MainForm.m_WriteParams.MemoryBank = (MEMORY_BANK)m_EncodeForm.comboBoxEditMemoryBank.SelectedIndex;

            if (m_MainForm.m_WriteParams.MemoryBank == MEMORY_BANK.MEMORY_BANK_EPC)
            {
                m_EncodeForm.textEditOffset.Text = "4";
                m_EncodeForm.textEditNoOfBytesLenght.Text = "12";
            }
            else
            {
                m_EncodeForm.textEditOffset.Text = "0";
                m_EncodeForm.textEditNoOfBytesLenght.Text = "0";
            }
        }
        public async void endcode(string tagID,string barcode,string num)
        {
            try
            {
                ushort length = ushort.Parse(m_EncodeForm.textEditNoOfBytesLenght.Text);

                if (tagID.Length == 0)
                {
                    m_MainForm.StatusBottom.Caption = "No TagID is defined";
                    return;
                }
                else if (length * 2 > (Helpers.Constant.CompanyCode + m_EncodeForm.Barcode+num).Length)
                {
                    m_MainForm.StatusBottom.Caption = "Data length has to be a word size (2X)";
                    return;
                }
                else if (length % 2 != 0)
                {
                    m_MainForm.StatusBottom.Caption = "Data length has to be a even number";
                    return;
                }
                else
                {
                    m_MainForm.StatusBottom.Caption = "";
                }

                string accessPassword = m_EncodeForm.textEditPassword.Text;
                m_MainForm.m_WriteParams.AccessPassword = 0;
                if (accessPassword.Length > 0)
                {
                    if (accessPassword.StartsWith("0x"))
                    {
                        accessPassword = accessPassword.Substring(2, accessPassword.Length - 2);
                    }
                    m_MainForm.m_WriteParams.AccessPassword = uint.Parse(
                        accessPassword, System.Globalization.NumberStyles.HexNumber);
                }

                m_MainForm.m_WriteParams.MemoryBank = (MEMORY_BANK)m_EncodeForm.comboBoxEditMemoryBank.SelectedIndex;
                m_MainForm.m_WriteParams.ByteOffset = ushort.Parse(m_EncodeForm.textEditOffset.Text);
                m_MainForm.m_WriteParams.WriteDataLength = length;

                byte[] writeData = new byte[m_MainForm.m_WriteParams.WriteDataLength];
                for (int index = 0; index < m_MainForm.m_WriteParams.WriteDataLength; index += 2)
                {
                    writeData[index] = byte.Parse((Helpers.Constant.CompanyCode + m_EncodeForm.Barcode+num).Substring(index * 2, 2),
                        System.Globalization.NumberStyles.HexNumber);
                    writeData[index + 1] = byte.Parse((Helpers.Constant.CompanyCode + m_EncodeForm.Barcode+num).Substring((index + 1) * 2, 2),
                        System.Globalization.NumberStyles.HexNumber);
                }
                m_MainForm.m_WriteParams.WriteData = writeData;
                m_MainForm.m_SelectedTagID = tagID;


                m_MainForm.accessBackgroundWorker.RunWorkerAsync(ACCESS_OPERATION_CODE.ACCESS_OPERATION_WRITE);
                //m_EncodeForm.simpleButtonEnCode.Enabled = false;

                List<WRRDetail> wRRDetails = await WRRDetailRepository.GetUnderBarcode(textEditWRRNumber.Text,barcode);
                if (wRRDetails.Count > 0)
                {
                    WRRDetail wRRDetail = wRRDetails[0];
                    wRRDetail.Status = int.Parse(wRRDetail.Status)+1+"";
                    await WRRDetailRepository.Update(wRRDetail, textEditWRRNumber.Text, barcode, wRRDetail.Ordinal);
                }

            }
            catch (Exception ex)
            {
                m_MainForm.StatusBottom.Caption = ex.Message;
                m_EncodeForm.simpleButtonEnCode.Enabled = true;
            }
        }
        //public void EncodeFrm_writeButton_Click(object sender, EventArgs e)
        //{
        //    //int num = m_EncodeForm.gridViewEncode.RowCount - 1;
        //    //int i = 0;
        //    //while (true)
        //    //{
        //    //    int num2 = i;
        //    //    int num3 = num;
        //    //    if (num2 > num3)
        //    //    {
        //    //        break;
        //    //    }
        //    //    int rowHandle = m_EncodeForm.gridViewEncode.GetRowHandle(i);
        //    //    string tagID = (string)m_EncodeForm.gridViewEncode.GetRowCellValue(0, "TagID");
        //    //    endcode(tagID, $"{int.Parse((0 + 1) + ""):D6}");

        //    //    i++;
        //    //}



        //    for (int i = 0; i <3; i++)
        //    {
        //        string tagID = (string)m_EncodeForm.gridViewEncode.GetRowCellValue(0, "TagID");              
        //        endcode(tagID, $"{int.Parse((i + 1) + ""):D6}");
        //        m_EncodeForm.gridViewEncode.DeleteRow(0);
        //    }

        //}

        public  async void repositoryItemButtonEdit1_EncodeFrm_ButtonClick(object sender, ButtonPressedEventArgs e)
        {

            if (/*determine a button*/e.Button.Tag != null)
            {
                string tagID = (string)m_EncodeForm.gridViewEncode.GetRowCellValue(m_EncodeForm.gridViewEncode.FocusedRowHandle, "TagID");
                string barcode = m_EncodeForm.Barcode;
                List<WRRDetail> wRRDetails = await WRRDetailRepository.GetUnderBarcode(textEditWRRNumber.Text, m_EncodeForm.Barcode);
                if (wRRDetails.Count > 0)
                {
                    string status =wRRDetails[0].Status;
                    DialogResult dialogResult = MessageBoxEx.Show(this, "Tag ID: " + tagID + "barcode: " + barcode + "No.:" + $"{int.Parse(status) + 1:D6}" + "?", "Warning!!!", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
                    if (dialogResult == DialogResult.Yes)
                    {
                        endcode(tagID, barcode, $"{int.Parse(status) + 1:D6}");
                    }
                }        
            }

        }

        private async Task popupMenuSelectHandlingStatus()
        {
            List<HandlingStatus> handlingStatuses = await handlingStatusRepository.GetAll();
            foreach (HandlingStatus handlingStatus in handlingStatuses)
            {
                comboBoxEditHandlingStatus.Properties.Items.Add(handlingStatus.HandlingStatusName);
            }
            comboBoxEditHandlingStatus.SelectedIndex = 1;
        }

        private async Task popupMenuSelectBranch()
        {
            List<Branch> branches = await branchRepository.getAllBranchAsync();
            foreach (Branch branch in branches)
            {
                comboBoxEditBranch.Properties.Items.Add(branch.BranchName);
            }
            comboBoxEditBranch.SelectedIndex = 1;
        }

        private void YourDGV_CellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            //if (e.Column.FieldName.Equals("QuantityByItem"))
            //{
            //    getTotalQuantityByItem();
            //}
            //if (e.Column.FieldName.Equals("QuantityByPack"))
            //{
            //    getTotalQuantityByPack();
            //}
        }

        private void gridView1_ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            GridView view = sender as GridView;

            object val = e.Value;

            if (view.FocusedColumn.FieldName == "GoodsID")
            {
                if (val == null || string.IsNullOrWhiteSpace(e.Value.ToString()))
                {
                    e.Valid = false;
                    e.ErrorText = "Bạn nhập mã hàng null";
                }
                if (checkExistGoodsID(e.Value.ToString()) == true)
                {

                    e.Valid = false;
                    e.ErrorText = "Mã hàng hóa trùng";
                }
            }
        }

        private Boolean checkExistGoodsID(String goodsID)
        {
            int num = gridViewWRRDetail.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewWRRDetail.GetRowCellValue(i, gridViewWRRDetail.Columns["colGoodsID"]) == "")
                {
                    break;
                }
                int rowHandle = gridViewWRRDetail.GetRowHandle(i);
                String goodsID1 = (String)gridViewWRRDetail.GetRowCellValue(rowHandle, "GoodsID");
                if (goodsID.Equals(goodsID1))
                {
                    return true;
                }
                i++;
            }
            return false;
        }

        private Boolean checkRowBeforeNull()
        {
            int rowHandle = gridViewWRRDetail.FocusedRowHandle;
            String goodsID1 = (String)gridViewWRRDetail.GetRowCellValue(rowHandle - 1, "GoodsID");
            if (rowHandle > 0 && goodsID1 == null)
            {
                return true;
            }
            return false;
        }

        private void gridView1_ShownEditor(object sender, EventArgs e)
        {
            GridView view = sender as GridView;

            view.ActiveEditor.IsModified = true;
        }

        //private void GridView1_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        //{
        //    e.Valid = !IsDublicatedRowFound(e.Row);
        //    if (!e.Valid)
        //        e.ErrorText = "The record is not unique. ";
        //}
        //bool IsDublicatedRowFound(object row)
        //{
        //    for (int i = 0; i < gridViewWRRDetail.DataRowCount; i++)
        //        if (InvoicesHaveSameValues(row, gridViewWRRDetail.GetRow(i)))
        //            return true;
        //    return false;
        //}
        //bool InvoicesHaveSameValues(object row1, object row2)
        //{
        //    var lastEditedInvoice = row1 as WRRDetail;
        //    var currentlyProcessedRow = row2 as WRRDetail;
        //    if (lastEditedInvoice == null || currentlyProcessedRow == null)
        //        return false;

        //    if (lastEditedInvoice.GoodsID!= currentlyProcessedRow.GoodsID)
        //        return false;

        //    return true;
        //}


        private void getTotalQuantityByItem()
        {
            int totalQuantityByItem = 0;
            int num = gridViewWRRDetail.RowCount - 1;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewWRRDetail.GetRowHandle(i);
                if (gridViewWRRDetail.GetRowCellValue(rowHandle, "Barcode") == null)
                {
                    break;
                }
                int quantityByItem = (int)gridViewWRRDetail.GetRowCellValue(rowHandle, "Quantity");
                totalQuantityByItem += quantityByItem;
                i++;
            }
            textEditTotalQuantityByItem.Text = totalQuantityByItem.ToString();
        }

        private void getTotalQuantityByPack()
        {
            int totalQuantityByPack = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewWRRDetail.GetRowHandle(i);
                if (gridViewWRRDetail.GetRowCellValue(rowHandle, "Barcode") == null)
                {
                    break;
                }

                totalQuantityByPack++;
                i++;
            }
            textEditTotalQuantityByPack.Text = totalQuantityByPack.ToString();
        }

        private void wRRHeaderGridView_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {

            if (e.Column == colNo)
            {
                int rowHandle = gridViewWRRDetail.GetRowHandle(e.ListSourceRowIndex);
                if (rowHandle >= 0)
                {
                    e.DisplayText = (rowHandle + 1).ToString();
                }
            }
        }

        private async Task sbLoadDataForGridWRRDetailAsync(String wRRNumber)
        {
            List<WRRDetail> wRRDetails = await WRRDetailRepository.GetUnderID(wRRNumber);
            int mMaxRow = 100;
            if (wRRDetails.Count < mMaxRow)
            {
                int num = mMaxRow - wRRDetails.Count;
                int i = 1;
                while (true)
                {
                    int num2 = i;
                    int num3 = num;
                    if (num2 > num3)
                    {
                        break;
                    }
                    wRRDetails.Add(new WRRDetail());
                    i++;
                }
            }
            gridControlWRRDetail.DataSource = wRRDetails;
            getTotalQuantityByItem();
            getTotalQuantityByPack();
        }

        

        public async void getlastWRRNumberAsync()
        {
            lastWRRNumber = await wRRHeaderRepository.GetLastWRRNumber();
            String wRRNumberYear = DateTime.Now.Year.ToString().Substring(2, 2);
            if (lastWRRNumber == "")
            {
                textEditWRRNumber.Text = wRRNumberYear + "000" + "-" + $"{1:D5}";
            }
            else
            {
                string[] arrListStr = lastWRRNumber.Split('-');
                String wRRNumberPartOne = arrListStr[0];
                String wRRNumberPartTwo = arrListStr[1];
                wRRNumberPartTwo = (int.Parse(wRRNumberPartTwo) + 1).ToString();
                textEditWRRNumber.Text = wRRNumberYear + "000" + "-" + $"{int.Parse(wRRNumberPartTwo):D5}";
            }


        }

        public async void createWRRHeaderAsync()
        {
            WRRHeader wRRHeader = new WRRHeader();
            wRRHeader.WRRNumber = textEditWRRNumber.Text;
            wRRHeader.WRRDate = DateTime.Parse(dateEditWRRDate.DateTime.ToString("yyyy-MM-dd"));
            wRRHeader.HandlingStatusID = (comboBoxEditHandlingStatus.SelectedIndex - 1).ToString();
            wRRHeader.HandlingStatusName = comboBoxEditHandlingStatus.SelectedItem.ToString();
            wRRHeader.BranchID = $"{int.Parse(comboBoxEditBranch.SelectedIndex.ToString()) - 1:D3}";
            wRRHeader.BranchName = comboBoxEditBranch.SelectedItem.ToString();
            wRRHeader.Note = "";
            wRRHeader.TotalQuantity = Decimal.Parse(textEditTotalQuantityByItem.Text);
            wRRHeader.CreatedUserID = WMMessage.User.UserID;
            wRRHeader.CreatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));

            if (await wRRHeaderRepository.Create(wRRHeader) > 0)
            {
                await createWRRDetailAsync();
                WMPublic.sbMessageSaveNewSuccess(this);
            }
        }

        public async Task createWRRDetailAsync()
        {
            int num = gridViewWRRDetail.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewWRRDetail.GetRowCellValue(i, "Barcode") == null)
                {
                    break;
                }
                int rowHandle = gridViewWRRDetail.GetRowHandle(i);
                WRRDetail wRRDetail = new WRRDetail();
                wRRDetail.WRRNumber = textEditWRRNumber.Text;
                wRRDetail.Ordinal = rowHandle + 1;
                wRRDetail.RFIDTagID = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "RFIDTagID");
                wRRDetail.Barcode = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "Barcode");
                wRRDetail.StyleColorSize = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "StyleColorSize");
                wRRDetail.Style = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "Style");
                wRRDetail.Color = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "Color");
                wRRDetail.Sku = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "Sku");
                wRRDetail.Size = (int?)gridViewWRRDetail.GetRowCellValue(rowHandle, "Size");
                wRRDetail.Quantity = (int?)gridViewWRRDetail.GetRowCellValue(rowHandle, "Quantity");
                wRRDetail.Gender = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "Gender");
                wRRDetail.ProductGroup = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "ProductGroup");
                wRRDetail.DescriptionVN = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "DescriptionVN");
                wRRDetail.LifeStylePerformance = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "LifeStylePerformance");
                wRRDetail.Div = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "Div");
                wRRDetail.Outsole = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "Outsole");
                wRRDetail.Category = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "Category");
                wRRDetail.Productline = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "Productline");
                wRRDetail.Description = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "Description");
                wRRDetail.EXWorkUSD = (decimal?)gridViewWRRDetail.GetRowCellValue(rowHandle, "EXWorkUSD");
                wRRDetail.EXWorkVND = (decimal?)gridViewWRRDetail.GetRowCellValue(rowHandle, "EXWorkVND");
                wRRDetail.AmountVND = (decimal?)gridViewWRRDetail.GetRowCellValue(rowHandle, "AmountVND");
                wRRDetail.RetailPrice = (decimal?)gridViewWRRDetail.GetRowCellValue(rowHandle, "RetailPrice");
                wRRDetail.Season = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "Season");
                wRRDetail.Coo = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "Coo");
                wRRDetail.Material = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "Material");
                wRRDetail.InvoiceNo = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "InvoiceNo");
                if (((DateTime?)gridViewWRRDetail.GetRowCellValue(rowHandle, "DateOfInvoice")).ToString() != "")
                {
                    wRRDetail.DateOfInvoice = ((DateTime?)gridViewWRRDetail.GetRowCellValue(rowHandle, "DateOfInvoice"));
                }

                wRRDetail.SaleContract = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "SaleContract");
                wRRDetail.Shipment = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "Shipment");
                wRRDetail.FactoryName = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "FactoryName");
                wRRDetail.FactoryAdress = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "FactoryAdress");
                wRRDetail.FactoryNameVN = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "FactoryNameVN");
                wRRDetail.FactoryAdressVN = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "FactoryAdressVN");
                wRRDetail.NewPrice = (decimal?)gridViewWRRDetail.GetRowCellValue(rowHandle, "NewPrice");
                wRRDetail.ChangePrice = (decimal?)gridViewWRRDetail.GetRowCellValue(rowHandle, "ChangePrice");
                wRRDetail.Status = "0";

                if (await WRRDetailRepository.Create(wRRDetail) > 0)
                {
                    i++;
                }
            }
        }

        public async void createWRDataHeaderAsync()
        {
            WRRHeader wRRHeader = await wRRHeaderRepository.GetUnderID(textEditWRRNumber.Text);
            WRDataHeader wRDataHeader = new WRDataHeader();
            wRDataHeader.WRDNumber = textEditWRRNumber.Text;
            wRDataHeader.WRDDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
            wRDataHeader.WRRNumber = textEditWRRNumber.Text;
            wRDataHeader.BranchID = wRRHeader.BranchID;
            wRDataHeader.BranchName = wRRHeader.BranchName;
            wRDataHeader.HandlingStatusID = "0";
            wRDataHeader.HandlingStatusName = "Chưa duyệt";
            wRDataHeader.Note = "";
            wRDataHeader.Status = "0";
            wRDataHeader.TotalQuantity = 0m;
            wRDataHeader.TotalQuantityOrg = Decimal.Parse(textEditTotalQuantityByItem.Text);
            wRDataHeader.TotalGoods = 0m;
            wRDataHeader.TotalGoodsOrg = Decimal.Parse(textEditTotalQuantityByPack.Text);
            wRDataHeader.CreatedUserID = WMMessage.User.UserID;
            wRDataHeader.CreatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));

            if (await wRDataHeaderRepository.Create(wRDataHeader) > 0)
            {
                //createWRDataDetailAsync();
                createWRDataGeneralAsync();
                MessageBox.Show("Đã tạo dữ liệu nhập");
            }
        }

        //public async Task createWRDataDetailAsync()
        //{
        //    int num = gridViewWRRDetail.RowCount - 1;
        //    int ordinal = 0;
        //    for (int i = 0; i < gridViewWRRDetail.RowCount - 1; i++)
        //    {
        //        Decimal? quantityByPackDecimal = (Decimal?)gridViewWRRDetail.GetRowCellValue(rowHandle, "QuantityByPack");
        //        int quantityByPack = Convert.ToInt32(quantityByPackDecimal);
        //        Decimal? quantityByItemDecimal = (Decimal?)gridViewWRRDetail.GetRowCellValue(rowHandle, "QuantityByItem");
        //        int quantityByItem = Convert.ToInt32(quantityByItemDecimal);
        //        Decimal? packingVolumeDecimal = (Decimal?)gridViewWRRDetail.GetRowCellValue(rowHandle, "PackingVolume");
        //        int packingVolume = Convert.ToInt32(packingVolumeDecimal);
        //        for (int j = 1; j <= quantityByPack; j++)
        //        {
        //            int num2 = i;
        //            int num3 = num;
        //            if (num2 > num3 || gridViewWRRDetail.GetRowCellValue(i, gridViewWRRDetail.Columns["colGoodsID"]) == "")
        //            {
        //                break;
        //            }
        //            WRDataDetail wRDataDetail = new WRDataDetail();
        //            wRDataDetail.IDCode = j + "/" + quantityByPack;
        //            wRDataDetail.WRDNumber = textEditWRRNumber.Text;
        //            wRDataDetail.GoodsID = (string?)gridViewWRRDetail.GetRowCellValue(rowHandle, "GoodsID");
        //            wRDataDetail.GoodsName = (string?)gridViewWRRDetail.GetRowCellValue(rowHandle, "GoodsName");
        //            wRDataDetail.Ordinal = ordinal + 1;
        //            wRDataDetail.LocationID = "";
        //            wRDataDetail.Quantity = 0m;
        //            wRDataDetail.TotalQuantity = 0m;
        //            wRDataDetail.TotalGoods = 0m;
        //            wRDataDetail.PackingQuantity = 0m;
        //            if (quantityByItem % quantityByPack != 0)
        //            {
        //                if (j < quantityByPack)
        //                {
        //                    wRDataDetail.QuantityOrg = packingVolume;
        //                }
        //                else
        //                {
        //                    wRDataDetail.QuantityOrg = quantityByItem % (packingVolume);
        //                }
        //            }
        //            else
        //            {
        //                wRDataDetail.QuantityOrg = quantityByItem / quantityByPack;
        //            }
        //            wRDataDetail.TotalQuantityOrg = Decimal.Parse(textEditTotalQuantityByItem.Text);
        //            List<WRRDetail> wRRDetails = await WRRDetailRepository.GetUnderID(textEditWRRNumber.Text);
        //            wRDataDetail.TotalGoodsOrg = Convert.ToDecimal(wRRDetails.Count);
        //            wRDataDetail.LocationIDOrg = (String?)gridViewWRRDetail.GetRowCellValue(rowHandle, "LocationID");

        //            wRDataDetail.CreatorID = WMMessage.User.UserID; ;
        //            wRDataDetail.CreatedDateTime = DateTime.Now.ToString("yyyy-MM-dd");
        //            wRDataDetail.Status = "1";
        //            wRDataDetail.PackingVolume = (Decimal?)gridViewWRRDetail.GetRowCellValue(rowHandle, "PackingVolume");
        //            wRDataDetail.QuantityByPack = (Decimal?)gridViewWRRDetail.GetRowCellValue(rowHandle, "QuantityByPack");
        //            wRDataDetail.QuantityByItem = (Decimal?)gridViewWRRDetail.GetRowCellValue(rowHandle, "QuantityByItem");
        //            wRDataDetail.Note = "";
        //            wRDataDetail.ScanOption = (Int16?)gridViewWRRDetail.GetRowCellValue(rowHandle, "ScanOption");
        //            wRDataDetail.SupplierCode = (String?)gridViewWRRDetail.GetRowCellValue(rowHandle, "SupplierCode");
        //            wRDataDetail.ASNNumber = (String?)gridViewWRRDetail.GetRowCellValue(rowHandle, "ASNNumber");
        //            wRDataDetail.PackingSlip = (String?)gridViewWRRDetail.GetRowCellValue(rowHandle, "PackingSlip");
        //            await wRDataDetailRepository.Create(wRDataDetail);
        //            ordinal++;
        //        }
        //    }
        //}

        public async void createWRDataGeneralAsync()
        {
            
            int num = gridViewWRRDetail.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewWRRDetail.GetRowCellValue(i, "Barcode") == null)
                {
                    
                    break;
                }
                int rowHandle = gridViewWRRDetail.GetRowHandle(i);

                WRDataGeneral wRDataGeneral = new WRDataGeneral();
                wRDataGeneral.WRDNumber = textEditWRRNumber.Text;
                wRDataGeneral.Ordinal = i+ 1;
                wRDataGeneral.Barcode = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "Barcode");
                wRDataGeneral.StyleColorSize = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "StyleColorSize");
                wRDataGeneral.Style = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "Style");
                wRDataGeneral.Color = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "Color");
                wRDataGeneral.Sku = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "Sku");
                wRDataGeneral.Size = (int?)gridViewWRRDetail.GetRowCellValue(rowHandle, "Size");
                wRDataGeneral.QuantityOrg = (int?)gridViewWRRDetail.GetRowCellValue(rowHandle, "Quantity");
                wRDataGeneral.Quantity = 0;
                wRDataGeneral.Gender = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "Gender");
                wRDataGeneral.ProductGroup = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "ProductGroup");
                wRDataGeneral.DescriptionVN = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "DescriptionVN");
                wRDataGeneral.LifeStylePerformance = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "LifeStylePerformance");
                wRDataGeneral.Div = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "Div");
                wRDataGeneral.Outsole = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "Outsole");
                wRDataGeneral.Category = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "Category");
                wRDataGeneral.Productline = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "Productline");
                wRDataGeneral.Description = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "Description");
                wRDataGeneral.EXWorkUSD = (decimal?)gridViewWRRDetail.GetRowCellValue(rowHandle, "EXWorkUSD");
                wRDataGeneral.EXWorkVND = (decimal?)gridViewWRRDetail.GetRowCellValue(rowHandle, "EXWorkVND");
                wRDataGeneral.AmountVND = (decimal?)gridViewWRRDetail.GetRowCellValue(rowHandle, "AmountVND");
                wRDataGeneral.RetailPrice = (decimal?)gridViewWRRDetail.GetRowCellValue(rowHandle, "RetailPrice");
                wRDataGeneral.Season = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "Season");
                wRDataGeneral.Coo = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "Coo");
                wRDataGeneral.Material = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "Material");
                wRDataGeneral.InvoiceNo = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "InvoiceNo");
                if (((DateTime?)gridViewWRRDetail.GetRowCellValue(rowHandle, "DateOfInvoice")).ToString() != "")
                {
                    wRDataGeneral.DateOfInvoice = ((DateTime?)gridViewWRRDetail.GetRowCellValue(rowHandle, "DateOfInvoice"));
                   
                }

                wRDataGeneral.SaleContract = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "SaleContract");
                wRDataGeneral.Shipment = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "Shipment");
                wRDataGeneral.FactoryName = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "FactoryName");
                wRDataGeneral.FactoryAdress = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "FactoryAdress");
                wRDataGeneral.FactoryNameVN = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "FactoryNameVN");
                wRDataGeneral.FactoryAdressVN = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "FactoryAdressVN");
                wRDataGeneral.NewPrice = (decimal?)gridViewWRRDetail.GetRowCellValue(rowHandle, "NewPrice");
                wRDataGeneral.ChangePrice = (decimal?)gridViewWRRDetail.GetRowCellValue(rowHandle, "ChangePrice");
                wRDataGeneral.Status = "0";
                if (await wRDataGeneralRepository.Create(wRDataGeneral) > 0)
                {
                    i++;
                }
                
            }
        }

        //public async Task createSGDataHeaderAsync()
        //{
        //    WRRHeader wRRHeader = await wRRHeaderRepository.GetUnderID(textEditWRRNumber.Text);
        //    WRDataHeader wRDataHeader = new WRDataHeader();
        //    wRDataHeader.WRDNumber = textEditWRRNumber.Text;
        //    wRDataHeader.WRDDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
        //    wRDataHeader.WRRNumber = textEditWRRNumber.Text;
        //    wRDataHeader.BranchID = wRRHeader.BranchID;
        //    wRDataHeader.BranchName = wRRHeader.BranchName;
        //    wRDataHeader.HandlingStatusID = "0";
        //    wRDataHeader.HandlingStatusName = "Chưa duyệt";
        //    wRDataHeader.Note = "";
        //    wRDataHeader.Status = "0";
        //    wRDataHeader.TotalQuantity = 0m;
        //    wRDataHeader.TotalQuantityOrg = Decimal.Parse(textEditTotalQuantityByItem.Text);
        //    wRDataHeader.CreatedUserID = WMMessage.User.UserID;
        //    wRDataHeader.CreatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));

        //    if (await wRDataHeaderRepository.Create(wRDataHeader) > 0)
        //    {
        //        //createWRDataDetailAsync();
        //        createSGDataGeneralAsync();
        //        MessageBox.Show("Đã tạo dữ liệu nhập");
        //    }
        //}

        //public async Task createSGDataGeneralAsync()
        //{
        //    int num = gridViewWRRDetail.RowCount - 1;
        //    for (int i = 0; i < gridViewWRRDetail.RowCount - 1; i++)
        //    {
        //        int num2 = i;
        //        int num3 = num;
        //        if (num2 > num3 || gridViewWRRDetail.GetRowCellValue(i, gridViewWRRDetail.Columns["colGoodsID"]) == "")
        //        {
        //            break;
        //        }
        //        WRDataGeneral wRDataGeneral = new WRDataGeneral();
        //        wRDataGeneral.IDCode = "";
        //        wRDataGeneral.WRDNumber = textEditWRRNumber.Text;
        //        wRDataGeneral.GoodsID = (string?)gridViewWRRDetail.GetRowCellValue(rowHandle, "GoodsID");
        //        wRDataGeneral.RFIDTagID = (string?)gridViewWRRDetail.GetRowCellValue(rowHandle, "RFIDTagID");
        //        wRDataGeneral.GoodsName = (string?)gridViewWRRDetail.GetRowCellValue(rowHandle, "GoodsName");
        //        wRDataGeneral.Ordinal = i + 1;
        //        wRDataGeneral.LocationID = "";
        //        wRDataGeneral.Quantity = 0m;
        //        wRDataGeneral.TotalQuantity = 0m;
        //        wRDataGeneral.TotalGoods = 0m;
        //        wRDataGeneral.PackingQuantity = 0m;
        //        wRDataGeneral.QuantityOrg = (Decimal?)gridViewWRRDetail.GetRowCellValue(rowHandle, "QuantityByItem");
        //        wRDataGeneral.TotalQuantityOrg = Decimal.Parse(textEditTotalQuantityByItem.Text);

        //        List<WRRDetail> wRRDetails = await WRRDetailRepository.GetUnderID(textEditWRRNumber.Text);
        //        wRDataGeneral.TotalGoodsOrg = Convert.ToDecimal(wRRDetails.Count);
        //        wRDataGeneral.LocationIDOrg = (String?)gridViewWRRDetail.GetRowCellValue(rowHandle, "LocationID");

        //        wRDataGeneral.CreatorID = WMMessage.User.UserID; ;
        //        wRDataGeneral.CreatedDateTime = DateTime.Now.ToString("dd/MM/yyyy");
        //        wRDataGeneral.Status = "1";
        //        wRDataGeneral.PackingVolume = (Decimal?)gridViewWRRDetail.GetRowCellValue(rowHandle, "PackingVolume");
        //        wRDataGeneral.QuantityByPack = (Decimal?)gridViewWRRDetail.GetRowCellValue(rowHandle, "QuantityByPack");
        //        wRDataGeneral.QuantityByItem = (Decimal?)gridViewWRRDetail.GetRowCellValue(rowHandle, "QuantityByItem");
        //        wRDataGeneral.Note = "";
        //        wRDataGeneral.ScanOption = (Int16?)gridViewWRRDetail.GetRowCellValue(rowHandle, "ScanOption");
        //        wRDataGeneral.GoodsGroupID = (string?)gridViewWRRDetail.GetRowCellValue(rowHandle, "GoodsGroupID");
        //        wRDataGeneral.LotID = (string?)gridViewWRRDetail.GetRowCellValue(rowHandle, "LotID");
        //        wRDataGeneral.PalletID = (string?)gridViewWRRDetail.GetRowCellValue(rowHandle, "PalletID");
        //        if (gridViewWRRDetail.GetRowCellValue(rowHandle, "MFGDate") != "")
        //        {
        //            wRDataGeneral.MFGDate = (DateTime?)gridViewWRRDetail.GetRowCellValue(rowHandle, "MFGDate");
        //        }
        //        if (gridViewWRRDetail.GetRowCellValue(rowHandle, "EXPDate") != "")
        //        {
        //            wRDataGeneral.EXPDate = (DateTime?)gridViewWRRDetail.GetRowCellValue(rowHandle, "EXPDate");
        //        }
        //        if (gridViewWRRDetail.GetRowCellValue(rowHandle, "InputDate") != "")
        //        {
        //            wRDataGeneral.InputDate = (DateTime?)gridViewWRRDetail.GetRowCellValue(rowHandle, "InputDate");
        //        }
        //        wRDataGeneral.Remark = (string?)gridViewWRRDetail.GetRowCellValue(rowHandle, "Remark");
        //        await wRDataGeneralRepository.Create(wRDataGeneral);
        //    }
        //}




        public async Task updateWRRHeaderAsync()
        {
            WRRHeader wRRHeader = await wRRHeaderRepository.GetUnderID(textEditWRRNumber.Text);
            wRRHeader.WRRNumber = textEditWRRNumber.Text;
            wRRHeader.WRRDate = DateTime.Parse(dateEditWRRDate.DateTime.ToString("yyyy-MM-dd"));
            wRRHeader.HandlingStatusID = (comboBoxEditHandlingStatus.SelectedIndex - 1).ToString();
            wRRHeader.HandlingStatusName = comboBoxEditHandlingStatus.SelectedItem.ToString();
            wRRHeader.BranchID = $"{int.Parse(comboBoxEditBranch.SelectedIndex.ToString()) - 1:D3}";
            wRRHeader.BranchName = comboBoxEditBranch.SelectedItem.ToString();
            wRRHeader.Note = "";
            wRRHeader.TotalQuantity = Decimal.Parse(textEditTotalQuantityByItem.Text);
            wRRHeader.UpdatedUserID = WMMessage.User.UserID;
            wRRHeader.UpdatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));

            if (await wRRHeaderRepository.Update(wRRHeader, textEditWRRNumber.Text) > 0)
            {
                updateWRRDetailAsync();
                WMPublic.sbMessageSaveChangeSuccess(this);
                if (wRRHeader.HandlingStatusID.Equals("2"))
                {
                    createWRDataHeaderAsync();
                }
            }

        }

        public async Task updateWRRDetailAsync()
        {
            int num = gridViewWRRDetail.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewWRRDetail.GetRowCellValue(i, gridViewWRRDetail.Columns["colBarcode"]) == "")
                {
                    break;
                }
                int rowHandle = gridViewWRRDetail.GetRowHandle(i);
                WRRDetail wRRDetail = new WRRDetail();
                wRRDetail.WRRNumber = textEditWRRNumber.Text;
                wRRDetail.Ordinal = i + 1;
                wRRDetail.RFIDTagID = (string?)gridViewWRRDetail.GetRowCellValue(rowHandle, "RFIDTagID");
                wRRDetail.Barcode = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "Barcode");
                wRRDetail.StyleColorSize = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "StyleColorSize");
                wRRDetail.Style = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "Style");
                wRRDetail.Color = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "Color");
                wRRDetail.Sku = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "Sku");
                wRRDetail.Size = (int?)gridViewWRRDetail.GetRowCellValue(rowHandle, "Size");
                wRRDetail.Quantity = (int?)gridViewWRRDetail.GetRowCellValue(rowHandle, "Quantity");
                wRRDetail.Gender = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "Gender");
                wRRDetail.ProductGroup = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "ProductGroup");
                wRRDetail.DescriptionVN = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "DescriptionVN");
                wRRDetail.LifeStylePerformance = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "LifeStylePerformance");
                wRRDetail.Div = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "Div");
                wRRDetail.Outsole = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "Outsole");
                wRRDetail.Category = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "Category");
                wRRDetail.Productline = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "Productline");
                wRRDetail.Description = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "Description");
                wRRDetail.EXWorkUSD = (decimal?)gridViewWRRDetail.GetRowCellValue(rowHandle, "EXWorkUSD");
                wRRDetail.EXWorkVND = (decimal?)gridViewWRRDetail.GetRowCellValue(rowHandle, "EXWorkVND");
                wRRDetail.AmountVND = (decimal?)gridViewWRRDetail.GetRowCellValue(rowHandle, "AmountVND");
                wRRDetail.RetailPrice = (decimal?)gridViewWRRDetail.GetRowCellValue(rowHandle, "RetailPrice");
                wRRDetail.Season = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "Season");
                wRRDetail.Coo = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "Coo");
                wRRDetail.Material = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "Material");
                wRRDetail.InvoiceNo = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "InvoiceNo");
                wRRDetail.DateOfInvoice = (DateTime?)gridViewWRRDetail.GetRowCellValue(rowHandle, "DateOfInvoice");
                wRRDetail.SaleContract = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "SaleContract");
                wRRDetail.Shipment = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "Shipment");
                wRRDetail.FactoryName = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "FactoryName");
                wRRDetail.FactoryAdress = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "FactoryAdress");
                wRRDetail.FactoryNameVN = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "FactoryNameVN");
                wRRDetail.FactoryAdressVN = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "FactoryAdressVN");
                wRRDetail.NewPrice = (decimal?)gridViewWRRDetail.GetRowCellValue(rowHandle, "NewPrice");
                wRRDetail.ChangePrice = (decimal?)gridViewWRRDetail.GetRowCellValue(rowHandle, "ChangePrice");
                wRRDetail.Status = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "Status");

                if (await WRRDetailRepository.Update(wRRDetail, textEditWRRNumber.Text, wRRDetail.Barcode, wRRDetail.Ordinal) > 0)
                {
                    i++;
                }
            }
        }

        public async Task DeleteWRRHeaderAsync()
        {
            if ((await wRRHeaderRepository.Delete(textEditWRRNumber.Text)) > 0)
            {
                await DeleteWRRDetailAsync();
                sbLoadDataForGridWRRDetailAsync(textEditWRRNumber.Text);
                WMPublic.sbMessageDeleteSuccess(this);
            }
        }

        public async Task DeleteWRRDetailAsync()
        {
            await WRRDetailRepository.Delete(textEditWRRNumber.Text);
            clearAsync();
            gridControlWRRDetail.DataSource = null;
        }

        private void clearAsync()
        {
            textEditWRRNumber.Text = "";
            dateEditWRRDate.Text = DateTime.Now.ToString("dd-MM-yyyy");
            comboBoxEditHandlingStatus.SelectedIndex = 1;
            textEditTotalQuantityByItem.Text = "0";
            textEditTotalQuantityByPack.Text = "0";
            List<WRRDetail> wRRDetails = new List<WRRDetail>();

            for (int i = 0; i < 100; i++)
            {
                wRRDetails.Add(new WRRDetail());
            }
            gridControlWRRDetail.DataSource = wRRDetails;
        }

        private async Task checkExistWRRNumber(String wRRNumber)
        {
            if (!wRRNumber.Equals("") && (await wRRHeaderRepository.checkExistAsync(wRRNumber)) == true)
            {
                isWRRNumberExist = true;
            }
            else
            {
                isWRRNumberExist = false;
            }
        }


        private async void WRRNumber_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                WRRHeader wRRHeader = new WRRHeader();
                wRRHeader = await wRRHeaderRepository.GetUnderID(textEditWRRNumber.Text);
                if (wRRHeader != null)
                {
                    dateEditWRRDate.Text = DateTime.Parse(wRRHeader.WRRDate.ToString()).ToShortDateString();
                    comboBoxEditBranch.SelectedIndex = int.Parse($"{int.Parse(wRRHeader.BranchID) + 1:D1}");
                    comboBoxEditHandlingStatus.SelectedIndex = int.Parse(wRRHeader.HandlingStatusID) + 1;
                    sbLoadDataForGridWRRDetailAsync(textEditWRRNumber.Text);
                }
                else
                {
                    MessageBox.Show("Số phiếu yêu cầu không tồn tại");
                    return;
                }

            }
        }


        private void dataGridViewWRR_CellDoubleClick(object sender, EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView sndr =
                sender as DevExpress.XtraGrid.Views.Grid.GridView;

            DevExpress.Utils.DXMouseEventArgs dxMouseEventArgs =
                e as DevExpress.Utils.DXMouseEventArgs;


            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hitInfo =
                   sndr.CalcHitInfo(dxMouseEventArgs.Location);
            if (hitInfo.InRowCell)
            {
                if (checkRowBeforeNull() == true)
                {
                    MessageBox.Show("Hàng phía trước đang trống");
                    return;
                }
                if (gridViewWRRDetail.FocusedColumn.FieldName.Equals("GoodsID"))
                {
                    frmSearchGoods frmSearchGoods = new frmSearchGoods();
                    frmSearchGoods.isReceipRequisition = true;
                    frmSearchGoods.ShowDialog(this);
                    frmSearchGoods.Dispose();
                    insertData();
                }
            }
        }

        private async void textEditWRRNumer_CellDoubleClick(object sender, EventArgs e)
        {
            frmSearchWRR frmSearchWRR = new frmSearchWRR();
            frmSearchWRR.ShowDialog(this);
            frmSearchWRR.Dispose();
            if (selectedWRRNumber != null)
            {
                textEditWRRNumber.Text = selectedWRRNumber;
                dateEditWRRDate.Text = selectedWRRDate.ToString("dd/MM/yyyy");
                comboBoxEditBranch.SelectedIndex = int.Parse(selectedBranchID);
                comboBoxEditHandlingStatus.SelectedIndex = int.Parse(selectedHandlingID) + 1;
                await sbLoadDataForGridWRRDetailAsync(selectedWRRNumber);
            }
            await checkExistWRRNumber(textEditWRRNumber.Text);

        }

        private void insertData()
        {
            int rowHandle = gridViewWRRDetail.FocusedRowHandle;
            gridViewWRRDetail.SetRowCellValue(rowHandle, gridViewWRRDetail.Columns["GoodsID"], selectedGoodsID);
            gridViewWRRDetail.SetRowCellValue(rowHandle, gridViewWRRDetail.Columns["GoodsName"], selectedGoodsName);
            selectedGoodsID = "";
            selectedGoodsName = "";
        }

        private async Task selectFileExcelAsync()
        {
            OpenFileDialog fdlg = new OpenFileDialog();
            fdlg.Title = "Select file";
            fdlg.InitialDirectory = @"d:\";
            fdlg.Filter = "Excel Sheet(*.xls)|*.xlsx|All Files(*.*)|*.*";
            fdlg.FilterIndex = 1;
            fdlg.RestoreDirectory = true;
            if (fdlg.ShowDialog() == DialogResult.OK)
            {
                List<WRRDetail> wRRDetails = await ExcelToListAsync(fdlg.FileName);
                int mMaxRow = 100;
                if (wRRDetails.Count < mMaxRow)
                {
                    int num = mMaxRow - wRRDetails.Count;
                    int i = 1;
                    while (true)
                    {
                        int num2 = i;
                        int num3 = num;
                        if (num2 > num3)
                        {
                            break;
                        }
                        wRRDetails.Add(new WRRDetail());
                        i++;
                    }
                }
                gridControlWRRDetail.DataSource = wRRDetails;
                getTotalQuantityByItem();
                getTotalQuantityByPack();
                Application.DoEvents();
            }
        }


        private async Task<List<WRRDetail>> ExcelToListAsync(String fileName)
        {
            FileInfo file = new FileInfo(Path.Combine(fileName));

            ExcelPackage.LicenseContext = LicenseContext.Commercial;
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            using (ExcelPackage package = new ExcelPackage(file))
            {

                ExcelWorksheet workSheet = package.Workbook.Worksheets.First();
                dateEditWRRDate.Text = DateTime.Now.ToString("yyyy-MM-dd");

                int totalRows = workSheet.Dimension.Rows;
                List<WRRDetail> wRRDetails = new List<WRRDetail>();

                for (int i = 2; i <= totalRows; i++)
                {
                    try
                    {
                        if (workSheet.Cells[i, 25].Text.Trim().Equals(""))
                        {
                            wRRDetails.Add(new WRRDetail
                            {
                                Barcode = workSheet?.Cells[i, 1].Text.Trim(),
                                StyleColorSize = workSheet?.Cells[i, 2].Text.Trim(),
                                Style = workSheet?.Cells[i, 3].Text.Trim(),
                                Color = workSheet?.Cells[i, 4].Text.Trim(),
                                Sku = workSheet?.Cells[i, 5].Text.Trim(),
                                Size = int.Parse(workSheet?.Cells[i, 6].Text.Trim()),
                                Quantity = int.Parse(workSheet?.Cells[i, 7].Text.Trim()),
                                Gender = workSheet?.Cells[i, 8].Text.Trim(),
                                ProductGroup = workSheet?.Cells[i, 9].Text.Trim(),
                                DescriptionVN = workSheet?.Cells[i, 10].Text.Trim(),
                                LifeStylePerformance = workSheet?.Cells[i, 11].Text.Trim(),
                                Div = workSheet?.Cells[i, 12].Text.Trim(),
                                Outsole = workSheet?.Cells[i, 13].Text.Trim(),
                                Category = workSheet?.Cells[i, 14].Text.Trim(),
                                Productline = workSheet?.Cells[i, 15].Text.Trim(),
                                Description = workSheet?.Cells[i, 16].Text.Trim(),
                                EXWorkUSD = workSheet?.Cells[i, 17].Text.Trim() == "" ? 0m : decimal.Parse(workSheet?.Cells[i, 17].Text.Trim()),
                                EXWorkVND = workSheet?.Cells[i, 18].Text.Trim() == "" ? 0m : decimal.Parse(workSheet?.Cells[i, 18].Text.Trim()),
                                AmountVND = workSheet?.Cells[i, 19].Text.Trim() == "" ? 0m : decimal.Parse(workSheet?.Cells[i, 19].Text.Trim()),
                                RetailPrice = workSheet?.Cells[i, 20].Text.Trim() == "" ? 0m : decimal.Parse(workSheet?.Cells[i, 20].Text.Trim()),
                                Season = workSheet?.Cells[i, 21].Text.Trim(),
                                Coo = workSheet?.Cells[i, 22].Text.Trim(),
                                Material = workSheet?.Cells[i, 23].Text.Trim(),
                                InvoiceNo = workSheet?.Cells[i, 24].Text.Trim(),
                                SaleContract = workSheet?.Cells[i, 26].Text.Trim(),
                                Shipment = workSheet?.Cells[i, 27].Text.Trim(),
                                FactoryName = workSheet?.Cells[i, 28].Text.Trim(),
                                FactoryAdress = workSheet?.Cells[i, 29].Text.Trim(),
                                FactoryNameVN = workSheet?.Cells[i, 30].Text.Trim(),
                                FactoryAdressVN = workSheet?.Cells[i, 31].Text.Trim(),
                                NewPrice = workSheet?.Cells[i, 32].Text.Trim() == "" ? 0m : decimal.Parse(workSheet?.Cells[i, 32].Text.Trim()),
                                ChangePrice = workSheet?.Cells[i, 33].Text.Trim() == "" ? 0m : decimal.Parse(workSheet?.Cells[i, 33].Text.Trim()),
                                Status = "0"
                            });
                        }
                        else
                        {
                            wRRDetails.Add(new WRRDetail
                            {
                                Barcode = workSheet?.Cells[i, 1].Text.Trim(),
                                StyleColorSize = workSheet?.Cells[i, 2].Text.Trim(),
                                Style = workSheet?.Cells[i, 3].Text.Trim(),
                                Color = workSheet?.Cells[i, 4].Text.Trim(),
                                Sku = workSheet?.Cells[i, 5].Text.Trim(),
                                Size = int.Parse(workSheet?.Cells[i, 6].Text.Trim()),
                                Quantity = int.Parse(workSheet?.Cells[i, 7].Text.Trim()),
                                Gender = workSheet?.Cells[i, 8].Text.Trim(),
                                ProductGroup = workSheet?.Cells[i, 9].Text.Trim(),
                                DescriptionVN = workSheet?.Cells[i, 10].Text.Trim(),
                                LifeStylePerformance = workSheet?.Cells[i, 11].Text.Trim(),
                                Div = workSheet?.Cells[i, 12].Text.Trim(),
                                Outsole = workSheet?.Cells[i, 13].Text.Trim(),
                                Category = workSheet?.Cells[i, 14].Text.Trim(),
                                Productline = workSheet?.Cells[i, 15].Text.Trim(),
                                Description = workSheet?.Cells[i, 16].Text.Trim(),
                                EXWorkUSD = workSheet?.Cells[i, 17].Text.Trim() == "" ? 0m : decimal.Parse(workSheet?.Cells[i, 17].Text.Trim()),
                                EXWorkVND = workSheet?.Cells[i, 18].Text.Trim() == "" ? 0m : decimal.Parse(workSheet?.Cells[i, 18].Text.Trim()),
                                AmountVND = workSheet?.Cells[i, 19].Text.Trim() == "" ? 0m : decimal.Parse(workSheet?.Cells[i, 19].Text.Trim()),
                                RetailPrice = workSheet?.Cells[i, 20].Text.Trim() == "" ? 0m : decimal.Parse(workSheet?.Cells[i, 20].Text.Trim()),
                                Season = workSheet?.Cells[i, 21].Text.Trim(),
                                Coo = workSheet?.Cells[i, 22].Text.Trim(),
                                Material = workSheet?.Cells[i, 23].Text.Trim(),
                                InvoiceNo = workSheet?.Cells[i, 24].Text.Trim(),
                                DateOfInvoice = DateTime.ParseExact(workSheet?.Cells[i, 25].Text.Trim(), "dd/MM/yyyy", new CultureInfo("vi-VI")),
                                SaleContract = workSheet?.Cells[i, 26].Text.Trim(),
                                Shipment = workSheet?.Cells[i, 27].Text.Trim(),
                                FactoryName = workSheet?.Cells[i, 28].Text.Trim(),
                                FactoryAdress = workSheet?.Cells[i, 29].Text.Trim(),
                                FactoryNameVN = workSheet?.Cells[i, 30].Text.Trim(),
                                FactoryAdressVN = workSheet?.Cells[i, 31].Text.Trim(),
                                NewPrice = workSheet?.Cells[i, 32].Text.Trim() == "" ? 0m : decimal.Parse(workSheet?.Cells[i, 32].Text.Trim()),
                                ChangePrice = workSheet?.Cells[i, 33].Text.Trim() == "" ? 0m : decimal.Parse(workSheet?.Cells[i, 33].Text.Trim()),
                                Status="0"
                            });
                        }


                    }
                    catch (Exception) { }
                }
                return wRRDetails;
            }
        }


        //barbutton click
        public async void button_Click(object sender, EventArgs e)
        {

            ButtonEventArgs eArg = (DevExpress.XtraBars.Docking2010.ButtonEventArgs)e;
            String tag = eArg.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "new":
                    if (WMPublic.sbMessageAddNewRequest(this) == true)
                    {
                        getlastWRRNumberAsync();
                        clearAsync();
                    }
                    break;
                case "save":
                    await checkExistWRRNumber(textEditWRRNumber.Text);

                    if (isWRRNumberExist == true)
                    {
                        if (WMPublic.sbMessageSaveChangeRequest(this) == true)
                        {
                            updateWRRHeaderAsync();
                        }
                    }
                    else
                    {
                        if (WMPublic.sbMessageSaveNewRequest(this) == true)
                        {
                            
                            createWRRHeaderAsync();
                        }
                    }
                    break;
                case "delete":
                    if (WMPublic.sbMessageDeleteRequest(this) == true)
                    {

                        DeleteWRRHeaderAsync();
                    }
                    break;
                case "search":
                    break;
                case "refesh":
                    sbLoadDataForGridWRRDetailAsync(textEditWRRNumber.Text);
                    break;
                case "import":
                    await selectFileExcelAsync();
                    break;
                case "export":
                    break;
                case "print":
                    break;
                case "close":
                    break;
            }

        }

        private void simpleButtonRead_Click(object sender, EventArgs e)
        {

            m_ReadTagForm.ShowDialog(this);
            
        }

        private void simpleButtonWriteTag_Click(object sender, EventArgs e)
        {
            m_WriteTagForm.ShowDialog(this);
            
        }

        private void gridViewWRRDetail_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            int i = 0;
            i = e.RowHandle;
            if ((i >= 0) && (String.Compare(e.Column.FieldName, "No", false) == 0))
            {
                string barcode = ((gridViewWRRDetail.GetRowCellValue(i, gridViewWRRDetail.Columns["Barcode"]) == "") ? "" : Convert.ToString(gridViewWRRDetail.GetRowCellValue(i, gridViewWRRDetail.Columns["Barcode"])));
                e.Appearance.Font = new Font("Tahoma", 8f, FontStyle.Bold);
                foreach (string s in m_MainForm.tagIDs)
                {
                    if (barcode.Equals(s.Substring(s.Length-17,12)))
                    {
                        e.Appearance.BackColor = Helpers.ColorHelper.GetSystemDrawingColorFromHexString("#FF99FF");
                    }
                }
            }
        }

        

        private void GridView1_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            if (e.Menu == null && e.MenuType == GridMenuType.User && e.HitInfo.HitTest == GridHitTest.EmptyRow)
            {
                return;
            }
            if (e.MenuType == DevExpress.XtraGrid.Views.Grid.GridMenuType.Row)
            {
                string barcode = (string)(sender as GridView).GetFocusedRowCellValue("Barcode");
                int qty = (int)(sender as GridView).GetFocusedRowCellValue("Quantity");
                string qtyEncoded = (string)(sender as GridView).GetFocusedRowCellValue("Status");
                DXMenuItem item = new DXMenuItem(barcode);
                item.Click += (o, args) => {
                    m_EncodeForm.Barcode = barcode;
                    m_EncodeForm.Quantity =qty;
                    m_EncodeForm.QuantityEncoded =int.Parse(qtyEncoded);
                    m_EncodeForm.ShowDialog(this);
                    
                };
                e.Menu.Items.Add(item);
                
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            m_EncodeForm.ShowDialog(this);
        }

        private void StartRead()
        {
            m_MainForm.readRDButton_Click();
            simpleButtonStart.Enabled = false;
            simpleButtonStop.Enabled = true;
        }

        private void StopRead()
        {
            m_MainForm.readRDButton_Click();
            simpleButtonStart.Enabled =true;
            simpleButtonStop.Enabled =false;
        }
    }
}
