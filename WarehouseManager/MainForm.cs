﻿using DevExpress.XtraBars;
using DevExpress.XtraTab;
using DevExpress.XtraTab.ViewInfo;
using Symbol.RFID3;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls.Ribbon;
using System.Windows.Forms;
using WarehouseManager.Helpers;
using WarehouseManager.Repository;
using WarehouseManager.Models;

namespace WarehouseManager
{
    public partial class MainForm : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        const int LEADING_SPACE = 12;
        const int CLOSE_SPACE = 15;
        const int CLOSE_AREA = 15;
        String tabUserManagerText = "";

        public Symbol.RFID3.RFIDReader m_ReaderAPI;
        internal bool m_IsConnected;
        internal AccessOperationResult m_AccessOpResult;

        internal string m_SelectedTagID = null;
        internal TagAccess.ReadAccessParams m_ReadParams = null;
        private bool m_IsBlockWrite;
        internal TagAccess.WriteAccessParams m_WriteParams;

        private delegate void UpdateStatus(Events.StatusEventData eventData);
        private UpdateStatus m_UpdateStatusHandler = null;
        private delegate void UpdateRead(Events.ReadEventData eventData);
        private UpdateRead m_UpdateReadHandler = null;
        private TagData m_ReadTag = null;
        private Hashtable m_TagTable;
        private uint m_TagTotalCount;


        internal ReadForm m_ReadForm = null;
        internal ConnectionForm m_ConnectionForm = null;
        internal ReceiptDataControl m_ReceiptDataControl = null;
        internal ReceiptRequisitionControl m_ReceiptRequisitionControl = null;
        internal ReaderConfigForm m_ReaderConfigForm = null;

        RFIDReaderRepository rFIDReaderRepository;
        RFIDSignalRepository rFIDSignalRepository;
        bool isReaderExist = false;

        public List<string> tagIDs = null;
        public String tagIDRD = "";
        public string readData = "";

        private Symbol.RFID3.TriggerInfo m_TriggerInfo = null;
        public List<string> tagList;




        public MainForm()
        {
            InitializeComponent();

            this.connectBackgroundWorker.WorkerReportsProgress = true;
            this.connectBackgroundWorker.WorkerSupportsCancellation = true;
            this.connectBackgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.connectBackgroundWorker_DoWork);
            this.connectBackgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.connectBackgroundWorker_RunWorkerCompleted);
            this.connectBackgroundWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.connectBackgroundWorker_ProgressChanged);

            this.accessBackgroundWorker.WorkerReportsProgress = true;
            this.accessBackgroundWorker.WorkerSupportsCancellation = true;
            this.accessBackgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.accessBackgroundWorker_DoWork);
            this.accessBackgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.accessBackgroundWorker_RunWorkerCompleted);
            this.accessBackgroundWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.accessBackgroundWorker_ProgressChanged);

            m_ReadForm = new ReadForm(this);
            m_ConnectionForm = new ConnectionForm(this);
            m_ReceiptDataControl = new ReceiptDataControl(this);
            m_ReceiptRequisitionControl = new ReceiptRequisitionControl(this);
            m_ReaderConfigForm = new ReaderConfigForm(this);



            m_UpdateStatusHandler = new UpdateStatus(myUpdateStatus);
            m_AccessOpResult = new AccessOperationResult();
            m_IsConnected = false;

            //read
            m_ReadParams = new TagAccess.ReadAccessParams();
            m_ReadParams.MemoryBank = MEMORY_BANK.MEMORY_BANK_EPC;
            m_ReadParams.AccessPassword = 0;
            m_ReadParams.ByteOffset = 0;
            m_ReadParams.ByteCount = 0;

            //write
            m_WriteParams = new TagAccess.WriteAccessParams();
            m_WriteParams.MemoryBank = MEMORY_BANK.MEMORY_BANK_EPC;
            m_WriteParams.AccessPassword = 0;
            m_WriteParams.ByteOffset = 0;
            m_WriteParams.WriteDataLength = 0;

            m_ReadTag = new Symbol.RFID3.TagData();
            m_UpdateReadHandler = new UpdateRead(myUpdateRead);
            m_TagTable = new Hashtable();
            m_IsConnected = false;
            m_TagTotalCount = 0;


            this.Load += MainForm_Load;
            updateLanguage();
            this.WindowState = FormWindowState.Maximized;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            this.Dock = DockStyle.Fill;

            ribbon.ItemClick += barButtonItem_ItemClick;
            xtraTabControl1.CloseButtonClick += closeButtonTabpage_Click;

            tagIDs = new List<string>();
        }

        private void MainForm_Load(Object sender, EventArgs e)
        {
            barHeaderUserInfo.Caption = WMMessage.User.LoginName;
            rFIDReaderRepository = new RFIDReaderRepository();
            rFIDSignalRepository = new RFIDSignalRepository();
        }

        public Symbol.RFID3.TriggerInfo getTriggerInfo()
        {
            if (null == m_TriggerInfo)
            {
                m_TriggerInfo = new TriggerInfo();
                fetchTriggerParams();
            }
            else
            {
            }
            m_TriggerInfo.EnableTagEventReport = true;
            return m_TriggerInfo;
        }

        private void fetchTriggerParams()
        {
            if (null == m_TriggerInfo)
            {
                m_TriggerInfo = new TriggerInfo();
            }

            if (m_ReaderAPI.ReaderCapabilities.IsTagEventReportingSupported)
            {
                m_TriggerInfo.TagEventReportInfo.ReportNewTagEvent = (TAG_EVENT_REPORT_TRIGGER)2;
                m_TriggerInfo.TagEventReportInfo.ReportTagBackToVisibilityEvent = (TAG_EVENT_REPORT_TRIGGER)2;
                m_TriggerInfo.TagEventReportInfo.ReportTagInvisibleEvent = (TAG_EVENT_REPORT_TRIGGER)2;
                m_TriggerInfo.TagEventReportInfo.ReportTagMovingEvent = (TAG_MOVING_EVENT_REPORT)0;
                m_TriggerInfo.TagEventReportInfo.NewTagEventModeratedTimeoutMilliseconds = ushort.Parse("500");
                m_TriggerInfo.TagEventReportInfo.TagBackToVisibilityModeratedTimeoutMilliseconds = ushort.Parse("500");
                m_TriggerInfo.TagEventReportInfo.TagInvisibleEventModeratedTimeoutMilliseconds = ushort.Parse("500");
                m_TriggerInfo.TagEventReportInfo.TagStationaryModeratedTimeoutMilliseconds = ushort.Parse("500");
            }
            m_TriggerInfo.TagReportTrigger = uint.Parse("1");
            m_TriggerInfo.ReportTriggers.Period = uint.Parse("0");
        }

        public string Status
        {
            get
            {
                return ConnectStatusHeader.Caption;
            }
            set { ConnectStatusHeader.Caption = value; }
        }

        private void updateLanguage()
        {
            ResourceManager rm;
            String cultureName = WMMessage.msgLanguage;
            if (cultureName.Equals("vi-VN"))
            {
                rm = new
                   ResourceManager("WarehouseManager.Resource.vi_local", typeof(MainForm).Assembly);
            }
            else
            {
                rm = new
                    ResourceManager("WarehouseManager.Resource.en_local", typeof(MainForm).Assembly);
            }

            // LoginForm.ActiveForm.Text = rm.GetString("login_form");
            ribbonPageSystem.Text = rm.GetString("system");
            ribbonPageCategory.Text = rm.GetString("category");
            ribbonPageWarehouse.Text = rm.GetString("warehouse");
            ribbonPageHelp.Text = rm.GetString("help");
            barButtonItemVietnam.Caption = rm.GetString("english");
            barButtonItemEnglish.Caption = rm.GetString("vietnamese");
            ribbonPageGroupUser.Text = rm.GetString("user");
            ribbonPageGroupUserManager.Text = rm.GetString("user_manager");
            ribbonPageGroupGoodsManager.Text = rm.GetString("goods_manager");
            barButtonItemLogin.Caption = rm.GetString("login");
            barButtonItemLogout.Caption = rm.GetString("logout");
            barButtonItemChangePass.Caption = rm.GetString("change_password");
            barButtonItemAddUser.Caption = rm.GetString("user_role");
            barButtonItemGoods.Caption = rm.GetString("goods");
            barButtonItemGoodsLine.Caption = rm.GetString("goods_line");
            barButtonItemGoodsType.Caption = rm.GetString("goods_type");
            barButtonItemGoodsCategory.Caption = rm.GetString("goods_category");
            tabUserManagerText = rm.GetString("user_manager");
        }

        void barButtonItemLogin_ItemClick(object sender, ItemClickEventArgs e)
        {
            LoginForm loginForm = new LoginForm();

            loginForm.Show();
        }

        void barButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            String tag = e.Item.Name;

            switch (tag)
            {
                case "barButtonItemAddUser":
                    /* Navigate to page A */
                    WMPublic.TabCreating(xtraTabControl1, tabUserManagerText, "tabUserManager", new UserManagerControl());
                    break;
                case "barButtonItemLogin":
                    /* Navigate to page B */
                    LoginForm loginForm = new LoginForm();
                    loginForm.Show();
                    break;
                case "barButtonItemLogout":
                    /* Navigate to page C*/
                    LoginForm loginForm2 = new LoginForm();
                    loginForm2.Show();
                    break;
                case "barButtonItemGoods":
                    /* Navigate to page D */
                    WMPublic.TabCreating(xtraTabControl1, "Quản lý hàng hóa", "tabGoods", new GoodManagerControl());
                    break;
                case "barButtonItemGoodsType":
                    /* Navigate to page E */
                    WMPublic.TabCreating(xtraTabControl1, "Quản lý loại hàng hóa", "tabGoodsType", new GoodTypeControl());
                    break;
                case "barButtonItemGoodsLine":
                    /* Navigate to page E */
                    WMPublic.TabCreating(xtraTabControl1, "Quản lý ngành hàng", "tabGoodsLine", new GoodLineControl());
                    break;
                case "barButtonItemReceiptRequisition":
                    /* Navigate to page E */
                    WMPublic.TabCreating(xtraTabControl1, "Yêu cầu nhập kho", "tabReceiptRequisition", m_ReceiptRequisitionControl);
                    break;
                //case "barButtonItemReceiptData":
                //    /* Navigate to page E */
                //    WMPublic.TabCreating(xtraTabControl1, "Dữ liệu nhập", "tabReceiptData", new ReceiptDataControl());
                //    break;
                case "barButtonItemReceiptData":
                    /* Navigate to page E */
                    WMPublic.TabCreating2(xtraTabControl1, "CONVEYANCE CONTENS LIST", "tabReceiptData", new ReceiptDataControl(this));
                    break;
                case "barButtonItemIssueRequisition":
                    /* Navigate to page E */
                    WMPublic.TabCreating(xtraTabControl1, "Yêu cầu xuất kho", "tabIssueRequisition", new IssueRequisitionControl());
                    break;
                case "barButtonItemIssueData":
                    /* Navigate to page E */
                    WMPublic.TabCreating(xtraTabControl1, "Dữ liệu xuất", "tabIssueData", new IssueDataControl());
                    break;
                case "barButtonItemWarehouseReceipt":
                    /* Navigate to page E */
                    WMPublic.TabCreating(xtraTabControl1, "Phiếu nhập kho", "tabWarehouseReceipt", new WarehouseReceiptControl());
                    break;
                case "barButtonItemProcessInventory":
                    /* Navigate to page E */
                    WMPublic.TabCreating(xtraTabControl1, "Xử lý tồn kho", "tabProcessInventory", new InventoryProcessControl());
                    break;
                case "barButtonItemIssueOrder":
                    /* Navigate to page E */
                    WMPublic.TabCreating(xtraTabControl1, "Phiếu xuất kho", "tabIssueOrder", new IssueOrderControl());
                    break;
                    break;
                case "barButtonItemReport":
                    /* Navigate to page E */
                    WMPublic.TabCreating(xtraTabControl1, "Báo cáo", "tabReport", new ReportControl());
                    break;
                case "barButtonItemColor":
                    /* Navigate to page E */
                    WMPublic.TabCreating(xtraTabControl1, "Màu sắc", "tabColor", new ColorControl());
                    break;
                case "barButtonItemUnit":
                    /* Navigate to page E */
                    WMPublic.TabCreating(xtraTabControl1, "Đơn vị", "tabUnit", new UnitControl());
                    break;
                case "barButtonItemTallySheet":
                    /* Navigate to page E */
                    WMPublic.TabCreating(xtraTabControl1, "Phiếu kiểm kê", "tabTallSheet", new TallySheetControl());
                    break;
                case "barButtonItemProcessTally":
                    /* Navigate to page E */
                    WMPublic.TabCreating(xtraTabControl1, "Xử lý kiểm kê", "tabProcessTally", new ProcesTallyControl());
                    break;
                case "barButtonItemBackupRestore":
                    /* Navigate to page E */
                    WMPublic.TabCreating(xtraTabControl1, "Lưu trữ và phục hồi", "tabBackupRestore", new BackupRestoreDatabaseControl());
                    break;
                case "barButtonItemReadForm":
                    /* Navigate to page E */
                    WMPublic.TabCreating(xtraTabControl1, "Inventory RFID", "tabInventoryRFID", m_ReadForm);
                    break;
                case "barButtonItemRFIDReaderManager":
                    /* Navigate to page E */
                    WMPublic.TabCreating(xtraTabControl1, "RFID Reader Manager", "tabRFIDReaderManager", new RFIDReaderManagerControl());
                    break;
                case "barButtonItemReaderConfig":
                    m_ReaderConfigForm.ShowDialog();
                    break;
            }
        }

        private void closeButtonTabpage_Click(object sender, EventArgs e)
        {
            ClosePageButtonEventArgs EArg = (DevExpress.XtraTab.ViewInfo.ClosePageButtonEventArgs)e;
            string name = EArg.Page.Text;//Get the text of the closed tab
            foreach (XtraTabPage page in xtraTabControl1.TabPages)//Traverse to get the same text as the closed tab
            {

                if (page.Text == name)
                {
                    if (WMPublic.sbMessageCloseTab(this) == true)
                    {
                        if (page.Name == "tabInventoryRFID")
                        {
                            xtraTabControl1.TabPages.Remove(page);
                            return;
                        }
                        else
                        {
                            xtraTabControl1.TabPages.Remove(page);
                            //page.Dispose();
                            return;
                        }
                    }
                }
            }
        }

        private void barButtonItemVietnam_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (xtraTabControl1.TabPages.Count > 0)
            {
                WMPublic.sbMessageCloseTabpageRequest(this);
            }
            else
            {
                WMMessage.msgLanguage = "vi-VN";
                ResourceManager resourceManager = new ResourceManager("WarehouseManager.Resource.vi_local", Assembly.GetExecutingAssembly());
                updateLanguage();
            }
        }

        private void barButtonItemEnglish_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (xtraTabControl1.TabPages.Count > 0)
            {
                WMPublic.sbMessageCloseTabpageRequest(this);
            }
            else
            {
                WMMessage.msgLanguage = "en-US";
                ResourceManager resourceManager = new ResourceManager("WarehouseManager.Resource.en_local", Assembly.GetExecutingAssembly());
                updateLanguage();
            }
        }

        private void barButtonItemConnectReader_ItemClick(object sender, ItemClickEventArgs e)
        {
            m_ConnectionForm.ShowDialog();
        }



        internal class AccessOperationResult
        {
            public RFIDResults m_Result;
            public String m_VendorMessage;
            public String m_StatusDescription;
            public ACCESS_OPERATION_CODE m_OpCode;

            public AccessOperationResult()
            {
                m_Result = RFIDResults.RFID_NO_ACCESS_IN_PROGRESS;
                m_OpCode = ACCESS_OPERATION_CODE.ACCESS_OPERATION_READ;
            }
        }

        private void myUpdateStatus(Events.StatusEventData eventData)
        {
            switch (eventData.StatusEventType)
            {
                case Symbol.RFID3.Events.STATUS_EVENT_TYPE.INVENTORY_START_EVENT:
                    StatusBottom.Caption = "Inventory started";
                    break;
                case Symbol.RFID3.Events.STATUS_EVENT_TYPE.INVENTORY_STOP_EVENT:
                    StatusBottom.Caption = "Inventory stopped";
                    break;
                case Symbol.RFID3.Events.STATUS_EVENT_TYPE.ACCESS_START_EVENT:
                    StatusBottom.Caption = "Access Operation started";
                    break;
                case Symbol.RFID3.Events.STATUS_EVENT_TYPE.ACCESS_STOP_EVENT:
                    StatusBottom.Caption = "Access Operation stopped";

                    if (this.m_SelectedTagID == string.Empty)
                    {
                        uint successCount, failureCount;
                        successCount = failureCount = 0;
                        m_ReaderAPI.Actions.TagAccess.GetLastAccessResult(ref successCount, ref failureCount);
                        StatusBottom.Caption = "Access completed - Success Count: " + successCount.ToString()
                            + ", Failure Count: " + failureCount.ToString();
                    }
                    resetButtonState();
                    break;
                case Symbol.RFID3.Events.STATUS_EVENT_TYPE.BUFFER_FULL_WARNING_EVENT:
                    StatusBottom.Caption = " Buffer full warning";
                    myUpdateRead(null);
                    break;
                case Symbol.RFID3.Events.STATUS_EVENT_TYPE.BUFFER_FULL_EVENT:
                    StatusBottom.Caption = "Buffer full";
                    myUpdateRead(null);
                    break;
                case Symbol.RFID3.Events.STATUS_EVENT_TYPE.DISCONNECTION_EVENT:
                    StatusBottom.Caption = "Disconnection Event " + eventData.DisconnectionEventData.DisconnectEventInfo.ToString();
                    connectBackgroundWorker.RunWorkerAsync("Disconnect");
                    break;
                case Symbol.RFID3.Events.STATUS_EVENT_TYPE.ANTENNA_EVENT:
                    StatusBottom.Caption = "Antenna Status Update";
                    break;
                case Symbol.RFID3.Events.STATUS_EVENT_TYPE.READER_EXCEPTION_EVENT:
                    StatusBottom.Caption = "Reader ExceptionEvent " + eventData.ReaderExceptionEventData.ReaderExceptionEventInfo;
                    break;
                default:
                    break;
            }
        }

        private void myUpdateRead(Events.ReadEventData eventData)
        {

            ListViewItem item;

            Symbol.RFID3.TagData[] tagData = m_ReaderAPI.Actions.GetReadTags(1000);
            if (tagData != null)
            {
                for (int nIndex = 0; nIndex < tagData.Length; nIndex++)
                {
                    if (tagData[nIndex].OpCode == ACCESS_OPERATION_CODE.ACCESS_OPERATION_NONE ||
                        (tagData[nIndex].OpCode == ACCESS_OPERATION_CODE.ACCESS_OPERATION_READ &&
                        tagData[nIndex].OpStatus == ACCESS_OPERATION_STATUS.ACCESS_SUCCESS))
                    {
                        Symbol.RFID3.TagData tag = tagData[nIndex];
                        string tagID = tag.TagID;
                        bool isFound = false;


                        lock (m_TagTable.SyncRoot)
                        {
                            isFound = m_TagTable.ContainsKey(tagID);
                            if (!isFound)
                            {
                                tagID = tag.TagID + ((uint)tag.MemoryBank + tag.MemoryBankDataOffset);
                                isFound = m_TagTable.ContainsKey(tagID);
                            }
                        }

                        if (isFound)
                        {

                            uint count = 0;
                            item = (ListViewItem)m_TagTable[tagID];
                            try
                            {
                                count = uint.Parse(item.SubItems[2].Text) + tagData[nIndex].TagSeenCount;
                                m_TagTotalCount += tagData[nIndex].TagSeenCount;
                            }
                            catch (FormatException fe)
                            {
                                StatusBottom.Caption = fe.Message;
                                break;
                            }

                            item.SubItems[8].Text = "Sổ tay Hà Phan";

                            item.SubItems[1].Text = tag.AntennaID.ToString();

                            item.SubItems[2].Text = count.ToString();

                            item.SubItems[3].Text = tag.PeakRSSI.ToString();

                            string memoryBank = tag.MemoryBank.ToString();

                            int index = memoryBank.LastIndexOf('_');
                            if (index != -1)
                            {
                                memoryBank = memoryBank.Substring(index + 1);
                            }
                            if (tag.MemoryBankData.Length > 0 && !memoryBank.Equals(item.SubItems[5].Text))
                            {
                                item.SubItems[5].Text = tag.MemoryBankData;
                                item.SubItems[6].Text = memoryBank;
                                item.SubItems[7].Text = tag.MemoryBankDataOffset.ToString();


                                lock (m_TagTable.SyncRoot)
                                {
                                    m_TagTable.Remove(tagID);
                                    m_TagTable.Add(tag.TagID + tag.MemoryBank.ToString()
                                        + tag.MemoryBankDataOffset.ToString(), item);
                                }
                            }
                            item.SubItems[9].Text = getTagEvent(tag);

                        }
                        else
                        {
                            item = new ListViewItem(tag.TagID);
                            ListViewItem.ListViewSubItem subItem;

                            //
                            subItem = new ListViewItem.ListViewSubItem(item, tag.AntennaID.ToString());
                            item.SubItems.Add(subItem);

                            //
                            subItem = new ListViewItem.ListViewSubItem(item, tag.TagSeenCount.ToString());
                            m_TagTotalCount += tag.TagSeenCount;
                            item.SubItems.Add(subItem);

                            subItem = new ListViewItem.ListViewSubItem(item, tag.PeakRSSI.ToString());
                            item.SubItems.Add(subItem);

                            subItem = new ListViewItem.ListViewSubItem(item, tag.PC.ToString("X"));
                            item.SubItems.Add(subItem);

                            subItem = new ListViewItem.ListViewSubItem(item, "");
                            item.SubItems.Add(subItem);

                            subItem = new ListViewItem.ListViewSubItem(item, "");
                            item.SubItems.Add(subItem);

                            subItem = new ListViewItem.ListViewSubItem(item, "");
                            item.SubItems.Add(subItem);


                            subItem = new ListViewItem.ListViewSubItem(item, "Sổ tay Hà Phan");
                            item.SubItems.Add(subItem);

                            //
                            subItem = new ListViewItem.ListViewSubItem(item, getTagEvent(tag));
                            item.SubItems.Add(subItem);

                            m_ReadForm.inventoryList.BeginUpdate();
                            m_ReadForm.inventoryList.Items.Add(item);
                            m_ReadForm.inventoryList.EndUpdate();

                            lock (m_TagTable.SyncRoot)
                            {
                                m_TagTable.Add(tagID, item);
                            }
                            tagIDs.Clear();
                            tagIDs.Add(tagID.Substring(0, tagID.Length - 1));
                        }
                    }
                }


                m_ReadForm.textEditTotalTag.Text = m_TagTable.Count + "(" + m_TagTotalCount + ")";
                m_ReadForm.textEditTotalTag.Update();
                m_ReceiptRequisitionControl.TotalTag = m_TagTable.Count + "";

                tagList = new List<string>();
                List<string> tagEventList = new List<string>();
                List<TagReceiver> tagReceivers = new List<TagReceiver>();



                foreach (string s in m_TagTable.Keys)
                {
                    tagList.Add(s.Substring(0, s.Length - 1));
                    ListViewItem listViewItem = (ListViewItem)m_TagTable[s];
                    tagEventList.Add(listViewItem.SubItems[9].Text);

                    TagReceiver tagReceiver = new TagReceiver();
                    tagReceiver.TagID = s.Substring(0, s.Length - 1);
                    tagReceiver.TagEvent = listViewItem.SubItems[9].Text;
                    tagReceivers.Add(tagReceiver);
                }
                m_ReceiptRequisitionControl.listTag = tagList;
                m_ReceiptRequisitionControl.listTagEvent = tagEventList;

                if (tagReceivers.Count > 0)
                {
                    TagReceiver tagReceiver1 = tagReceivers.Where(s => !s.TagEvent.Contains("Gone")).FirstOrDefault();
                    if (tagReceiver1 != null)
                    {
                        m_ReceiptRequisitionControl.TagID = tagReceiver1.TagID;
                        m_ReceiptRequisitionControl.TagEvent = tagReceiver1.TagEvent;
                    }
                }

                m_ReceiptRequisitionControl.gridEncode = tagReceivers;

            }
        }

        private void connectBackgroundWorker_DoWork(object sender, DoWorkEventArgs workEventArgs)
        {
            connectBackgroundWorker.ReportProgress(0, workEventArgs.Argument);

            if ((string)workEventArgs.Argument == "Connect")
            {
                m_ReaderAPI = new Symbol.RFID3.RFIDReader(m_ConnectionForm.IpText, uint.Parse(m_ConnectionForm.PortText), 0);
                try
                {
                    m_ReaderAPI.Connect();
                    m_IsConnected = true;
                    workEventArgs.Result = "Connect Succeed";
                    StatusBottom.Caption = "Connect Succeed";
                    //Logger.WriteLogger("Connected");

                }
                catch (OperationFailureException operationException)
                {
                    workEventArgs.Result = operationException.StatusDescription;
                }
                catch (Exception ex)
                {
                    workEventArgs.Result = ex.Message;
                }
            }
            else if ((string)workEventArgs.Argument == "Disconnect")
            {
                try
                {

                    m_ReaderAPI.Disconnect();
                    m_IsConnected = false;
                    workEventArgs.Result = "Disconnect Succeed";
                    StatusBottom.Caption = "Disconnect Succeed";
                    m_ReaderAPI = null;
                    //Logger.WriteLogger("DisConnected");

                }
                catch (OperationFailureException ofe)
                {
                    workEventArgs.Result = ofe.Result;
                }
            }

        }

        private void connectBackgroundWorker_ProgressChanged(object sender,
            ProgressChangedEventArgs progressEventArgs)
        {
            m_ConnectionForm.simpleButtonConnect.Enabled = false;
        }

        private void connectBackgroundWorker_RunWorkerCompleted(object sender,
            RunWorkerCompletedEventArgs connectEventArgs)
        {
            if (m_ConnectionForm.simpleButtonConnect.Text == "Connect")
            {
                if (connectEventArgs.Result.ToString() == "Connect Succeed")
                {
                    barButtonItemReaderConfig.Enabled = true;
                    /*
                     *  UI Updates
                     */
                    m_ConnectionForm.simpleButtonConnect.Text = "Disconnect";
                    m_ConnectionForm.textEditReaderIP.Enabled = false;
                    m_ConnectionForm.textEditPort.Enabled = false;
                    m_ConnectionForm.Close();
                    m_ReadForm.simpleButtonRead.Enabled = true;
                    m_ReadForm.simpleButtonRead.Text = "Start Reading";

                    /*
                     *  Events Registration
                     */
                    m_ReaderAPI.Events.ReadNotify += new Events.ReadNotifyHandler(Events_ReadNotify);
                    m_ReaderAPI.Events.AttachTagDataWithReadEvent = false;
                    m_ReaderAPI.Events.StatusNotify += new Events.StatusNotifyHandler(Events_StatusNotify);
                    m_ReaderAPI.Events.NotifyGPIEvent = true;
                    m_ReaderAPI.Events.NotifyBufferFullEvent = true;
                    m_ReaderAPI.Events.NotifyBufferFullWarningEvent = true;
                    m_ReaderAPI.Events.NotifyReaderDisconnectEvent = true;
                    m_ReaderAPI.Events.NotifyReaderExceptionEvent = true;
                    m_ReaderAPI.Events.NotifyAccessStartEvent = true;
                    m_ReaderAPI.Events.NotifyAccessStopEvent = true;
                    m_ReaderAPI.Events.NotifyInventoryStartEvent = true;
                    m_ReaderAPI.Events.NotifyInventoryStopEvent = true;

                    this.Text = "Connected to " + m_ConnectionForm.IpText;

                    checkExistReader(m_ConnectionForm.IpText);
                    if (isReaderExist == false)
                    {
                        createFRIDReaderAsync(m_ReaderAPI);
                    }
                    else
                    {
                        updateFRIDReaderAsync(m_ReaderAPI, m_ConnectionForm.IpText);
                    }


                }
            }
            else if (m_ConnectionForm.simpleButtonConnect.Text == "Disconnect")
            {
                if (connectEventArgs.Result.ToString() == "Disconnect Succeed")
                {
                    this.Text = "CS_RFID3_Host_Sample1";

                    m_ConnectionForm.simpleButtonConnect.Text = "Connect";
                    m_ConnectionForm.textEditReaderIP.Enabled = true;
                    m_ConnectionForm.textEditPort.Enabled = true;
                    m_ReadForm.simpleButtonRead.Enabled = false;
                    m_ReadForm.simpleButtonRead.Text = "Start Reading";
                }
            }
            StatusBottom.Caption = connectEventArgs.Result.ToString();
            //Logger.WriteLogger(connectEventArgs.Result.ToString());
            m_ConnectionForm.simpleButtonConnect.Enabled = true;
        }

        private void accessBackgroundWorker_DoWork(object sender, DoWorkEventArgs accessEvent)
        {
            try
            {
                m_AccessOpResult.m_OpCode = (ACCESS_OPERATION_CODE)accessEvent.Argument;
                m_AccessOpResult.m_Result = RFIDResults.RFID_API_SUCCESS;

                if ((ACCESS_OPERATION_CODE)accessEvent.Argument == ACCESS_OPERATION_CODE.ACCESS_OPERATION_READ)//read
                {
                    if (m_SelectedTagID != String.Empty)
                    {
                        m_ReadTag = m_ReaderAPI.Actions.TagAccess.ReadWait(
                        m_SelectedTagID, m_ReadParams, null);

                    }
                    else
                    {
                        StatusBottom.Caption = "Enter Tag-Id";
                    }
                }
                else if ((ACCESS_OPERATION_CODE)accessEvent.Argument == ACCESS_OPERATION_CODE.ACCESS_OPERATION_WRITE)//write
                {
                    if (m_SelectedTagID != String.Empty)
                    {
                        m_ReaderAPI.Actions.TagAccess.WriteWait(
                            m_SelectedTagID, m_WriteParams, null);
                    }
                    else
                    {
                        StatusBottom.Caption = "Enter Tag-Id";
                    }
                }

            }
            catch (OperationFailureException ofe)
            {
                m_AccessOpResult.m_Result = ofe.Result;
                m_AccessOpResult.m_StatusDescription = ofe.StatusDescription;
                m_AccessOpResult.m_VendorMessage = ofe.VendorMessage;

            }
            catch (InvalidUsageException ex)
            {
                m_AccessOpResult.m_Result = RFIDResults.RFID_API_PARAM_ERROR;
                m_AccessOpResult.m_StatusDescription = ex.Info;
                m_AccessOpResult.m_VendorMessage = ex.VendorMessage;

            }
            accessEvent.Result = m_AccessOpResult;
        }

        private void accessBackgroundWorker_ProgressChanged(object sender,
            ProgressChangedEventArgs pce)
        {

        }

        private void accessBackgroundWorker_RunWorkerCompleted(object sender,
            RunWorkerCompletedEventArgs accessEvents)
        {
            int index = 0;
            if (accessEvents.Error != null)
            {
                StatusBottom.Caption = accessEvents.Error.Message;
            }
            else
            {
                // Handle AccessWait Operations              
                AccessOperationResult accessOpResult = (AccessOperationResult)accessEvents.Result;
                if (accessOpResult.m_Result == RFIDResults.RFID_API_SUCCESS)
                {
                    if (accessOpResult.m_OpCode == ACCESS_OPERATION_CODE.ACCESS_OPERATION_READ && m_ReadTag.OpStatus == ACCESS_OPERATION_STATUS.ACCESS_SUCCESS)
                    {
                        if (m_ReadForm.inventoryList.Items.Count > 0 && m_ReadForm.inventoryList.SelectedItems.Count > 0)
                        {
                            ListViewItem item = m_ReadForm.inventoryList.SelectedItems[0];
                            string tagID = m_ReadTag.TagID + m_ReadTag.MemoryBank.ToString()
                                + m_ReadTag.MemoryBankDataOffset.ToString();

                            if (item.SubItems[5].Text.Length > 0)
                            {
                                bool isFound = false;

                                // Search or add new one
                                lock (m_TagTable.SyncRoot)
                                {
                                    isFound = m_TagTable.ContainsKey(tagID);
                                }

                                if (!isFound)
                                {
                                    ListViewItem newItem = new ListViewItem(m_ReadTag.TagID);

                                    ListViewItem.ListViewSubItem subItem = new ListViewItem.ListViewSubItem(newItem, m_ReadTag.AntennaID.ToString());
                                    newItem.SubItems.Add(subItem);

                                    subItem = new ListViewItem.ListViewSubItem(item, m_ReadTag.TagSeenCount.ToString());
                                    newItem.SubItems.Add(subItem);

                                    subItem = new ListViewItem.ListViewSubItem(item, m_ReadTag.PeakRSSI.ToString());
                                    newItem.SubItems.Add(subItem);

                                    subItem = new ListViewItem.ListViewSubItem(item, m_ReadTag.PC.ToString("X"));
                                    newItem.SubItems.Add(subItem);

                                    string memoryBank = m_ReadTag.MemoryBank.ToString();
                                    index = memoryBank.LastIndexOf('_');
                                    if (index != -1)
                                    {
                                        memoryBank = memoryBank.Substring(index + 1);
                                    }
                                    subItem = new ListViewItem.ListViewSubItem(item, memoryBank);
                                    newItem.SubItems.Add(subItem);

                                    subItem = new ListViewItem.ListViewSubItem(item, m_ReadTag.MemoryBankDataOffset.ToString());
                                    newItem.SubItems.Add(subItem);

                                    subItem = new ListViewItem.ListViewSubItem(newItem, getTagEvent(m_ReadTag));
                                    m_TagTotalCount += m_ReadTag.TagSeenCount;
                                    newItem.SubItems.Add(subItem);

                                    m_ReadForm.inventoryList.BeginUpdate();
                                    m_ReadForm.inventoryList.Items.Add(newItem);
                                    m_ReadForm.inventoryList.EndUpdate();

                                    lock (m_TagTable.SyncRoot)
                                    {
                                        m_TagTable.Add(tagID, newItem);
                                    }
                                }
                                else
                                {
                                    item.SubItems[5].Text = m_ReadTag.MemoryBankData;
                                    item.SubItems[7].Text = m_ReadTag.MemoryBankDataOffset.ToString();
                                }
                            }
                            else
                            {
                                // Empty Memory Bank Slot
                                item.SubItems[5].Text = m_ReadTag.MemoryBankData;

                                string memoryBank = m_ReadParams.MemoryBank.ToString();
                                index = memoryBank.LastIndexOf('_');
                                if (index != -1)
                                {
                                    memoryBank = memoryBank.Substring(index + 1);
                                }
                                item.SubItems[5].Text = memoryBank;
                                item.SubItems[7].Text = m_ReadTag.MemoryBankDataOffset.ToString();

                                lock (m_TagTable.SyncRoot)
                                {
                                    if (m_ReadTag.TagID != null)
                                    {
                                        m_TagTable.Remove(m_ReadTag.TagID);
                                    }
                                    m_TagTable.Add(tagID, item);
                                }
                            }
                            m_ReceiptRequisitionControl.DataRead = m_ReadTag.MemoryBankData;
                            StatusBottom.Caption = "Read Succeed";
                        }
                    }
                    else if (accessOpResult.m_OpCode == ACCESS_OPERATION_CODE.ACCESS_OPERATION_WRITE)
                    {
                        StatusBottom.Caption = "Write Succeed";
                    }
                }
                else
                {
                    StatusBottom.Caption = accessOpResult.m_StatusDescription + " [" + accessOpResult.m_VendorMessage + "]";
                }
                resetButtonState();
            }
        }

        private void resetButtonState()
        {
            if (this != null)
                m_ReadForm.simpleButtonRead.Enabled = true;
        }


        public void Events_StatusNotify(object sender, Events.StatusEventArgs statusEventArgs)
        {
            try
            {
                this.Invoke(m_UpdateStatusHandler, new object[] { statusEventArgs.StatusEventData });
            }
            catch (Exception)
            {
            }
        }

        private void Events_ReadNotify(object sender, Events.ReadEventArgs readEventArgs)
        {
            try
            {
                this.Invoke(m_UpdateReadHandler, new object[] { readEventArgs.ReadEventData });
            }
            catch (Exception)
            {

            }
        }


        //private void simpleButtonConnect_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        connectBackgroundWorker.RunWorkerAsync(m_ConnectionForm.simpleButtonConnect.Text);

        //    }
        //    catch (Exception ex)
        //    {
        //        m_ConnectionForm.labelStatus.Text = ex.Message;
        //        StatusBottom.Caption = ex.Message;
        //        Logger.WriteLogger(ex.Message);
        //    }
        //}

        //private void ConnectionForm_Load(object sender, EventArgs e)
        //{

        //}
        //private void ConnectionForm_FormClosing(object sender, FormClosingEventArgs e)
        //{
        //    try
        //    {
        //        if (m_IsConnected)
        //        {
        //            connectBackgroundWorker.RunWorkerAsync("Disconnect");
        //        }
        //        this.Dispose();
        //    }
        //    catch (Exception ex)
        //    {
        //        labelStatus.Text = ex.Message;
        //    }
        //}

        public void readButton_Click(object sender, EventArgs e)
        {

            try
            {
                if (m_IsConnected)
                {
                    if (m_ReadForm.simpleButtonRead.Text == "Start Reading")
                    {
                        if (m_ReaderAPI.Actions.TagAccess.OperationSequence.Length > 0)
                        {
                            // Before inventory purge all tags
                            m_ReaderAPI.Actions.PurgeTags();
                            m_ReaderAPI.Actions.TagAccess.OperationSequence.PerformSequence(null, getTriggerInfo(), null);
                        }

                        m_ReaderAPI.Actions.Inventory.Perform(null, getTriggerInfo(), null);

                        m_ReadForm.inventoryList.Items.Clear();
                        m_TagTable.Clear();
                        m_TagTotalCount = 0;

                        m_ReadForm.simpleButtonRead.Text = "Stop Reading";
                    }
                    else if (m_ReadForm.simpleButtonRead.Text == "Stop Reading")
                    {
                        if (m_ReaderAPI.Actions.TagAccess.OperationSequence.Length > 0)
                        {
                            m_ReaderAPI.Actions.TagAccess.OperationSequence.StopSequence();
                        }
                        else
                        {
                            m_ReaderAPI.Actions.Inventory.Stop();
                        }

                        m_ReadForm.simpleButtonRead.Text = "Start Reading";
                    }
                }
                else
                {
                    StatusBottom.Caption = "Please connect to a reader";
                }
            }
            catch (InvalidOperationException ioe)
            {
                StatusBottom.Caption = ioe.Message;
            }
            catch (InvalidUsageException iue)
            {
                StatusBottom.Caption = iue.Info;
            }
            catch (OperationFailureException ofe)
            {
                StatusBottom.Caption = ofe.Result + ":" + ofe.StatusDescription;
            }
            catch (Exception ex)
            {
                StatusBottom.Caption = ex.Message;
            }
        }

        public void readRDButton_Click()
        {
            try
            {
                if (m_IsConnected)
                {
                    if (m_ReadForm.simpleButtonRead.Text == "Start Reading")
                    {
                        if (m_ReaderAPI.Actions.TagAccess.OperationSequence.Length > 0)
                        {
                            m_ReaderAPI.Actions.PurgeTags();
                            m_ReaderAPI.Actions.TagAccess.OperationSequence.PerformSequence(null, getTriggerInfo(), null);
                        }
                        m_ReaderAPI.Actions.Inventory.Perform(null, getTriggerInfo(), null);

                        m_ReadForm.inventoryList.Items.Clear();
                        m_TagTable.Clear();
                        m_TagTotalCount = 0;

                        m_ReadForm.simpleButtonRead.Text = "Stop Reading";

                    }
                    else if (m_ReadForm.simpleButtonRead.Text == "Stop Reading")
                    {
                        if (m_ReaderAPI.Actions.TagAccess.OperationSequence.Length > 0)
                        {
                            m_ReaderAPI.Actions.TagAccess.OperationSequence.StopSequence();
                        }
                        else
                        {
                            m_ReaderAPI.Actions.Inventory.Stop();
                        }

                        m_ReadForm.simpleButtonRead.Text = "Start Reading";
                    }
                }
                else
                {
                    StatusBottom.Caption = "Please connect to a reader";
                }
            }
            catch (InvalidOperationException ioe)
            {
                StatusBottom.Caption = ioe.Message;
            }
            catch (InvalidUsageException iue)
            {
                StatusBottom.Caption = iue.Info;
            }
            catch (OperationFailureException ofe)
            {
                StatusBottom.Caption = ofe.Result + ":" + ofe.StatusDescription;
            }
            catch (Exception ex)
            {
                StatusBottom.Caption = ex.Message;
            }
        }

        public void clearReports_Click(object sender, EventArgs e)
        {
            m_ReadForm.textEditTotalTag.Text = "0(0)";
            m_ReadForm.inventoryList.Items.Clear();
            this.m_TagTable.Clear();
        }

        public void saveTag_Click(object sender, EventArgs e)
        {
            m_ReadForm.textEditTotalTag.Text = "0(0)";
            m_ReadForm.inventoryList.Items.Clear();
            this.m_TagTable.Clear();
        }

        public async Task createFRIDReaderAsync(Symbol.RFID3.RFIDReader rFIDReader)
        {
            ReaderCapabilities cap = rFIDReader.ReaderCapabilities;
            Models.RFIDReader reader = new Models.RFIDReader();
            reader.HostName = m_ConnectionForm.IpText;
            reader.Port = m_ConnectionForm.PortText;
            reader.Model = cap.ModelName;
            reader.ReaderID = cap.ReaderID.ID;
            reader.Firmware = cap.FirwareVersion;
            reader.SerialNumber = "";
            reader.MfgDate = null;
            reader.Description = "";
            reader.DepartmentID = "";
            reader.Status = "1";
            reader.CreatedUserID = WMMessage.User.UserID; ;
            reader.CreatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));

            if (await rFIDReaderRepository.Create(reader) > 0)
            {
                MessageBox.Show("Đã lưu đầu đọc RFID");
            }
        }

        public async Task updateFRIDReaderAsync(Symbol.RFID3.RFIDReader rFIDReader, String hostName)
        {
            ReaderCapabilities cap = rFIDReader.ReaderCapabilities;
            Models.RFIDReader reader = await rFIDReaderRepository.GetUnderID(hostName);
            reader.HostName = m_ConnectionForm.IpText;
            reader.Port = m_ConnectionForm.PortText;
            reader.Model = cap.ModelName;
            reader.ReaderID = cap.ReaderID.ID;
            reader.Firmware = cap.FirwareVersion;
            reader.SerialNumber = "";
            reader.MfgDate = null;
            reader.Description = "";
            reader.DepartmentID = "";
            reader.Status = "1";
            reader.CreatedUserID = WMMessage.User.UserID; ;
            reader.CreatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));

            if (await rFIDReaderRepository.Update(reader, hostName) > 0)
            {
                MessageBox.Show("Đã cập nhật đầu đọc RFID");
            }
        }
        private async Task checkExistReader(String hostName)
        {
            if (!hostName.Equals("") && (await rFIDReaderRepository.checkExistAsync(hostName)) == true)
            {
                isReaderExist = true;
            }
        }

        public async Task createRFIDTagDataAsync()
        {
            foreach (ListViewItem item in m_ReadForm.inventoryList.Items)
            {
                RFIDSignal rFIDSignal = new RFIDSignal();
                rFIDSignal.RFIDTag = item.SubItems[0].Text;
                rFIDSignal.UpdatedDate = null;
                rFIDSignal.AntennaID = item.SubItems[1].Text;
                rFIDSignal.TagSeenCount = item.SubItems[2].Text;
                rFIDSignal.PeakRSSI = item.SubItems[3].Text;
                rFIDSignal.PC = item.SubItems[4].Text;
                rFIDSignal.MemoryBankData = item.SubItems[5].Text;
                rFIDSignal.Hostname = m_ConnectionForm.IpText;
                rFIDSignal.Port = m_ConnectionForm.PortText;
                rFIDSignal.InsertedDate = null;
                rFIDSignal.AssetID = null;
                rFIDSignal.AssetName = null;
                //rFIDSignal.RFIDTag = tag.TagID;
                //rFIDSignal.UpdatedDate = null;
                //rFIDSignal.AntennaID = tag.AntennaID + "";
                //rFIDSignal.TagSeenCount = tag.TagSeenCount + "";
                //rFIDSignal.PeakRSSI = tag.PeakRSSI + "";
                //rFIDSignal.PC = tag.PC + "";
                //rFIDSignal.MemoryBankData = tag.MemoryBankData;
                //rFIDSignal.Hostname = m_ConnectionForm.IpText;
                //rFIDSignal.Port = m_ConnectionForm.PortText;
                //rFIDSignal.InsertedDate = null;
                //rFIDSignal.AssetID = null;
                //rFIDSignal.AssetName = null;
                if (await rFIDSignalRepository.Create(rFIDSignal) > 0)
                {

                }
            }
        }

        private string getTagEvent(TagData tag)
        {
            string eventString = "None";
            if (tag.TagEvent != TAG_EVENT.NONE)
            {
                switch (tag.TagEvent)
                {
                    case TAG_EVENT.NEW_TAG_VISIBLE:
                        eventString = "New";
                        break;
                    case TAG_EVENT.TAG_BACK_TO_VISIBILITY:
                        eventString = "Back";
                        break;
                    case TAG_EVENT.TAG_NOT_VISIBLE:
                        eventString = "Gone";
                        break;
                    case TAG_EVENT.TAG_MOVING:
                        eventString = "Moving";
                        break;
                    case TAG_EVENT.TAG_STATIONARY:
                        eventString = "Stationary";
                        break;
                    default:
                        eventString = "None";
                        break;

                }

            }
            return eventString;
        }
    }

}