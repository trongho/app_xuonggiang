﻿using DevExpress.XtraEditors;
using Symbol.RFID3;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WarehouseManager
{
    public partial class ReadTagForm : DevExpress.XtraEditors.XtraForm
    {
        internal ReceiptRequisitionControl m_ReceiptRequisitionControl;
        
        public ReadTagForm(ReceiptRequisitionControl receiptRequisitionControl)
        {
            InitializeComponent();
            
            m_ReceiptRequisitionControl = receiptRequisitionControl;

            this.Load += m_ReceiptRequisitionControl.ReadTagForm_Load;
            comboBoxEditMemoryBank.SelectedIndexChanged += m_ReceiptRequisitionControl.comboBoxEditMemoryBank_SelectedIndexChanged;
            textEditOffset.TextChanged += m_ReceiptRequisitionControl.offset_TB_TextChanged;
            textEditNoOfBytesLenght.TextChanged += m_ReceiptRequisitionControl.length_TB_TextChanged;
            simpleButtonReadTag.Click += receiptRequisitionControl.readButton_Click;

           
           
        }
    }
}