﻿using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarehouseManager.Models;
using WarehouseManager.Repository;

namespace WarehouseManager
{
   
    public partial class UnitControl : DevExpress.XtraEditors.XtraUserControl
    {
        UnitRepository unitRepository;
        Boolean isExist = false;
        public UnitControl()
        {
            InitializeComponent();
            unitRepository = new UnitRepository();
            this.Load += UnitControl_Load;
        }

        private void UnitControl_Load(Object sender,EventArgs e)
        {
            windowsUIButtonPanel1.ButtonClick += button_Click;
            gridViewData.RowClick += gridView_RowClick;
            gridViewData.CustomColumnDisplayText += GridView_CustomColumnDisplayText;
            sbLoadDataForGridAsync();
            
        }

        

        private void GridView_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {

            if (e.Column == colNo)
            {
                int rowHandle = gridViewData.GetRowHandle(e.ListSourceRowIndex);
                if (rowHandle >= 0)
                {
                    e.DisplayText = (rowHandle + 1).ToString();
                }
            }
        }

        private async Task CreatAsync() {
            Unit unit = new Unit();
            if (textEditUnitID.Text.Equals("") || textEditUnitID.Text.Equals(""))
            {
                return;
            }
            unit.UnitID = textEditUnitID.Text;
            unit.UnitRate =Double.Parse(textEditUnitRate.Text);
            unit.StockUnitID = textEditColorStockUnitID.Text;
            unit.Status = "0";
            if (unit.UnitRate > 0)
            {
                unit.Type = 1;
            }
            else
            {
                unit.Type = 0;
            }
            unit.Description = textEditDescription.Text;
            unit.CreatedUserID = WMMessage.User.UserID;
            unit.CreatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
            if (await unitRepository.Create(unit) > 0)
            {
                WMPublic.sbMessageSaveNewSuccess(this);
                sbLoadDataForGridAsync();
            }
        }
        private async Task UpdateAsync() {
            Unit unit = await unitRepository.GetUnderID(textEditUnitID.Text);
            if (textEditUnitID.Text.Equals("") || textEditColorStockUnitID.Text.Equals(""))
            {
                return;
            }
            unit.UnitID = textEditUnitID.Text;
            unit.UnitRate = Double.Parse(textEditUnitRate.Text);
            unit.StockUnitID = textEditColorStockUnitID.Text;
            unit.Status = "0";
            if (unit.UnitRate > 0)
            {
                unit.Type = 1;
            }
            else
            {
                unit.Type = 0;
            }
            unit.Description = textEditDescription.Text;
            unit.UpdatedUserID = WMMessage.User.UserID;
            unit.UpdatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
            if (await unitRepository.Update(unit,textEditUnitID.Text) > 0)
            {
                WMPublic.sbMessageSaveChangeSuccess(this);
                sbLoadDataForGridAsync();
            }
        }

        private async Task DeleteAsync()
        {
            if(await unitRepository.Delete(textEditUnitID.Text) > 0)
            {
                WMPublic.sbMessageDeleteSuccess(this);
                sbLoadDataForGridAsync();
            }
        }

        private async Task sbLoadDataForGridAsync() {
            List<Unit> units = await unitRepository.GetAll();
            int mMaxRow = 100;
            if (units.Count < mMaxRow)
            {
                int num = mMaxRow - units.Count;
                int i = 1;
                while (true)
                {
                    int num2 = i;
                    int num3 = num;
                    if (num2 > num3)
                    {
                        break;
                    }
                    units.Add(new Unit());
                    i++;
                }
            }
            gridControlData.DataSource = units;
        }
        private void clear() {
            textEditUnitID.Text = "";
            textEditColorStockUnitID.Text = "";
            textEditUnitRate.Text = "";
            textEditDescription.Text = "";
        }
        private async Task checkExistAsync()
        {
            isExist = await unitRepository.checkExistAsync(textEditUnitID.Text);
        }

        private void gridView_RowClick(Object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            textEditUnitID.Text = (string)(sender as GridView).GetFocusedRowCellValue("UnitID");
            textEditColorStockUnitID.Text= (string)(sender as GridView).GetFocusedRowCellValue("StockUnitID");
            textEditUnitRate.Text = (string)(sender as GridView).GetFocusedRowCellValue("UnitRate").ToString();
            textEditDescription.Text= (string)(sender as GridView).GetFocusedRowCellValue("Description");
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.F2)
            {
                if (WMPublic.sbMessageAddNewRequest(this) == true)
                {
                    clear();
                }
                return true;
            }
            if (keyData == Keys.F3)
            {
                checkExistAsync();
                if (isExist == true)
                {
                    if (WMPublic.sbMessageSaveChangeRequest(this) == true)
                    {
                        UpdateAsync();
                    }

                }
                else
                {
                    if (WMPublic.sbMessageSaveNewRequest(this) == true)
                    {
                        CreatAsync();
                    }
                }
                return true;
            }
            if (keyData == Keys.F4)
            {
                if (WMPublic.sbMessageDeleteRequest(this) == true)
                {
                    DeleteAsync();
                }
                return true;
            }
            if (keyData == Keys.F6)
            {
                sbLoadDataForGridAsync();
                return true;
            }
            else
            {
                return base.ProcessCmdKey(ref msg, keyData);
            }
        }

            //barbutton click
            public async void button_Click(object sender, EventArgs e)
        {

            ButtonEventArgs eArg = (DevExpress.XtraBars.Docking2010.ButtonEventArgs)e;
            String tag = eArg.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "new":
                    if (WMPublic.sbMessageAddNewRequest(this) == true)
                    {
                        clear();
                    }
                    break;
                case "save":
                    checkExistAsync();
                    if (isExist == true)
                    {
                        if (WMPublic.sbMessageSaveChangeRequest(this) == true)
                        {
                            UpdateAsync();
                        }

                    }
                    else
                    {
                        if (WMPublic.sbMessageSaveNewRequest(this) == true)
                        {
                            CreatAsync();
                        }
                    }
                    break;
                case "delete":
                    if (WMPublic.sbMessageDeleteRequest(this) == true)
                    {
                        DeleteAsync();
                    }
                    break;
                case "search":
                    break;
                case "refesh":
                    sbLoadDataForGridAsync();
                    break;
                case "import":
                    break;
                case "export":
                    break;
                case "print":
                    break;
                case "close":
                    break;
            }

        }
    }
}
