﻿using DevExpress.XtraEditors;
using Symbol.RFID3;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarehouseManager.Helpers;

namespace WarehouseManager
{
    public partial class ConnectionForm : DevExpress.XtraEditors.XtraForm
    {

        //public RFIDReader m_ReaderAPI;
        //internal bool m_IsConnected;
        //internal AccessOperationResult m_AccessOpResult;

        //internal string m_SelectedTagID = null;
        //internal TagAccess.ReadAccessParams m_ReadParams = null;

        //private delegate void UpdateStatus(Events.StatusEventData eventData);
        //private UpdateStatus m_UpdateStatusHandler = null;
        //private delegate void UpdateRead(Events.ReadEventData eventData);
        //private UpdateRead m_UpdateReadHandler = null;
        //private TagData m_ReadTag = null;
        //private Hashtable m_TagTable;
        //private uint m_TagTotalCount;


        internal MainForm m_MainForm=null;
        //internal ReadForm m_ReadForm = null;
        

        public ConnectionForm(MainForm mainForm)
        {
            InitializeComponent();
            //connectBackgroundWorker.WorkerReportsProgress = true;
            //connectBackgroundWorker.WorkerSupportsCancellation = true;
            //this.connectBackgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.connectBackgroundWorker_DoWork);
            //this.connectBackgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.connectBackgroundWorker_RunWorkerCompleted);
            //this.connectBackgroundWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.connectBackgroundWorker_ProgressChanged);

            //m_ReadForm = new ReadForm(this);

    
            this.StartPosition = FormStartPosition.CenterScreen;
            m_MainForm = mainForm;
            
            //m_UpdateStatusHandler = new UpdateStatus(myUpdateStatus);
            //m_AccessOpResult = new AccessOperationResult();
            //m_IsConnected = false;

            //m_ReadParams = new TagAccess.ReadAccessParams();
            //m_ReadParams.MemoryBank = MEMORY_BANK.MEMORY_BANK_USER;
            //m_ReadParams.AccessPassword = 0;
            //m_ReadParams.ByteOffset = 0;
            //m_ReadParams.ByteCount = 0;

            //m_ReadTag = new Symbol.RFID3.TagData();
            //m_UpdateStatusHandler = new UpdateStatus(myUpdateStatus);
            //m_UpdateReadHandler = new UpdateRead(myUpdateRead);
            //m_TagTable = new Hashtable();
            //m_AccessOpResult = new AccessOperationResult();
            //m_IsConnected = false;
            //m_TagTotalCount = 0;
        }

        public string IpText
        {
            get
            {
                return textEditReaderIP.Text;
            }
            set
            {
                textEditReaderIP.Text = value;
            }
        }

        public string PortText
        {
            get
            {
                return textEditPort.Text;
            }
            set
            {
                textEditPort.Text = value;
            }
        }

        //internal class AccessOperationResult
        //{
        //    public RFIDResults m_Result;
        //    public String m_VendorMessage;
        //    public String m_StatusDescription;
        //    public ACCESS_OPERATION_CODE m_OpCode;

        //    public AccessOperationResult()
        //    {
        //        m_Result = RFIDResults.RFID_NO_ACCESS_IN_PROGRESS;
        //        m_OpCode = ACCESS_OPERATION_CODE.ACCESS_OPERATION_READ;
        //    }
        //}

        //private void myUpdateStatus(Events.StatusEventData eventData)
        //{
        //    switch (eventData.StatusEventType)
        //    {
        //        case Symbol.RFID3.Events.STATUS_EVENT_TYPE.INVENTORY_START_EVENT:
        //            m_MainForm.StatusBottom.Caption= "Inventory started";
        //            break;
        //        case Symbol.RFID3.Events.STATUS_EVENT_TYPE.INVENTORY_STOP_EVENT:
        //            m_MainForm.StatusBottom.Caption = "Inventory stopped";
        //            break;
        //        case Symbol.RFID3.Events.STATUS_EVENT_TYPE.ACCESS_START_EVENT:
        //            m_MainForm.StatusBottom.Caption = "Access Operation started";
        //            break;
        //        case Symbol.RFID3.Events.STATUS_EVENT_TYPE.ACCESS_STOP_EVENT:
        //            m_MainForm.StatusBottom.Caption = "Access Operation stopped";

        //            if (this.m_SelectedTagID == string.Empty)
        //            {
        //                uint successCount, failureCount;
        //                successCount = failureCount = 0;
        //                m_ReaderAPI.Actions.TagAccess.GetLastAccessResult(ref successCount, ref failureCount);
        //                m_MainForm.StatusBottom.Caption = "Access completed - Success Count: " + successCount.ToString()
        //                    + ", Failure Count: " + failureCount.ToString();
        //            }
        //            resetButtonState();
        //            break;
        //        case Symbol.RFID3.Events.STATUS_EVENT_TYPE.BUFFER_FULL_WARNING_EVENT:
        //            m_MainForm.StatusBottom.Caption = " Buffer full warning";
        //            myUpdateRead(null);
        //            break;
        //        case Symbol.RFID3.Events.STATUS_EVENT_TYPE.BUFFER_FULL_EVENT:
        //            m_MainForm.StatusBottom.Caption = "Buffer full";
        //            myUpdateRead(null);
        //            break;
        //        case Symbol.RFID3.Events.STATUS_EVENT_TYPE.DISCONNECTION_EVENT:
        //            m_MainForm.StatusBottom.Caption = "Disconnection Event " + eventData.DisconnectionEventData.DisconnectEventInfo.ToString();
        //            connectBackgroundWorker.RunWorkerAsync("Disconnect");
        //            break;
        //        case Symbol.RFID3.Events.STATUS_EVENT_TYPE.ANTENNA_EVENT:
        //            m_MainForm.StatusBottom.Caption = "Antenna Status Update";
        //            break;
        //        case Symbol.RFID3.Events.STATUS_EVENT_TYPE.READER_EXCEPTION_EVENT:
        //            m_MainForm.StatusBottom.Caption = "Reader ExceptionEvent " + eventData.ReaderExceptionEventData.ReaderExceptionEventInfo;
        //            break;
        //        default:
        //            break;
        //    }
        //}

        //private void myUpdateRead(Events.ReadEventData eventData)
        //{
        //    int index = 0;
        //    ListViewItem item;

        //    Symbol.RFID3.TagData[] tagData = m_ReaderAPI.Actions.GetReadTags(1000);
        //    if (tagData != null)
        //    {
        //        for (int nIndex = 0; nIndex < tagData.Length; nIndex++)
        //        {
        //            if (tagData[nIndex].OpCode == ACCESS_OPERATION_CODE.ACCESS_OPERATION_NONE ||
        //                (tagData[nIndex].OpCode == ACCESS_OPERATION_CODE.ACCESS_OPERATION_READ &&
        //                tagData[nIndex].OpStatus == ACCESS_OPERATION_STATUS.ACCESS_SUCCESS))
        //            {
        //                Symbol.RFID3.TagData tag = tagData[nIndex];
        //                string tagID = tag.TagID;
        //                bool isFound = false;

        //                lock (m_TagTable.SyncRoot)
        //                {
        //                    isFound = m_TagTable.ContainsKey(tagID);
        //                    if (!isFound)
        //                    {
        //                        tagID += ((uint)tag.MemoryBank + tag.MemoryBankDataOffset);
        //                        isFound = m_TagTable.ContainsKey(tagID);
        //                    }
        //                }

        //                if (isFound)
        //                {
        //                    uint count = 0;
        //                    item = (ListViewItem)m_TagTable[tagID];
        //                    try
        //                    {
        //                        count = uint.Parse(item.SubItems[2].Text) + tagData[nIndex].TagSeenCount;
        //                        m_TagTotalCount += tagData[nIndex].TagSeenCount;
        //                    }
        //                    catch (FormatException fe)
        //                    {
        //                        m_MainForm.StatusBottom.Caption = fe.Message;
        //                        break;
        //                    }
        //                    item.SubItems[1].Text = tag.AntennaID.ToString();
        //                    item.SubItems[2].Text = count.ToString();
        //                    item.SubItems[3].Text = tag.PeakRSSI.ToString();

        //                    string memoryBank = tag.MemoryBank.ToString();
        //                    index = memoryBank.LastIndexOf('_');
        //                    if (index != -1)
        //                    {
        //                        memoryBank = memoryBank.Substring(index + 1);
        //                    }
        //                    if (tag.MemoryBankData.Length > 0 && !memoryBank.Equals(item.SubItems[5].Text))
        //                    {
        //                        item.SubItems[5].Text = tag.MemoryBankData;
        //                        item.SubItems[6].Text = memoryBank;
        //                        item.SubItems[7].Text = tag.MemoryBankDataOffset.ToString();

        //                        lock (m_TagTable.SyncRoot)
        //                        {
        //                            m_TagTable.Remove(tagID);
        //                            m_TagTable.Add(tag.TagID + tag.MemoryBank.ToString()
        //                                + tag.MemoryBankDataOffset.ToString(), item);
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    item = new ListViewItem(tag.TagID);
        //                    ListViewItem.ListViewSubItem subItem;

        //                    subItem = new ListViewItem.ListViewSubItem(item, tag.AntennaID.ToString());
        //                    item.SubItems.Add(subItem);

        //                    subItem = new ListViewItem.ListViewSubItem(item, tag.TagSeenCount.ToString());
        //                    m_TagTotalCount += tag.TagSeenCount;
        //                    item.SubItems.Add(subItem);

        //                    subItem = new ListViewItem.ListViewSubItem(item, tag.PeakRSSI.ToString());
        //                    item.SubItems.Add(subItem);
        //                    subItem = new ListViewItem.ListViewSubItem(item, tag.PC.ToString("X"));
        //                    item.SubItems.Add(subItem);

        //                    subItem = new ListViewItem.ListViewSubItem(item, "");
        //                    item.SubItems.Add(subItem);
        //                    subItem = new ListViewItem.ListViewSubItem(item, "");
        //                    item.SubItems.Add(subItem);
        //                    subItem = new ListViewItem.ListViewSubItem(item, "");
        //                    item.SubItems.Add(subItem);

        //                    m_ReadForm.inventoryList.BeginUpdate();
        //                    m_ReadForm.inventoryList.Items.Add(item);
        //                    m_ReadForm.inventoryList.EndUpdate();

        //                    lock (m_TagTable.SyncRoot)
        //                    {
        //                        m_TagTable.Add(tagID, item);
        //                    }
        //                }
        //            }
        //        }
        //        m_ReadForm.textEditTotalTag.Text = m_TagTable.Count + "(" + m_TagTotalCount + ")";
        //    }
        //}


        //private void connectBackgroundWorker_DoWork(object sender, DoWorkEventArgs workEventArgs)
        //{
        //    connectBackgroundWorker.ReportProgress(0, workEventArgs.Argument);

        //    if ((string)workEventArgs.Argument == "Connect")
        //    {
        //        m_ReaderAPI = new RFIDReader(IpText, uint.Parse(PortText), 0);

        //        try
        //        {
        //            m_ReaderAPI.Connect();
        //            m_IsConnected = true;
        //            workEventArgs.Result = "Connect Succeed";
        //            m_MainForm.StatusBottom.Caption = "Connect Succeed";
        //            Logger.WriteLogger("Connected");

        //        }
        //        catch (OperationFailureException operationException)
        //        {
        //            workEventArgs.Result = operationException.StatusDescription;
        //        }
        //        catch (Exception ex)
        //        {
        //            workEventArgs.Result = ex.Message;
        //        }
        //    }
        //    else if ((string)workEventArgs.Argument == "Disconnect")
        //    {
        //        try
        //        {

        //            m_ReaderAPI.Disconnect();
        //            m_IsConnected = false;
        //            workEventArgs.Result = "Disconnect Succeed";
        //            m_MainForm.StatusBottom.Caption = "Disconnect Succeed";
        //            m_ReaderAPI = null;
        //            Logger.WriteLogger("DisConnected");

        //        }
        //        catch (OperationFailureException ofe)
        //        {
        //            workEventArgs.Result = ofe.Result;
        //        }
        //    }

        //}

        //private void connectBackgroundWorker_ProgressChanged(object sender,
        //    ProgressChangedEventArgs progressEventArgs)
        //{
        //    simpleButtonConnect.Enabled = false;
        //}

        //private void connectBackgroundWorker_RunWorkerCompleted(object sender,
        //    RunWorkerCompletedEventArgs connectEventArgs)
        //{
        //    if (simpleButtonConnect.Text == "Connect")
        //    {
        //        if (connectEventArgs.Result.ToString() == "Connect Succeed")
        //        {
        //            /*
        //             *  UI Updates
        //             */
        //            simpleButtonConnect.Text = "Disconnect";
        //            textEditReaderIP.Enabled = false;
        //            textEditPort.Enabled = false;
        //            //this.Close();
        //            m_ReadForm.simpleButtonRead.Enabled = true;
        //            m_ReadForm.simpleButtonRead.Text = "Start Reading";

        //            /*
        //             *  Events Registration
        //             */
        //            m_ReaderAPI.Events.ReadNotify += new Events.ReadNotifyHandler(Events_ReadNotify);
        //            m_ReaderAPI.Events.AttachTagDataWithReadEvent = false;
        //            m_ReaderAPI.Events.StatusNotify += new Events.StatusNotifyHandler(Events_StatusNotify);
        //            m_ReaderAPI.Events.NotifyGPIEvent = true;
        //            m_ReaderAPI.Events.NotifyBufferFullEvent = true;
        //            m_ReaderAPI.Events.NotifyBufferFullWarningEvent = true;
        //            m_ReaderAPI.Events.NotifyReaderDisconnectEvent = true;
        //            m_ReaderAPI.Events.NotifyReaderExceptionEvent = true;
        //            m_ReaderAPI.Events.NotifyAccessStartEvent = true;
        //            m_ReaderAPI.Events.NotifyAccessStopEvent = true;
        //            m_ReaderAPI.Events.NotifyInventoryStartEvent = true;
        //            m_ReaderAPI.Events.NotifyInventoryStopEvent = true;

        //            this.Text = "Connected to " + IpText;
        //        }
        //    }
        //    else if (simpleButtonConnect.Text == "Disconnect")
        //    {
        //        if (connectEventArgs.Result.ToString() == "Disconnect Succeed")
        //        {
        //            this.Text = "CS_RFID3_Host_Sample1";

        //            simpleButtonConnect.Text = "Connect";
        //            textEditReaderIP.Enabled = true;
        //            textEditPort.Enabled = true;
        //            m_ReadForm.simpleButtonRead.Enabled = false;
        //            m_ReadForm.simpleButtonRead.Text = "Start Reading";
        //        }
        //    }
        //    labelStatus.Text = connectEventArgs.Result.ToString();
        //    m_MainForm.StatusBottom.Caption= connectEventArgs.Result.ToString();
        //    Logger.WriteLogger(connectEventArgs.Result.ToString());
        //    simpleButtonConnect.Enabled = true;
        //}

        //private void accessBackgroundWorker_DoWork(object sender, DoWorkEventArgs accessEvent)
        //{
        //    try
        //    {
        //        m_AccessOpResult.m_OpCode = (ACCESS_OPERATION_CODE)accessEvent.Argument;
        //        m_AccessOpResult.m_Result = RFIDResults.RFID_API_SUCCESS;

        //        if ((ACCESS_OPERATION_CODE)accessEvent.Argument == ACCESS_OPERATION_CODE.ACCESS_OPERATION_READ)
        //        {
        //            if (m_SelectedTagID != String.Empty)
        //            {
        //                m_ReadTag = m_ReaderAPI.Actions.TagAccess.ReadWait(
        //                m_SelectedTagID, m_ReadParams, null);
        //            }
        //            else
        //            {
        //                m_MainForm.StatusBottom.Caption = "Enter Tag-Id";
        //            }
        //        }
        //    }
        //    catch (OperationFailureException ofe)
        //    {
        //        m_AccessOpResult.m_Result = ofe.Result;
        //        m_AccessOpResult.m_StatusDescription = ofe.StatusDescription;
        //        m_AccessOpResult.m_VendorMessage = ofe.VendorMessage;
        //    }
        //    catch (InvalidUsageException ex)
        //    {
        //        m_AccessOpResult.m_Result = RFIDResults.RFID_API_PARAM_ERROR;
        //        m_AccessOpResult.m_StatusDescription = ex.Info;
        //        m_AccessOpResult.m_VendorMessage = ex.VendorMessage;
        //    }
        //    accessEvent.Result = m_AccessOpResult;
        //}

        //private void accessBackgroundWorker_ProgressChanged(object sender,
        //    ProgressChangedEventArgs pce)
        //{

        //}

        //private void accessBackgroundWorker_RunWorkerCompleted(object sender,
        //    RunWorkerCompletedEventArgs accessEvents)
        //{
        //    int index = 0;
        //    if (accessEvents.Error != null)
        //    {
        //        m_MainForm.StatusBottom.Caption = accessEvents.Error.Message;
        //    }
        //    else
        //    {
        //        // Handle AccessWait Operations              
        //        AccessOperationResult accessOpResult = (AccessOperationResult)accessEvents.Result;
        //        if (accessOpResult.m_Result == RFIDResults.RFID_API_SUCCESS)
        //        {
        //            if (accessOpResult.m_OpCode == ACCESS_OPERATION_CODE.ACCESS_OPERATION_READ)
        //            {
        //                if (m_ReadForm.inventoryList.SelectedItems.Count > 0)
        //                {
        //                    ListViewItem item = m_ReadForm.inventoryList.SelectedItems[0];
        //                    string tagID = m_ReadTag.TagID + m_ReadTag.MemoryBank.ToString()
        //                        + m_ReadTag.MemoryBankDataOffset.ToString();

        //                    if (item.SubItems[5].Text.Length > 0)
        //                    {
        //                        bool isFound = false;

        //                        // Search or add new one
        //                        lock (m_TagTable.SyncRoot)
        //                        {
        //                            isFound = m_TagTable.ContainsKey(tagID);
        //                        }

        //                        if (!isFound)
        //                        {
        //                            ListViewItem newItem = new ListViewItem(m_ReadTag.TagID);
        //                            ListViewItem.ListViewSubItem subItem = new ListViewItem.ListViewSubItem(newItem, m_ReadTag.AntennaID.ToString());
        //                            newItem.SubItems.Add(subItem);
        //                            subItem = new ListViewItem.ListViewSubItem(item, m_ReadTag.TagSeenCount.ToString());
        //                            newItem.SubItems.Add(subItem);
        //                            subItem = new ListViewItem.ListViewSubItem(item, m_ReadTag.PeakRSSI.ToString());
        //                            newItem.SubItems.Add(subItem);
        //                            subItem = new ListViewItem.ListViewSubItem(item, m_ReadTag.PC.ToString("X"));
        //                            newItem.SubItems.Add(subItem);
        //                            subItem = new ListViewItem.ListViewSubItem(item, m_ReadTag.MemoryBankData);
        //                            newItem.SubItems.Add(subItem);

        //                            string memoryBank = m_ReadTag.MemoryBank.ToString();
        //                            index = memoryBank.LastIndexOf('_');
        //                            if (index != -1)
        //                            {
        //                                memoryBank = memoryBank.Substring(index + 1);
        //                            }

        //                            subItem = new ListViewItem.ListViewSubItem(item, memoryBank);
        //                            newItem.SubItems.Add(subItem);
        //                            subItem = new ListViewItem.ListViewSubItem(item, m_ReadTag.MemoryBankDataOffset.ToString());
        //                            newItem.SubItems.Add(subItem);

        //                            m_ReadForm.inventoryList.BeginUpdate();
        //                            m_ReadForm.inventoryList.Items.Add(newItem);
        //                            m_ReadForm.inventoryList.EndUpdate();

        //                            lock (m_TagTable.SyncRoot)
        //                            {
        //                                m_TagTable.Add(tagID, newItem);
        //                            }
        //                        }
        //                    }
        //                    else
        //                    {
        //                        // Empty Memory Bank Slot
        //                        item.SubItems[5].Text = m_ReadTag.MemoryBankData;

        //                        string memoryBank = m_ReadParams.MemoryBank.ToString();
        //                        index = memoryBank.LastIndexOf('_');
        //                        if (index != -1)
        //                        {
        //                            memoryBank = memoryBank.Substring(index + 1);
        //                        }
        //                        item.SubItems[6].Text = memoryBank;
        //                        item.SubItems[7].Text = m_ReadTag.MemoryBankDataOffset.ToString();

        //                        lock (m_TagTable.SyncRoot)
        //                        {
        //                            if (m_ReadTag.TagID != null)
        //                            {
        //                                m_TagTable.Remove(m_ReadTag.TagID);
        //                            }
        //                            m_TagTable.Add(tagID, item);
        //                        }
        //                    }
        //                    //this.m_ReadForm.ReadData_TB.Text = m_ReadTag.MemoryBankData;
        //                    m_MainForm.StatusBottom.Caption = "Read Succeed";
        //                }
        //            }
        //        }
        //        else
        //        {
        //            m_MainForm.StatusBottom.Caption = accessOpResult.m_StatusDescription + " [" + accessOpResult.m_VendorMessage + "]";
        //        }
        //        resetButtonState();
        //    }
        //}

        //private void resetButtonState()
        //{
        //    if (this != null)
        //        m_ReadForm.simpleButtonRead.Enabled = true;
        //}


        //public void Events_StatusNotify(object sender, Events.StatusEventArgs statusEventArgs)
        //{
        //    try
        //    {
        //        this.Invoke(m_UpdateStatusHandler, new object[] { statusEventArgs.StatusEventData });
        //    }
        //    catch (Exception)
        //    {
        //    }
        //}

        //private void Events_ReadNotify(object sender, Events.ReadEventArgs readEventArgs)
        //{
        //    try
        //    {
        //        this.Invoke(m_UpdateReadHandler, new object[] { readEventArgs.ReadEventData });
        //    }
        //    catch (Exception)
        //    {

        //    }
        //}

        private void simpleButtonConnect_Click(object sender, EventArgs e)
        {
            try
            {
                m_MainForm.connectBackgroundWorker.RunWorkerAsync(simpleButtonConnect.Text);

            }
            catch (Exception ex)
            {
                m_MainForm.StatusBottom.Caption = ex.Message;
                //Logger.WriteLogger(ex.Message);
            }
        }

        private void ConnectionForm_Load(object sender, EventArgs e)
        {

        }
        //public void readButton_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (m_IsConnected)
        //        {
        //            if (m_ReadForm.simpleButtonRead.Text == "Start Reading")
        //            {
        //                m_ReaderAPI.Actions.Inventory.Perform(null, null, null);

        //                m_ReadForm.inventoryList.Items.Clear();
        //                m_TagTable.Clear();
        //                m_TagTotalCount = 0;

        //                m_ReadForm.simpleButtonRead.Text = "Stop Reading";
        //            }
        //            else if (m_ReadForm.simpleButtonRead.Text == "Stop Reading")
        //            {
        //                if (m_ReaderAPI.Actions.TagAccess.OperationSequence.Length > 0)
        //                {
        //                    m_ReaderAPI.Actions.TagAccess.OperationSequence.StopSequence();
        //                }
        //                else
        //                {
        //                    m_ReaderAPI.Actions.Inventory.Stop();
        //                }

        //                m_ReadForm.simpleButtonRead.Text = "Start Reading";
        //            }
        //        }
        //        else
        //        {
        //            m_MainForm.StatusBottom.Caption = "Please connect to a reader";
        //        }
        //    }
        //    catch (InvalidOperationException ioe)
        //    {
        //        m_MainForm.StatusBottom.Caption = ioe.Message;
        //    }
        //    catch (InvalidUsageException iue)
        //    {
        //        m_MainForm.StatusBottom.Caption = iue.Info;
        //    }
        //    catch (OperationFailureException ofe)
        //    {
        //        m_MainForm.StatusBottom.Caption = ofe.Result + ":" + ofe.StatusDescription;
        //    }
        //    catch (Exception ex)
        //    {
        //        m_MainForm.StatusBottom.Caption = ex.Message;
        //    }
        //}


    }

}