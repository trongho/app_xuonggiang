﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WarehouseManager
{
    public partial class EncodeForm : DevExpress.XtraEditors.XtraForm
    {
        internal ReceiptRequisitionControl m_ReceiptRequisitionControl = null;
        public EncodeForm(ReceiptRequisitionControl receiptRequisitionControl)
        {
            InitializeComponent();
            m_ReceiptRequisitionControl = receiptRequisitionControl;
            this.Load += m_ReceiptRequisitionControl.EncodeFrm_Load;
            //this.FormClosing += m_ReceiptRequisitionControl.EncodeFrm_FormClosing;
            comboBoxEditMemoryBank.SelectedIndexChanged += m_ReceiptRequisitionControl.EncodeFrm_memBank_CB_SelectedIndexChanged;
            //simpleButtonEnCode.Click += receiptRequisitionControl.EncodeFrm_writeButton_Click;
            repositoryItemButtonEdit1.ButtonClick += receiptRequisitionControl.repositoryItemButtonEdit1_EncodeFrm_ButtonClick;
        }

        public string Barcode
        {
            get
            {
                return textEditBarcode.Text;
            }
            set
            {
                textEditBarcode.Text = value;
            }
        }

        public int Quantity
        {
            get
            {
                return int.Parse(textEditQuantity.Text);
            }
            set
            {
                textEditQuantity.Text =value+"";
            }
        }

        public int QuantityEncoded
        {
            get
            {
                return int.Parse(textEditEncoded.Text);
            }
            set
            {
                textEditEncoded.Text = value + "";
            }
        }


    }
}