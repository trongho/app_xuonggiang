﻿using DevExpress.XtraEditors;
using Symbol.RFID3;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WarehouseManager
{
    public partial class TagDataForm : DevExpress.XtraEditors.XtraForm
    {
        private ReadForm m_ReadForm = null;
        
        public TagDataForm(ReadForm readForm)
        {
            InitializeComponent();
            m_ReadForm=readForm;
            this.Load += m_ReadForm.TagDataForm_Load;
        }

        
    }
}