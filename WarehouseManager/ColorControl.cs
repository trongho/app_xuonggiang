﻿using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarehouseManager.Models;
using WarehouseManager.Repository;

namespace WarehouseManager
{
   
    public partial class ColorControl : DevExpress.XtraEditors.XtraUserControl
    {
        ColorRepository colorRepository;
        public ColorControl()
        {
            InitializeComponent();
            colorRepository = new ColorRepository();
            this.Load += ColorControl_Load;
        }

        private void ColorControl_Load(Object sender,EventArgs e)
        {
            windowsUIButtonPanel1.ButtonClick += button_Click;
            gridViewData.RowClick += gridView_RowClick;
            gridViewData.CustomColumnDisplayText += GridView_CustomColumnDisplayText;
            sbLoadDataForGridAsync();
        }

        private void GridView_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {

            if (e.Column == colNo)
            {
                int rowHandle = gridViewData.GetRowHandle(e.ListSourceRowIndex);
                if (rowHandle >= 0)
                {
                    e.DisplayText = (rowHandle + 1).ToString();
                }
            }
        }

        private async Task CreatAsync() {
            Color color = new Color();
            if (textEditColorID.Text.Equals("") || textEditColorName.Text.Equals(""))
            {
                return;
            }
            color.ColorID = textEditColorID.Text;
            color.ColorName = textEditColorName.Text;
            color.Description = textEditDescription.Text;
            color.CreatedUserID = WMMessage.User.UserID;
            color.CreatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
            if (await colorRepository.Create(color) > 0)
            {
                WMPublic.sbMessageSaveNewSuccess(this);
                sbLoadDataForGridAsync();
            }
        }
        private async Task UpdateAsync() {
            if (textEditColorID.Text.Equals("") || textEditColorName.Text.Equals(""))
            {
                return;
            }
            Color color = await colorRepository.GetUnderID(textEditColorID.Text);
            color.ColorID = textEditColorID.Text;
            color.ColorName = textEditColorName.Text;
            color.Description = textEditDescription.Text;
            color.UpdatedUserID = WMMessage.User.UserID;
            color.UpdatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
            if (await colorRepository.Update(color,textEditColorID.Text) > 0)
            {
                WMPublic.sbMessageSaveChangeSuccess(this);
                sbLoadDataForGridAsync();
            }
        }

        private async Task DeleteAsync()
        {
            if(await colorRepository.Delete(textEditColorID.Text) > 0)
            {
                WMPublic.sbMessageDeleteSuccess(this);
                sbLoadDataForGridAsync();
            }
        }

        private async Task sbLoadDataForGridAsync() {
            List<Color> colors = await colorRepository.GetAll();
            int mMaxRow = 100;
            if (colors.Count < mMaxRow)
            {
                int num = mMaxRow - colors.Count;
                int i = 1;
                while (true)
                {
                    int num2 = i;
                    int num3 = num;
                    if (num2 > num3)
                    {
                        break;
                    }
                    colors.Add(new Color());
                    i++;
                }
            }
            gridControlData.DataSource = colors;
        }
        private void clear() {
            textEditColorID.Text = "";
            textEditColorName.Text = "";
            textEditDescription.Text = "";
        }
        private async Task<bool> checkExistAsync()
        {
            Boolean isExist = await colorRepository.checkExistAsync(textEditColorID.Text);
            return isExist;
        }

        private void gridView_RowClick(Object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            textEditColorID.Text = (string)(sender as GridView).GetFocusedRowCellValue("ColorID");
            textEditColorName.Text= (string)(sender as GridView).GetFocusedRowCellValue("ColorName");
            textEditDescription.Text= (string)(sender as GridView).GetFocusedRowCellValue("Description");
        }

        //barbutton click
        public async void button_Click(object sender, EventArgs e)
        {

            ButtonEventArgs eArg = (DevExpress.XtraBars.Docking2010.ButtonEventArgs)e;
            String tag = eArg.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "new":
                    if (WMPublic.sbMessageAddNewRequest(this) == true)
                    {
                        clear();
                    }
                    break;
                case "save":

                    if (await checkExistAsync() == true)
                    {
                        if (WMPublic.sbMessageSaveChangeRequest(this) == true)
                        {
                            UpdateAsync();
                        }

                    }
                    else
                    {
                        if (WMPublic.sbMessageSaveNewRequest(this) == true)
                        {
                            CreatAsync();
                        }
                    }
                    break;
                case "delete":
                    if (WMPublic.sbMessageDeleteRequest(this) == true)
                    {
                        DeleteAsync();
                    }
                    break;
                case "search":
                    break;
                case "refesh":
                    sbLoadDataForGridAsync();
                    break;
                case "import":
                    break;
                case "export":
                    break;
                case "print":
                    break;
                case "close":
                    this.Dispose();
                    break;
            }

        }
    }
}
