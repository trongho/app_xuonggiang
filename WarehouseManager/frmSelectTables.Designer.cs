﻿
namespace WarehouseManager
{
    partial class frmSelectTables
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSelectTables));
            this.listViewSheetTable = new System.Windows.Forms.ListView();
            this.simpleButtonOK = new DevExpress.XtraEditors.SimpleButton();
            this.SuspendLayout();
            // 
            // listViewSheetTable
            // 
            this.listViewSheetTable.HideSelection = false;
            this.listViewSheetTable.Location = new System.Drawing.Point(2, 2);
            this.listViewSheetTable.Name = "listViewSheetTable";
            this.listViewSheetTable.Size = new System.Drawing.Size(300, 326);
            this.listViewSheetTable.TabIndex = 0;
            this.listViewSheetTable.UseCompatibleStateImageBehavior = false;
            this.listViewSheetTable.View = System.Windows.Forms.View.List;
            this.listViewSheetTable.SelectedIndexChanged += new System.EventHandler(this.listViewSheetTable_SelectedIndexChanged);
            // 
            // simpleButtonOK
            // 
            this.simpleButtonOK.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonOK.ImageOptions.Image")));
            this.simpleButtonOK.Location = new System.Drawing.Point(216, 334);
            this.simpleButtonOK.Name = "simpleButtonOK";
            this.simpleButtonOK.Size = new System.Drawing.Size(86, 36);
            this.simpleButtonOK.TabIndex = 1;
            this.simpleButtonOK.Text = "OK";
            this.simpleButtonOK.Click += new System.EventHandler(this.simpleButtonOK_Click);
            // 
            // frmSelectTables
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(305, 374);
            this.Controls.Add(this.simpleButtonOK);
            this.Controls.Add(this.listViewSheetTable);
            this.Name = "frmSelectTables";
            this.Text = "frmSelectTables";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView listViewSheetTable;
        private DevExpress.XtraEditors.SimpleButton simpleButtonOK;
    }
}