﻿using Symbol.RFID3;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarehouseManager.Helpers;
using WarehouseManager.Models;

namespace WarehouseManager
{
    public partial class ReadForm : DevExpress.XtraEditors.XtraForm
    {
        internal TagAccess.ReadAccessParams m_ReadParams = null;
        internal MainForm m_MainForm;
        internal TagDataForm m_TagDataForm;

        public ReadForm(MainForm mainForm)
        {
            InitializeComponent();
            this.Load += Read_Load;
            m_MainForm = mainForm;
            m_ReadParams = new TagAccess.ReadAccessParams();
            m_ReadParams.MemoryBank = MEMORY_BANK.MEMORY_BANK_USER;
            m_ReadParams.AccessPassword = 0;
            m_ReadParams.ByteOffset = 0;
            m_ReadParams.ByteCount = 0;

            simpleButtonRead.Click += mainForm.readButton_Click;
            simpleButtonClearTag.Click += mainForm.clearReports_Click;
            this.FormClosing += ReadForm_FormClosing;
            this.simpleButtonSaveDatabase.Click += saveDatabase_Click;

            


        }

        public void ReadForm_FormClosing(object sender, FormClosingEventArgs e)
        {

            this.Hide();

        }

        private void Read_Load(object sender, EventArgs e)
        {
            m_TagDataForm = new TagDataForm(this);
            inventoryList.SelectedIndexChanged += inventoryList_SelectedIndexChanged;
        }

        public void saveDatabase_Click(object sender, EventArgs e)
        {
            m_MainForm.createRFIDTagDataAsync();
        }

        public void inventoryList_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                contextMenuStrip1.Show(inventoryList, new Point(e.X, e.Y));
            }
            //if (e.Button == MouseButtons.Right)
            //{
            //    ContextMenu m = new ContextMenu();
            //    m.MenuItems.Add(new MenuItem("Tag Data"));
            //    m.MenuItems.Add(new MenuItem("Read Tag"));
            //    m.MenuItems.Add(new MenuItem("Write Tag"));



            //    int currentMouseOverRow = inventoryList.HitTest(e.X, e.Y).Item.Index;

            //    ListViewItem item = inventoryList.SelectedItems[0];
            //    m.MenuItems.Add(item.SubItems[0].Text);

            //    if (currentMouseOverRow >= 0)
            //    {
            //        m.MenuItems.Add(new MenuItem(string.Format("Do something to row {0}", currentMouseOverRow.ToString())));
            //    }

            //    m.Show(inventoryList, new Point(e.X, e.Y));


            //}
        }

        public void contextMenuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            //ToolStripItem item = e.ClickedItem;
            //if(item.Text== "Tag Data")
            //{
            //    m_TagDataForm.ShowDialog(this);

            //}
            //switch (item.Text)
            //{
            //    case "Tag Data":
            //        m_TagDataForm.ShowDialog(this);
            //        MessageBox.Show("aaa");
            //        break;
            //    case "Read Tag":
            //        break;
            //    case "Write tag":
            //        break;
            //    default:

            //        break;
            //}

        }

        public void TagDataForm_Load(object sender, EventArgs e)
        {
            try
            {
                if (m_MainForm.m_ReaderAPI.IsConnected)
                {

                    ListViewItem item = inventoryList.SelectedItems[0];

                    //m_TagDataForm.tagDataView.Items[0].SubItems.Add(item.SubItems[0].Text);
                    if (item.SubItems.Count > 0)
                    {
                        m_TagDataForm.tagDataView.Items[0].SubItems.Add(item.SubItems[1].Text);
                        m_TagDataForm.tagDataView.Items[1].SubItems.Add(item.SubItems[3].Text);
                        //m_TagDataForm.tagDataView.Items[3].SubItems.Add(item.SubItems[5].Text);
                        //m_TagDataForm.tagDataView.Items[4].SubItems.Add(item.SubItems[6].Text);
                        //m_TagDataForm.tagDataView.Items[5].SubItems.Add(item.SubItems[7].Text);
                        //m_TagDataForm.tagDataView.Items[6].SubItems.Add(item.SubItems[8].Text);
                        //m_TagDataForm.tagDataView.Items[7].SubItems.Add(item.SubItems[9].Text);
                        //int length = item.SubItems[6].Text.Length / 2;
                        //m_TagDataForm.tagDataView.Items[8].SubItems.Add(length.ToString());
                    }

                }
            }
            catch (Exception ex)
            {
                m_MainForm.StatusBottom.Caption = ex.Message;
            }
        }

        private void inventoryList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (inventoryList.SelectedItems.Count > 0)
            {

            }
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (inventoryList.SelectedItems.Count > 0)
            {
                m_TagDataForm.ShowDialog(this);
               
            }

        }

        private void toolStripMenuItemReadTag_Click(object sender, EventArgs e)
        {
           
        }

        private void toolStripMenuItemWriteTag_Click(object sender, EventArgs e)
        {

        }
    }
}
