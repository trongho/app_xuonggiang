﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WarehouseManager
{
    public partial class WriteTagForm : DevExpress.XtraEditors.XtraForm
    {
        internal ReceiptRequisitionControl m_ReceiptRequisitionControl;
        public WriteTagForm(ReceiptRequisitionControl receiptRequisitionControl)
        {
            InitializeComponent();
            m_ReceiptRequisitionControl = receiptRequisitionControl;
            this.Load += m_ReceiptRequisitionControl.Write_Load;
            this.FormClosing += m_ReceiptRequisitionControl.Write_FormClosing;
            comboBoxEditMemoryBank.SelectedIndexChanged += m_ReceiptRequisitionControl.memBank_CB_SelectedIndexChanged;
            simpleButtonWriteTag.Click += receiptRequisitionControl.writeButton_Click;
        }

        public string Barcode
        {
            get
            {
                return textEditBarcode.Text;
            }
            set
            {
                textEditBarcode.Text = value;
            }
        }
    }
}