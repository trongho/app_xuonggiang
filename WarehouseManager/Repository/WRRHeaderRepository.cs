﻿using DevExpress.XtraEditors.Filtering.Templates;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WarehouseManager.Helpers;
using webservice.Entities;

namespace WarehouseManager.Repository
{
    class WRRHeaderRepository
    {
        public WRRHeaderRepository()
        {
        }

        public async Task<long> Create(WRRHeader entry)
        {
            long result = -1;
            string URI = Constant.BaseURL+"wrrheader/Post";

            using (var client = new HttpClient())
            {
                var content = new StringContent(JsonConvert.SerializeObject(entry), Encoding.UTF8, "application/json");
                try
                {
                    using (var response = await client.PostAsync(URI, content))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            result = 1;
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return result;
        }

        public async Task<List<WRRHeader>> GetAll()
        {
            string URI = Constant.BaseURL+"wrrheader";
            List<WRRHeader> entrys = new List<WRRHeader>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<WRRHeader[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<WRRHeader> GetUnderID(String id)
        {
            string URI = Constant.BaseURL+"wrrheader/GetByID/" + id;
            List<WRRHeader> entrys = new List<WRRHeader>();
            WRRHeader wRRHeader = new WRRHeader();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<WRRHeader[]>(fileJsonString).ToList();
                            if (entrys.Count > 0)
                            {
                                wRRHeader = entrys[0];
                            }
                            else
                            {
                                wRRHeader=null;
                            }
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return wRRHeader;
        }

        public async Task<List<WRRHeader>> GetUnderDate(DateTime fromDate,DateTime toDate)
        {
            String fromDateString = fromDate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
            String toDateString = toDate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
            string URI = Constant.BaseURL+"wrrheader/FilterByDate/" +fromDateString+"/"+toDateString;
            List<WRRHeader> entrys = new List<WRRHeader>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<WRRHeader[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<List<WRRHeader>> GetUnderBranch(String branchID)
        {
            string URI = Constant.BaseURL+"wrrheader/FilterByBranch/" + branchID;
            List<WRRHeader> entrys = new List<WRRHeader>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<WRRHeader[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<List<WRRHeader>> GetUnderHandlingStatus(String handlingStatusID)
        {
            string URI = Constant.BaseURL+"wrrheader/FilterByHandlingStatus/" + handlingStatusID;
            List<WRRHeader> entrys =new List<WRRHeader>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<WRRHeader[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<long> Update(WRRHeader wRRHeader, String id)
        {
            long result = -1;
            string URI = Constant.BaseURL+"wrrheader/Put/" + id;
            using (var client = new HttpClient())
            {
                using (var response = await client.PutAsJsonAsync(URI, wRRHeader))
                {

                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }

        public async Task<long> Delete(String id)
        {
            long result = -1;
            string URI = Constant.BaseURL+"wrrheader/Delete/" + id;
            using (var client = new HttpClient())
            {
                using (var response = await client.DeleteAsync(URI))
                {


                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }

        public async Task<Boolean> checkExistAsync(String id)
        {
            Boolean result = false;
            string URI = Constant.BaseURL+"wrrheader/CheckExist/" + id;
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            result = Boolean.Parse(await response.Content.ReadAsStringAsync());
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return result;
        }

        public async Task<String> GetLastWRRNumber()
        {
            string URI = Constant.BaseURL+"wrrheader/lastWRRNumber";
            String lastWRRNumber = "";
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            lastWRRNumber = fileJsonString;
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return lastWRRNumber;
        }
    }
}
