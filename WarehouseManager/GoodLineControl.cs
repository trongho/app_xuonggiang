﻿
using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarehouseManager.Helpers;
using WarehouseManager.Models;
using LicenseContext = OfficeOpenXml.LicenseContext;

namespace WarehouseManager
{
    public partial class GoodLineControl : DevExpress.XtraEditors.XtraUserControl
    {
        public static string SelectedTable = string.Empty;
        String existedId ="false";

        public GoodLineControl()
        {
            InitializeComponent();
            this.Load += GoodsLineComtrolLoad;
            
        }

        private void GoodsLineComtrolLoad(Object sender,EventArgs e)
        {         
            windowsUIButtonPanel1.ButtonClick += button_Click;
            gridView1.RowCellClick += gridViewRowClick;
            gridView1.KeyDown += gridView_KeyUp;
            gridView1.KeyUp += gridView_KeyUp;
            getAllGoodsLineAsync();
        }


        private void simpleButton1_Click(object sender, EventArgs e)
        {
            selectFileExcel();
        }

        private void selectFileExcel()
        {
            OpenFileDialog fdlg = new OpenFileDialog();
            fdlg.Title = "Select file";
            fdlg.InitialDirectory = @"c:\";
            fdlg.FileName = textEdit1.Text;
            fdlg.Filter = "Excel Sheet(*.xls)|*.xlsx|All Files(*.*)|*.*";
            fdlg.FilterIndex = 1;
            fdlg.RestoreDirectory = true;
            if (fdlg.ShowDialog() == DialogResult.OK)
            {
                textEdit1.Text = fdlg.FileName;
                selectSheet(textEdit1.Text);
                Application.DoEvents();
            }
        }

        private void selectSheet(String fileName)
        {

            if (fileName.Trim() != string.Empty)
            {


                FileInfo file = new FileInfo(Path.Combine(fileName));
                ExcelPackage.LicenseContext = LicenseContext.Commercial;
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                using (ExcelPackage package = new ExcelPackage(file))
                {
                    string[] strTables = package.Workbook.Worksheets.Select(x => x.Name).ToArray();
                    frmSelectTables objSelectTable = new frmSelectTables(strTables);
                    objSelectTable.ShowDialog(this);
                    objSelectTable.Dispose();
                    if ((SelectedTable != string.Empty) && (SelectedTable != null))
                    {
                        List<GoodsLine> goodsLines = ExcelToList(textEdit1.Text);
                        foreach(GoodsLine goodsLine in goodsLines)
                        {
                            createGoodsLineAsync(goodsLine);
                        }
                        getAllGoodsLineAsync();
                        //gridControl1.DataSource = goodsLines.ToList();
                    }
                }
            }
        }

        private List<GoodsLine> ExcelToList(String fileName)
        {          
            FileInfo file = new FileInfo(Path.Combine(fileName));

            ExcelPackage.LicenseContext = LicenseContext.Commercial;
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            using (ExcelPackage package = new ExcelPackage(file))
            {

                ExcelWorksheet workSheet = package.Workbook.Worksheets[SelectedTable];
                int totalRows = workSheet.Dimension.Rows;

                List<GoodsLine> goodsLineList = new List<GoodsLine>();

                for (int i = 2; i <= totalRows; i++)
                {
                    goodsLineList.Add(new GoodsLine
                    {
                        GoodsLineID = workSheet.Cells[i, 1].Value.ToString(),
                        GoodsLineName = workSheet.Cells[i, 2].Value.ToString(),
                        Description = workSheet.Cells[i, 3].Value.ToString()
                    });                   
                }
                return goodsLineList;
            }
        }

        private void saveGridDataToDatabase()
        {
            List<GoodsLine> goodsLines = (gridControl1.DataSource as IEnumerable<GoodsLine>).ToList();
        }


        private async Task clearTextAsync()
        {
            textGoodsLineID.Text = "";
            textEdit3.Text = "";
            textEdit4.Text = "";
        }

        private void gridViewRowClick(Object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                String goodsLineID = (string)(sender as GridView).GetFocusedRowCellValue("GoodsLineID");
                String goodsLineName = (string)(sender as GridView).GetFocusedRowCellValue("GoodsLineName");
                String description = (string)(sender as GridView).GetFocusedRowCellValue("Description");
          
                textGoodsLineID.Text = goodsLineID;
                textEdit3.Text = goodsLineName;
                textEdit4.Text = description;
            }
        }


            private void gridView_KeyUp(Object sender, KeyEventArgs e)
        {
            
                String goodsLineID = (string)(sender as GridView).GetFocusedRowCellValue("GoodsLineID");
                String goodsLineName = (string)(sender as GridView).GetFocusedRowCellValue("GoodsLineName");
                String description = (string)(sender as GridView).GetFocusedRowCellValue("Description");

                textGoodsLineID.Text = goodsLineID;
                textEdit3.Text = goodsLineName;
                textEdit4.Text = description;
           
        }

        public async Task createGoodsLineAsync(GoodsLine goodsLine)
        {
            string URI = "https://localhost:5001/api/goodsline/PostGoodsLine";
            
            using (var client = new HttpClient())
            {
                //client.DefaultRequestHeaders.ExpectContinue = false;
                var content = new StringContent(JsonConvert.SerializeObject(goodsLine), Encoding.UTF8, "application/json");
                try
                {
                    using (var response = await client.PostAsync(URI, content))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            WMPublic.sbMessageCreateGoodsLineSuccess(this);
                            getAllGoodsLineAsync();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
        }

        public async Task createGoodsLineAsync()
        {
            string URI = "https://localhost:5001/api/goodsline/PostGoodsLine";
            GoodsLine goodsLine=new GoodsLine();
            goodsLine.GoodsLineID = textGoodsLineID.Text;
            goodsLine.GoodsLineName = textEdit3.Text;
            goodsLine.Description = textEdit4.Text;


            using (var client = new HttpClient())
            {
                //client.DefaultRequestHeaders.ExpectContinue = false;
                var content = new StringContent(JsonConvert.SerializeObject(goodsLine), Encoding.UTF8, "application/json");
                try
                {
                    using (var response = await client.PostAsync(URI, content))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            WMPublic.sbMessageCreateGoodsLineSuccess(this);
                            getAllGoodsLineAsync();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
        }

        private async Task checkExistAsync(String id)
        {
            string URI = "https://localhost:5001/api/goodsline/CheckExist/"+ id;
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                           existedId = await response.Content.ReadAsStringAsync();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
        }

        public async Task updateDataAsync(String id)
        {
            string URI = "https://localhost:5001/api/goodsline/PutGoodsLine/" + id;
            List<GoodsLine> goodsLines = new List<GoodsLine>();
            GoodsLine goodsLine = new GoodsLine();
            goodsLine.GoodsLineID = textGoodsLineID.Text;
            goodsLine.GoodsLineName = textEdit3.Text;
            goodsLine.Description = textEdit4.Text;

            using (var client = new HttpClient())
            {
                using (var response = await client.PutAsJsonAsync(URI,goodsLine))
                {

                    if (response.IsSuccessStatusCode)
                    {
                        getAllGoodsLineAsync();
                        WMPublic.sbMessageUpdateGoodsLineSuccess(this);
                    }
                }
            }
        }

        public async Task DeleteDataAsync(String id)
        {
            string URI = "https://localhost:5001/api/goodsline/DeleteGoodsLine/" + id;
            
            using (var client = new HttpClient())
            {
                using (var response = await client.DeleteAsync(URI))
                {
                   

                    if (response.IsSuccessStatusCode)
                    {
                        getAllGoodsLineAsync();
                        WMPublic.sbMessageDeleteGoodsLineSuccess(this);
                    }
                    else
                    {
                        MessageBox.Show("Fail");
                    }
                }
            }
        }

        public async Task getAllGoodsLineAsync()
        {            
            string URI = "https://localhost:5001/api/goodsline";
            List<GoodsLine> rights = new List<GoodsLine>();         
            using (var client = new HttpClient())
            {             
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            gridControl1.DataSource = JsonConvert.DeserializeObject<GoodsLine[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
        }


        //change background color row
        private void gridView_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            //if (e.Column == colName)
            //{
                var age =gridView1.GetRowCellValue(e.RowHandle,gridView1.Columns["Goods Line ID"]);
                if (age.Equals("A1"))
                    e.Appearance.BackColor = System.Drawing.Color.Red;
                else
                    e.Appearance.BackColor = System.Drawing.Color.FromArgb(0xD2, 0xFD, 0x91);
            //}
        }


        public void button_Click(object sender, EventArgs e)
        {

            ButtonEventArgs eArg = (DevExpress.XtraBars.Docking2010.ButtonEventArgs)e;
            String tag = eArg.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "new":
                    /* Navigate to page A */
                    clearTextAsync();
                    break;
                case "save":
                    /* Navigate to page B */
                    checkExistAsync(textGoodsLineID.Text);
                    if (existedId.Equals("true"))
                    {
                        updateDataAsync(textGoodsLineID.Text);
                    }
                    else
                    {
                        createGoodsLineAsync();

                    }
                    
                    break;
                case "delete":
                    /* Navigate to page C*/
                    
                    DeleteDataAsync(textGoodsLineID.Text);
                    break;
                case "search":
                    /* Navigate to page D */
                                    
                    break;
                case "Ad5":
                    /* Navigate to page E */
                    break;
            }
        }

    }
}
