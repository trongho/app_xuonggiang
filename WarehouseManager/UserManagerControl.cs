﻿using DevExpress.XtraBars;
using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraTab;
using DevExpress.XtraTab.ViewInfo;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarehouseManager.Models;
using WarehouseManager.Repository;

namespace WarehouseManager
{
    public partial class UserManagerControl : DevExpress.XtraEditors.XtraUserControl
    {
        UserRepository userRepository;
        UserRightRepository userRightRepository;
        RightRepository rightRepository;
        ScreenRepository screenRepository;
        UserRightOnScreenRepository userRightOnScreenRepository;
        ReportRepository reportRepository;
        UserRightOnReportRepository UserRightOnReportRepository;
        BranchRepository branchRepository;
        Boolean isValueExist = false;
        Boolean isValueUserScreenIDExist = false;

        Int16 SaveNewRight = 0;
        Int16 SaveChangeRight = 0;
        Int16 DeleteRight = 0;
        Int16 PrintRight = 0;
        Int16 ImportRight = 0;
        Int16 ExportRight = 0;

        String lastUserID = "";

        List<Models.Screen> curentScreenUnderUserIDs = new List<Models.Screen>();
        public UserManagerControl()
        {
            InitializeComponent();
            updateLanguage();
            userRepository = new UserRepository();
            userRightRepository = new UserRightRepository();
            rightRepository = new RightRepository();
            screenRepository = new ScreenRepository();
            userRightOnScreenRepository = new UserRightOnScreenRepository();
            reportRepository = new ReportRepository();
            UserRightOnReportRepository = new UserRightOnReportRepository();
            branchRepository = new BranchRepository();
            this.Load += UserManagerControlLoad;

        
        }

        private void UserManagerControlLoad(Object sender, EventArgs e)
        {
            popupMenuSelectBranchAsync();
            checkGranRight();
            
            windowsUIButtonPanel1.ButtonClick += button_Click;
            sbLoadDataForGridRightAsync();
            sbLoadDataForGridUserAsync();
            sbLoadDataForGridScreenAsync();
            sbLoadDataForGridReportAsync();
            customCheckEdit();
           
            gridViewUser.RowClick += gridViewUser_RowClick;
            gridViewUser.CustomColumnDisplayText += userGridView_CustomColumnDisplayText;
            gridViewRight.CustomColumnDisplayText += rightGridView_CustomColumnDisplayText;
            gridViewScreen.CustomColumnDisplayText += screenGridView_CustomColumnDisplayText;
            gridViewReport.CustomColumnDisplayText += reportGridView_CustomColumnDisplayText;
           
            //this.repositoryItemCheckEdit1.QueryCheckStateByValue += new DevExpress.XtraEditors.Controls.QueryCheckStateByValueEventHandler(this.repositoryItemCheckEdit1_QueryCheckStateByValue);
            //gridViewRight.OptionsSelection.MultiSelect = true;
            //gridViewRight.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.RowSelect;
            //gridViewRight.MouseDown += setRight_MouseUp;
        }

        public async Task getLastUserAsync()
        {
            lastUserID = await userRepository.GetLastUserID();
        }

        private void updateLanguage()
        {
            ResourceManager rm;
            String cultureName = WMMessage.msgLanguage;
            if (cultureName.Equals("vi-VN"))
            {
                rm = new
                   ResourceManager("WarehouseManager.Resource.vi_local", typeof(UserManagerControl).Assembly);
            }
            else
            {
                rm = new
                    ResourceManager("WarehouseManager.Resource.en_local", typeof(UserManagerControl).Assembly);
            }

            //WindowsUIButton buttonAdd = windowsUIButtonPanel1.Buttons[0] as WindowsUIButton;
            ((WindowsUIButton)windowsUIButtonPanel1.Buttons[0]).Caption = rm.GetString("addnew");
            ((WindowsUIButton)windowsUIButtonPanel1.Buttons[2]).Caption = rm.GetString("save");
            ((WindowsUIButton)windowsUIButtonPanel1.Buttons[4]).Caption = rm.GetString("delete");
            ((WindowsUIButton)windowsUIButtonPanel1.Buttons[6]).Caption = rm.GetString("search");
            ((WindowsUIButton)windowsUIButtonPanel1.Buttons[8]).Caption = rm.GetString("refesh");
            ((WindowsUIButton)windowsUIButtonPanel1.Buttons[10]).Caption = rm.GetString("import_data");
            ((WindowsUIButton)windowsUIButtonPanel1.Buttons[12]).Caption = rm.GetString("export_data");
            ((WindowsUIButton)windowsUIButtonPanel1.Buttons[14]).Caption = rm.GetString("print");
            ((WindowsUIButton)windowsUIButtonPanel1.Buttons[16]).Caption = rm.GetString("exit");

            layoutControlItemUserId.Text = rm.GetString("userid");
            layoutControlItemUsername.Text = rm.GetString("username");
            layoutControlItemPassword.Text = rm.GetString("password");
            layoutControlItemFullname.Text = rm.GetString("fullname");
            layoutControlItemDescription.Text = rm.GetString("description");
            layoutControlItemBranch.Text = rm.GetString("branch");
            layoutControlItemEmail.Text = rm.GetString("email");
            layoutControlItemPosition.Text = rm.GetString("position");
            layoutControlItemAdress.Text = rm.GetString("adress");
            layoutControlItemPhone.Text = rm.GetString("phone");
        }

        private async Task popupMenuSelectBranchAsync()
        {
            List<Branch> branches = await branchRepository.getAllBranchAsync();
            foreach (Branch branch in branches)
            {
                comboBoxEditBranch.Properties.Items.Add(branch.BranchName);
            }
            comboBoxEditBranch.SelectedIndex = 0;
        }

        private void customCheckEdit()
        {
            RepositoryItemCheckEdit checkEdit = new RepositoryItemCheckEdit();
            gridControlScreen.RepositoryItems.Add(checkEdit);
            gridViewScreen.Columns["SaveNewRight"].ColumnEdit = checkEdit;
            gridViewScreen.Columns["SaveChangeRight"].ColumnEdit = checkEdit;
            gridViewScreen.Columns["DeleteRight"].ColumnEdit = checkEdit;
            gridViewScreen.Columns["PrintRight"].ColumnEdit = checkEdit;
            gridViewScreen.Columns["ImportRight"].ColumnEdit = checkEdit;
            gridViewScreen.Columns["ExportRight"].ColumnEdit = checkEdit;
            gridViewReport.Columns["ViewRight"].ColumnEdit = checkEdit;
            gridViewReport.Columns["PrintRight"].ColumnEdit = checkEdit;
            gridViewRight.Columns["GrantRight"].ColumnEdit = checkEdit;
            checkEdit.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.UserDefined;
            checkEdit.PictureChecked = Helpers.ImageHelper.resizeImage(Properties.Resources.active, new Size(10, 10));
            checkEdit.PictureUnchecked = Helpers.ImageHelper.resizeImage(Properties.Resources.blocked, new Size(10, 10));
            checkEdit.ValueChecked = (long)1;
            checkEdit.ValueUnchecked = (long)0;
          
            checkEdit.QueryCheckStateByValue+= new DevExpress.XtraEditors.Controls.QueryCheckStateByValueEventHandler(this.repositoryItemCheckEdit1_QueryCheckStateByValue);
        }
          
       private void CheckEdit1_CheckedChanged1(object sender, EventArgs e)
        {
            var edit = (CheckEdit)sender;
            //XtraMessageBox.Show(string.Format("CheckEdit Value: {0}", edit.EditValue));
            SaveNewRight=Int16.Parse(edit.EditValue+"");

            MessageBox.Show((string)gridViewScreen.GetFocusedRowCellValue("ScreenName")+SaveNewRight + "");

            
        }

        // Return the total amount for a specific row.
        decimal getTotalValue(GridView view, int listSourceRowIndex)
        {
            decimal unitPrice = Convert.ToDecimal(view.GetListSourceRowCellValue(listSourceRowIndex, "UnitPrice"));
            decimal quantity = Convert.ToDecimal(view.GetListSourceRowCellValue(listSourceRowIndex, "Quantity"));
            decimal discount = Convert.ToDecimal(view.GetListSourceRowCellValue(listSourceRowIndex, "Discount"));
            return unitPrice * quantity * (1 - discount);
        }

        private void repositoryItemCheckEdit1_QueryCheckStateByValue(object sender, DevExpress.XtraEditors.Controls.QueryCheckStateByValueEventArgs e)
        {
            string val = "";
            if (e.Value != null)
            {
                val = e.Value.ToString();
            }
            else
            {
                val = "False";//The default is not selected   
            }
            switch (val)
            {
                case "True":
                case "Yes":
                case "1":
                    e.CheckState = CheckState.Checked;
                    break;
                case "False":
                case "No":
                case "0":
                    e.CheckState = CheckState.Unchecked;
                    break;
                default:
                    e.CheckState = CheckState.Unchecked;
                    break;
            }
            e.Handled = true;
        }

        private async Task sbLoadDataForGridUserAsync()
        {
            gridControlUser.DataSource = await userRepository.getAllUserAsync();
        }

        private async Task sbLoadDataForGridRightAsync()
        {
            gridControlRight.DataSource = await rightRepository.getAllRightAsync();
        }

        private async Task sbLoadDataForGridScreenAsync()
        {         
            gridControlScreen.DataSource = await screenRepository.getAllScreenAsync();
            
        }
        private async Task sbLoadDataForGridReportAsync()
        {
            gridControlReport.DataSource = await reportRepository.getAllReportAsync();

        }

        private async Task sbLoadDataForGridScreenUnderUserIDAsync(String userID)
        {
            List<Models.Screen> screens = await getListScreenFromUserIdAsync(userID);
            gridControlScreen.DataSource = screens;

        }
        private async Task sbLoadDataForGridReportUnderUserIDAsync(String userID)
        {
            List<Report> reports = await getListReportFromUserIdAsync(userID);
            gridControlReport.DataSource = reports;

        }
        private async Task sbLoadDataForGridRightUnderUserIDAsync(String userID)
        {
            List<Right> rights = await getListRightFromUserIdAsync(userID);
            gridControlRight.DataSource = rights;

        }

        private void userGridView_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {

            if (e.Column == colNo2)
            {
                int rowHandle = gridViewUser.GetRowHandle(e.ListSourceRowIndex);
                if (rowHandle >= 0)
                {
                    e.DisplayText = (rowHandle + 1).ToString();
                }
            }
        }

        private void rightGridView_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {

            if (e.Column == colNo)
            {
                int rowHandle = gridViewRight.GetRowHandle(e.ListSourceRowIndex);
                if (rowHandle >= 0)
                {
                    e.DisplayText = (rowHandle + 1).ToString();
                }
            }
        }

        private void screenGridView_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {

            if (e.Column == colNo3)
            {
                int rowHandle = gridViewScreen.GetRowHandle(e.ListSourceRowIndex);
                if (rowHandle >= 0)
                {
                    e.DisplayText = (rowHandle + 1).ToString();
                }
            }
        }

        private void reportGridView_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {

            if (e.Column == colNo4)
            {
                int rowHandle = gridViewReport.GetRowHandle(e.ListSourceRowIndex);
                if (rowHandle >= 0)
                {
                    e.DisplayText = (rowHandle + 1).ToString();
                }
            }
        }

        private async Task clearTextAsync()
        {
            await getLastUserAsync();
            textEditUserID.Text = "";
            textEditUsername.Text = "";
            textEditFullName.Text = "";
            textEditEmail.Text = "";
            textEditAdress.Text = "";
            textEditPosition.Text = "";
            textEditPassword.Text = "";
        }

        public async Task createUserAsync()
        {
            User user = new User();
            user.BranchID = $"{int.Parse(comboBoxEditBranch.SelectedIndex.ToString()) - 1:D3}";
            user.BranchName = comboBoxEditBranch.SelectedItem.ToString();
            user.UserName = textEditUsername.Text;
            user.Password = textEditPassword.Text;
            user.LoginName = textEditFullName.Text;
            user.Email = textEditEmail.Text;
            user.Adress = textEditAdress.Text;
            user.Position = textEditPosition.Text;
            user.UserID = textEditUserID.Text;
            user.CreatedUserID = WMMessage.User.UserID;
            user.CreateDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
            if (checkEditActive.Checked)
            {
                user.Active = true;
            }
            else
            {
                user.Active = false;
            }
            if (checkEditBlocked.Checked)
            {
                user.Blocked = true;
            }
            else
            {
                user.Blocked = false;
            }



            if (await userRepository.createUserAsync(user) > 0)
            {
                createUserRightOnScreen();
                createUserRightOnReport();
                createUserRightAsync();
                sbLoadDataForGridUserAsync();
                WMPublic.sbMessageCreateUserSuccess(this);
            }
        }

        public async Task updateUserAsync()
        {
            User user = new User();
            user.BranchID = $"{int.Parse(comboBoxEditBranch.SelectedIndex.ToString()) - 1:D3}";
            user.BranchName = comboBoxEditBranch.SelectedItem.ToString();
            user.UserName = textEditUsername.Text;
            user.Password = textEditPassword.Text;
            user.LoginName = textEditFullName.Text;
            user.Email = textEditEmail.Text;
            user.Adress = textEditAdress.Text;
            user.Position = textEditPosition.Text;
            user.Password = textEditPassword.Text;
            user.UserID = textEditUserID.Text;
            user.UpdatedUserID = WMMessage.User.UserID;
            user.UpdatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
            if (checkEditActive.Checked)
            {
                user.Active = true;
            }
            else
            {
                user.Active = false;
            }
            if (checkEditBlocked.Checked)
            {
                user.Blocked = true;
            }
            else
            {
                user.Blocked = false;
            }
            

            if (await userRepository.updateUserAsync(user, textEditUserID.Text) > 0)
            {
                updateUserRightOnScreenAsync();
                updateUserRightOnReportAsync();
                updateUserRightAsync();
                sbLoadDataForGridUserAsync();
                WMPublic.sbMessageUpdateUserSuccess(this);
            }
        }

        public async Task DeleteUserAsync()
        {
            if ((await userRepository.DeleteDataAsync(textEditUserID.Text)) > 0)
            {
                DeleteUserRightOnScreenAsync();
                DeleteUserRightOnReportAsync();
                DeleteUserRightAsync();
                sbLoadDataForGridUserAsync();
                WMPublic.sbMessageDeleteUserSuccess(this);
            }
        }

        //create right on screen
        public async Task createUserRightOnScreen()
        {
            int num = gridViewScreen.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewScreen.GetRowCellValue(i, gridViewScreen.Columns["colScreenID"]) == "")
                {
                    break;
                }
                int rowHandle = gridViewScreen.GetRowHandle(i);
                UserRightOnScreen userRightOnScreen = new UserRightOnScreen();
                userRightOnScreen.UserID = textEditUserID.Text;           
                userRightOnScreen.Ordinal = (short?)(i + 1);
                userRightOnScreen.ScreenID = (string)gridViewScreen.GetRowCellValue(rowHandle, "ScreenID");
                userRightOnScreen.ScreenName = (string)gridViewScreen.GetRowCellValue(rowHandle, "ScreenName");
                userRightOnScreen.ScreenNameEN = (string)gridViewScreen.GetRowCellValue(rowHandle, "ScreenNameEN");
                userRightOnScreen.SaveNewRight = (Int16)gridViewScreen.GetRowCellValue(rowHandle, "SaveNewRight");
                userRightOnScreen.SaveChangeRight = (Int16)gridViewScreen.GetRowCellValue(rowHandle, "SaveChangeRight");
                userRightOnScreen.DeleteRight = (Int16)gridViewScreen.GetRowCellValue(rowHandle, "DeleteRight");
                userRightOnScreen.PrintRight = (Int16)gridViewScreen.GetRowCellValue(rowHandle, "PrintRight");
                userRightOnScreen.ImportRight = (Int16)gridViewScreen.GetRowCellValue(rowHandle, "ImportRight");
                userRightOnScreen.ExportRight = (Int16)gridViewScreen.GetRowCellValue(rowHandle, "ExportRight");
                if (await userRightOnScreenRepository.createUserRightOnscreenAsync(userRightOnScreen) > 0)
                {
                    i++;
                }
            }
        }


        //update right on screen
        public async Task updateUserRightOnScreenAsync()
        {

            int num = gridViewScreen.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewScreen.GetRowCellValue(i, gridViewScreen.Columns["colScreenID"]) == "")
                {
                    break;
                }
                int rowHandle = gridViewScreen.GetRowHandle(i);
                UserRightOnScreen userRightOnScreen = new UserRightOnScreen();

                userRightOnScreen.UserID = textEditUserID.Text;
                userRightOnScreen.Ordinal = (short?)(i + 1);
                userRightOnScreen.ScreenID = (string)gridViewScreen.GetRowCellValue(rowHandle, "ScreenID");
                userRightOnScreen.ScreenName = (string)gridViewScreen.GetRowCellValue(rowHandle, "ScreenName");
                userRightOnScreen.ScreenNameEN = (string)gridViewScreen.GetRowCellValue(rowHandle, "ScreenNameEN");
                userRightOnScreen.SaveNewRight = (Int16)gridViewScreen.GetRowCellValue(rowHandle, "SaveNewRight");
                userRightOnScreen.SaveChangeRight = (Int16)gridViewScreen.GetRowCellValue(rowHandle, "SaveChangeRight");
                userRightOnScreen.DeleteRight = (Int16)gridViewScreen.GetRowCellValue(rowHandle, "DeleteRight");
                userRightOnScreen.PrintRight = (Int16)gridViewScreen.GetRowCellValue(rowHandle, "PrintRight");
                userRightOnScreen.ImportRight = (Int16)gridViewScreen.GetRowCellValue(rowHandle, "ImportRight");
                userRightOnScreen.ExportRight = (Int16)gridViewScreen.GetRowCellValue(rowHandle, "ExportRight");
                if (await userRightOnScreenRepository.updateUserRightOnScreen(userRightOnScreen, textEditUserID.Text, userRightOnScreen.ScreenID) > 0)
                {
                    i++;
                }
            }
        }

        public async Task DeleteUserRightOnScreenAsync()
        {
            List<Models.Screen> screens = await getListScreenFromUserIdAsync(textEditUserID.Text);
            foreach (Models.Screen screen in screens)
            {
                if ((await userRightOnScreenRepository.DeleteDataAsync(textEditUserID.Text, screen.ScreenID) > 0))
                {
                    sbLoadDataForGridScreenUnderUserIDAsync(textEditUserID.Text);
                }
            }
        }

        //create right on report
        public async Task createUserRightOnReport()
        {
            int num = gridViewReport.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewReport.GetRowCellValue(i, gridViewReport.Columns["colReportID"]) == "")
                {
                    break;
                }
                int rowHandle = gridViewReport.GetRowHandle(i);
                UserRightOnReport userRightOnReport = new UserRightOnReport();
                userRightOnReport.UserID = textEditUserID.Text;
                userRightOnReport.Ordinal = (short?)(i + 1);
                userRightOnReport.ReportID = (string)gridViewReport.GetRowCellValue(rowHandle, "ReportID");
                userRightOnReport.ReportName = (string)gridViewReport.GetRowCellValue(rowHandle, "ReportName");
                userRightOnReport.ReportNameEN = (string)gridViewReport.GetRowCellValue(rowHandle, "ReportNameEN");
                userRightOnReport.ViewRight = (Int16)gridViewReport.GetRowCellValue(rowHandle, "ViewRight");
                userRightOnReport.PrintRight = (Int16)gridViewReport.GetRowCellValue(rowHandle, "PrintRight");
                if (await UserRightOnReportRepository.createUserRightOnReportAsync(userRightOnReport) > 0)
                {
                    i++;
                }
            }
        }


        //update right on screen
        public async Task updateUserRightOnReportAsync()
        {

            int num = gridViewReport.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewReport.GetRowCellValue(i, gridViewReport.Columns["colReportID"]) == "")
                {
                    break;
                }
                int rowHandle = gridViewReport.GetRowHandle(i);
                UserRightOnReport userRightOnReport = new UserRightOnReport();
                userRightOnReport.UserID = textEditUserID.Text;
                userRightOnReport.Ordinal = (short?)(i + 1);
                userRightOnReport.ReportID = (string)gridViewReport.GetRowCellValue(rowHandle, "ReportID");
                userRightOnReport.ReportName = (string)gridViewReport.GetRowCellValue(rowHandle, "ReportName");
                userRightOnReport.ReportNameEN = (string)gridViewReport.GetRowCellValue(rowHandle, "ReportNameEN");
                userRightOnReport.ViewRight = (Int16)gridViewReport.GetRowCellValue(rowHandle, "ViewRight");
                userRightOnReport.PrintRight = (Int16)gridViewReport.GetRowCellValue(rowHandle, "PrintRight");
                if (await UserRightOnReportRepository.updateUserRightOnReport(userRightOnReport, textEditUserID.Text, userRightOnReport.ReportID) > 0)
                {
                    i++;
                }
            }
        }

        public async Task DeleteUserRightOnReportAsync()
        {
            List<Report> reports = await getListReportFromUserIdAsync(textEditUserID.Text);
            foreach (Report report in reports)
            {
                if ((await UserRightOnReportRepository.DeleteUserRightOnReportAsync(textEditUserID.Text, report.ReportID) > 0))
                {
                    sbLoadDataForGridReportUnderUserIDAsync(textEditUserID.Text);
                }
            }
        }

        //create user right
        public async Task createUserRightAsync()
        {
            int num = gridViewRight.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewRight.GetRowCellValue(i, gridViewScreen.Columns["colRightID"]) == "")
                {
                    break;
                }
                int rowHandle = gridViewScreen.GetRowHandle(i);
                UserRight userRight = new UserRight();
                userRight.UserID = textEditUserID.Text;
                userRight.Ordinal = (short?)(i + 1);
                userRight.RightID = (string)gridViewRight.GetRowCellValue(rowHandle, "RightID");
                userRight.RightName = (string)gridViewRight.GetRowCellValue(rowHandle, "RightName");
                userRight.RightNameEN = (string)gridViewRight.GetRowCellValue(rowHandle, "RightNameEN");
                userRight.GrantRight = (Int16)gridViewRight.GetRowCellValue(rowHandle, "GrantRight");
                if (await userRightRepository.createUserRightAsync(userRight) > 0)
                {
                    i++;
                }
            }          
        }

        public async Task updateUserRightAsync()
        {

            int num = gridViewRight.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewRight.GetRowCellValue(i, gridViewRight.Columns["colRightID"]) == "")
                {
                    break;
                }
                int rowHandle = gridViewScreen.GetRowHandle(i);
                UserRight userRight = new UserRight();

                userRight.UserID = textEditUserID.Text;
                userRight.Ordinal = (short?)(i + 1);
                userRight.RightID = (string)gridViewRight.GetRowCellValue(rowHandle, "RightID");
                userRight.RightName = (string)gridViewRight.GetRowCellValue(rowHandle, "RightName");
                userRight.RightNameEN = (string)gridViewRight.GetRowCellValue(rowHandle, "RightNameEN");
                userRight.GrantRight = (Int16)gridViewRight.GetRowCellValue(rowHandle, "GrantRight");
                if (await userRightRepository.updateUserRight(userRight, textEditUserID.Text, userRight.RightID) > 0)
                {
                    i++;
                }
            }
        }

        public async Task DeleteUserRightAsync()
        {
            List<Right> rights = await getListRightFromUserIdAsync(textEditUserID.Text);
            foreach (Right right in rights)
            {
                if ((await userRightRepository.DeleteDataAsync(textEditUserID.Text,right.RightID) > 0))
                {
                    sbLoadDataForGridRightUnderUserIDAsync(textEditUserID.Text);
                }
            }
        }

        private async Task<List<Models.Screen>> getListScreenFromUserIdAsync(String userID)
        {
            List<Models.Screen> screens = new List<Models.Screen>();
            List<UserRightOnScreen>  userRightOnScreens = await userRightOnScreenRepository.getUserRightOnScreenAsync(userID);
            foreach(UserRightOnScreen userRightOnScreen in userRightOnScreens)
            {
                Models.Screen screen = new Models.Screen();
                screen.ScreenID = userRightOnScreen.ScreenID;
                screen.Ordinal = userRightOnScreen.Ordinal;
                screen.ScreenName = userRightOnScreen.ScreenName;
                screen.ScreenNameEN = userRightOnScreen.ScreenNameEN;
                screen.SaveNewRight = userRightOnScreen.SaveNewRight;
                screen.SaveChangeRight = userRightOnScreen.SaveChangeRight;
                screen.DeleteRight = userRightOnScreen.DeleteRight;
                screen.PrintRight = userRightOnScreen.PrintRight;
                screen.ImportRight = userRightOnScreen.ImportRight;
                screen.ExportRight = userRightOnScreen.ExportRight;
                screens.Add(screen);
            }
            return screens;
        }

        private async Task<List<Report>> getListReportFromUserIdAsync(String userID)
        {
            List<Report> reports = new List<Report>();
            List<UserRightOnReport> userRightOnReports = await UserRightOnReportRepository.getUserRightOnReportAsync(userID);
            foreach (UserRightOnReport userRightOnReport in userRightOnReports)
            {
                Report report = new Report();
                report.ReportID = userRightOnReport.UserID;
                report.ReportName = userRightOnReport.ReportName;
                report.ReportNameEN = userRightOnReport.ReportNameEN;
                report.Ordinal = userRightOnReport.Ordinal;
                report.ViewRight = userRightOnReport.ViewRight;
                report.PrintRight = userRightOnReport.PrintRight;
                reports.Add(report);
            }
            return reports;
        }

        private async Task<List<Right>> getListRightFromUserIdAsync(String userID)
        {
            List<Right> rights = new List<Right>();
            List<UserRight> userRights = await userRightRepository.getUserRightByUserIDAsync(userID);
            foreach (UserRight userRight in userRights)
            {
                Right right = new Right();
                right.RightID = userRight.RightID;
                right.Ordinal = userRight.Ordinal;
                right.RightName = userRight.RightName;
                right.RightNameEN = userRight.RightNameEN;
                right.GrantRight = userRight.GrantRight;
                rights.Add(right);
            }
            return rights;
        }



        private void gridViewUser_RowClick(Object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            String userID = (string)(sender as GridView).GetFocusedRowCellValue("UserID");
            String username = (string)(sender as GridView).GetFocusedRowCellValue("UserName");
            String fullname = (string)(sender as GridView).GetFocusedRowCellValue("LoginName");
            String email = (string)(sender as GridView).GetFocusedRowCellValue("Email");
            String adress = (string)(sender as GridView).GetFocusedRowCellValue("Adress");
            String position = (string)(sender as GridView).GetFocusedRowCellValue("Position");

            textEditUserID.Text = userID;
            textEditUsername.Text = username;
            textEditFullName.Text = fullname;
            textEditEmail.Text = email;
            textEditAdress.Text = adress;
            textEditPosition.Text = position;
            textEditPassword.Text = "";
            checkExistAsync();
            sbLoadDataForGridScreenUnderUserIDAsync(userID);
            sbLoadDataForGridRightUnderUserIDAsync(userID);
            sbLoadDataForGridReportUnderUserIDAsync(userID);
        }

        private void gridView_KeyUp(Object sender, KeyEventArgs e)
        {

            String userID = (string)(sender as GridView).GetFocusedRowCellValue("UserID");
            String username = (string)(sender as GridView).GetFocusedRowCellValue("UserName");
            String fullname = (string)(sender as GridView).GetFocusedRowCellValue("LoginName");
            String email = (string)(sender as GridView).GetFocusedRowCellValue("Email");
            String adress = (string)(sender as GridView).GetFocusedRowCellValue("Adress");
            String position = (string)(sender as GridView).GetFocusedRowCellValue("Position");

            textEditUserID.Text = userID;
            textEditUsername.Text = username;
            textEditFullName.Text = fullname;
            textEditEmail.Text = email;
            textEditAdress.Text = adress;
            textEditPosition.Text = position;
            textEditPassword.Text = "";

        }
   
        //check input text
        private bool WithErrors()
        {
            if (textEditUsername.Text.Trim() == String.Empty)
                return true; // Returns true if no input or only space is found
            if (textEditPassword.Text.Trim() == String.Empty)
                return true;
            // Other textBoxes.

            return false;
        }

        //check exist user id
        private async Task checkExistAsync()
        {
            if ((await userRepository.checkExistAsync(textEditUserID.Text)) == true)
            {
                isValueExist = true;
            }
          
        }
     
        //check grant right
        private async Task checkGranRight()
        {
            String userId = WMMessage.User.UserID;         
            List<Right> rights = await rightRepository.getAllRightAsync();
            bool exitLoop=false;
            foreach (Right right in rights)
            {
                UserRight userRight = await userRightRepository.getUserRightAsync(userId, right.RightID);
                Int16 grantRight = (Int16)userRight.GrantRight;
                switch (right.RightID)
                {
                    case "AdjustGGD":
                        if (userRight!=null && grantRight == 0)
                            ((WindowsUIButton)windowsUIButtonPanel1.Buttons[0]).Enabled = false;
                            exitLoop = false;
                        break;
                    case "AdjustIO":
                        if (userRight != null && grantRight == 0)
                            ((WindowsUIButton)windowsUIButtonPanel1.Buttons[2]).Enabled = false;
                            exitLoop = false;
                        break;
                }
                if (exitLoop) break;
            }
           
        }

        //barbutton click
        public void button_Click(object sender, EventArgs e)
        {

            ButtonEventArgs eArg = (DevExpress.XtraBars.Docking2010.ButtonEventArgs)e;
            String tag = eArg.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "new":
                    
                    clearTextAsync();
                    break;
                case "save":
                    if (WithErrors())
                    {
                        WMPublic.sbMessageEmptyFieldError(this);
                    }
                    else
                    {
                        if (isValueExist == true)
                        {
                            updateUserAsync();
                           
                        }
                        else
                        {
                            createUserAsync();                           
                        }
                    }
                    
                    break;
                case "delete":
                    DeleteUserAsync();
                    break;
                case "search":                 
                    break;
                case "refesh":
                    sbLoadDataForGridUserAsync();
                    break;
                case "import":
                    
                    break;
                case "export":
                    break;
                case "print":
                    break;
                case "close":
                    break;
            }

        }
    }
}
