﻿using DevExpress.Utils;
using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraGrid.Accessibility;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using OfficeOpenXml.FormulaParsing.Excel.Operators;
using QRCoder;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Media;
using WarehouseManager.Helpers;
using WarehouseManager.Models;
using WarehouseManager.Repository;
using webservice.Entities;

namespace WarehouseManager
{
    public partial class ReceiptDataControl : DevExpress.XtraEditors.XtraUserControl
    {
        WRDataHeaderRepository wRDataHeaderRepository;
        WRDataDetailRepository wRDataDetailRepository;
        WRDataGeneralRepository wRDataGeneralRepository;
        WRRHeaderRepository wRRHeaderRepository;
        HandlingStatusRepository handlingStatusRepository;
        WRHeaderRepository wRHeaderRepository;
        WRDetailRepository wRDetailRepository;
        public static String selectedWRDNumber;
        Timer t = new Timer();
        internal MainForm m_MainForm = null;

        Boolean isGeneralClick = false;
        public ListView inventoryListRD = null;
        public List<string> tagIDs = null;
        Boolean isTagIDsExist = false;
        List<WRDataGeneral> wRDataGeneralsPublic = null;
        WRDataHeader wRDataHeaderPublic = null;

        public ReceiptDataControl(MainForm mainForm)
        {
            InitializeComponent();
            tagIDs = new List<string>();
            m_MainForm = mainForm;
            wRDataHeaderRepository = new WRDataHeaderRepository();
            wRDataDetailRepository = new WRDataDetailRepository();
            wRDataGeneralRepository = new WRDataGeneralRepository();
            wRRHeaderRepository = new WRRHeaderRepository();
            wRHeaderRepository = new WRHeaderRepository();
            wRDetailRepository = new WRDetailRepository();
            handlingStatusRepository = new HandlingStatusRepository();
            popupMenuSelectHandlingStatus();
            sbLoadDataForGridWRDataGeneralAsync(textEditWRDNumber.Text);
            this.Load += ReceiptDataControl_Load;
        }

        private void ReceiptDataControl_Load(Object sender, EventArgs e)
        {
            windowsUIButtonPanel1.ButtonClick += button_Click;
            textEditWRDNumber.KeyDown += new KeyEventHandler(WRDNumber_KeyDown);
            textEditWRDNumber.DoubleClick += textEditWDRNumer_CellDoubleClick;
            gridViewWRDataDetail.CustomDrawCell += grdData_CustomDrawCell;
            gridViewWRDataDetail.CustomColumnDisplayText += GridView_CustomColumnDisplayText;
            simpleButtonOn.Click += butOn_Click;
            simpleButtonOff.Click += butOff_Click;
            t.Tick += new EventHandler(this.Timer_Tick);
            

            textEditTotalTag.Text = m_MainForm.tagIDs.Count + "";

            inventoryListRD = new ListView();
            List<String> headerList = new List<String>();
            headerList.Add("EPCID");
            headerList.Add("Antenna");
            headerList.Add("TagCount");
            headerList.Add("RSSI");
            headerList.Add("PCBits");
            headerList.Add("MemoryBankData");
            headerList.Add("MB");
            headerList.Add("Offset");
            headerList.ForEach(name => inventoryListRD.Columns.Add(name));

        }

        private void GridView_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {

            if (e.Column == colNo)
            {
                int rowHandle = gridViewWRDataDetail.GetRowHandle(e.ListSourceRowIndex);
                if (rowHandle >= 0)
                {
                    e.DisplayText = (rowHandle + 1).ToString();
                }
            }
        }

        private async Task popupMenuSelectHandlingStatus()
        {
            List<HandlingStatus> handlingStatuses = await handlingStatusRepository.GetAll();
            foreach (HandlingStatus handlingStatus in handlingStatuses)
            {
                comboBoxEditHandlingStatus.Properties.Items.Add(handlingStatus.HandlingStatusName);
            }
            comboBoxEditHandlingStatus.SelectedIndex = 1;
        }

        //private async Task sbLoadDataForGridWRDataDeteilAsync(String wRDNumber)
        //{
        //    List<WRDataDetail> wRDataDetails = await wRDataDetailRepository.GetUnderID(wRDNumber);
        //    int mMaxRow = 100;
        //    if (wRDataDetails.Count < mMaxRow)
        //    {
        //        int num = mMaxRow - wRDataDetails.Count;
        //        int i = 1;
        //        while (true)
        //        {
        //            int num2 = i;
        //            int num3 = num;
        //            if (num2 > num3)
        //            {
        //                break;
        //            }
        //            wRDataDetails.Add(new WRDataDetail());
        //            i++;
        //        }
        //    }
        //    gridControlWRDataDetail.DataSource = wRDataDetails;
        //    getTotalQuantityOrg();
        //    getTotalQuantity();
        //    getPackingQuantity();
        //    textTotalQuantity_CustomDraw();

        //}

        private async Task sbLoadDataForGridWRDataGeneralAsync(String wRDNumber)
        {
            List<WRDataGeneral> wRDataGenerals = await wRDataGeneralRepository.GetUnderID(wRDNumber);
            int mMaxRow = 100;
            if (wRDataGenerals.Count < mMaxRow)
            {
                int num = mMaxRow - wRDataGenerals.Count;
                int i = 1;
                while (true)
                {
                    int num2 = i;
                    int num3 = num;
                    if (num2 > num3)
                    {
                        break;
                    }
                    wRDataGenerals.Add(new WRDataGeneral());
                    i++;
                }
            }
            gridControlWRDataDetail.DataSource = wRDataGenerals;
            getTotalQuantityOrg();
            getTotalQuantity();
            //getPackingQuantity();
            textTotalQuantity_CustomDraw();
        }

        //private void getPackingQuantity()
        //{
        //    Decimal packingQuantity = 0;
        //    int i = 0;
        //    while (true)
        //    {
        //        int rowHandle = gridViewWRDataDetail.GetRowHandle(i);
        //        if (gridViewWRDataDetail.GetRowCellValue(rowHandle, "Barcode") == null)
        //        {
        //            break;
        //        }
        //        Decimal packingVolume = (Decimal)gridViewWRDataDetail.GetRowCellValue(rowHandle, "PackingVolume");
        //        Decimal quantity = (Decimal)gridViewWRDataDetail.GetRowCellValue(rowHandle, "Quantity");
        //        packingQuantity = quantity / packingVolume;
        //        if (packingQuantity >= 1)
        //        {
        //            packingQuantity = Math.Ceiling(packingQuantity);
        //        }
        //        else
        //        {
        //            packingQuantity = Math.Floor(packingQuantity);

        //        }
        //        gridViewWRDataDetail.SetRowCellValue(rowHandle, "PackingQuantity", packingQuantity);
        //        i++;
        //    }
        //}




        private void getTotalQuantityOrg()
        {
            int totalQuantityOrg = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewWRDataDetail.GetRowHandle(i);
                if (gridViewWRDataDetail.GetRowCellValue(rowHandle, "Barcode") == null)
                {
                    break;
                }
                int quantityOrg = (int)gridViewWRDataDetail.GetRowCellValue(rowHandle, "QuantityOrg");
                totalQuantityOrg += quantityOrg;
                i++;
            }
            textEditTotalQuantityOrg.Text = totalQuantityOrg.ToString("0.#####");
        }

        private void getTotalQuantity()
        {
            int totalQuantity = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewWRDataDetail.GetRowHandle(i);
                if (gridViewWRDataDetail.GetRowCellValue(rowHandle, "Barcode") == null)
                {
                    break;
                }
                int quantity = (int)gridViewWRDataDetail.GetRowCellValue(rowHandle, "Quantity");
                totalQuantity += quantity;
                i++;
            }
            textEditTotalQuantity.Text = totalQuantity.ToString("0.#####");
        }

        private void QRCodeGeneral(String code)
        {
            QRCodeGenerator.ECCLevel eccLevel = QRCodeGenerator.ECCLevel.Q;
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(code, eccLevel);
            QRCode qrCode = new QRCode(qrCodeData);
            Bitmap qrCodeBitmap = qrCode.GetGraphic(2, System.Drawing.Color.Black, System.Drawing.Color.White, new Bitmap(10, 10));
            pictureBox1.Image = qrCodeBitmap;
            Helpers.ImageHelper.resizeImage(pictureBox1.Image, new Size(100, 100));
            pictureBox1.SizeMode = PictureBoxSizeMode.CenterImage;

            //QRCodeGenerator.ECCLevel eccLevel = QRCodeGenerator.ECCLevel.L;
            //using (QRCodeGenerator qrGenerator = new QRCodeGenerator())
            //{
            //    using (QRCodeData qrCodeData = qrGenerator.CreateQrCode(code, eccLevel))
            //    {
            //        using (QRCode qrCode = new QRCode(qrCodeData))
            //        {

            //            pictureBox1.BackgroundImage = qrCode.GetGraphic(20);

            //            this.pictureBox1.Size = new System.Drawing.Size(pictureBox1.Width, pictureBox1.Height);
            //            //Set the SizeMode to center the image.
            //            this.pictureBox1.SizeMode = PictureBoxSizeMode.CenterImage;

            //            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            //        }
            //    }
            //}
        }

        public async Task updateWRDataHeaderAsync()
        {
            WRDataHeader wRDataHeader = await wRDataHeaderRepository.GetUnderID(textEditWRDNumber.Text);
            if (wRDataHeader.HandlingStatusID.Equals("2"))
            {
                MessageBox.Show("Dữ liệu nhập đã duyệt mức 2, không thể chỉnh sửa");
                return;
            }

            Decimal totalQuantityOrg = Decimal.Parse(textEditTotalQuantityOrg.Text);
            Decimal totalQuantity = Decimal.Parse(textEditTotalQuantity.Text);
            Decimal subQuantity = totalQuantityOrg - totalQuantity;
            if (subQuantity == totalQuantityOrg)
            {
                wRDataHeader.Status = "0";
            }
            else if (subQuantity < totalQuantityOrg)
            {
                wRDataHeader.Status = "1";
            }
            else if (subQuantity == 0)
            {
                wRDataHeader.Status = "2";
            }
            else
            {
                wRDataHeader.Status = "3";
            }


            wRDataHeader.WRDNumber = textEditWRDNumber.Text;
            wRDataHeader.WRDDate = DateTime.Parse(dateEditWRDDate.DateTime.ToString("yyyy-MM-dd"));
            wRDataHeader.WRRNumber = textEditWRDNumber.Text;
            wRDataHeader.HandlingStatusID = (comboBoxEditHandlingStatus.SelectedIndex - 1).ToString();
            wRDataHeader.HandlingStatusName = comboBoxEditHandlingStatus.SelectedItem.ToString();
            wRDataHeader.Note = "";
            wRDataHeader.TotalQuantity = Decimal.Parse(textEditTotalQuantity.Text);
            wRDataHeader.TotalQuantityOrg = Decimal.Parse(textEditTotalQuantityOrg.Text);
            wRDataHeader.UpdatedUserID = WMMessage.User.UserID;
            wRDataHeader.UpdatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));

            if (await wRDataHeaderRepository.Update(wRDataHeader, textEditWRDNumber.Text) > 0)
            {
                WMPublic.sbMessageSaveChangeSuccess(this);
                //if (comboBoxEditHandlingStatus.SelectedIndex == 3)
                //{
                //    if (isGeneralClick == false)
                //    {
                //        MessageBox.Show("Xem tổng hợp để duyệt mức 2");
                //    }
                //    else
                //    {
                //        createWRHeaderAsync();
                //    }
                //}
            }

        }

        //public async Task updateWRDataDetailAsync()
        //{
        //    int num = gridViewWRDataDetail.RowCount - 1;
        //    int i = 0;
        //    while (true)
        //    {
        //        int num2 = i;
        //        int num3 = num;
        //        if (num2 > num3 || gridViewWRDataDetail.GetRowCellValue(i, gridViewWRDataDetail.Columns["colBarcode"]) == "")
        //        {
        //            break;
        //        }
        //        int rowHandle = gridViewWRDataDetail.GetRowHandle(i);
        //        WRDataDetail wRDataDetail = await wRDataDetailRepository.GetUnderMultiID(textEditWRDNumber.Text, (string?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "Barcode"), i + 1);
        //        wRDataDetail.WRDNumber = textEditWRDNumber.Text;
        //        wRDataDetail.GoodsID = (string?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "GoodsID");
        //        wRDataDetail.GoodsName = (string?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "GoodsName");
        //        wRDataDetail.Ordinal = wRDataDetail.Ordinal;
        //        wRDataDetail.IDCode = wRDataDetail.IDCode;
        //        wRDataDetail.LocationID = (string?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "LocationID");
        //        wRDataDetail.Quantity = (Decimal?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "Quantity");
        //        wRDataDetail.TotalQuantity = (Decimal?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "TotalQuantity");
        //        wRDataDetail.TotalGoods = (Decimal?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "TotalGoods");
        //        wRDataDetail.QuantityOrg = (Decimal?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "QuantityOrg");
        //        wRDataDetail.TotalQuantityOrg = (Decimal?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "TotalQuantityOrg");
        //        wRDataDetail.TotalGoodsOrg = (Decimal?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "TotalGoodsOrg");
        //        wRDataDetail.LocationIDOrg = (string?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "LocationIDOrg");
        //        wRDataDetail.EditerID = WMMessage.User.UserID; ;
        //        wRDataDetail.EditedDateTime = DateTime.Now.ToString("yyyy-MM-dd");
        //        wRDataDetail.Status = "1";
        //        wRDataDetail.PackingVolume = (Decimal?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "PackingVolume");
        //        wRDataDetail.QuantityByPack = (Decimal?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "QuantityByPack");
        //        wRDataDetail.QuantityByItem = (Decimal?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "QuantityByItem");
        //        wRDataDetail.Note = "";
        //        wRDataDetail.ScanOption = (Int16?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "ScanOption");
        //        wRDataDetail.PackingQuantity = (Decimal?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "PackingQuantity");


        //        if (await wRDataDetailRepository.Update(wRDataDetail, textEditWRDNumber.Text, wRDataDetail.GoodsID, wRDataDetail.Ordinal) > 0)
        //        {
        //            i++;
        //        }
        //    }
        //}

        public async Task updateWRDataGeneralAsync()
        {
            int num = gridViewWRDataDetail.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewWRDataDetail.GetRowCellValue(i, "Barcode") == null)
                {
                    break;
                }
                int rowHandle = gridViewWRDataDetail.GetRowHandle(i);
                WRDataGeneral wRDataGeneral = new WRDataGeneral();
                wRDataGeneral.WRDNumber = textEditWRDNumber.Text;
                wRDataGeneral.Ordinal = i + 1;
                wRDataGeneral.Quantity = (int?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "Quantity");
                wRDataGeneral.QuantityOrg = (int?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "QuantityOrg");
                wRDataGeneral.Barcode = (string)gridViewWRDataDetail.GetRowCellValue(i, "Barcode");
                wRDataGeneral.StyleColorSize = (string)gridViewWRDataDetail.GetRowCellValue(i, "StyleColorSize");
                wRDataGeneral.Style = (string)gridViewWRDataDetail.GetRowCellValue(i, "Style");
                wRDataGeneral.Color = (string)gridViewWRDataDetail.GetRowCellValue(i, "Color");
                wRDataGeneral.Sku = (string)gridViewWRDataDetail.GetRowCellValue(i, "Sku");
                wRDataGeneral.Size = (int?)gridViewWRDataDetail.GetRowCellValue(i, "Size");
                wRDataGeneral.Gender = (string)gridViewWRDataDetail.GetRowCellValue(i, "Gender");
                wRDataGeneral.ProductGroup = (string)gridViewWRDataDetail.GetRowCellValue(i, "ProductGroup");
                wRDataGeneral.DescriptionVN = (string)gridViewWRDataDetail.GetRowCellValue(i, "DescriptionVN");
                wRDataGeneral.LifeStylePerformance = (string)gridViewWRDataDetail.GetRowCellValue(i, "LifeStylePerformance");
                wRDataGeneral.Div = (string)gridViewWRDataDetail.GetRowCellValue(i, "Div");
                wRDataGeneral.Outsole = (string)gridViewWRDataDetail.GetRowCellValue(i, "Outsole");
                wRDataGeneral.Category = (string)gridViewWRDataDetail.GetRowCellValue(i, "Category");
                wRDataGeneral.Productline = (string)gridViewWRDataDetail.GetRowCellValue(i, "Productline");
                wRDataGeneral.Description = (string)gridViewWRDataDetail.GetRowCellValue(i, "Description");
                wRDataGeneral.EXWorkUSD = (decimal?)gridViewWRDataDetail.GetRowCellValue(i, "EXWorkUSD");
                wRDataGeneral.EXWorkVND = (decimal?)gridViewWRDataDetail.GetRowCellValue(i, "EXWorkVND");
                wRDataGeneral.AmountVND = (decimal?)gridViewWRDataDetail.GetRowCellValue(i, "AmountVND");
                wRDataGeneral.RetailPrice = (decimal?)gridViewWRDataDetail.GetRowCellValue(i, "RetailPrice");
                wRDataGeneral.Season = (string)gridViewWRDataDetail.GetRowCellValue(i, "Season");
                wRDataGeneral.Coo = (string)gridViewWRDataDetail.GetRowCellValue(i, "Coo");
                wRDataGeneral.Material = (string)gridViewWRDataDetail.GetRowCellValue(i, "Material");
                wRDataGeneral.InvoiceNo = (string)gridViewWRDataDetail.GetRowCellValue(i, "InvoiceNo");
                if (((DateTime?)gridViewWRDataDetail.GetRowCellValue(i, "DateOfInvoice")).ToString() != "")
                {
                    wRDataGeneral.DateOfInvoice = ((DateTime?)gridViewWRDataDetail.GetRowCellValue(i, "DateOfInvoice"));
                }

                wRDataGeneral.SaleContract = (string)gridViewWRDataDetail.GetRowCellValue(i, "SaleContract");
                wRDataGeneral.Shipment = (string)gridViewWRDataDetail.GetRowCellValue(i, "Shipment");
                wRDataGeneral.FactoryName = (string)gridViewWRDataDetail.GetRowCellValue(i, "FactoryName");
                wRDataGeneral.FactoryAdress = (string)gridViewWRDataDetail.GetRowCellValue(i, "FactoryAdress");
                wRDataGeneral.FactoryNameVN = (string)gridViewWRDataDetail.GetRowCellValue(i, "FactoryNameVN");
                wRDataGeneral.FactoryAdressVN = (string)gridViewWRDataDetail.GetRowCellValue(i, "FactoryAdressVN");
                wRDataGeneral.NewPrice = (decimal?)gridViewWRDataDetail.GetRowCellValue(i, "NewPrice");
                wRDataGeneral.ChangePrice = (decimal?)gridViewWRDataDetail.GetRowCellValue(i, "ChangePrice");
                wRDataGeneral.Status = (string)gridViewWRDataDetail.GetRowCellValue(i, "Status");
                if (((DateTime?)gridViewWRDataDetail.GetRowCellValue(i, "InputDate")).ToString() != "")
                {
                    wRDataGeneral.InputDate = (DateTime?)gridViewWRDataDetail.GetRowCellValue(i, "InputDate");
                }
                if (await wRDataGeneralRepository.Update(wRDataGeneral, textEditWRDNumber.Text, wRDataGeneral.Barcode, wRDataGeneral.Ordinal) > 0)
                {
                    i++;
                }
            }
        }

        public async Task DeleteWRDataHeaderAsync()
        {
            if ((await wRDataHeaderRepository.Delete(textEditWRDNumber.Text)) > 0)
            {
                await DeleteWRDataDetailAsync();
                await DeleteWRDataGeneralAsync();
                //sbLoadDataForGridWRDataDeteilAsync(textEditWRDNumber.Text);
                sbLoadDataForGridWRDataGeneralAsync(textEditWRDNumber.Text);
                WMPublic.sbMessageDeleteSuccess(this);
            }
        }

        public async Task DeleteWRDataDetailAsync()
        {
            await wRDataDetailRepository.Delete(textEditWRDNumber.Text);
            gridControlWRDataDetail.DataSource = null;
        }

        public async Task DeleteWRDataGeneralAsync()
        {
            await wRDataGeneralRepository.Delete(textEditWRDNumber.Text);
            clearAsync();
            gridControlWRDataDetail.DataSource = null;
        }

        private void clearAsync()
        {
            textEditWRDNumber.Text = "";
            dateEditWRDDate.Text = DateTime.Now.ToString("dd-MM-yyyy");
            comboBoxEditHandlingStatus.SelectedIndex = 1;
            textEditTotalQuantityOrg.Text = "0";
            textEditTotalQuantity.Text = "0";
            //List<WRDataDetail> wRDataDetails = new List<WRDataDetail>();
            List<WRDataGeneral> wRDataGenerals = new List<WRDataGeneral>();


            for (int i = 0; i < 100; i++)
            {
                wRDataGenerals.Add(new WRDataGeneral());
            }
            gridControlWRDataDetail.DataSource = wRDataGenerals;

        }

        //public async Task createWRHeaderAsync()
        //{
        //    WRDataHeader wRDataHeader = await wRDataHeaderRepository.GetUnderID(textEditWRDNumber.Text);
        //    WRHeader wRHeader = new WRHeader();
        //    wRHeader.WRNumber = textEditWRDNumber.Text;
        //    wRHeader.WRDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
        //    wRHeader.ReferenceNumber = textEditWRDNumber.Text;
        //    wRHeader.WRRNumber = textEditWRDNumber.Text;
        //    wRHeader.WRRReference = textEditWRDNumber.Text;
        //    wRHeader.BranchID = wRDataHeader.BranchID;
        //    wRHeader.BranchName = wRDataHeader.BranchName;
        //    wRHeader.TotalQuantity = Decimal.Parse(textEditTotalQuantity.Text);
        //    wRHeader.HandlingStatusID = "0";
        //    wRHeader.HandlingStatusName = "Chưa duyệt";
        //    wRHeader.ModalityID = "09";
        //    wRHeader.ModalityName = "Khác";
        //    wRHeader.CreatedUserID = WMMessage.User.UserID;
        //    wRHeader.CreatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
        //    wRHeader.WarehouseID = "<>";

        //    if (await wRHeaderRepository.Create(wRHeader) > 0)
        //    {
        //        createWRDetailAsync();
        //        MessageBox.Show("Đã tạo phiếu nhập");
        //    }
        //}

        //public async Task createWRDetailAsync()
        //{
        //    int num = gridViewWRDataDetail.RowCount - 1;
        //    int i = 0;
        //    while (true)
        //    {
        //        int num2 = i;
        //        int num3 = num;
        //        if (num2 > num3 || gridViewWRDataDetail.GetRowCellValue(i, gridViewWRDataDetail.Columns["colGoodsID"]) == "")
        //        {
        //            break;
        //        }
        //        int rowHandle = gridViewWRDataDetail.GetRowHandle(i);
        //        WRDetail wRDetail = new WRDetail();
        //        wRDetail.WRNumber = textEditWRDNumber.Text;
        //        wRDetail.Ordinal = i + 1;
        //        wRDetail.GoodsID = (string?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "GoodsID");
        //        wRDetail.GoodsName = (string?)gridViewWRDataDetail.GetRowCellValue(i, "GoodsName");
        //        wRDetail.Quantity = (Decimal?)gridViewWRDataDetail.GetRowCellValue(i, "Quantity");
        //        wRDetail.ReceiptQuantity = (Decimal?)gridViewWRDataDetail.GetRowCellValue(i, "Quantity");
        //        wRDetail.ReceiptAmount = 0m;
        //        wRDetail.QuantityOrdered = (Decimal?)gridViewWRDataDetail.GetRowCellValue(i, "QuantityOrg");
        //        wRDetail.QuantityReceived = (Decimal?)gridViewWRDataDetail.GetRowCellValue(i, "Quantity");
        //        wRDetail.Note = "";
        //        if (await wRDetailRepository.Create(wRDetail) > 0)
        //        {
        //            i++;
        //        }
        //    }
        //}


        private async void WRDNumber_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                WRDataHeader wRDataHeader = new WRDataHeader();
                wRDataHeader = await wRDataHeaderRepository.GetUnderID(textEditWRDNumber.Text);
                if (wRDataHeader != null)
                {
                    dateEditWRDDate.Text = DateTime.Parse(wRDataHeader.WRDDate.ToString()).ToShortDateString();
                    comboBoxEditHandlingStatus.SelectedIndex = int.Parse(wRDataHeader.HandlingStatusID) + 1;
                    await sbLoadDataForGridWRDataGeneralAsync(textEditWRDNumber.Text);
                }
                else
                {
                    MessageBox.Show("Số phiếu yêu cầu không tồn tại");
                    return;
                }

            }
        }

        private async void textEditWDRNumer_CellDoubleClick(object sender, EventArgs e)
        {
            frmSearchWRD frmSearchWRD = new frmSearchWRD();
            frmSearchWRD.ShowDialog(this);
            frmSearchWRD.Dispose();
            if (selectedWRDNumber != null)
            {
                textEditWRDNumber.Text = selectedWRDNumber;
                WRDataHeader wRDataHeader = await wRDataHeaderRepository.GetUnderID(textEditWRDNumber.Text);
                wRDataGeneralsPublic = await wRDataGeneralRepository.GetUnderID(textEditWRDNumber.Text);

                wRDataHeaderPublic = wRDataHeader;
                if (wRDataHeader != null)
                {
                    dateEditWRDDate.Text = DateTime.Parse(wRDataHeader.WRDDate.ToString()).ToShortDateString();
                    comboBoxEditHandlingStatus.SelectedIndex = int.Parse(wRDataHeader.HandlingStatusID) + 1;
                    await sbLoadDataForGridWRDataGeneralAsync(textEditWRDNumber.Text);
                }
                QRCodeGeneral(selectedWRDNumber);
            }

        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if (textEditWRDNumber.Text != "")
            {

                sbLoadDataForGridWRDataGeneralAsync(textEditWRDNumber.Text);


            }
            if (m_MainForm.tagList != null)
            {
                if (wRDataGeneralsPublic != null && wRDataGeneralsPublic.Count > 0)
                {
                    
                    compareTagIDAsync(wRDataGeneralsPublic, m_MainForm.tagList);
                }
            }
        }

        private async void compareTagIDAsync(List<WRDataGeneral> wRDataGenerals1, List<string> tagIDs)
        {
            
            foreach (string s in tagIDs)
            {
                foreach (WRDataGeneral wRDataGeneral in wRDataGenerals1)
                {

                    if (wRDataGeneral.IDCode!=null&&wRDataGeneral.IDCode.Contains(s.Trim().Substring(s.Length - 18, 18)))
                    {         
                        return;
                    }
                    else
                    {
                        if (s.Trim().Substring(s.Length - 18, 12) == wRDataGeneral.Barcode.Trim())
                        {
                            wRDataGeneral.Quantity += 1;
                            wRDataGeneral.IDCode = wRDataGeneral.IDCode + s.Trim().Substring(s.Length - 18, 18) + ",";
                            if (await wRDataGeneralRepository.Update(wRDataGeneral, wRDataGeneral.WRDNumber, wRDataGeneral.Barcode, wRDataGeneral.Ordinal) > 0)
                            {

                                wRDataHeaderPublic.TotalQuantity += wRDataGeneral.QuantityOrg;
                                await wRDataHeaderRepository.Update(wRDataHeaderPublic, textEditWRDNumber.Text);

                            }
                        }
                    }

                }
            }
        }


        //private async Task compareTagIDAsync(List<WRDataGeneral> wRDataGenerals1, List<String> tagIDs)
        //{
        //    for (int i = 0; i < wRDataGenerals1.Count; i++)
        //    {
        //        for (int j = 0; j < tagIDs.Count; j++)
        //        {
        //            if (wRDataGenerals1[i].Quantity == 0 && tagIDs[j].Trim().Substring(0, tagIDs[j].Length - 1) == wRDataGenerals1[i].RFIDTagID.Trim())
        //            {
        //                wRDataGenerals1[i].Quantity = wRDataGenerals1[i].QuantityOrg;

        //                wRDataGenerals1[i].TotalQuantity += wRDataGenerals1[i].QuantityOrg;

        //                if (await wRDataGeneralRepository.Update(wRDataGenerals1[i], wRDataGenerals1[i].WRDNumber, wRDataGenerals1[i].RFIDTagID, wRDataGenerals1[i].Ordinal) > 0)
        //                {

        //                    wRDataHeaderPublic.TotalQuantity += wRDataGenerals1[i].QuantityOrg;

        //                    await wRDataHeaderRepository.Update(wRDataHeaderPublic, textEditWRDNumber.Text);
        //                }
        //            }
        //        }
        //    }
        //}

        private void grdData_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            int i = 0;
            i = e.RowHandle;
            if ((i >= 0) && (String.Compare(e.Column.FieldName, "Quantity", false) == 0))
            {
                double mQuantity = ((gridViewWRDataDetail.GetRowCellValue(i, gridViewWRDataDetail.Columns["Quantity"]) == "") ? 0.0 : Convert.ToDouble(gridViewWRDataDetail.GetRowCellValue(i, gridViewWRDataDetail.Columns["Quantity"])));
                double mQuantityOrg = ((gridViewWRDataDetail.GetRowCellValue(i, gridViewWRDataDetail.Columns["QuantityOrg"]) == "") ? 0.0 : Convert.ToDouble(gridViewWRDataDetail.GetRowCellValue(i, gridViewWRDataDetail.Columns["QuantityOrg"])));
                //double mPackingQuantity = ((gridViewWRDataDetail.GetRowCellValue(i, gridViewWRDataDetail.Columns["PackingQuantity"]) == "") ? 0.0 : Convert.ToDouble(gridViewWRDataDetail.GetRowCellValue(i, gridViewWRDataDetail.Columns["PackingQuantity"])));
                e.Appearance.Font = new Font("Tahoma", 8f, FontStyle.Bold);
                if (mQuantity == mQuantityOrg && mQuantity != 0.0)
                {
                    e.Appearance.BackColor = System.Drawing.Color.LightGreen;
                }
                if (mQuantity > mQuantityOrg && mQuantity != 0.0)
                {
                    e.Appearance.BackColor = System.Drawing.Color.Yellow;
                }
                if (mQuantity < mQuantityOrg && mQuantity != 0.0)
                {
                    e.Appearance.BackColor = System.Drawing.Color.Red;
                }
            }
            //if ((i >= 0) && (String.Compare(e.Column.FieldName, "PackingQuantity", false) == 0))
            //{
            //    double mQuantity = ((gridViewWRDataDetail.GetRowCellValue(i, gridViewWRDataDetail.Columns["Quantity"]) == "") ? 0.0 : Convert.ToDouble(gridViewWRDataDetail.GetRowCellValue(i, gridViewWRDataDetail.Columns["Quantity"])));
            //    double mQuantityOrg = ((gridViewWRDataDetail.GetRowCellValue(i, gridViewWRDataDetail.Columns["QuantityOrg"]) == "") ? 0.0 : Convert.ToDouble(gridViewWRDataDetail.GetRowCellValue(i, gridViewWRDataDetail.Columns["QuantityOrg"])));
            //    e.Appearance.Font = new Font("Tahoma", 8f, FontStyle.Bold);
            //    if (mQuantity == mQuantityOrg && mQuantity != 0.0)
            //    {
            //        e.Appearance.BackColor = System.Drawing.Color.LightGreen;
            //    }
            //    if (mQuantity > mQuantityOrg && mQuantity != 0.0)
            //    {
            //        e.Appearance.BackColor = System.Drawing.Color.Yellow;
            //    }
            //    if (mQuantity < mQuantityOrg && mQuantity != 0.0)
            //    {
            //        e.Appearance.BackColor = System.Drawing.Color.Red;
            //    }
            //}
        }

        private void textTotalQuantity_CustomDraw()
        {
            //if (String.Compare(textEditTotalQuantityOrg.Text, "", false) == 0)
            //{
            double mTotalQuantity = ((textEditTotalQuantity.Text == "") ? 0.0 : Convert.ToDouble(textEditTotalQuantity.Text));
            double mTotalQuantityOrg = ((textEditTotalQuantityOrg.Text == "") ? 0.0 : Convert.ToDouble(textEditTotalQuantityOrg.Text));
            textEditTotalQuantity.Font = new Font("Tahoma", 8f, FontStyle.Bold);
            textEditTotalQuantityOrg.Font = new Font("Tahoma", 8f, FontStyle.Bold);
            if (mTotalQuantity == mTotalQuantityOrg && mTotalQuantity != 0.0)
            {
                textEditTotalQuantity.BackColor = System.Drawing.Color.LightGreen;
                textEditTotalQuantityOrg.BackColor = System.Drawing.Color.LightGreen;
            }
            if (mTotalQuantity > mTotalQuantityOrg && mTotalQuantity != 0.0)
            {
                textEditTotalQuantity.BackColor = System.Drawing.Color.Yellow;
                textEditTotalQuantityOrg.BackColor = System.Drawing.Color.LightGreen;
            }
            if (mTotalQuantity < mTotalQuantityOrg && mTotalQuantity != 0.0)
            {
                textEditTotalQuantity.BackColor = System.Drawing.Color.Red;
                textEditTotalQuantityOrg.BackColor = System.Drawing.Color.LightGreen;
            }
            //}
        }

        private void butOn_Click(object sender, EventArgs e)
        {
            if (m_MainForm.m_IsConnected == false)
            {
                m_MainForm.m_ConnectionForm.ShowDialog();
            }
            MessageBox.Show("Bắt đầu quét nhập kho");
            simpleButtonOn.Enabled = false;
            simpleButtonOff.Enabled = true;
            t.Start();
            t.Enabled = true;
            m_MainForm.readRDButton_Click();

           
        }

        private void butOff_Click(object sender, EventArgs e)
        {
            if (m_MainForm.m_IsConnected == false)
            {
                m_MainForm.m_ConnectionForm.ShowDialog();
            }
            MessageBox.Show("Kết thúc quét nhập kho");
            simpleButtonOn.Enabled = true;
            simpleButtonOff.Enabled = false;
            t.Start();
            t.Enabled = false;
            m_MainForm.readRDButton_Click();
            
        }

        //private void butDetail_Click(object sender, EventArgs e)
        //{
        //    sbLoadDataForGridWRDataDeteilAsync(textEditWRDNumber.Text);
        //    isGeneralClick = false;

        //}

        //private void butGeneral_Click(object sender, EventArgs e)
        //{
        //    sbLoadDataForGridWRDataGeneralAsync(textEditWRDNumber.Text);
        //    isGeneralClick = true;
        //}

        //barbutton click
        public async void button_Click(object sender, EventArgs e)
        {
            ButtonEventArgs eArg = (DevExpress.XtraBars.Docking2010.ButtonEventArgs)e;
            String tag = eArg.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "new":
                    if (WMPublic.sbMessageAddNewRequest(this) == true)
                    {
                        clearAsync();
                    }
                    break;
                case "save":
                    if (WMPublic.sbMessageSaveChangeRequest(this) == true)
                    {
                        updateWRDataHeaderAsync();
                    }
                    break;
                case "delete":
                    if (WMPublic.sbMessageDeleteRequest(this) == true)
                    {
                        DeleteWRDataHeaderAsync();
                    }
                    break;
                case "search":

                    break;
                case "refesh":
                    sbLoadDataForGridWRDataGeneralAsync(textEditWRDNumber.Text);
                    break;
                case "import":
                    break;
                case "export":
                    break;
                case "print":
                    break;
                case "close":
                    Demo demo = new Demo();
                    demo.Show();
                    break;
            }

        }

        private void gridControlWRDataDetail_Click(object sender, EventArgs e)
        {

        }
    }
}
