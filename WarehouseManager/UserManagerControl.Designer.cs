﻿
namespace WarehouseManager
{
    partial class UserManagerControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserManagerControl));
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions2 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions3 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions4 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions5 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions6 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions7 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions8 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions9 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition1 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition2 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition3 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition4 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition1 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition2 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition3 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition4 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition5 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition6 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition7 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition8 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition9 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition10 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition11 = new DevExpress.XtraLayout.RowDefinition();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.windowsUIButtonPanel1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.gridControlUser = new DevExpress.XtraGrid.GridControl();
            this.userBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.wARHOUSE_HPDataSet = new WarehouseManager.WARHOUSE_HPDataSet();
            this.gridViewUser = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colNo2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLoginName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.textEditUserID = new DevExpress.XtraEditors.TextEdit();
            this.textEditFullName = new DevExpress.XtraEditors.TextEdit();
            this.textEditAdress = new DevExpress.XtraEditors.TextEdit();
            this.textEditPosition = new DevExpress.XtraEditors.TextEdit();
            this.comboBoxEditBranch = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEditPhone = new DevExpress.XtraEditors.TextEdit();
            this.textEditEmail = new DevExpress.XtraEditors.TextEdit();
            this.textEditUsername = new DevExpress.XtraEditors.TextEdit();
            this.textEditPassword = new DevExpress.XtraEditors.TextEdit();
            this.textEditDescription = new DevExpress.XtraEditors.TextEdit();
            this.checkEditActive = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditBlocked = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemUserId = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemFullname = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemAdress = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemPosition = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemBranch = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemPhone = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemEmail = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemUsername = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemPassword = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageScreen = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlScreen = new DevExpress.XtraGrid.GridControl();
            this.gridViewScreen = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colNo3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colScreenName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colScreenNameEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaveNewRight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaveChangeRight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeleteRight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrintRight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colImportRight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExportRight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colScreenID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrdinal1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckedComboBoxEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.repositoryItemComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.xtraTabPageReport = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlReport = new DevExpress.XtraGrid.GridControl();
            this.reportBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewReport = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colReportID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportNameEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrdinal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNo4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colViewRight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrintRight2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckedComboBoxEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.repositoryItemComboBox3 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.xtraTabPageOther = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlRight = new DevExpress.XtraGrid.GridControl();
            this.gridViewRight = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRightID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRightName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRightNameEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrantRight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrdinal3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckedComboBoxEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.screenBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.userTableAdapter = new WarehouseManager.WARHOUSE_HPDataSetTableAdapters.UserTableAdapter();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.screenTableAdapter = new WarehouseManager.WARHOUSE_HPDataSetTableAdapters.ScreenTableAdapter();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.reportTableAdapter = new WarehouseManager.WARHOUSE_HPDataSetTableAdapters.ReportTableAdapter();
            this.popupMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.colActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBlocked = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBranchID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBranchName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAdress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPosition = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wARHOUSE_HPDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditUserID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditFullName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditAdress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPosition.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditBranch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPhone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditUsername.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPassword.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditActive.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditBlocked.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemUserId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemFullname)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAdress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPosition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemBranch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemUsername)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPageScreen.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlScreen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewScreen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            this.xtraTabPageReport.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            this.xtraTabPageOther.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.screenBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.windowsUIButtonPanel1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(779, 194, 650, 400);
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(1354, 37);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // windowsUIButtonPanel1
            // 
            this.windowsUIButtonPanel1.AllowGlyphSkinning = false;
            this.windowsUIButtonPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            windowsUIButtonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions1.Image")));
            windowsUIButtonImageOptions1.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions2.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions2.Image")));
            windowsUIButtonImageOptions2.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions3.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions3.Image")));
            windowsUIButtonImageOptions3.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions4.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions4.Image")));
            windowsUIButtonImageOptions4.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions5.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions5.Image")));
            windowsUIButtonImageOptions5.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions6.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions6.Image")));
            windowsUIButtonImageOptions6.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions7.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions7.Image")));
            windowsUIButtonImageOptions7.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions8.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions8.Image")));
            windowsUIButtonImageOptions8.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions9.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions9.Image")));
            windowsUIButtonImageOptions9.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            this.windowsUIButtonPanel1.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F2) Thêm mới", true, windowsUIButtonImageOptions1, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "new", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F3) Lưu", true, windowsUIButtonImageOptions2, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "save", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F4) Xóa", true, windowsUIButtonImageOptions3, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "delete", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F5) Tìm kiếm", true, windowsUIButtonImageOptions4, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "search", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F6) Làm mới", true, windowsUIButtonImageOptions5, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "refesh", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F7) Nhập dữ liệu", true, windowsUIButtonImageOptions6, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "import", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F8) Xuất dữ liệu", true, windowsUIButtonImageOptions7, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "export", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F9) In ấn", true, windowsUIButtonImageOptions8, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "print", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F10) Thoát", true, windowsUIButtonImageOptions9, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "close", -1, false)});
            this.windowsUIButtonPanel1.ContentAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.windowsUIButtonPanel1.Location = new System.Drawing.Point(2, 2);
            this.windowsUIButtonPanel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.windowsUIButtonPanel1.Name = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.Padding = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.windowsUIButtonPanel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.windowsUIButtonPanel1.Size = new System.Drawing.Size(1350, 32);
            this.windowsUIButtonPanel1.TabIndex = 6;
            this.windowsUIButtonPanel1.Text = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.UseButtonBackgroundImages = false;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.Root.Name = "Root";
            this.Root.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.Root.Size = new System.Drawing.Size(1354, 37);
            this.Root.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.windowsUIButtonPanel1;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1354, 37);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // gridControlUser
            // 
            this.gridControlUser.DataSource = this.userBindingSource;
            this.gridControlUser.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.gridControlUser.Location = new System.Drawing.Point(574, 14);
            this.gridControlUser.MainView = this.gridViewUser;
            this.gridControlUser.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridControlUser.Name = "gridControlUser";
            this.gridControlUser.Size = new System.Drawing.Size(766, 296);
            this.gridControlUser.TabIndex = 4;
            this.gridControlUser.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewUser});
            // 
            // userBindingSource
            // 
            this.userBindingSource.DataMember = "User";
            this.userBindingSource.DataSource = this.wARHOUSE_HPDataSet;
            // 
            // wARHOUSE_HPDataSet
            // 
            this.wARHOUSE_HPDataSet.DataSetName = "WARHOUSE_HPDataSet";
            this.wARHOUSE_HPDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridViewUser
            // 
            this.gridViewUser.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colNo2,
            this.colUserID,
            this.colUserName,
            this.colLoginName,
            this.colActive,
            this.colBlocked,
            this.colBranchID,
            this.colBranchName,
            this.colAdress,
            this.colEmail,
            this.colPosition,
            this.colDescription});
            this.gridViewUser.DetailHeight = 431;
            this.gridViewUser.GridControl = this.gridControlUser;
            this.gridViewUser.Name = "gridViewUser";
            // 
            // colNo2
            // 
            this.colNo2.Caption = "No";
            this.colNo2.FieldName = "No2";
            this.colNo2.MinWidth = 23;
            this.colNo2.Name = "colNo2";
            this.colNo2.OptionsColumn.ReadOnly = true;
            this.colNo2.Visible = true;
            this.colNo2.VisibleIndex = 0;
            this.colNo2.Width = 58;
            // 
            // colUserID
            // 
            this.colUserID.FieldName = "UserID";
            this.colUserID.MinWidth = 23;
            this.colUserID.Name = "colUserID";
            this.colUserID.Visible = true;
            this.colUserID.VisibleIndex = 1;
            this.colUserID.Width = 154;
            // 
            // colUserName
            // 
            this.colUserName.FieldName = "UserName";
            this.colUserName.MinWidth = 23;
            this.colUserName.Name = "colUserName";
            this.colUserName.Visible = true;
            this.colUserName.VisibleIndex = 2;
            this.colUserName.Width = 293;
            // 
            // colLoginName
            // 
            this.colLoginName.FieldName = "LoginName";
            this.colLoginName.MinWidth = 23;
            this.colLoginName.Name = "colLoginName";
            this.colLoginName.Visible = true;
            this.colLoginName.VisibleIndex = 3;
            this.colLoginName.Width = 301;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.gridControlUser);
            this.layoutControl2.Controls.Add(this.textEditUserID);
            this.layoutControl2.Controls.Add(this.textEditFullName);
            this.layoutControl2.Controls.Add(this.textEditAdress);
            this.layoutControl2.Controls.Add(this.textEditPosition);
            this.layoutControl2.Controls.Add(this.comboBoxEditBranch);
            this.layoutControl2.Controls.Add(this.textEditPhone);
            this.layoutControl2.Controls.Add(this.textEditEmail);
            this.layoutControl2.Controls.Add(this.textEditUsername);
            this.layoutControl2.Controls.Add(this.textEditPassword);
            this.layoutControl2.Controls.Add(this.textEditDescription);
            this.layoutControl2.Controls.Add(this.checkEditActive);
            this.layoutControl2.Controls.Add(this.checkEditBlocked);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl2.Location = new System.Drawing.Point(0, 37);
            this.layoutControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(357, 206, 650, 400);
            this.layoutControl2.Root = this.layoutControlGroup1;
            this.layoutControl2.Size = new System.Drawing.Size(1354, 324);
            this.layoutControl2.TabIndex = 1;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // textEditUserID
            // 
            this.textEditUserID.Location = new System.Drawing.Point(115, 14);
            this.textEditUserID.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textEditUserID.Name = "textEditUserID";
            this.textEditUserID.Size = new System.Drawing.Size(455, 23);
            this.textEditUserID.StyleController = this.layoutControl2;
            this.textEditUserID.TabIndex = 5;
            // 
            // textEditFullName
            // 
            this.textEditFullName.Location = new System.Drawing.Point(115, 41);
            this.textEditFullName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textEditFullName.Name = "textEditFullName";
            this.textEditFullName.Size = new System.Drawing.Size(455, 23);
            this.textEditFullName.StyleController = this.layoutControl2;
            this.textEditFullName.TabIndex = 6;
            // 
            // textEditAdress
            // 
            this.textEditAdress.Location = new System.Drawing.Point(115, 68);
            this.textEditAdress.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textEditAdress.Name = "textEditAdress";
            this.textEditAdress.Size = new System.Drawing.Size(455, 23);
            this.textEditAdress.StyleController = this.layoutControl2;
            this.textEditAdress.TabIndex = 7;
            // 
            // textEditPosition
            // 
            this.textEditPosition.Location = new System.Drawing.Point(115, 95);
            this.textEditPosition.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textEditPosition.Name = "textEditPosition";
            this.textEditPosition.Size = new System.Drawing.Size(455, 23);
            this.textEditPosition.StyleController = this.layoutControl2;
            this.textEditPosition.TabIndex = 8;
            // 
            // comboBoxEditBranch
            // 
            this.comboBoxEditBranch.Location = new System.Drawing.Point(115, 122);
            this.comboBoxEditBranch.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.comboBoxEditBranch.Name = "comboBoxEditBranch";
            this.comboBoxEditBranch.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditBranch.Size = new System.Drawing.Size(455, 23);
            this.comboBoxEditBranch.StyleController = this.layoutControl2;
            this.comboBoxEditBranch.TabIndex = 9;
            // 
            // textEditPhone
            // 
            this.textEditPhone.Location = new System.Drawing.Point(115, 149);
            this.textEditPhone.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textEditPhone.Name = "textEditPhone";
            this.textEditPhone.Size = new System.Drawing.Size(455, 23);
            this.textEditPhone.StyleController = this.layoutControl2;
            this.textEditPhone.TabIndex = 10;
            // 
            // textEditEmail
            // 
            this.textEditEmail.Location = new System.Drawing.Point(115, 176);
            this.textEditEmail.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textEditEmail.Name = "textEditEmail";
            this.textEditEmail.Size = new System.Drawing.Size(455, 23);
            this.textEditEmail.StyleController = this.layoutControl2;
            this.textEditEmail.TabIndex = 11;
            // 
            // textEditUsername
            // 
            this.textEditUsername.Location = new System.Drawing.Point(115, 203);
            this.textEditUsername.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textEditUsername.Name = "textEditUsername";
            this.textEditUsername.Size = new System.Drawing.Size(455, 23);
            this.textEditUsername.StyleController = this.layoutControl2;
            this.textEditUsername.TabIndex = 12;
            // 
            // textEditPassword
            // 
            this.textEditPassword.Location = new System.Drawing.Point(115, 230);
            this.textEditPassword.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textEditPassword.Name = "textEditPassword";
            this.textEditPassword.Properties.PasswordChar = '*';
            this.textEditPassword.Properties.UseSystemPasswordChar = true;
            this.textEditPassword.Size = new System.Drawing.Size(455, 23);
            this.textEditPassword.StyleController = this.layoutControl2;
            this.textEditPassword.TabIndex = 13;
            // 
            // textEditDescription
            // 
            this.textEditDescription.Location = new System.Drawing.Point(115, 257);
            this.textEditDescription.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textEditDescription.Name = "textEditDescription";
            this.textEditDescription.Size = new System.Drawing.Size(105, 23);
            this.textEditDescription.StyleController = this.layoutControl2;
            this.textEditDescription.TabIndex = 14;
            // 
            // checkEditActive
            // 
            this.checkEditActive.Location = new System.Drawing.Point(224, 257);
            this.checkEditActive.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.checkEditActive.Name = "checkEditActive";
            this.checkEditActive.Properties.Caption = "Active";
            this.checkEditActive.Size = new System.Drawing.Size(66, 20);
            this.checkEditActive.StyleController = this.layoutControl2;
            this.checkEditActive.TabIndex = 15;
            // 
            // checkEditBlocked
            // 
            this.checkEditBlocked.Location = new System.Drawing.Point(294, 257);
            this.checkEditBlocked.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.checkEditBlocked.Name = "checkEditBlocked";
            this.checkEditBlocked.Properties.Caption = "Blocked";
            this.checkEditBlocked.Size = new System.Drawing.Size(276, 20);
            this.checkEditBlocked.StyleController = this.layoutControl2;
            this.checkEditBlocked.TabIndex = 16;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItemUserId,
            this.layoutControlItemFullname,
            this.layoutControlItemAdress,
            this.layoutControlItemPosition,
            this.layoutControlItemBranch,
            this.layoutControlItemPhone,
            this.layoutControlItemEmail,
            this.layoutControlItemUsername,
            this.layoutControlItemPassword,
            this.layoutControlItemDescription,
            this.layoutControlItem4,
            this.layoutControlItem5});
            this.layoutControlGroup1.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.layoutControlGroup1.Name = "Root";
            columnDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition1.Width = 15.789473684210526D;
            columnDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition2.Width = 5.2631578947368416D;
            columnDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition3.Width = 21.052631578947366D;
            columnDefinition4.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition4.Width = 57.89473684210526D;
            this.layoutControlGroup1.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition1,
            columnDefinition2,
            columnDefinition3,
            columnDefinition4});
            rowDefinition1.Height = 9.09090909090909D;
            rowDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition2.Height = 9.09090909090909D;
            rowDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition3.Height = 9.09090909090909D;
            rowDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition4.Height = 9.09090909090909D;
            rowDefinition4.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition5.Height = 9.09090909090909D;
            rowDefinition5.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition6.Height = 9.09090909090909D;
            rowDefinition6.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition7.Height = 9.09090909090909D;
            rowDefinition7.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition8.Height = 9.09090909090909D;
            rowDefinition8.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition9.Height = 9.09090909090909D;
            rowDefinition9.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition10.Height = 9.09090909090909D;
            rowDefinition10.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition11.Height = 9.09090909090909D;
            rowDefinition11.SizeType = System.Windows.Forms.SizeType.Percent;
            this.layoutControlGroup1.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition1,
            rowDefinition2,
            rowDefinition3,
            rowDefinition4,
            rowDefinition5,
            rowDefinition6,
            rowDefinition7,
            rowDefinition8,
            rowDefinition9,
            rowDefinition10,
            rowDefinition11});
            this.layoutControlGroup1.Size = new System.Drawing.Size(1354, 324);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.gridControlUser;
            this.layoutControlItem2.Location = new System.Drawing.Point(560, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.OptionsTableLayoutItem.ColumnIndex = 3;
            this.layoutControlItem2.OptionsTableLayoutItem.RowSpan = 11;
            this.layoutControlItem2.Size = new System.Drawing.Size(770, 300);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItemUserId
            // 
            this.layoutControlItemUserId.Control = this.textEditUserID;
            this.layoutControlItemUserId.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemUserId.Name = "layoutControlItemUserId";
            this.layoutControlItemUserId.OptionsTableLayoutItem.ColumnSpan = 3;
            this.layoutControlItemUserId.Size = new System.Drawing.Size(560, 27);
            this.layoutControlItemUserId.Text = "Mã người dùng";
            this.layoutControlItemUserId.TextSize = new System.Drawing.Size(98, 17);
            // 
            // layoutControlItemFullname
            // 
            this.layoutControlItemFullname.Control = this.textEditFullName;
            this.layoutControlItemFullname.Location = new System.Drawing.Point(0, 27);
            this.layoutControlItemFullname.Name = "layoutControlItemFullname";
            this.layoutControlItemFullname.OptionsTableLayoutItem.ColumnSpan = 3;
            this.layoutControlItemFullname.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemFullname.Size = new System.Drawing.Size(560, 27);
            this.layoutControlItemFullname.Text = "Tên người dùng";
            this.layoutControlItemFullname.TextSize = new System.Drawing.Size(98, 17);
            // 
            // layoutControlItemAdress
            // 
            this.layoutControlItemAdress.Control = this.textEditAdress;
            this.layoutControlItemAdress.Location = new System.Drawing.Point(0, 54);
            this.layoutControlItemAdress.Name = "layoutControlItemAdress";
            this.layoutControlItemAdress.OptionsTableLayoutItem.ColumnSpan = 3;
            this.layoutControlItemAdress.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItemAdress.Size = new System.Drawing.Size(560, 27);
            this.layoutControlItemAdress.Text = "Địa chỉ";
            this.layoutControlItemAdress.TextSize = new System.Drawing.Size(98, 17);
            // 
            // layoutControlItemPosition
            // 
            this.layoutControlItemPosition.Control = this.textEditPosition;
            this.layoutControlItemPosition.Location = new System.Drawing.Point(0, 81);
            this.layoutControlItemPosition.Name = "layoutControlItemPosition";
            this.layoutControlItemPosition.OptionsTableLayoutItem.ColumnSpan = 3;
            this.layoutControlItemPosition.OptionsTableLayoutItem.RowIndex = 3;
            this.layoutControlItemPosition.Size = new System.Drawing.Size(560, 27);
            this.layoutControlItemPosition.Text = "Chức vụ";
            this.layoutControlItemPosition.TextSize = new System.Drawing.Size(98, 17);
            // 
            // layoutControlItemBranch
            // 
            this.layoutControlItemBranch.Control = this.comboBoxEditBranch;
            this.layoutControlItemBranch.Location = new System.Drawing.Point(0, 108);
            this.layoutControlItemBranch.Name = "layoutControlItemBranch";
            this.layoutControlItemBranch.OptionsTableLayoutItem.ColumnSpan = 3;
            this.layoutControlItemBranch.OptionsTableLayoutItem.RowIndex = 4;
            this.layoutControlItemBranch.Size = new System.Drawing.Size(560, 27);
            this.layoutControlItemBranch.Text = "Chi nhánh";
            this.layoutControlItemBranch.TextSize = new System.Drawing.Size(98, 16);
            // 
            // layoutControlItemPhone
            // 
            this.layoutControlItemPhone.Control = this.textEditPhone;
            this.layoutControlItemPhone.CustomizationFormText = "Số điện thoại";
            this.layoutControlItemPhone.Location = new System.Drawing.Point(0, 135);
            this.layoutControlItemPhone.Name = "layoutControlItemPhone";
            this.layoutControlItemPhone.OptionsTableLayoutItem.ColumnSpan = 3;
            this.layoutControlItemPhone.OptionsTableLayoutItem.RowIndex = 5;
            this.layoutControlItemPhone.Size = new System.Drawing.Size(560, 27);
            this.layoutControlItemPhone.Text = "Số điện thoại";
            this.layoutControlItemPhone.TextSize = new System.Drawing.Size(98, 17);
            // 
            // layoutControlItemEmail
            // 
            this.layoutControlItemEmail.Control = this.textEditEmail;
            this.layoutControlItemEmail.Location = new System.Drawing.Point(0, 162);
            this.layoutControlItemEmail.Name = "layoutControlItemEmail";
            this.layoutControlItemEmail.OptionsTableLayoutItem.ColumnSpan = 3;
            this.layoutControlItemEmail.OptionsTableLayoutItem.RowIndex = 6;
            this.layoutControlItemEmail.Size = new System.Drawing.Size(560, 27);
            this.layoutControlItemEmail.Text = "Email";
            this.layoutControlItemEmail.TextSize = new System.Drawing.Size(98, 16);
            // 
            // layoutControlItemUsername
            // 
            this.layoutControlItemUsername.Control = this.textEditUsername;
            this.layoutControlItemUsername.Location = new System.Drawing.Point(0, 189);
            this.layoutControlItemUsername.Name = "layoutControlItemUsername";
            this.layoutControlItemUsername.OptionsTableLayoutItem.ColumnSpan = 3;
            this.layoutControlItemUsername.OptionsTableLayoutItem.RowIndex = 7;
            this.layoutControlItemUsername.Size = new System.Drawing.Size(560, 27);
            this.layoutControlItemUsername.Text = "Tên đăng nhập";
            this.layoutControlItemUsername.TextSize = new System.Drawing.Size(98, 17);
            // 
            // layoutControlItemPassword
            // 
            this.layoutControlItemPassword.Control = this.textEditPassword;
            this.layoutControlItemPassword.Location = new System.Drawing.Point(0, 216);
            this.layoutControlItemPassword.Name = "layoutControlItemPassword";
            this.layoutControlItemPassword.OptionsTableLayoutItem.ColumnSpan = 3;
            this.layoutControlItemPassword.OptionsTableLayoutItem.RowIndex = 8;
            this.layoutControlItemPassword.Size = new System.Drawing.Size(560, 27);
            this.layoutControlItemPassword.Text = "Mật khẩu";
            this.layoutControlItemPassword.TextSize = new System.Drawing.Size(98, 17);
            // 
            // layoutControlItemDescription
            // 
            this.layoutControlItemDescription.Control = this.textEditDescription;
            this.layoutControlItemDescription.Location = new System.Drawing.Point(0, 243);
            this.layoutControlItemDescription.Name = "layoutControlItemDescription";
            this.layoutControlItemDescription.OptionsTableLayoutItem.RowIndex = 9;
            this.layoutControlItemDescription.OptionsTableLayoutItem.RowSpan = 2;
            this.layoutControlItemDescription.Size = new System.Drawing.Size(210, 57);
            this.layoutControlItemDescription.Text = "Mô tả";
            this.layoutControlItemDescription.TextSize = new System.Drawing.Size(98, 17);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.checkEditActive;
            this.layoutControlItem4.Location = new System.Drawing.Point(210, 243);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItem4.OptionsTableLayoutItem.RowIndex = 9;
            this.layoutControlItem4.Size = new System.Drawing.Size(70, 27);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.checkEditBlocked;
            this.layoutControlItem5.Location = new System.Drawing.Point(280, 243);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItem5.OptionsTableLayoutItem.RowIndex = 9;
            this.layoutControlItem5.Size = new System.Drawing.Size(280, 27);
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.xtraTabControl1);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl3.Location = new System.Drawing.Point(0, 361);
            this.layoutControl3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(414, 128, 650, 400);
            this.layoutControl3.Root = this.layoutControlGroup2;
            this.layoutControl3.Size = new System.Drawing.Size(1354, 397);
            this.layoutControl3.TabIndex = 2;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Location = new System.Drawing.Point(14, 14);
            this.xtraTabControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPageScreen;
            this.xtraTabControl1.Size = new System.Drawing.Size(1326, 369);
            this.xtraTabControl1.TabIndex = 4;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageScreen,
            this.xtraTabPageReport,
            this.xtraTabPageOther});
            // 
            // xtraTabPageScreen
            // 
            this.xtraTabPageScreen.Controls.Add(this.gridControlScreen);
            this.xtraTabPageScreen.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.xtraTabPageScreen.Name = "xtraTabPageScreen";
            this.xtraTabPageScreen.Size = new System.Drawing.Size(1320, 336);
            this.xtraTabPageScreen.Text = "Phân quyền trên màn hình";
            // 
            // gridControlScreen
            // 
            this.gridControlScreen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlScreen.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridControlScreen.Location = new System.Drawing.Point(0, 0);
            this.gridControlScreen.MainView = this.gridViewScreen;
            this.gridControlScreen.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridControlScreen.Name = "gridControlScreen";
            this.gridControlScreen.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit2,
            this.repositoryItemCheckedComboBoxEdit2,
            this.repositoryItemComboBox2,
            this.repositoryItemTextEdit2});
            this.gridControlScreen.Size = new System.Drawing.Size(1320, 336);
            this.gridControlScreen.TabIndex = 2;
            this.gridControlScreen.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewScreen});
            // 
            // gridViewScreen
            // 
            this.gridViewScreen.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colNo3,
            this.colScreenName,
            this.colScreenNameEN,
            this.colSaveNewRight,
            this.colSaveChangeRight,
            this.colDeleteRight,
            this.colPrintRight,
            this.colImportRight,
            this.colExportRight,
            this.colScreenID,
            this.colOrdinal1});
            this.gridViewScreen.DetailHeight = 431;
            this.gridViewScreen.GridControl = this.gridControlScreen;
            this.gridViewScreen.Name = "gridViewScreen";
            this.gridViewScreen.OptionsSelection.MultiSelect = true;
            // 
            // colNo3
            // 
            this.colNo3.Caption = "No";
            this.colNo3.MinWidth = 23;
            this.colNo3.Name = "colNo3";
            this.colNo3.OptionsColumn.ReadOnly = true;
            this.colNo3.Visible = true;
            this.colNo3.VisibleIndex = 0;
            this.colNo3.Width = 44;
            // 
            // colScreenName
            // 
            this.colScreenName.Caption = "Tên màn hình";
            this.colScreenName.FieldName = "ScreenName";
            this.colScreenName.MinWidth = 23;
            this.colScreenName.Name = "colScreenName";
            this.colScreenName.Visible = true;
            this.colScreenName.VisibleIndex = 1;
            this.colScreenName.Width = 205;
            // 
            // colScreenNameEN
            // 
            this.colScreenNameEN.Caption = "Screen Name";
            this.colScreenNameEN.FieldName = "ScreenNameEN";
            this.colScreenNameEN.MinWidth = 23;
            this.colScreenNameEN.Name = "colScreenNameEN";
            this.colScreenNameEN.Visible = true;
            this.colScreenNameEN.VisibleIndex = 2;
            this.colScreenNameEN.Width = 323;
            // 
            // colSaveNewRight
            // 
            this.colSaveNewRight.Caption = "Lưu mới";
            this.colSaveNewRight.FieldName = "SaveNewRight";
            this.colSaveNewRight.MinWidth = 23;
            this.colSaveNewRight.Name = "colSaveNewRight";
            this.colSaveNewRight.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.colSaveNewRight.Visible = true;
            this.colSaveNewRight.VisibleIndex = 3;
            this.colSaveNewRight.Width = 87;
            // 
            // colSaveChangeRight
            // 
            this.colSaveChangeRight.Caption = "Lưu thay đổi";
            this.colSaveChangeRight.FieldName = "SaveChangeRight";
            this.colSaveChangeRight.MinWidth = 23;
            this.colSaveChangeRight.Name = "colSaveChangeRight";
            this.colSaveChangeRight.Visible = true;
            this.colSaveChangeRight.VisibleIndex = 4;
            this.colSaveChangeRight.Width = 87;
            // 
            // colDeleteRight
            // 
            this.colDeleteRight.Caption = "Xóa";
            this.colDeleteRight.FieldName = "DeleteRight";
            this.colDeleteRight.MinWidth = 23;
            this.colDeleteRight.Name = "colDeleteRight";
            this.colDeleteRight.Visible = true;
            this.colDeleteRight.VisibleIndex = 5;
            this.colDeleteRight.Width = 87;
            // 
            // colPrintRight
            // 
            this.colPrintRight.Caption = "In ấn";
            this.colPrintRight.FieldName = "PrintRight";
            this.colPrintRight.MinWidth = 23;
            this.colPrintRight.Name = "colPrintRight";
            this.colPrintRight.Visible = true;
            this.colPrintRight.VisibleIndex = 6;
            this.colPrintRight.Width = 87;
            // 
            // colImportRight
            // 
            this.colImportRight.Caption = "Nhập dữ liệu";
            this.colImportRight.FieldName = "ImportRight";
            this.colImportRight.MinWidth = 23;
            this.colImportRight.Name = "colImportRight";
            this.colImportRight.Visible = true;
            this.colImportRight.VisibleIndex = 7;
            this.colImportRight.Width = 87;
            // 
            // colExportRight
            // 
            this.colExportRight.Caption = "Xuất dữ liệu";
            this.colExportRight.FieldName = "ExportRight";
            this.colExportRight.MinWidth = 23;
            this.colExportRight.Name = "colExportRight";
            this.colExportRight.Visible = true;
            this.colExportRight.VisibleIndex = 8;
            this.colExportRight.Width = 87;
            // 
            // colScreenID
            // 
            this.colScreenID.Caption = "Screen ID";
            this.colScreenID.FieldName = "ScreenID";
            this.colScreenID.MinWidth = 23;
            this.colScreenID.Name = "colScreenID";
            this.colScreenID.Width = 87;
            // 
            // colOrdinal1
            // 
            this.colOrdinal1.Caption = "Ordinal";
            this.colOrdinal1.FieldName = "Ordinal";
            this.colOrdinal1.MinWidth = 24;
            this.colOrdinal1.Name = "colOrdinal1";
            this.colOrdinal1.ShowUnboundExpressionMenu = true;
            this.colOrdinal1.Width = 87;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AllowGrayed = true;
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.RadioGroupIndex = 0;
            this.repositoryItemCheckEdit2.ValueChecked = "1";
            this.repositoryItemCheckEdit2.ValueUnchecked = "0";
            // 
            // repositoryItemCheckedComboBoxEdit2
            // 
            this.repositoryItemCheckedComboBoxEdit2.AllowMultiSelect = true;
            this.repositoryItemCheckedComboBoxEdit2.AutoHeight = false;
            this.repositoryItemCheckedComboBoxEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCheckedComboBoxEdit2.Name = "repositoryItemCheckedComboBoxEdit2";
            // 
            // repositoryItemComboBox2
            // 
            this.repositoryItemComboBox2.AutoHeight = false;
            this.repositoryItemComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox2.Name = "repositoryItemComboBox2";
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // xtraTabPageReport
            // 
            this.xtraTabPageReport.Controls.Add(this.gridControlReport);
            this.xtraTabPageReport.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.xtraTabPageReport.Name = "xtraTabPageReport";
            this.xtraTabPageReport.Size = new System.Drawing.Size(1320, 336);
            this.xtraTabPageReport.Text = "Phân quyền trên báo cáo";
            // 
            // gridControlReport
            // 
            this.gridControlReport.DataSource = this.reportBindingSource;
            this.gridControlReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlReport.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridControlReport.Location = new System.Drawing.Point(0, 0);
            this.gridControlReport.MainView = this.gridViewReport;
            this.gridControlReport.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridControlReport.Name = "gridControlReport";
            this.gridControlReport.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit3,
            this.repositoryItemCheckedComboBoxEdit3,
            this.repositoryItemComboBox3,
            this.repositoryItemTextEdit3});
            this.gridControlReport.Size = new System.Drawing.Size(1320, 336);
            this.gridControlReport.TabIndex = 2;
            this.gridControlReport.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewReport});
            // 
            // reportBindingSource
            // 
            this.reportBindingSource.DataMember = "Report";
            this.reportBindingSource.DataSource = this.wARHOUSE_HPDataSet;
            // 
            // gridViewReport
            // 
            this.gridViewReport.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colReportID,
            this.colReportName,
            this.colReportNameEN,
            this.colOrdinal,
            this.colNo4,
            this.colViewRight,
            this.colPrintRight2});
            this.gridViewReport.DetailHeight = 431;
            this.gridViewReport.GridControl = this.gridControlReport;
            this.gridViewReport.Name = "gridViewReport";
            // 
            // colReportID
            // 
            this.colReportID.Caption = "Report ID";
            this.colReportID.FieldName = "ReportID";
            this.colReportID.MinWidth = 23;
            this.colReportID.Name = "colReportID";
            this.colReportID.Width = 87;
            // 
            // colReportName
            // 
            this.colReportName.Caption = "Tên báo cáo";
            this.colReportName.FieldName = "ReportName";
            this.colReportName.MinWidth = 23;
            this.colReportName.Name = "colReportName";
            this.colReportName.Visible = true;
            this.colReportName.VisibleIndex = 1;
            this.colReportName.Width = 292;
            // 
            // colReportNameEN
            // 
            this.colReportNameEN.Caption = "Report Name EN";
            this.colReportNameEN.FieldName = "ReportNameEN";
            this.colReportNameEN.MinWidth = 23;
            this.colReportNameEN.Name = "colReportNameEN";
            this.colReportNameEN.Visible = true;
            this.colReportNameEN.VisibleIndex = 2;
            this.colReportNameEN.Width = 292;
            // 
            // colOrdinal
            // 
            this.colOrdinal.Caption = "Ordinal";
            this.colOrdinal.FieldName = "Ordinal";
            this.colOrdinal.MinWidth = 23;
            this.colOrdinal.Name = "colOrdinal";
            this.colOrdinal.Width = 296;
            // 
            // colNo4
            // 
            this.colNo4.Caption = "No";
            this.colNo4.FieldName = "No";
            this.colNo4.MinWidth = 23;
            this.colNo4.Name = "colNo4";
            this.colNo4.Visible = true;
            this.colNo4.VisibleIndex = 0;
            this.colNo4.Width = 78;
            // 
            // colViewRight
            // 
            this.colViewRight.Caption = "View Right";
            this.colViewRight.FieldName = "ViewRight";
            this.colViewRight.MinWidth = 23;
            this.colViewRight.Name = "colViewRight";
            this.colViewRight.Visible = true;
            this.colViewRight.VisibleIndex = 3;
            this.colViewRight.Width = 87;
            // 
            // colPrintRight2
            // 
            this.colPrintRight2.Caption = "gridColumn1";
            this.colPrintRight2.CustomizationCaption = "Print Right";
            this.colPrintRight2.FieldName = "PrintRight";
            this.colPrintRight2.MinWidth = 23;
            this.colPrintRight2.Name = "colPrintRight2";
            this.colPrintRight2.Visible = true;
            this.colPrintRight2.VisibleIndex = 4;
            this.colPrintRight2.Width = 87;
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AllowGrayed = true;
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.RadioGroupIndex = 0;
            this.repositoryItemCheckEdit3.ValueChecked = "1";
            this.repositoryItemCheckEdit3.ValueUnchecked = "0";
            // 
            // repositoryItemCheckedComboBoxEdit3
            // 
            this.repositoryItemCheckedComboBoxEdit3.AllowMultiSelect = true;
            this.repositoryItemCheckedComboBoxEdit3.AutoHeight = false;
            this.repositoryItemCheckedComboBoxEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCheckedComboBoxEdit3.Name = "repositoryItemCheckedComboBoxEdit3";
            // 
            // repositoryItemComboBox3
            // 
            this.repositoryItemComboBox3.AutoHeight = false;
            this.repositoryItemComboBox3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox3.Name = "repositoryItemComboBox3";
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // xtraTabPageOther
            // 
            this.xtraTabPageOther.Controls.Add(this.gridControlRight);
            this.xtraTabPageOther.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.xtraTabPageOther.Name = "xtraTabPageOther";
            this.xtraTabPageOther.Size = new System.Drawing.Size(1320, 336);
            this.xtraTabPageOther.Text = "Phân quyền khác";
            // 
            // gridControlRight
            // 
            this.gridControlRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlRight.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridControlRight.Location = new System.Drawing.Point(0, 0);
            this.gridControlRight.MainView = this.gridViewRight;
            this.gridControlRight.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridControlRight.Name = "gridControlRight";
            this.gridControlRight.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemCheckedComboBoxEdit1,
            this.repositoryItemComboBox1,
            this.repositoryItemTextEdit1});
            this.gridControlRight.Size = new System.Drawing.Size(1320, 336);
            this.gridControlRight.TabIndex = 1;
            this.gridControlRight.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewRight});
            // 
            // gridViewRight
            // 
            this.gridViewRight.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colNo,
            this.colRightID,
            this.colRightName,
            this.colRightNameEN,
            this.colGrantRight,
            this.colOrdinal3});
            this.gridViewRight.DetailHeight = 431;
            this.gridViewRight.GridControl = this.gridControlRight;
            this.gridViewRight.Name = "gridViewRight";
            // 
            // colNo
            // 
            this.colNo.Caption = "No";
            this.colNo.MinWidth = 23;
            this.colNo.Name = "colNo";
            this.colNo.OptionsColumn.ReadOnly = true;
            this.colNo.Visible = true;
            this.colNo.VisibleIndex = 0;
            this.colNo.Width = 44;
            // 
            // colRightID
            // 
            this.colRightID.FieldName = "RightID";
            this.colRightID.MinWidth = 23;
            this.colRightID.Name = "colRightID";
            this.colRightID.Width = 139;
            // 
            // colRightName
            // 
            this.colRightName.FieldName = "RightName";
            this.colRightName.MinWidth = 23;
            this.colRightName.Name = "colRightName";
            this.colRightName.Visible = true;
            this.colRightName.VisibleIndex = 1;
            this.colRightName.Width = 205;
            // 
            // colRightNameEN
            // 
            this.colRightNameEN.FieldName = "RightNameEN";
            this.colRightNameEN.MinWidth = 23;
            this.colRightNameEN.Name = "colRightNameEN";
            this.colRightNameEN.Visible = true;
            this.colRightNameEN.VisibleIndex = 2;
            this.colRightNameEN.Width = 323;
            // 
            // colGrantRight
            // 
            this.colGrantRight.Caption = "Cấp quyền";
            this.colGrantRight.FieldName = "GrantRight";
            this.colGrantRight.MinWidth = 23;
            this.colGrantRight.Name = "colGrantRight";
            this.colGrantRight.Visible = true;
            this.colGrantRight.VisibleIndex = 3;
            this.colGrantRight.Width = 87;
            // 
            // colOrdinal3
            // 
            this.colOrdinal3.Caption = "Ordinal";
            this.colOrdinal3.FieldName = "Ordinal";
            this.colOrdinal3.MinWidth = 24;
            this.colOrdinal3.Name = "colOrdinal3";
            this.colOrdinal3.Width = 87;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AllowGrayed = true;
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.RadioGroupIndex = 0;
            this.repositoryItemCheckEdit1.ValueChecked = "1";
            this.repositoryItemCheckEdit1.ValueUnchecked = "0";
            // 
            // repositoryItemCheckedComboBoxEdit1
            // 
            this.repositoryItemCheckedComboBoxEdit1.AllowMultiSelect = true;
            this.repositoryItemCheckedComboBoxEdit1.AutoHeight = false;
            this.repositoryItemCheckedComboBoxEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCheckedComboBoxEdit1.Name = "repositoryItemCheckedComboBoxEdit1";
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3});
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1354, 397);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.xtraTabControl1;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(1330, 373);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // screenBindingSource
            // 
            this.screenBindingSource.DataMember = "Screen";
            this.screenBindingSource.DataSource = this.wARHOUSE_HPDataSet;
            // 
            // userTableAdapter
            // 
            this.userTableAdapter.ClearBeforeFill = true;
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // screenTableAdapter
            // 
            this.screenTableAdapter.ClearBeforeFill = true;
            // 
            // barManager1
            // 
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlTop.Size = new System.Drawing.Size(1354, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 758);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlBottom.Size = new System.Drawing.Size(1354, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 758);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1354, 0);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 758);
            // 
            // reportTableAdapter
            // 
            this.reportTableAdapter.ClearBeforeFill = true;
            // 
            // popupMenu1
            // 
            this.popupMenu1.Manager = this.barManager1;
            this.popupMenu1.Name = "popupMenu1";
            // 
            // colActive
            // 
            this.colActive.FieldName = "Active";
            this.colActive.MinWidth = 25;
            this.colActive.Name = "colActive";
            this.colActive.Width = 87;
            // 
            // colBlocked
            // 
            this.colBlocked.FieldName = "Blocked";
            this.colBlocked.MinWidth = 25;
            this.colBlocked.Name = "colBlocked";
            this.colBlocked.Width = 87;
            // 
            // colBranchID
            // 
            this.colBranchID.FieldName = "BranchID";
            this.colBranchID.MinWidth = 25;
            this.colBranchID.Name = "colBranchID";
            this.colBranchID.Width = 87;
            // 
            // colBranchName
            // 
            this.colBranchName.FieldName = "BranchName";
            this.colBranchName.MinWidth = 25;
            this.colBranchName.Name = "colBranchName";
            this.colBranchName.Width = 87;
            // 
            // colAdress
            // 
            this.colAdress.FieldName = "Adress";
            this.colAdress.MinWidth = 25;
            this.colAdress.Name = "colAdress";
            this.colAdress.Width = 87;
            // 
            // colEmail
            // 
            this.colEmail.FieldName = "Email";
            this.colEmail.MinWidth = 25;
            this.colEmail.Name = "colEmail";
            this.colEmail.Width = 87;
            // 
            // colPosition
            // 
            this.colPosition.FieldName = "Position";
            this.colPosition.MinWidth = 25;
            this.colPosition.Name = "colPosition";
            this.colPosition.Width = 87;
            // 
            // colDescription
            // 
            this.colDescription.FieldName = "Description";
            this.colDescription.MinWidth = 25;
            this.colDescription.Name = "colDescription";
            this.colDescription.Width = 87;
            // 
            // UserManagerControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl3);
            this.Controls.Add(this.layoutControl2);
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "UserManagerControl";
            this.Size = new System.Drawing.Size(1354, 758);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wARHOUSE_HPDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEditUserID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditFullName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditAdress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPosition.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditBranch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPhone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditUsername.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPassword.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditActive.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditBlocked.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemUserId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemFullname)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAdress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPosition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemBranch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemUsername)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPageScreen.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlScreen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewScreen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            this.xtraTabPageReport.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            this.xtraTabPageOther.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.screenBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraGrid.GridControl gridControlUser;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewUser;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageScreen;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageReport;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageOther;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraGrid.GridControl gridControlRight;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewRight;
        private DevExpress.XtraGrid.Columns.GridColumn colNo;
        private DevExpress.XtraGrid.Columns.GridColumn colRightID;
        private DevExpress.XtraGrid.Columns.GridColumn colRightName;
        private DevExpress.XtraGrid.Columns.GridColumn colRightNameEN;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repositoryItemCheckedComboBoxEdit1;
        private System.Windows.Forms.BindingSource userBindingSource;
        private WARHOUSE_HPDataSet wARHOUSE_HPDataSet;
        private DevExpress.XtraGrid.Columns.GridColumn colUserID;
        private DevExpress.XtraGrid.Columns.GridColumn colUserName;
        private DevExpress.XtraGrid.Columns.GridColumn colLoginName;
        private WARHOUSE_HPDataSetTableAdapters.UserTableAdapter userTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colNo2;
        private DevExpress.XtraEditors.TextEdit textEditUserID;
        private DevExpress.XtraEditors.TextEdit textEditFullName;
        private DevExpress.XtraEditors.TextEdit textEditAdress;
        private DevExpress.XtraEditors.TextEdit textEditPosition;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditBranch;
        private DevExpress.XtraEditors.TextEdit textEditPhone;
        private DevExpress.XtraEditors.TextEdit textEditEmail;
        private DevExpress.XtraEditors.TextEdit textEditUsername;
        private DevExpress.XtraEditors.TextEdit textEditPassword;
        private DevExpress.XtraEditors.TextEdit textEditDescription;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemUserId;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemFullname;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemAdress;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemPosition;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemBranch;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemPhone;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemEmail;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemUsername;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemPassword;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDescription;
        private DevExpress.XtraEditors.CheckEdit checkEditActive;
        private DevExpress.XtraEditors.CheckEdit checkEditBlocked;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraGrid.Columns.GridColumn colGrantRight;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.GridControl gridControlScreen;
        private System.Windows.Forms.BindingSource screenBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewScreen;
        private DevExpress.XtraGrid.Columns.GridColumn colNo3;
        private DevExpress.XtraGrid.Columns.GridColumn colScreenName;
        private DevExpress.XtraGrid.Columns.GridColumn colScreenNameEN;
        private DevExpress.XtraGrid.Columns.GridColumn colSaveNewRight;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repositoryItemCheckedComboBoxEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private WARHOUSE_HPDataSetTableAdapters.ScreenTableAdapter screenTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colSaveChangeRight;
        private DevExpress.XtraGrid.Columns.GridColumn colDeleteRight;
        private DevExpress.XtraGrid.Columns.GridColumn colPrintRight;
        private DevExpress.XtraGrid.Columns.GridColumn colImportRight;
        private DevExpress.XtraGrid.Columns.GridColumn colExportRight;
        private DevExpress.XtraGrid.Columns.GridColumn colScreenID;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraGrid.Columns.GridColumn colOrdinal1;
        private System.Windows.Forms.BindingSource reportBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colOrdinal3;
        private WARHOUSE_HPDataSetTableAdapters.ReportTableAdapter reportTableAdapter;
        private DevExpress.XtraGrid.GridControl gridControlReport;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewReport;
        private DevExpress.XtraGrid.Columns.GridColumn colReportID;
        private DevExpress.XtraGrid.Columns.GridColumn colReportName;
        private DevExpress.XtraGrid.Columns.GridColumn colReportNameEN;
        private DevExpress.XtraGrid.Columns.GridColumn colOrdinal;
        private DevExpress.XtraGrid.Columns.GridColumn colNo4;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repositoryItemCheckedComboBoxEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn colViewRight;
        private DevExpress.XtraGrid.Columns.GridColumn colPrintRight2;
        private DevExpress.XtraBars.PopupMenu popupMenu1;
        private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel windowsUIButtonPanel1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraGrid.Columns.GridColumn colActive;
        private DevExpress.XtraGrid.Columns.GridColumn colBlocked;
        private DevExpress.XtraGrid.Columns.GridColumn colBranchID;
        private DevExpress.XtraGrid.Columns.GridColumn colBranchName;
        private DevExpress.XtraGrid.Columns.GridColumn colAdress;
        private DevExpress.XtraGrid.Columns.GridColumn colEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colPosition;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
    }
}
