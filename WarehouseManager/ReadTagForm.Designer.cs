﻿
namespace WarehouseManager
{
    partial class ReadTagForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraLayout.ColumnDefinition columnDefinition1 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition2 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition1 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition2 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition3 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition4 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition5 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition6 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition7 = new DevExpress.XtraLayout.RowDefinition();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.textEditTagID = new DevExpress.XtraEditors.TextEdit();
            this.textEditPassword = new DevExpress.XtraEditors.TextEdit();
            this.comboBoxEditMemoryBank = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEditOffset = new DevExpress.XtraEditors.TextEdit();
            this.textEditNoOfBytesLenght = new DevExpress.XtraEditors.TextEdit();
            this.simpleButtonReadTag = new DevExpress.XtraEditors.SimpleButton();
            this.textEditDataRead = new DevExpress.XtraEditors.MemoEdit();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTagID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPassword.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditMemoryBank.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditOffset.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditNoOfBytesLenght.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDataRead.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.textEditTagID);
            this.layoutControl1.Controls.Add(this.textEditPassword);
            this.layoutControl1.Controls.Add(this.comboBoxEditMemoryBank);
            this.layoutControl1.Controls.Add(this.textEditOffset);
            this.layoutControl1.Controls.Add(this.textEditNoOfBytesLenght);
            this.layoutControl1.Controls.Add(this.simpleButtonReadTag);
            this.layoutControl1.Controls.Add(this.textEditDataRead);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(449, 139, 650, 400);
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(362, 363);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // textEditTagID
            // 
            this.textEditTagID.Location = new System.Drawing.Point(113, 12);
            this.textEditTagID.Name = "textEditTagID";
            this.textEditTagID.Size = new System.Drawing.Size(237, 20);
            this.textEditTagID.StyleController = this.layoutControl1;
            this.textEditTagID.TabIndex = 4;
            // 
            // textEditPassword
            // 
            this.textEditPassword.Location = new System.Drawing.Point(113, 44);
            this.textEditPassword.Name = "textEditPassword";
            this.textEditPassword.Size = new System.Drawing.Size(237, 20);
            this.textEditPassword.StyleController = this.layoutControl1;
            this.textEditPassword.TabIndex = 5;
            // 
            // comboBoxEditMemoryBank
            // 
            this.comboBoxEditMemoryBank.Location = new System.Drawing.Point(113, 77);
            this.comboBoxEditMemoryBank.Name = "comboBoxEditMemoryBank";
            this.comboBoxEditMemoryBank.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditMemoryBank.Properties.Items.AddRange(new object[] {
            "RESERVED",
            "EPC",
            "TID",
            "USER"});
            this.comboBoxEditMemoryBank.Size = new System.Drawing.Size(237, 20);
            this.comboBoxEditMemoryBank.StyleController = this.layoutControl1;
            this.comboBoxEditMemoryBank.TabIndex = 6;
            // 
            // textEditOffset
            // 
            this.textEditOffset.Location = new System.Drawing.Point(113, 110);
            this.textEditOffset.Name = "textEditOffset";
            this.textEditOffset.Size = new System.Drawing.Size(237, 20);
            this.textEditOffset.StyleController = this.layoutControl1;
            this.textEditOffset.TabIndex = 7;
            // 
            // textEditNoOfBytesLenght
            // 
            this.textEditNoOfBytesLenght.Location = new System.Drawing.Point(113, 143);
            this.textEditNoOfBytesLenght.Name = "textEditNoOfBytesLenght";
            this.textEditNoOfBytesLenght.Size = new System.Drawing.Size(237, 20);
            this.textEditNoOfBytesLenght.StyleController = this.layoutControl1;
            this.textEditNoOfBytesLenght.TabIndex = 8;
            // 
            // simpleButtonReadTag
            // 
            this.simpleButtonReadTag.Location = new System.Drawing.Point(183, 308);
            this.simpleButtonReadTag.Name = "simpleButtonReadTag";
            this.simpleButtonReadTag.Size = new System.Drawing.Size(167, 22);
            this.simpleButtonReadTag.StyleController = this.layoutControl1;
            this.simpleButtonReadTag.TabIndex = 10;
            this.simpleButtonReadTag.Text = "Read Tag";
            // 
            // textEditDataRead
            // 
            this.textEditDataRead.Location = new System.Drawing.Point(113, 176);
            this.textEditDataRead.Name = "textEditDataRead";
            this.textEditDataRead.Properties.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.textEditDataRead.Size = new System.Drawing.Size(237, 128);
            this.textEditDataRead.StyleController = this.layoutControl1;
            this.textEditDataRead.TabIndex = 9;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7});
            this.Root.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.Root.Name = "Root";
            columnDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition1.Width = 50D;
            columnDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition2.Width = 50D;
            this.Root.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition1,
            columnDefinition2});
            rowDefinition1.Height = 9.589041095890412D;
            rowDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition2.Height = 9.589041095890412D;
            rowDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition3.Height = 9.589041095890412D;
            rowDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition4.Height = 9.589041095890412D;
            rowDefinition4.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition5.Height = 9.589041095890412D;
            rowDefinition5.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition6.Height = 38.356164383561648D;
            rowDefinition6.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition7.Height = 13.698630136986301D;
            rowDefinition7.SizeType = System.Windows.Forms.SizeType.Percent;
            this.Root.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition1,
            rowDefinition2,
            rowDefinition3,
            rowDefinition4,
            rowDefinition5,
            rowDefinition6,
            rowDefinition7});
            this.Root.Size = new System.Drawing.Size(362, 363);
            this.Root.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.textEditTagID;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.OptionsTableLayoutItem.ColumnSpan = 2;
            this.layoutControlItem1.Size = new System.Drawing.Size(342, 32);
            this.layoutControlItem1.Text = "Tag ID( Hex)";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.textEditPassword;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 32);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.OptionsTableLayoutItem.ColumnSpan = 2;
            this.layoutControlItem2.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItem2.Size = new System.Drawing.Size(342, 33);
            this.layoutControlItem2.Text = "Password( Hex)";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.comboBoxEditMemoryBank;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 65);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.OptionsTableLayoutItem.ColumnSpan = 2;
            this.layoutControlItem3.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItem3.Size = new System.Drawing.Size(342, 33);
            this.layoutControlItem3.Text = "Memory Bank";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.textEditOffset;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 98);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.OptionsTableLayoutItem.ColumnSpan = 2;
            this.layoutControlItem4.OptionsTableLayoutItem.RowIndex = 3;
            this.layoutControlItem4.Size = new System.Drawing.Size(342, 33);
            this.layoutControlItem4.Text = "Offset( Byte)";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.textEditNoOfBytesLenght;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 131);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.OptionsTableLayoutItem.ColumnSpan = 2;
            this.layoutControlItem5.OptionsTableLayoutItem.RowIndex = 4;
            this.layoutControlItem5.Size = new System.Drawing.Size(342, 33);
            this.layoutControlItem5.Text = "No. Of Bytes Lenght";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.textEditDataRead;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 164);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.OptionsTableLayoutItem.ColumnSpan = 2;
            this.layoutControlItem6.OptionsTableLayoutItem.RowIndex = 5;
            this.layoutControlItem6.Size = new System.Drawing.Size(342, 132);
            this.layoutControlItem6.Text = "Data Read( Hex)";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.simpleButtonReadTag;
            this.layoutControlItem7.Location = new System.Drawing.Point(171, 296);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItem7.OptionsTableLayoutItem.RowIndex = 6;
            this.layoutControlItem7.Size = new System.Drawing.Size(171, 47);
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // ReadTagForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(362, 363);
            this.Controls.Add(this.layoutControl1);
            this.Name = "ReadTagForm";
            this.Text = "ReadTagForm";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEditTagID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPassword.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditMemoryBank.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditOffset.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditNoOfBytesLenght.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDataRead.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        public DevExpress.XtraEditors.TextEdit textEditTagID;
        public DevExpress.XtraEditors.TextEdit textEditPassword;
        public DevExpress.XtraEditors.ComboBoxEdit comboBoxEditMemoryBank;
        public DevExpress.XtraEditors.TextEdit textEditOffset;
        public DevExpress.XtraEditors.TextEdit textEditNoOfBytesLenght;
        public DevExpress.XtraEditors.SimpleButton simpleButtonReadTag;
        public DevExpress.XtraEditors.MemoEdit textEditDataRead;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
    }
}