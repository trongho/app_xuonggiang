﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WarehouseManager.Responses
{
    public class LoginResponse
    {
        public string Username { get; set; }
        public string LoginName { get; set; }
        public string Token { get; set; }
    }
}
