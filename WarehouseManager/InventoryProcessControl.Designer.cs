﻿
namespace WarehouseManager
{
    partial class InventoryProcessControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InventoryProcessControl));
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions2 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions3 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions4 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition1 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition2 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition3 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition4 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition5 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition1 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition2 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition6 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition7 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition8 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition9 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition10 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition11 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition12 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition13 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition14 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition3 = new DevExpress.XtraLayout.RowDefinition();
            this.wRRDetailBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.wARHOUSE_HPDataSet1 = new WarehouseManager.WARHOUSE_HPDataSet1();
            this.wRRDetailTableAdapter = new WarehouseManager.WARHOUSE_HPDataSet1TableAdapters.WRRDetailTableAdapter();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.windowsUIButtonPanel1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.comboBoxEditBranch = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEditWarehouse = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEditProcessingPeriod = new DevExpress.XtraEditors.ComboBoxEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemBranch = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemWarehouse = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemProcessingPeriod = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleLabelItem1 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.gridControlInventory = new DevExpress.XtraGrid.GridControl();
            this.inventoryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.wARHOUSE_HPDataSet2 = new WarehouseManager.WARHOUSE_HPDataSet2();
            this.gridViewInventory = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colYear = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMonth = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWarehouseID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGoodsID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOpeningStockQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOpeningStockAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReceiptQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReceiptAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIssueQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIssueAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClosingStockQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClosingStockAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAverageCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.simpleLabelItem2 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.layoutControlItemTotalOpenStockQuantity = new DevExpress.XtraLayout.LayoutControlItem();
            this.textEditTotalOpenStockQuantity = new DevExpress.XtraEditors.TextEdit();
            this.layoutControl4 = new DevExpress.XtraLayout.LayoutControl();
            this.textEditTotalOpenStockAmount = new DevExpress.XtraEditors.TextEdit();
            this.textEditTotalReceiptQuantity = new DevExpress.XtraEditors.TextEdit();
            this.textEditTotalReceiptAmount = new DevExpress.XtraEditors.TextEdit();
            this.textEditTotalIssueQuantity = new DevExpress.XtraEditors.TextEdit();
            this.textEditTotalIssueAmount = new DevExpress.XtraEditors.TextEdit();
            this.textEditCloseStockQuantity = new DevExpress.XtraEditors.TextEdit();
            this.textEditCloseStockAmount = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemTotalReceiptQuantity = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemTotalIssueQuantity = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemTotalIssueAmount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemCloseStockQuantity = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.inventoryTableAdapter = new WarehouseManager.WARHOUSE_HPDataSet2TableAdapters.InventoryTableAdapter();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.textEditMessage = new DevExpress.XtraEditors.MemoEdit();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.wRRDetailBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wARHOUSE_HPDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditBranch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditWarehouse.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditProcessingPeriod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemBranch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWarehouse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemProcessingPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlInventory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inventoryBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wARHOUSE_HPDataSet2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewInventory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalOpenStockQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalOpenStockQuantity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl4)).BeginInit();
            this.layoutControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalOpenStockAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalReceiptQuantity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalReceiptAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalIssueQuantity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalIssueAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCloseStockQuantity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCloseStockAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalReceiptQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalIssueQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalIssueAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCloseStockQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditMessage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // wRRDetailBindingSource
            // 
            this.wRRDetailBindingSource.DataMember = "WRRDetail";
            this.wRRDetailBindingSource.DataSource = this.wARHOUSE_HPDataSet1;
            // 
            // wARHOUSE_HPDataSet1
            // 
            this.wARHOUSE_HPDataSet1.DataSetName = "WARHOUSE_HPDataSet1";
            this.wARHOUSE_HPDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // wRRDetailTableAdapter
            // 
            this.wRRDetailTableAdapter.ClearBeforeFill = true;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.Root.Name = "Root";
            this.Root.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.Root.Size = new System.Drawing.Size(1240, 37);
            this.Root.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.windowsUIButtonPanel1;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1240, 37);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // windowsUIButtonPanel1
            // 
            this.windowsUIButtonPanel1.AllowGlyphSkinning = false;
            this.windowsUIButtonPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            windowsUIButtonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions1.Image")));
            windowsUIButtonImageOptions1.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions2.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions2.Image")));
            windowsUIButtonImageOptions2.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions3.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions3.Image")));
            windowsUIButtonImageOptions3.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions4.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions4.Image")));
            windowsUIButtonImageOptions4.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            this.windowsUIButtonPanel1.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Mở khóa chứng từ", true, windowsUIButtonImageOptions1, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "unlock", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Xử lý tồn kho", true, windowsUIButtonImageOptions2, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "process", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F5) Làm mới", true, windowsUIButtonImageOptions3, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "refesh", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F10) Thoát", true, windowsUIButtonImageOptions4, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "close", -1, false)});
            this.windowsUIButtonPanel1.ContentAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.windowsUIButtonPanel1.Location = new System.Drawing.Point(2, 2);
            this.windowsUIButtonPanel1.Name = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.Padding = new System.Windows.Forms.Padding(5);
            this.windowsUIButtonPanel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.windowsUIButtonPanel1.Size = new System.Drawing.Size(1236, 32);
            this.windowsUIButtonPanel1.TabIndex = 4;
            this.windowsUIButtonPanel1.Text = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.UseButtonBackgroundImages = false;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.windowsUIButtonPanel1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(639, 159, 812, 500);
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(1240, 37);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.comboBoxEditBranch);
            this.layoutControl2.Controls.Add(this.comboBoxEditWarehouse);
            this.layoutControl2.Controls.Add(this.comboBoxEditProcessingPeriod);
            this.layoutControl2.Controls.Add(this.textEditMessage);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl2.Location = new System.Drawing.Point(0, 37);
            this.layoutControl2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(162, 100, 812, 551);
            this.layoutControl2.Root = this.layoutControlGroup1;
            this.layoutControl2.Size = new System.Drawing.Size(1240, 80);
            this.layoutControl2.TabIndex = 1;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // comboBoxEditBranch
            // 
            this.comboBoxEditBranch.Location = new System.Drawing.Point(100, 12);
            this.comboBoxEditBranch.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBoxEditBranch.Name = "comboBoxEditBranch";
            this.comboBoxEditBranch.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditBranch.Size = new System.Drawing.Size(274, 20);
            this.comboBoxEditBranch.StyleController = this.layoutControl2;
            this.comboBoxEditBranch.TabIndex = 4;
            // 
            // comboBoxEditWarehouse
            // 
            this.comboBoxEditWarehouse.Location = new System.Drawing.Point(100, 42);
            this.comboBoxEditWarehouse.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBoxEditWarehouse.Name = "comboBoxEditWarehouse";
            this.comboBoxEditWarehouse.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditWarehouse.Size = new System.Drawing.Size(274, 20);
            this.comboBoxEditWarehouse.StyleController = this.layoutControl2;
            this.comboBoxEditWarehouse.TabIndex = 5;
            // 
            // comboBoxEditProcessingPeriod
            // 
            this.comboBoxEditProcessingPeriod.Location = new System.Drawing.Point(466, 12);
            this.comboBoxEditProcessingPeriod.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBoxEditProcessingPeriod.Name = "comboBoxEditProcessingPeriod";
            this.comboBoxEditProcessingPeriod.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditProcessingPeriod.Size = new System.Drawing.Size(152, 20);
            this.comboBoxEditProcessingPeriod.StyleController = this.layoutControl2;
            this.comboBoxEditProcessingPeriod.TabIndex = 6;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemBranch,
            this.layoutControlItemWarehouse,
            this.layoutControlItemProcessingPeriod,
            this.simpleLabelItem1,
            this.layoutControlItem2});
            this.layoutControlGroup1.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.layoutControlGroup1.Name = "Root";
            columnDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition1.Width = 30D;
            columnDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition2.Width = 20D;
            columnDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition3.Width = 10D;
            columnDefinition4.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition4.Width = 10D;
            columnDefinition5.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition5.Width = 30D;
            this.layoutControlGroup1.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition1,
            columnDefinition2,
            columnDefinition3,
            columnDefinition4,
            columnDefinition5});
            rowDefinition1.Height = 50D;
            rowDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition2.Height = 50D;
            rowDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            this.layoutControlGroup1.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition1,
            rowDefinition2});
            this.layoutControlGroup1.Size = new System.Drawing.Size(1240, 80);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItemBranch
            // 
            this.layoutControlItemBranch.Control = this.comboBoxEditBranch;
            this.layoutControlItemBranch.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemBranch.Name = "layoutControlItemBranch";
            this.layoutControlItemBranch.Size = new System.Drawing.Size(366, 30);
            this.layoutControlItemBranch.Text = "Chi nhánh";
            this.layoutControlItemBranch.TextSize = new System.Drawing.Size(85, 13);
            // 
            // layoutControlItemWarehouse
            // 
            this.layoutControlItemWarehouse.Control = this.comboBoxEditWarehouse;
            this.layoutControlItemWarehouse.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItemWarehouse.Name = "layoutControlItemWarehouse";
            this.layoutControlItemWarehouse.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemWarehouse.Size = new System.Drawing.Size(366, 30);
            this.layoutControlItemWarehouse.Text = "Kho hàng";
            this.layoutControlItemWarehouse.TextSize = new System.Drawing.Size(85, 13);
            // 
            // layoutControlItemProcessingPeriod
            // 
            this.layoutControlItemProcessingPeriod.Control = this.comboBoxEditProcessingPeriod;
            this.layoutControlItemProcessingPeriod.Location = new System.Drawing.Point(366, 0);
            this.layoutControlItemProcessingPeriod.Name = "layoutControlItemProcessingPeriod";
            this.layoutControlItemProcessingPeriod.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItemProcessingPeriod.Size = new System.Drawing.Size(244, 30);
            this.layoutControlItemProcessingPeriod.Text = "Kỳ khóa sổ";
            this.layoutControlItemProcessingPeriod.TextSize = new System.Drawing.Size(85, 13);
            // 
            // simpleLabelItem1
            // 
            this.simpleLabelItem1.AllowHotTrack = false;
            this.simpleLabelItem1.Location = new System.Drawing.Point(610, 0);
            this.simpleLabelItem1.Name = "simpleLabelItem1";
            this.simpleLabelItem1.OptionsTableLayoutItem.ColumnIndex = 2;
            this.simpleLabelItem1.Size = new System.Drawing.Size(122, 30);
            this.simpleLabelItem1.Text = "Loại xử lý tồn kho";
            this.simpleLabelItem1.TextSize = new System.Drawing.Size(85, 13);
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.gridControlInventory);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl3.Location = new System.Drawing.Point(0, 117);
            this.layoutControl3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.Root = this.layoutControlGroup2;
            this.layoutControl3.Size = new System.Drawing.Size(1240, 507);
            this.layoutControl3.TabIndex = 2;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // gridControlInventory
            // 
            this.gridControlInventory.DataSource = this.inventoryBindingSource;
            this.gridControlInventory.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gridControlInventory.Location = new System.Drawing.Point(12, 12);
            this.gridControlInventory.MainView = this.gridViewInventory;
            this.gridControlInventory.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gridControlInventory.Name = "gridControlInventory";
            this.gridControlInventory.Size = new System.Drawing.Size(1216, 483);
            this.gridControlInventory.TabIndex = 4;
            this.gridControlInventory.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewInventory});
            // 
            // inventoryBindingSource
            // 
            this.inventoryBindingSource.DataMember = "Inventory";
            this.inventoryBindingSource.DataSource = this.wARHOUSE_HPDataSet2;
            // 
            // wARHOUSE_HPDataSet2
            // 
            this.wARHOUSE_HPDataSet2.DataSetName = "WARHOUSE_HPDataSet2";
            this.wARHOUSE_HPDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridViewInventory
            // 
            this.gridViewInventory.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colYear,
            this.colMonth,
            this.colWarehouseID,
            this.colGoodsID,
            this.colOpeningStockQuantity,
            this.colOpeningStockAmount,
            this.colReceiptQuantity,
            this.colReceiptAmount,
            this.colIssueQuantity,
            this.colIssueAmount,
            this.colClosingStockQuantity,
            this.colClosingStockAmount,
            this.colAverageCost,
            this.colStatus});
            this.gridViewInventory.DetailHeight = 284;
            this.gridViewInventory.GridControl = this.gridControlInventory;
            this.gridViewInventory.Name = "gridViewInventory";
            // 
            // colYear
            // 
            this.colYear.FieldName = "Year";
            this.colYear.MinWidth = 21;
            this.colYear.Name = "colYear";
            // 
            // colMonth
            // 
            this.colMonth.FieldName = "Month";
            this.colMonth.MinWidth = 21;
            this.colMonth.Name = "colMonth";
            // 
            // colWarehouseID
            // 
            this.colWarehouseID.FieldName = "WarehouseID";
            this.colWarehouseID.MinWidth = 21;
            this.colWarehouseID.Name = "colWarehouseID";
            this.colWarehouseID.Visible = true;
            this.colWarehouseID.VisibleIndex = 0;
            // 
            // colGoodsID
            // 
            this.colGoodsID.Caption = "Mã hàng hóa";
            this.colGoodsID.FieldName = "GoodsID";
            this.colGoodsID.MinWidth = 21;
            this.colGoodsID.Name = "colGoodsID";
            this.colGoodsID.Visible = true;
            this.colGoodsID.VisibleIndex = 1;
            // 
            // colOpeningStockQuantity
            // 
            this.colOpeningStockQuantity.Caption = "Lượng tồn đầu";
            this.colOpeningStockQuantity.FieldName = "OpeningStockQuantity";
            this.colOpeningStockQuantity.MinWidth = 21;
            this.colOpeningStockQuantity.Name = "colOpeningStockQuantity";
            this.colOpeningStockQuantity.Visible = true;
            this.colOpeningStockQuantity.VisibleIndex = 2;
            // 
            // colOpeningStockAmount
            // 
            this.colOpeningStockAmount.Caption = "Trị tồn đầu";
            this.colOpeningStockAmount.FieldName = "OpeningStockAmount";
            this.colOpeningStockAmount.MinWidth = 21;
            this.colOpeningStockAmount.Name = "colOpeningStockAmount";
            this.colOpeningStockAmount.Visible = true;
            this.colOpeningStockAmount.VisibleIndex = 3;
            // 
            // colReceiptQuantity
            // 
            this.colReceiptQuantity.Caption = "Lượng nhập";
            this.colReceiptQuantity.FieldName = "ReceiptQuantity";
            this.colReceiptQuantity.MinWidth = 21;
            this.colReceiptQuantity.Name = "colReceiptQuantity";
            this.colReceiptQuantity.Visible = true;
            this.colReceiptQuantity.VisibleIndex = 4;
            // 
            // colReceiptAmount
            // 
            this.colReceiptAmount.Caption = "Trị nhập";
            this.colReceiptAmount.FieldName = "ReceiptAmount";
            this.colReceiptAmount.MinWidth = 21;
            this.colReceiptAmount.Name = "colReceiptAmount";
            this.colReceiptAmount.Visible = true;
            this.colReceiptAmount.VisibleIndex = 5;
            // 
            // colIssueQuantity
            // 
            this.colIssueQuantity.Caption = "Lượng xuất";
            this.colIssueQuantity.FieldName = "IssueQuantity";
            this.colIssueQuantity.MinWidth = 21;
            this.colIssueQuantity.Name = "colIssueQuantity";
            this.colIssueQuantity.Visible = true;
            this.colIssueQuantity.VisibleIndex = 6;
            // 
            // colIssueAmount
            // 
            this.colIssueAmount.Caption = "Trị xuất";
            this.colIssueAmount.FieldName = "IssueAmount";
            this.colIssueAmount.MinWidth = 21;
            this.colIssueAmount.Name = "colIssueAmount";
            this.colIssueAmount.Visible = true;
            this.colIssueAmount.VisibleIndex = 7;
            // 
            // colClosingStockQuantity
            // 
            this.colClosingStockQuantity.Caption = "Lượng tồn cuối";
            this.colClosingStockQuantity.FieldName = "ClosingStockQuantity";
            this.colClosingStockQuantity.MinWidth = 21;
            this.colClosingStockQuantity.Name = "colClosingStockQuantity";
            this.colClosingStockQuantity.Visible = true;
            this.colClosingStockQuantity.VisibleIndex = 8;
            // 
            // colClosingStockAmount
            // 
            this.colClosingStockAmount.Caption = "Trị tồn cuối";
            this.colClosingStockAmount.FieldName = "ClosingStockAmount";
            this.colClosingStockAmount.MinWidth = 21;
            this.colClosingStockAmount.Name = "colClosingStockAmount";
            this.colClosingStockAmount.Visible = true;
            this.colClosingStockAmount.VisibleIndex = 9;
            // 
            // colAverageCost
            // 
            this.colAverageCost.Caption = "Chi phí trung bình";
            this.colAverageCost.FieldName = "AverageCost";
            this.colAverageCost.MinWidth = 21;
            this.colAverageCost.Name = "colAverageCost";
            // 
            // colStatus
            // 
            this.colStatus.Caption = "Trạng thái";
            this.colStatus.FieldName = "Status";
            this.colStatus.MinWidth = 21;
            this.colStatus.Name = "colStatus";
            this.colStatus.Visible = true;
            this.colStatus.VisibleIndex = 10;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem6});
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1240, 507);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.gridControlInventory;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(1220, 487);
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.simpleLabelItem2,
            this.layoutControlItemTotalOpenStockQuantity,
            this.layoutControlItem8,
            this.layoutControlItemTotalReceiptQuantity,
            this.layoutControlItem10,
            this.layoutControlItemTotalIssueQuantity,
            this.layoutControlItemTotalIssueAmount,
            this.layoutControlItemCloseStockQuantity,
            this.layoutControlItem14});
            this.layoutControlGroup3.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.layoutControlGroup3.Name = "Root";
            columnDefinition6.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition6.Width = 15.789473684210526D;
            columnDefinition7.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition7.Width = 15.789473684210526D;
            columnDefinition8.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition8.Width = 5.2631578947368416D;
            columnDefinition9.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition9.Width = 15.789473684210526D;
            columnDefinition10.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition10.Width = 5.2631578947368416D;
            columnDefinition11.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition11.Width = 15.789473684210526D;
            columnDefinition12.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition12.Width = 5.2631578947368416D;
            columnDefinition13.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition13.Width = 15.789473684210526D;
            columnDefinition14.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition14.Width = 5.2631578947368416D;
            this.layoutControlGroup3.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition6,
            columnDefinition7,
            columnDefinition8,
            columnDefinition9,
            columnDefinition10,
            columnDefinition11,
            columnDefinition12,
            columnDefinition13,
            columnDefinition14});
            rowDefinition3.Height = 100D;
            rowDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            this.layoutControlGroup3.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition3});
            this.layoutControlGroup3.Size = new System.Drawing.Size(1240, 52);
            this.layoutControlGroup3.TextVisible = false;
            // 
            // simpleLabelItem2
            // 
            this.simpleLabelItem2.AllowHotTrack = false;
            this.simpleLabelItem2.Location = new System.Drawing.Point(0, 0);
            this.simpleLabelItem2.Name = "simpleLabelItem2";
            this.simpleLabelItem2.Size = new System.Drawing.Size(192, 32);
            this.simpleLabelItem2.Text = "Σ Lượng/Giá trị";
            this.simpleLabelItem2.TextSize = new System.Drawing.Size(71, 13);
            // 
            // layoutControlItemTotalOpenStockQuantity
            // 
            this.layoutControlItemTotalOpenStockQuantity.Control = this.textEditTotalOpenStockQuantity;
            this.layoutControlItemTotalOpenStockQuantity.Location = new System.Drawing.Point(192, 0);
            this.layoutControlItemTotalOpenStockQuantity.Name = "layoutControlItemTotalOpenStockQuantity";
            this.layoutControlItemTotalOpenStockQuantity.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItemTotalOpenStockQuantity.Size = new System.Drawing.Size(193, 32);
            this.layoutControlItemTotalOpenStockQuantity.Text = "Tồn đầu";
            this.layoutControlItemTotalOpenStockQuantity.TextSize = new System.Drawing.Size(71, 13);
            // 
            // textEditTotalOpenStockQuantity
            // 
            this.textEditTotalOpenStockQuantity.Location = new System.Drawing.Point(278, 12);
            this.textEditTotalOpenStockQuantity.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditTotalOpenStockQuantity.Name = "textEditTotalOpenStockQuantity";
            this.textEditTotalOpenStockQuantity.Size = new System.Drawing.Size(115, 20);
            this.textEditTotalOpenStockQuantity.StyleController = this.layoutControl4;
            this.textEditTotalOpenStockQuantity.TabIndex = 4;
            // 
            // layoutControl4
            // 
            this.layoutControl4.Controls.Add(this.textEditTotalOpenStockQuantity);
            this.layoutControl4.Controls.Add(this.textEditTotalOpenStockAmount);
            this.layoutControl4.Controls.Add(this.textEditTotalReceiptQuantity);
            this.layoutControl4.Controls.Add(this.textEditTotalReceiptAmount);
            this.layoutControl4.Controls.Add(this.textEditTotalIssueQuantity);
            this.layoutControl4.Controls.Add(this.textEditTotalIssueAmount);
            this.layoutControl4.Controls.Add(this.textEditCloseStockQuantity);
            this.layoutControl4.Controls.Add(this.textEditCloseStockAmount);
            this.layoutControl4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.layoutControl4.Location = new System.Drawing.Point(0, 609);
            this.layoutControl4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl4.Name = "layoutControl4";
            this.layoutControl4.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(816, 211, 812, 500);
            this.layoutControl4.Root = this.layoutControlGroup3;
            this.layoutControl4.Size = new System.Drawing.Size(1240, 52);
            this.layoutControl4.TabIndex = 3;
            this.layoutControl4.Text = "layoutControl4";
            // 
            // textEditTotalOpenStockAmount
            // 
            this.textEditTotalOpenStockAmount.Location = new System.Drawing.Point(397, 12);
            this.textEditTotalOpenStockAmount.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditTotalOpenStockAmount.Name = "textEditTotalOpenStockAmount";
            this.textEditTotalOpenStockAmount.Size = new System.Drawing.Size(60, 20);
            this.textEditTotalOpenStockAmount.StyleController = this.layoutControl4;
            this.textEditTotalOpenStockAmount.TabIndex = 5;
            // 
            // textEditTotalReceiptQuantity
            // 
            this.textEditTotalReceiptQuantity.Location = new System.Drawing.Point(535, 12);
            this.textEditTotalReceiptQuantity.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditTotalReceiptQuantity.Name = "textEditTotalReceiptQuantity";
            this.textEditTotalReceiptQuantity.Size = new System.Drawing.Size(115, 20);
            this.textEditTotalReceiptQuantity.StyleController = this.layoutControl4;
            this.textEditTotalReceiptQuantity.TabIndex = 6;
            // 
            // textEditTotalReceiptAmount
            // 
            this.textEditTotalReceiptAmount.Location = new System.Drawing.Point(654, 12);
            this.textEditTotalReceiptAmount.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditTotalReceiptAmount.Name = "textEditTotalReceiptAmount";
            this.textEditTotalReceiptAmount.Size = new System.Drawing.Size(60, 20);
            this.textEditTotalReceiptAmount.StyleController = this.layoutControl4;
            this.textEditTotalReceiptAmount.TabIndex = 7;
            // 
            // textEditTotalIssueQuantity
            // 
            this.textEditTotalIssueQuantity.Location = new System.Drawing.Point(792, 12);
            this.textEditTotalIssueQuantity.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditTotalIssueQuantity.Name = "textEditTotalIssueQuantity";
            this.textEditTotalIssueQuantity.Size = new System.Drawing.Size(115, 20);
            this.textEditTotalIssueQuantity.StyleController = this.layoutControl4;
            this.textEditTotalIssueQuantity.TabIndex = 8;
            // 
            // textEditTotalIssueAmount
            // 
            this.textEditTotalIssueAmount.Location = new System.Drawing.Point(911, 12);
            this.textEditTotalIssueAmount.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditTotalIssueAmount.Name = "textEditTotalIssueAmount";
            this.textEditTotalIssueAmount.Size = new System.Drawing.Size(60, 20);
            this.textEditTotalIssueAmount.StyleController = this.layoutControl4;
            this.textEditTotalIssueAmount.TabIndex = 9;
            // 
            // textEditCloseStockQuantity
            // 
            this.textEditCloseStockQuantity.Location = new System.Drawing.Point(1049, 12);
            this.textEditCloseStockQuantity.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditCloseStockQuantity.Name = "textEditCloseStockQuantity";
            this.textEditCloseStockQuantity.Size = new System.Drawing.Size(115, 20);
            this.textEditCloseStockQuantity.StyleController = this.layoutControl4;
            this.textEditCloseStockQuantity.TabIndex = 10;
            // 
            // textEditCloseStockAmount
            // 
            this.textEditCloseStockAmount.Location = new System.Drawing.Point(1168, 12);
            this.textEditCloseStockAmount.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditCloseStockAmount.Name = "textEditCloseStockAmount";
            this.textEditCloseStockAmount.Size = new System.Drawing.Size(60, 20);
            this.textEditCloseStockAmount.StyleController = this.layoutControl4;
            this.textEditCloseStockAmount.TabIndex = 11;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.textEditTotalOpenStockAmount;
            this.layoutControlItem8.Location = new System.Drawing.Point(385, 0);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItem8.Size = new System.Drawing.Size(64, 32);
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItemTotalReceiptQuantity
            // 
            this.layoutControlItemTotalReceiptQuantity.Control = this.textEditTotalReceiptQuantity;
            this.layoutControlItemTotalReceiptQuantity.Location = new System.Drawing.Point(449, 0);
            this.layoutControlItemTotalReceiptQuantity.Name = "layoutControlItemTotalReceiptQuantity";
            this.layoutControlItemTotalReceiptQuantity.OptionsTableLayoutItem.ColumnIndex = 3;
            this.layoutControlItemTotalReceiptQuantity.Size = new System.Drawing.Size(193, 32);
            this.layoutControlItemTotalReceiptQuantity.Text = "Nhập";
            this.layoutControlItemTotalReceiptQuantity.TextSize = new System.Drawing.Size(71, 13);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.textEditTotalReceiptAmount;
            this.layoutControlItem10.Location = new System.Drawing.Point(642, 0);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItem10.Size = new System.Drawing.Size(64, 32);
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlItemTotalIssueQuantity
            // 
            this.layoutControlItemTotalIssueQuantity.Control = this.textEditTotalIssueQuantity;
            this.layoutControlItemTotalIssueQuantity.Location = new System.Drawing.Point(706, 0);
            this.layoutControlItemTotalIssueQuantity.Name = "layoutControlItem11";
            this.layoutControlItemTotalIssueQuantity.OptionsTableLayoutItem.ColumnIndex = 5;
            this.layoutControlItemTotalIssueQuantity.Size = new System.Drawing.Size(193, 32);
            this.layoutControlItemTotalIssueQuantity.Text = "Xuất";
            this.layoutControlItemTotalIssueQuantity.TextSize = new System.Drawing.Size(71, 13);
            // 
            // layoutControlItemTotalIssueAmount
            // 
            this.layoutControlItemTotalIssueAmount.Control = this.textEditTotalIssueAmount;
            this.layoutControlItemTotalIssueAmount.Location = new System.Drawing.Point(899, 0);
            this.layoutControlItemTotalIssueAmount.Name = "layoutControlItemTotalIssueAmount";
            this.layoutControlItemTotalIssueAmount.OptionsTableLayoutItem.ColumnIndex = 6;
            this.layoutControlItemTotalIssueAmount.Size = new System.Drawing.Size(64, 32);
            this.layoutControlItemTotalIssueAmount.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemTotalIssueAmount.TextVisible = false;
            // 
            // layoutControlItemCloseStockQuantity
            // 
            this.layoutControlItemCloseStockQuantity.Control = this.textEditCloseStockQuantity;
            this.layoutControlItemCloseStockQuantity.Location = new System.Drawing.Point(963, 0);
            this.layoutControlItemCloseStockQuantity.Name = "layoutControlItemCloseStockQuantity";
            this.layoutControlItemCloseStockQuantity.OptionsTableLayoutItem.ColumnIndex = 7;
            this.layoutControlItemCloseStockQuantity.Size = new System.Drawing.Size(193, 32);
            this.layoutControlItemCloseStockQuantity.Text = "Tồn cuối";
            this.layoutControlItemCloseStockQuantity.TextSize = new System.Drawing.Size(71, 13);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.textEditCloseStockAmount;
            this.layoutControlItem14.Location = new System.Drawing.Point(1156, 0);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.OptionsTableLayoutItem.ColumnIndex = 8;
            this.layoutControlItem14.Size = new System.Drawing.Size(64, 32);
            this.layoutControlItem14.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem14.TextVisible = false;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.textEditTotalIssueQuantity;
            this.layoutControlItem11.Location = new System.Drawing.Point(825, 0);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.OptionsTableLayoutItem.ColumnIndex = 5;
            this.layoutControlItem11.Size = new System.Drawing.Size(225, 45);
            this.layoutControlItem11.TextSize = new System.Drawing.Size(115, 16);
            // 
            // inventoryTableAdapter
            // 
            this.inventoryTableAdapter.ClearBeforeFill = true;
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // textEditMessage
            // 
            this.textEditMessage.Location = new System.Drawing.Point(866, 12);
            this.textEditMessage.Name = "textEditMessage";
            this.textEditMessage.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.textEditMessage.Size = new System.Drawing.Size(362, 56);
            this.textEditMessage.StyleController = this.layoutControl2;
            this.textEditMessage.TabIndex = 7;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.textEditMessage;
            this.layoutControlItem2.Location = new System.Drawing.Point(854, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItem2.OptionsTableLayoutItem.RowSpan = 2;
            this.layoutControlItem2.Size = new System.Drawing.Size(366, 60);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // InventoryProcessControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl4);
            this.Controls.Add(this.layoutControl3);
            this.Controls.Add(this.layoutControl2);
            this.Controls.Add(this.layoutControl1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "InventoryProcessControl";
            this.Size = new System.Drawing.Size(1240, 661);
            ((System.ComponentModel.ISupportInitialize)(this.wRRDetailBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wARHOUSE_HPDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditBranch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditWarehouse.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditProcessingPeriod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemBranch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWarehouse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemProcessingPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlInventory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inventoryBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wARHOUSE_HPDataSet2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewInventory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalOpenStockQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalOpenStockQuantity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl4)).EndInit();
            this.layoutControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalOpenStockAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalReceiptQuantity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalReceiptAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalIssueQuantity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalIssueAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCloseStockQuantity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCloseStockAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalReceiptQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalIssueQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalIssueAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCloseStockQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditMessage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.BindingSource wRRDetailBindingSource;
        private WARHOUSE_HPDataSet1 wARHOUSE_HPDataSet1;
        private WARHOUSE_HPDataSet1TableAdapters.WRRDetailTableAdapter wRRDetailTableAdapter;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel windowsUIButtonPanel1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditBranch;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditWarehouse;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditProcessingPeriod;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemBranch;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemWarehouse;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemProcessingPeriod;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem1;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraGrid.GridControl gridControlInventory;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewInventory;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTotalOpenStockQuantity;
        private DevExpress.XtraEditors.TextEdit textEditTotalOpenStockQuantity;
        private DevExpress.XtraLayout.LayoutControl layoutControl4;
        private DevExpress.XtraEditors.TextEdit textEditTotalOpenStockAmount;
        private DevExpress.XtraEditors.TextEdit textEditTotalReceiptQuantity;
        private DevExpress.XtraEditors.TextEdit textEditTotalReceiptAmount;
        private DevExpress.XtraEditors.TextEdit textEditTotalIssueQuantity;
        private DevExpress.XtraEditors.TextEdit textEditTotalIssueAmount;
        private DevExpress.XtraEditors.TextEdit textEditCloseStockQuantity;
        private DevExpress.XtraEditors.TextEdit textEditCloseStockAmount;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTotalReceiptQuantity;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTotalIssueAmount;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemCloseStockQuantity;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTotalIssueQuantity;
        private System.Windows.Forms.BindingSource inventoryBindingSource;
        private WARHOUSE_HPDataSet2 wARHOUSE_HPDataSet2;
        private DevExpress.XtraGrid.Columns.GridColumn colYear;
        private DevExpress.XtraGrid.Columns.GridColumn colMonth;
        private DevExpress.XtraGrid.Columns.GridColumn colWarehouseID;
        private DevExpress.XtraGrid.Columns.GridColumn colGoodsID;
        private DevExpress.XtraGrid.Columns.GridColumn colOpeningStockQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colOpeningStockAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colReceiptQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colReceiptAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colIssueQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colIssueAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colClosingStockQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colClosingStockAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colAverageCost;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private WARHOUSE_HPDataSet2TableAdapters.InventoryTableAdapter inventoryTableAdapter;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.PrintDialog printDialog1;
        private DevExpress.XtraEditors.MemoEdit textEditMessage;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
    }
}
